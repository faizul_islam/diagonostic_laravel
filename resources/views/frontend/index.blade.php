 @extends('frontend/layouts.main')
 @section('content')
     <!-- Sweet Alert css -->
     <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />



     <div id="home" class="parallax first-section wow fadeIn" data-stellar-background-ratio="0.4" style="">

         @include('frontend.slider');
         {{-- <div class="container">
             <div class="row">
                 <div class="col-md-12 col-sm-12">
                     <div class="text-contant">
                         <h2>
                             <span class="center"><span class="icon"><img
                                         src="{{ asset('assets/frontend/images/icon-logo.png') }}"
                                         alt="#" /></span></span>
                             <a href="" class="typewrite" data-period="2000"
                                 data-type='[ "Welcome to Life Care", "We Care Your Health", "We are Expert!" ]'>
                                 <span class="wrap"></span>
                             </a>
                         </h2>
                     </div>
                 </div>
             </div>
             <!-- end row -->
         </div> --}}
         <!-- end container -->
     </div>
     <!-- end section -->
     {{-- <div id="time-table" class="time-table-section">
         <div class="container">
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                 <div class="row">
                     <div class="service-time one" style="background:#2895f1;">
                         <span class="info-icon"><i class="fa fa-ambulance" aria-hidden="true"></i></span>
                         <h3>Emergency Case</h3>
                         <p>Dignissimos ducimus qui blanditii sentium volta tum deleniti atque cori as quos dolores et quas
                             mole.</p>
                     </div>
                 </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                 <div class="row">
                     <div class="service-time middle" style="background:#0071d1;">
                         <span class="info-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                         <h3>Working Hours</h3>
                         <div class="time-table-section">
                             <ul>
                                 <li><span class="left">Monday - Friday</span><span class="right">8.00 –
                                         18.00</span></li>
                                 <li><span class="left">Saturday</span><span class="right">8.00 –
                                         16.00</span></li>
                                 <li><span class="left">Sunday</span><span class="right">8.00 –
                                         13.00</span></li>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                 <div class="row">
                     <div class="service-time three" style="background:#0060b1;">
                         <span class="info-icon"><i class="fa fa-hospital-o" aria-hidden="true"></i></span>
                         <h3>Clinic Timetable</h3>
                         <p>Dignissimos ducimus qui blanditii sentium volta tum deleniti atque cori as quos dolores et quas
                             mole.</p>
                     </div>
                 </div>
             </div>
         </div>
     </div> --}}
     <div id="about" class="section wow fadeIn">
         <div class="container">
             <div class="heading">
                 <span class="icon-logo"><img src="{{ asset('assets/frontend/images/icon-logo.png') }}"
                         alt="#"></span>
                 <h2>The Specialist Clinic</h2>
             </div>
             <!-- end title -->
             <div class="row">
                 <div class="col-md-6">
                     <div class="message-box">
                         <h4>What We Do</h4>
                         <h2>Clinic Service</h2>
                         <p class="lead">{{ $front_settings['about_us'] }} </p>
                         <a href="#services" data-scroll class="btn btn-light btn-radius btn-brd grd1 effect-1">Learn
                             More</a>
                     </div>
                     <!-- end messagebox -->
                 </div>
                 <!-- end col -->
                 <div class="col-md-6">
                     <div class="post-media wow fadeIn">
                         <?php
                         if ($front_settings['video_image'] == '') {
                             $video_image = asset('/assets/images/no_img.png');
                         } else {
                             $video_image = asset('/public/assets/images/uploads/web_settings/' . $front_settings['video_image']);
                         }
                         ?>
                         <img src="{{ $video_image }}" alt="" class="img-responsive">
                         <a href="{{ $front_settings['about_videos'] }}" data-rel="prettyPhoto[gal]"
                             class="playbutton"><i class="flaticon-play-button"></i></a>
                     </div>
                     <!-- end media -->
                 </div>
                 <!-- end col -->
             </div>
             <!-- end row -->
             {{-- <hr class="hr1"> --}}
             {{-- <div class="row">
                 <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="service-widget">
                         <div class="post-media wow fadeIn">
                             <a href="{{ asset('assets/frontend/images/clinic_01.jpg') }}" data-rel="prettyPhoto[gal]"
                                 class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                             <img src="{{ asset('assets/frontend/images/clinic_01.jpg') }}" alt="" class="img-responsive">
                         </div>
                         <h3>Digital Control Center</h3>
                     </div>
                     <!-- end service -->
                 </div>
                 <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="service-widget">
                         <div class="post-media wow fadeIn">
                             <a href="{{ asset('assets/frontend/images/clinic_02.jpg') }}" data-rel="prettyPhoto[gal]"
                                 class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                             <img src="{{ asset('assets/frontend/images/clinic_02.jpg') }}" alt="" class="img-responsive">
                         </div>
                         <h3>Hygienic Operating Room</h3>
                     </div>
                     <!-- end service -->
                 </div>
                 <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="service-widget">
                         <div class="post-media wow fadeIn">
                             <a href="{{ asset('assets/frontend/images/clinic_03.jpg') }}" data-rel="prettyPhoto[gal]"
                                 class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                             <img src="{{ asset('assets/frontend/images/clinic_03.jpg') }}" alt=""
                                 class="img-responsive">
                         </div>
                         <h3>Specialist Physicians</h3>
                     </div>
                     <!-- end service -->
                 </div>
                 <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="service-widget">
                         <div class="post-media wow fadeIn">
                             <a href="{{ asset('assets/frontend/images/clinic_01.jpg') }}" data-rel="prettyPhoto[gal]"
                                 class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                             <img src="{{ asset('assets/frontend/images/clinic_01.jpg') }}" alt=""
                                 class="img-responsive">
                         </div>
                         <h3>Digital Control Center</h3>
                     </div>
                     <!-- end service -->
                 </div>
             </div> --}}
             <!-- end row -->
         </div>
         <!-- end container -->
     </div>
     <div id="service" class="services wow fadeIn">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                     <div class="inner-services">
                         <@foreach ($front_service as $item)
                             <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                 <div class="serv">
                                     <span class="icon-service">
                                         {{-- <img src="{{ asset('assets/frontend/images/service-icon2.png') }}" alt="#" /> --}}
                                         <i class="{{ $item['icon'] }}"></i></span>
                                     <h4>PREMIUM FACILITIES</h4>
                                     <p>Lorem Ipsum is simply dummy text of the printing.</p>
                                 </div>
                             </div>
                             @endforeach>

                     </div>
                 </div>

             </div>
         </div>
     </div>
     <!-- end section -->

     <!-- doctor -->

     <div id="doctors" class="parallax section db" data-stellar-background-ratio="0.4" style="background:#fff;"
         data-scroll-id="doctors" tabindex="-1">
         <div class="container">

             <div class="heading">
                 <span class="icon-logo"><img src="{{ asset('assets/frontend/images/icon-logo.png') }}"
                         alt="#"></span>
                 <h2>The Specialist Clinic</h2>
             </div>

             <div class="row dev-list text-center">
                 @foreach ($doctor_list as $item)
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wow fadeIn" data-wow-duration="1s"
                         data-wow-delay="0.2s"
                         style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
                         <div class="widget clearfix">
                             <?php
                             
                             $dept = App\Models\Staff_department::where('id', '=', $item['department'])->first();
                             if ($item['photo'] == '') {
                                 $staff_photo = asset('/assets/images/no_img.png');
                             } else {
                                 $staff_photo = asset('/public/assets/images/uploads/employee/' . $item['photo']);
                             }
                             ?>

                             <img style="width:300px; height:300px;" src="{{ $staff_photo }}" alt=""
                                 class="img-responsive img-rounded">
                             <div class="widget-title">
                                 <h3>{{ $item['name'] }}</h3>
                                 <small>{{ $dept['name'] }}</small>
                             </div>
                             <!-- end title -->
                             <p>{{ $item['qualification'] }}
                             </p>

                             <div class="footer-social">
                                 <a href="{{ $item['facebook_url'] }}" class="btn grd1"><i
                                         class="fa fa-facebook"></i></a>

                                 <a href="{{ $item['twitter_url'] }}" class="btn grd1"><i
                                         class="fa fa-twitter"></i></a>
                                 <a href="{{ $item['linkedin_url'] }}" class="btn grd1"><i
                                         class="fa fa-linkedin"></i></a>
                             </div>
                         </div>
                         <!--widget -->
                     </div><!-- end col -->
                 @endforeach

             </div><!-- end row -->
         </div><!-- end container -->
     </div>

     <div id="price" class="section pr wow fadeIn"
         style="background-image:url('{{ asset('assets/frontend/images/price-bg.png') }}');">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="tab-content">
                         <div class="tab-pane active fade in" id="tab1">
                             <div class="row text-center">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="appointment-form">
                                         <h3><span>+</span> Book Appointment</h3>
                                         <div class="form">
                                             <p style="text-align: left">Create account and book appointment</p>
                                             <form action="{{ URL::to('storePatientForAppointment/') }}" method="post"
                                                 class="form-horizontal" enctype="multipart/form-data">
                                                 @csrf
                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                 <fieldset>


                                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 select-section">
                                                         <div class="row">
                                                             <div class="form-group">
                                                                 <input type="text" id="name" name="name"
                                                                     placeholder="Your Name" required />
                                                             </div>
                                                             <div class="form-group">
                                                                 <select class="form-control" name="gender" required>
                                                                     <option value="">Gender</option>
                                                                     <option value="1">Male</option>
                                                                     <option value="2">Female</option>
                                                                     <option value="3">Other</option>
                                                                 </select>
                                                             </div>

                                                         </div>
                                                     </div>


                                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 select-section">
                                                         <div class="row">
                                                             <div class="form-group">
                                                                 <input type="text" class="form-control" name="age"
                                                                     id="age" placeholder="Age" required>
                                                             </div>
                                                             <div class="form-group">
                                                                 <input class="form-control" name="mobile_no"
                                                                     id="mobile_no" type="text" placeholder="Mobile"
                                                                     required>

                                                             </div>
                                                             <div class="form-group">
                                                                 <input class="form-control" name="email" id="email"
                                                                     type="text" placeholder="Email" required>

                                                             </div>
                                                         </div>
                                                     </div>

                                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 select-section">
                                                         <div class="row">
                                                             <div class="form-group">
                                                                 <input type="text" id="username" name="username"
                                                                     placeholder="Username" required>
                                                             </div>
                                                             <div class="form-group">
                                                                 <input type="text" id="password" name="password"
                                                                     placeholder="Password" required>
                                                             </div>

                                                         </div>
                                                     </div>


                                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                         <div class="row">
                                                             <div class="form-group">
                                                                 <div class="center"><button
                                                                         type="submit">Submit</button></div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </fieldset>
                                             </form>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <!-- end row -->
                         </div>
                         <!-- end pane -->
                         <div class="tab-pane fade" id="tab2">
                             <div class="row text-center">
                                 <div class="col-md-6">
                                     <div class="pricing-table">
                                         <div class="pricing-table-header">
                                             <h2>Dedicated Server</h2>
                                             <h3>$85/month</h3>
                                         </div>
                                         <div class="pricing-table-space"></div>
                                         <div class="pricing-table-features">
                                             <p><i class="fa fa-envelope-o"></i> <strong>250</strong> Email Addresses</p>
                                             <p><i class="fa fa-rocket"></i> <strong>125GB</strong> of Storage</p>
                                             <p><i class="fa fa-database"></i> <strong>140</strong> Databases</p>
                                             <p><i class="fa fa-link"></i> <strong>60</strong> Domains</p>
                                             <p><i class="fa fa-life-ring"></i> <strong>24/7 Unlimited</strong> Support</p>
                                         </div>
                                         <div class="pricing-table-sign-up">
                                             <a href="#contact" data-scroll="" class="btn btn-dark btn-radius btn-brd">Order
                                                 Now</a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-6">
                                     <div class="pricing-table pricing-table-highlighted">
                                         <div class="pricing-table-header grd1">
                                             <h2>VPS Server</h2>
                                             <h3>$59/month</h3>
                                         </div>
                                         <div class="pricing-table-space"></div>
                                         <div class="pricing-table-text">
                                             <p>This is a perfect choice for small businesses and startups.</p>
                                         </div>
                                         <div class="pricing-table-features">
                                             <p><i class="fa fa-envelope-o"></i> <strong>150</strong> Email Addresses</p>
                                             <p><i class="fa fa-rocket"></i> <strong>65GB</strong> of Storage</p>
                                             <p><i class="fa fa-database"></i> <strong>60</strong> Databases</p>
                                             <p><i class="fa fa-link"></i> <strong>30</strong> Domains</p>
                                             <p><i class="fa fa-life-ring"></i> <strong>24/7 Unlimited</strong> Support</p>
                                         </div>
                                         <div class="pricing-table-sign-up">
                                             <a href="#contact" data-scroll=""
                                                 class="btn btn-light btn-radius btn-brd grd1 effect-1">Order Now</a>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <!-- end row -->
                         </div>
                         <!-- end pane -->
                     </div>
                     <!-- end content -->
                 </div>
                 <!-- end col -->
             </div>
         </div>
     </div>

     <!-- end doctor section -->

     <div id="testimonials" class="section wb wow fadeIn">
         <div class="container">
             <div class="heading">
                 <span class="icon-logo"><img src="{{ asset('assets/frontend/images/icon-logo.png') }}"
                         alt="#"></span>
                 <h2>Testimonials</h2>
             </div>
             <!-- end title -->
             <div class="row">
                 @foreach ($testimonial_list as $item)
                     <div class="col-md-6 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                         <div class="testimonial clearfix">
                             <?php
                             if ($item['image'] == '') {
                                 $image = asset('/assets/images/no_img.png');
                             } else {
                                 $image = asset('/public/assets/images/uploads/testimonial/' . $item['image']);
                             }
                             ?>
                             <div class="desc">
                                 <h3><i class="fa fa-quote-left"></i>{{ $item['title'] }}</h3>
                                 <p class="lead">{{ $item['description'] }}</p>
                             </div>
                             <div class="testi-meta">
                                 <img src="{{ $image }}" alt="" class="img-responsive alignleft">
                                 <h4>{{ $item['patient_name'] }} <small>-
                                         {{ $item['surname'] }}</small></h4>
                             </div>
                             <!-- end testi-meta -->
                         </div>
                         <!-- end testimonial -->
                     </div>
                 @endforeach


                 <!-- end col -->
             </div>
             <!-- end row -->
             <hr class="invis">
             {{-- <div class="row">
                 <div class="col-md-6 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                     <div class="testimonial clearfix">
                         <div class="desc">
                             <h3><i class="fa fa-quote-left"></i> The amazing clinic! Wonderful Support!</h3>
                             <p class="lead">They have got my project on time with the competition with a sed
                                 highly skilled, and experienced & professional team.</p>
                         </div>
                         <div class="testi-meta">
                             <img src="{{ asset('assets/frontend/images/testi_03.png') }}" alt=""
                                 class="img-responsive alignleft">
                             <h4>Amanda DOE <small>- Manager of Racer</small></h4>
                         </div>
                         <!-- end testi-meta -->
                     </div>
                     <!-- end testimonial -->
                 </div>
                 <!-- end col -->
                 <div class="col-md-6 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                     <div class="testimonial clearfix">
                         <div class="desc">
                             <h3><i class="fa fa-quote-left"></i> Thanks for Help us!</h3>
                             <p class="lead">They have got my project on time with the competition with a sed
                                 highly skilled, and experienced & professional team.</p>
                         </div>
                         <div class="testi-meta">
                             <img src="{{ asset('assets/frontend/images/testi_01.png') }}" alt=""
                                 class="img-responsive alignleft">
                             <h4>Martin Johnson <small>- Founder of Goosilo</small></h4>
                         </div>
                         <!-- end testi-meta -->
                     </div>
                     <!-- end testimonial -->
                 </div>
                 <!-- end col -->
             </div> --}}
             <!-- end row -->
         </div>
         <!-- end container -->
     </div>
     <!-- end section -->
     <div id="getintouch" class="section wb wow fadeIn" style="padding-bottom:0;">
         <div class="container">
             <div class="heading">
                 <span class="icon-logo"><img src="{{ asset('assets/frontend/images/icon-logo.png') }}"
                         alt="#"></span>
                 <h2>Get in Touch</h2>
             </div>
         </div>
         <div class="contact-section">
             <div class="form-contant">
                 <form id="ajax-contact" action="assets/mailer.php" method="post">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="form-group in_name">
                                 <input type="text" class="form-control" placeholder="Name" required="required">
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="form-group in_email">
                                 <input type="email" class="form-control" placeholder="E-mail" required="required">
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="form-group in_email">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone"
                                     required="required">
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="form-group in_email">
                                 <input type="text" class="form-control" id="subject" placeholder="Subject"
                                     required="required">
                             </div>
                         </div>
                         <div class="col-md-12">
                             <div class="form-group in_message">
                                 <textarea class="form-control" id="message" rows="5" placeholder="Message"
                                     required="required"></textarea>
                             </div>
                             <div class="actions">
                                 <input type="submit" value="Send Message" name="submit" id="submitButton"
                                     class="btn small" title="Submit Your Message!">
                             </div>
                         </div>
                     </div>
                 </form>
             </div>
             <div id="googleMap" style="width:100%;height:450px;"></div>
         </div>
     </div>
     <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

     <script>
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });

         $(document).on("click", "#username", function() {

             var mobile_no = $('#mobile_no').val();
             var email = $('#email').val();
             if (email == "") {
                 swal('Warning', "Please write first email", 'warning');
                 return false;
             }
             $.ajax({
                 url: "loginDupCheck",
                 // alert();
                 type: "post",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     email: email
                 },


                 success: function(data) {

                     if (data == 1) {

                         if (email != '') {
                             $("#username").val(email);
                             $("#username").attr("readonly", true);

                         } else {
                             // swal("Required!", "You have to set mobile no first", "warning");
                             swal("Required!", "You have to set email first", "warning");
                         }
                     } else {

                         $("#username").val('');
                         swal("Already Registered this email. please try with another");
                     }

                 }
             });

         });
     </script>


 @endsection
