<!DOCTYPE html>
<html lang="en">
<?php
$global_config = App\Models\Front_cms_settings::all();
$data['fav_icon'] = $global_config->toArray();
$global_config = $data['fav_icon'][0];
if ($global_config['fav_icon'] == '') {
    $img = asset('/assets/images/no_img.png');
} else {
    $img = asset('/public/assets/images/uploads/web_settings/' . $global_config['fav_icon']);
}
?>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Microxen Technology</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="{{ $img }}" type="image/x-icon" />
<link rel="apple-touch-icon" href="{{ $img }}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">

<!-- Site CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/style.css') }}">
<!-- Colors CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/css/colors.css') }}">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/css/versions.css') }}">
<!-- Responsive CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/css/responsive.css') }}">
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontend/css/custom.css') }}">
<!-- Modernizer for Portfolio -->
<script src="{{ asset('assets/frontend/js/modernizer.js') }}"></script>
<!-- [if lt IE 9] -->
</head>
