  <?php
  $global_config = App\Models\Front_cms_settings::all();
  $menus = App\Models\Front_cms_menu::where('publish', '=', 1)
      ->orderBy('ordering', 'ASC')
      ->get();
  $data['fav_icon'] = $global_config->toArray();
  $global_config = $data['fav_icon'][0];
  if ($global_config['fav_icon'] == '') {
      $img = asset('/assets/images/no_img.png');
  } else {
      $img = asset('/public/assets/images/uploads/web_settings/' . $global_config['logo']);
  }
  ?>
  <header>
      <div class="header-top wow fadeIn">
          <div class="container">
              <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ $img }}" alt="image"></a>
              <div class="right-header">
                  <div class="header-info">
                      <div class="info-inner">
                          <span class="icontop"><img src="{{ asset('assets/frontend/images/phone-icon.png') }}"
                                  alt="#"></span>
                          <span class="iconcont"><a
                                  href="tel:800 123 456">{{ $global_config['mobile_no'] }}</a></span>
                      </div>

                      <div class="info-inner">
                          <span class="icontop"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                          <span class="iconcont"><a data-scroll href="#">{!! $global_config['working_hours'] !!}</a></span>
                      </div>

                      <div class="info-inner">
                          <a href="{{ url('/login') }}"> <span class="icontop"><i class="fa fa-key"
                                      aria-hidden="true"></i></span>
                              <span class="iconcont">Login</span></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="header-bottom wow fadeIn">
          <div class="container">
              <nav class="main-menu">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                          aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars"
                              aria-hidden="true"></i></button>
                  </div>

                  <div id="navbar" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav">
                          <li><a class="active" href="{{ url('/') }}">Home</a></li>
                          @foreach ($menus as $menu)
                              <li><a data-scroll href="#{{ $menu->alias }}">{{ $menu->title }}</a></li>
                          @endforeach

                          {{-- <li><a data-scroll href="#about">About us</a></li>
                          <li><a data-scroll href="#service">Services</a></li>
                          <li><a data-scroll href="#doctors">Doctors</a></li>
                          <li><a data-scroll href="#price">Departments</a></li>
                          <li><a data-scroll href="#testimonials">Testimonials</a></li>
                          <li><a data-scroll href="#getintouch">Contact</a></li> --}}

                      </ul>
                  </div>
              </nav>
              {{-- <div class="serch-bar">
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input type="text" class="form-control input-lg" placeholder="Search" />
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="button">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div> --}}
          </div>
      </div>
  </header>
