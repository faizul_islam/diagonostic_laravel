@include('frontend.layouts.header')
<!-- End Leftbar -->

<body class="clinic_version">
    <!-- LOADER -->
    <div id="preloader">
        <img class="preloader" src="{{ asset('assets/frontend/images/loaders/heart-loading2.gif') }}" alt="">
    </div>
    <!-- END LOADER -->
    @include('frontend.layouts.nav')

    @yield('content')

</body>
@include('frontend.layouts.footer')

{{-- <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" />
        </svg></div> --}}
