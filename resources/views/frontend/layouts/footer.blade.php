    <?php
    $global_config = App\Models\Front_cms_settings::all();
    $data['fav_icon'] = $global_config->toArray();
    $global_config = $data['fav_icon'][0];
    if ($global_config['fav_icon'] == '') {
        $img = asset('/assets/images/no_img.png');
    } else {
        $img = asset('/public/assets/images/uploads/web_settings/' . $global_config['logo']);
    }
    ?>
    <footer id="footer" class="footer-area wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo padding">
                        <a href=""><img src="{{ $img }}" alt=""></a>
                        <p>Locavore pork belly scen ester pine est chill wave microdosing pop uple itarian cliche
                            artisan.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-info padding">
                        <h3>CONTACT US</h3>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> {!! $global_config['address'] !!}</p>
                        <p><i class="fa fa-paper-plane" aria-hidden="true"></i> {{ $global_config['email'] }}
                        </p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> {{ $global_config['mobile_no'] }}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="subcriber-info">
                        <h3>SUBSCRIBE</h3>
                        <p>Get healthy news, tip and solutions to your problems from our experts.</p>
                        <div class="subcriber-box">
                            <form id="mc-form" class="mc-form">
                                <div class="newsletter-form">
                                    <input type="email" autocomplete="off" id="mc-email" placeholder="Email address"
                                        class="form-control" name="EMAIL">
                                    <button class="mc-submit" type="submit"><i
                                            class="fa fa-paper-plane"></i></button>
                                    <div class="clearfix"></div>
                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div>
                                        <!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div>
                                        <!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div>
                                        <!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="copyright-area wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="footer-text">
                        <p>{!! $global_config['footer_text'] !!}
                            {{-- <a id="tw" href="#" target="_blank">ThemeWagon</a> --}}
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="social">
                        <ul class="social-links">
                            <li><a target="_blank" href="{{ $global_config['facebook_url'] }}"><i
                                        class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="{{ $global_config['twitter_url'] }}"><i
                                        class="fa fa-twitter"></i></a></li>
                            <li><a target="_blank" href="{{ $global_config['google_plus'] }}"><i
                                        class="fa fa-google-plus"></i></a>
                            </li>
                            <li><a target="_blank" href="{{ $global_config['youtube_url'] }}"><i
                                        class="fa fa-youtube"></i></a></li>
                            <li><a target="_blank" href="{{ $global_config['linkedin_url'] }}"><i
                                        class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyrights -->
    <a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
    <!-- all js files -->
    <script src="{{ asset('assets/frontend/js/all.js') }}"></script>
    <!-- all plugins -->
    <script src="{{ asset('assets/frontend/js/custom.js') }}"></script>
    <!-- map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNUPWkb4Cjd7Wxo-T4uoUldFjoiUA1fJc&callback=myMap">
    </script>
    </body>

    </html>
