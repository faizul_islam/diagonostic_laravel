<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />


<style>
    .page-title {
        width: 200px !important;
    }

</style>

<?php if(!empty($data['all_data'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i>Staff List</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">

                <form action="{{ URL::to('/storeSetRefer') }}" method="post" class="form-horizontal"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="font-size: 8px;">
                                    <th>Sl</th>
                                    <th><input type="checkbox"></th>
                                    <th>Staff Id</th>
                                    <th>Staff Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Percentage (%)</th>


                                </tr>
                            </thead>
                            <tbody>
                                <input type="hidden" name="test_id" value="<?php echo $data['test_id']; ?>">
                                <?php
							$i = 1;
							if (count($data['all_data'])) {
								foreach ($data['all_data'] as $key => $row):
                            //    echo "<pre>"; print_r($row); die();
								?>
                                <tr>
                                    <input type="hidden" name="assign[<?php echo $key; ?>][staff_id]"
                                        value="<?php echo $row['id']; ?>">
                                    <td><?php echo $i++; ?></td>
                                    <td class="center cb-chk-area">
                                        <div class="checkbox-replace">
                                            <label class="i-checks"><input type="checkbox"
                                                    name="assign[<?php echo $key; ?>][cb_select_staff]"
                                                    <?php echo $row['commission_id'] != 0 ? 'checked' : ''; ?> value="<?php echo $row['commission_id']; ?>" /><i></i></label>
                                        </div>
                                    </td>
                                    <td><?php echo $row['staff_id']; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><?php echo $row['designation_name']; ?></td>
                                    <td><?php echo $row['department_name']; ?></td>
                                    <td>
                                        <div class="form-group">
                                            <input type="number" name="assign[<?php echo $key; ?>][percentage]"
                                                class="form-control fn_percentage" value="<?php echo $row['percentage']; ?>"
                                                max="100" required />
                                        </div>
                                    </td>
                                </tr>
                                <?php
								endforeach;
							}else{
								echo '<tr><td colspan="7"><h5 class="text-danger text-center">' . 'No information available' . '</td></tr>';
							}
							?>
                            </tbody>

                        </table>
                    </div>

                    <div class="col-md-2 mb-sm" style="float: right;">

                        <div class="form-group">
                            <label class="control-label" style="margin-top: 23px;"></label>
                            <button type="submit" class="btn btn btn-default btn-block">{{ __('Save') }}</button>

                        </div>


                    </div>
                </form>


            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
