@section('title')
Appointment
@endsection
@extends('backend.layouts.main')

@section('style')

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


<!-- Summernote css -->
<link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
<!-- Code Mirror css -->
<link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
<!-- Start Contentbar -->
<div class="contentbar">
    <!-- Start row -->
    <div class="row">

        <!-- Start col -->
        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card m-b-30">

                <div class="card-body">
                    <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab" aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                Appointment List</a>
                        </li>


                    </ul>
                    <div class="tab-content" id="defaultTabContentLine">
                        <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                            <div class="table-responsive">
                                <table id="default-datatable" class="display table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="5%">Sl</th>
                                            {{-- <th width="10%">Appointment Id</th> --}}
                                            <th width="10%">Doctor Name</th>
                                            <th width="10%">Patient Name</th>
                                            <th width="8%">Date</th>
                                            <th width="10%">Consultation Schedule</th>
                                            <th width="10%">Serial</th>
                                            <th width="10%">Remarks</th>
                                            <th width="10%">Status</th>
                                            <th width="15%">Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; @endphp
                                        @foreach ($data['val'] as $item)
                                        @php $i++; @endphp
                                        <tr>


                                            <?php
                                            $staff = App\Models\Staffs::Where('id', '=', $item['doctor_id'])->get();
                                            $patient = App\Models\Patient::Where('id', '=', $item['patient_id'])->get();

                                            ?>
                                            <td>{{ $i }}</td>
                                            {{-- <td>{{ $item['appointment_id '] }}</td> --}}
                                            <td>{{ $staff[0]['name'] }}</td>
                                            <td>{{ $patient[0]['name'] }}</td>
                                            <td>{{ date('d-m-Y', strtotime($item['appointment_date'])) }}</td>
                                            <td>{{ $item['time_start'] . '-' . $item['time_end'] }}</td>
                                            <td>{{ $item['schedule'] }}</td>

                                            <td>{{ $item['consultation_fees'] }}</td>
                                            <td>
                                                <?php
                                                if ($item['status'] == 1) {
                                                    $status = 'Confirm';
                                                } elseif ($item['status'] == 2) {
                                                    $status = 'Closed';
                                                }
                                                ?>
                                                {{ $status }}
                                            </td>


                                            <td>
                                                <form action="{{ URL::to('editAppointment/') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="id" value="{{ $item['id'] }}">
                                                    @if ($item['status'] == 1)

                                                    <button type="button" class="btn btn-round btn-danger-rgba StatusChance" id="{{ $item['id'] }}" title="Status"><i class="fa fa-check"></i></button>
                                                    <button type="submit" class="btn btn-round btn-primary-rgba"><i class="fa fa-edit"></i></button>

                                                    @endif

                                                    <button type="button" class="btn btn-round btn-danger-rgba deleteData" id="{{ $item['id'] }}" title="Delete"><i class="fa fa-trash"></i></button>

                                                </form>


                                            </td>


                                        </tr>
                                        @endforeach


                                    </tbody>

                                </table>
                            </div>



                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End Contentbar -->
    @endsection
    @section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $(document).on("click", ".deleteData", function() {
                var id = $(this).attr('id');
                var base_url = $('#base_url').val();
                // alert(id);
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "deleteAppointment",
                            // alert();
                            type: "post",
                            data: {
                                id: id
                            },
                            success: function(data) {
                                swal("Information has been deleted", {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });
            });


            $(document).on("click", ".StatusChance", function() {
                var id = $(this).attr('id');
                var base_url = $('#base_url').val();
                // alert(id);
                swal({
                    title: "Are you sure?",
                    text: "Once status change, you will not be able to recover file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "StatusChance",
                            // alert();
                            type: "post",
                            data: {
                                id: id
                            },
                            success: function(data) {
                                swal("Status has been changed", {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });
            });


        });
    </script>
    <!-- Wysiwig js -->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <!-- Summernote JS -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- Code Mirror JS -->
    <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    <!-- Input Mask js -->
    <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>


    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
    @endsection