@section('title')
    Withdrawal
@endsection
@extends('backend.layouts.main')

@section('style')

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Withdrawal List</a>
                            </li>
                            <?php if( App\Http\Helpers::get_permission('commission_withdrawal', 'is_add')){ ?>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Add
                                    Withdrawal</a>
                            </li>
                            <?php } ?>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                {{-- <th width="10%">Appointment Id</th> --}}
                                                <th width="10%">Role</th>
                                                <th width="10%">Staff Id</th>
                                                <th width="10%">Staff Name</th>
                                                <th width="8%">Payout</th>
                                                <th width="10%">Paid By</th>
                                                <th width="10%">Date</th>
                                                <?php if( App\Http\Helpers::get_permission('commission_withdrawal', 'is_add')){ ?>

                                                <th width="15%">Action</th>
                                                <?php }?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['payout_commission'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <?php
                                                    $staff = App\Models\Staffs::Where('id', '=', $item['staff_id'])->get();
                                                    $users = App\Models\Users::Where('user_id', '=', $item['staff_id'])->get();
                                                    $roles = App\Models\Role::Where('id', '=', $users[0]['role'])->get();
                                                    
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $roles[0]['name'] }}</td>
                                                    <td>{{ $staff[0]['staff_id'] }}</td>
                                                    <td>{{ $staff[0]['name'] }}</td>

                                                    <td>{{ $item['amount'] }}</td>
                                                    <td>{{ $item['paid_by'] }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item['created_at'])) }}</td>

                                                    <?php if( App\Http\Helpers::get_permission('commission_withdrawal', 'is_add')){ ?>


                                                    <td>

                                                        <button type="button"
                                                            class="btn btn-round btn-danger-rgba deleteData"
                                                            id="{{ $item['id'] }}" title="Delete"><i
                                                                class="fa fa-trash"></i></button>




                                                    </td>
                                                    <?php }?>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('storeWithdrawal/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Role') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="role">
                                                @foreach ($data['role'] as $role)

                                                    <option value="{{ $role['id'] }}">
                                                        {{ $role['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('User') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="user_id" id="user_id">
                                                @foreach ($data['staff'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Pay Via') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="Payment_type">
                                                @foreach ($data['Payment_type'] as $Payment_type)

                                                    <option value="{{ $Payment_type['id'] }}">
                                                        {{ $Payment_type['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Amount') }}</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="amount">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="alert alert-custom mt-md p-sm m-none hidden-item" id="balance_div"
                                                style="display: none; border:1px solid rgb(75, 159, 75);padding: 0.40rem; margin-bottom: 0rem; color:#31708f">
                                                <i class="fa fa-info-circle"></i> <span id="current_balance">Current
                                                    Balance : ৳ <span id="amount"></span></span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Remarks') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="remarks" rows="2" class="form-control"
                                                aria-required="true"></textarea>
                                        </div>
                                    </div>

                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteWithdrawal",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });


                $(document).on("click", ".StatusChance", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once status change, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "StatusChance",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Status has been changed", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });


            });


            $(document).on("change", "#user_id", function() {

                var staff = $('#user_id').val();

                if (staff !== "") {
                    // $('#discount').val('0.00');
                    // $("#available_schedule").html("<option value=''><?php echo 'exploring'; ?>...</option>");
                    $.ajax({
                        url: "get_staff_balance",
                        type: "POST",
                        data: {
                            'staff': staff

                        },
                        //dataType: 'html',
                        success: function(data) {
                            // var response = jQuery.parseJSON(data);
                            // $('#available_schedule').html(response.schedule);

                            $('#balance_div').css('display', 'block');
                            $('#amount').html(data);

                        }
                    });
                }
            });
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Input Mask js -->
        <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>


        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
    @endsection
