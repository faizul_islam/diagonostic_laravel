@section('title')
    Refer List
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />


    <style>
        .page-title {
            width: 200px !important;
        }

    </style>
@endsection
@section('rightbar-content')
    <?php if(!empty($data['all_data'])){?>
    <div class="card m-b-30" id="showReport">


        <div class="card-body">

            <div class="tab-content" id="defaultTabContentLine">
                <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="font-size: 8px;">
                                    <th>Sl</th>
                                    <th>Role</th>
                                    <th>Staff Id</th>
                                    <th> Name</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Commission For</th>
                                    <th>Percentage (%)</th>
                                    <th>Date</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
						$i = 1;
						if (count($data['all_data'])){
							foreach ($data['all_data'] as $row){
							?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $row['role']; ?></td>
                                    <td><?php echo $row['staffid']; ?></td>
                                    <td><?php echo $row['staff_name']; ?></td>
                                    <td><?php echo $row['department_name']; ?></td>
                                    <td><?php echo $row['designation_name']; ?></td>
                                    <td><?php echo $row['test_name']; ?></td>
                                    <td><?php echo $row['percentage']; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($row['date'])); ?></td>
                                </tr>
                                <?php } }?>
                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <?php }else{?>
    <div class="alert alert-danger" role="alert">
        Data not found..
    </div>
    <?php } ?>


    </div>
    <!-- End col -->
    </div>
    <!-- End Contentbar -->

@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    <!-- Input Mask js -->
    <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
@endsection
