@section('title')
    DASHBOARD
@endsection
@extends('backend.layouts.main')
@section('style')

    <!-- Apex js -->
    <script src="{{ asset('assets/plugins/chartjs/chart.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/chartjs/utils.js') }}"></script>

    <!-- Apex css -->
    <link href="{{ asset('assets/plugins/apexcharts/apexcharts.css') }}" rel="stylesheet" type="text/css" />
    <!-- Slick css -->
    <link href="{{ asset('assets/plugins/slick/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">

        <!-- Start row -->
        <div class="row">
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/bitcoin.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Invoice</h5>

                                Today Total: <span style="text-align: right">{{ $labTestBill }}</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/1.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/ethereum.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Commission</h5>

                                Today Total: <span style="text-align: right">{{ $commision }}</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/2.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/ripple.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Income</h5>

                                Today Total: <span style="text-align: right">{{ $get_today_income }}</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/3.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/bcc.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Expense</h5>

                                Today Total: <span style="text-align: right">{{ $get_today_expense }}</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/4.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->

        <!-- Start row -->
        <div class="row">
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <span
                                class="align-self-center mr-3 action-icon badge badge-secondary-inverse btn-warning-rgba"><i
                                    class="fa fa-wheelchair"></i></span>
                            <div class="media-body">
                                <p class="mb-0">Patient</p>
                                <h5 class="mb-0">{{ $total_patient }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <span
                                class="align-self-center mr-3 action-icon badge badge-secondary-inverse btn-success-rgba"><i
                                    class="fa fa-user-md"></i></span>
                            <div class="media-body">
                                <p class="mb-0">Doctor</p>
                                <h5 class="mb-0">{{ $total_doctor }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <span class="align-self-center mr-3 action-icon badge badge-secondary-inverse btn-info-rgba"><i
                                    class="fa fa-users"></i></span>
                            <div class="media-body">
                                <p class="mb-0">Employee</p>
                                <h5 class="mb-0">{{ $total_employee }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <span
                                class="align-self-center mr-3 action-icon badge badge-secondary-inverse btn-warning-rgba"><i
                                    class="fa fa-table"></i></span>
                            <div class="media-body">
                                <p class="mb-0">Appointment</p>
                                <h5 class="mb-0">{{ $total_appointment }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->

        <div class="row">
            <?php  if (App\Http\Helpers::get_permission('patient_fees_summary_chart', 'is_view')) { ?>

            <!-- Start col -->
            <div class="col-lg-12 col-xl-12">
                <div class="card m-b-30">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-9">
                                <h5 class="card-title mb-0"><?php echo 'Patient Fees Summary' . ' - ' . date('F'); ?></h5>
                            </div>
                            <div class="col-3">
                                <div class="dropdown">
                                    <button class="btn btn-link p-0 font-18 float-right" type="button"
                                        id="widgetPatientTypes" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><i class="feather icon-more-horizontal-"></i></button>
                                    {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="widgetPatientTypes">
                                    <a class="dropdown-item font-13" href="#">Refresh</a>
                                    <a class="dropdown-item font-13" href="#">Export</a>
                                </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-0 pl-0 pr-2">
                        <div class="pe-chart">
                            <canvas id="monthlyBillsummary" style="height: 324px;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>





            <section class="panel">
                <div class="panel-body">
                    <h2 class="chart-title mb-md"></h2>
                    <div class="pe-chart">
                        <canvas id="incomeExpense" style="height: 328px;display:none;"></canvas>
                    </div>
                </div>
            </section>
        </div>


        <!-- End row -->

        <!-- End row -->
    </div>
    <!-- End Contentbar -->
@endsection
@section('script')
    <!-- Apex js -->
    {{-- <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('assets/plugins/apexcharts/irregular-data-series.js') }}"></script> --}}
    <!-- Slick js -->
    <script src="{{ asset('assets/plugins/slick/slick.min.js') }}"></script>
    <!-- Custom Dashboard js -->
    {{-- <script src="{{ asset('assets/js/custom/custom-dashboard.js') }}"></script> --}}
    <script src="{{ asset('assets/js/custom/custom-dashboard-hospital.js') }}"></script>
@endsection

<script type="application/javascript">
    // Annual Income Vs Expense JS
    var income = <?php echo json_encode($yearly_income_expense['income']); ?>;
    var expense = <?php echo json_encode($yearly_income_expense['expense']); ?>;

    var incomeVsExpense = {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                    label: "Income",
                    backgroundColor: 'rgba(0, 136, 204, .7)',
                    borderColor: '#F5F5F5',
                    borderWidth: 1,
                    data: income
                },
                {
                    label: "<?php echo 'expense'; ?>",
                    backgroundColor: 'rgba(204, 61, 61, 0.7)',
                    borderColor: '#F5F5F5',
                    borderWidth: 1,
                    data: expense
                }
            ]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                bodySpacing: 4
            },
            title: {
                display: false,
                text: 'Income Expense'
            },
            legend: {
                position: 'bottom',
                labels: {
                    boxWidth: 12
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: false,
                    }
                }],
                yAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: false,
                    },
                }]
            }
        }
    };

    // Patient Fees Summary
    var days = <?php echo json_encode($patient_fees_summary['days']); ?>;
    var net_payable = <?php echo json_encode($patient_fees_summary['total_bill']); ?>;
    var total_paid = <?php echo json_encode($patient_fees_summary['total_paid']); ?>;
    var total_due = <?php echo json_encode($patient_fees_summary['total_due']); ?>;
    var barChartData = {
        type: 'bar',
        data: {
            labels: days,
            datasets: [{
                label: "Net Payable",
                data: net_payable,
                backgroundColor: 'rgba(216, 27, 96, .8)',
                borderColor: '#F5F5F5',
                borderWidth: 1
            }, {
                label: "Total Paid",
                data: total_paid,
                backgroundColor: 'rgba(0, 136, 204, .8)',
                borderColor: '#F5F5F5',
                borderWidth: 1
            }, {
                label: "Total Due",
                data: total_due,
                backgroundColor: 'rgba(204, 102, 102, .8)',
                borderColor: '#F5F5F5',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            stacked: false,
            tooltips: {
                mode: 'index',
                bodySpacing: 4
            },
            title: {
                display: false,
                text: 'Income Expense'
            },
            legend: {
                position: 'bottom',
                labels: {
                    boxWidth: 12
                }
            }
        }
    };

    window.onload = function() {

        <?php if (App\Http\Helpers::get_permission('pathology_fees_summary_chart', 'is_view')): ?>
        var ctx = document.getElementById('incomeExpense').getContext('2d');
        window.myLine = new Chart(ctx, incomeVsExpense);
        <?php endif ?>
        <?php if (App\Http\Helpers::get_permission('annual_income_vs_expense_chart', 'is_view')): ?>
        var ctx2 = document.getElementById('monthlyBillsummary').getContext('2d');
        window.myBar = new Chart(ctx2, barChartData);
        <?php endif ?>
        <?php if (App\Http\Helpers::get_permission('appointment_status_chart', 'is_view')): ?>
        var ctx3 = document.getElementById('pieChart').getContext('2d');
        window.myDoughnut = new Chart(ctx3, appointmentDoughnut);
        <?php endif ?>
    };
</script>
