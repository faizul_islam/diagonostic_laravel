<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />


<style>
    .page-title {
        width: 200px !important;
    }

</style>

<?php if(!empty($data['all_data'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i> Due Collect Report</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr style="font-size: 8px;">
                                <th>Sl</th>
                                <th>Bill No</th>
                                <th>Patient Name</th>
                                <th>Pay Via</th>
                                <th>Payment Status</th>
                                <th>Due Collect</th>
                                <th>Billing Date</th>
                                <th>Collect Date</th>
                                <th>Collected By</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
				$i = 1;
				$total_netpayable = 0;
				$total_due = 0;
				$total_collect = 0;
				$total_net_due = 0;
				if (!empty($data['all_data'])){
					foreach ($data['all_data'] as $row):
						$total_collect += $row['amount'];
				?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $row['bill_no']; ?></td>
                                <td><?php echo $row['patient_name']; ?></td>
                                <td><?php echo $row['pay_via']; ?></td>
                                <td>
                                    <?php
                                    $labelMode = '';
                                    $status = $row['status'];
                                    if ($status == 2) {
                                        $status = 'partly_paid';
                                        $labelMode = 'label-info-custom';
                                    } elseif ($status == 3 || $row['total_due'] == 0) {
                                        $status = 'total_paid';
                                        $labelMode = 'label-success-custom';
                                    }
                                    echo "<span class='label " . $labelMode . "'>" . $status . '</span>';
                                    ?>
                                </td>
                                <td><?php echo number_format($row['amount'], 2, '.', ''); ?></td>
                                <td><?php echo date('d-m-Y', strtotime($row['bill_date'])); ?></td>
                                <td><?php echo date('d-m-Y', strtotime($row['paid_on'])); ?></td>
                                <td><?php echo $row['collect_by']; ?></td>
                                <td><a target="_blank" href="{{ URL::to('/test_bill_invoice/' . $row['id']) }}">
                                        <button type="button" class="btn btn-round btn-success"><i
                                                class="fa fa-eye mr-2"></i>
                                        </button>

                                    </a></td>
                            </tr>
                            <?php endforeach; }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><?php echo number_format($total_collect, 2, '.', ''); ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
