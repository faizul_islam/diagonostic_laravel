@section('title')
    Investigation Report
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('rightbar-content')

    <?php
    use App\Http\Helpers;
    $global_config = App\Models\Global_settings::all();
    $data['global_config'] = $global_config->toArray();
    $global_config = $data['global_config'][0];
    if ($global_config['text_logo'] == '') {
        $img = asset('/assets/images/no_img.png');
    } else {
        $img = asset('/public/assets/images/uploads/settings/' . $global_config['text_logo']);
    }
    
    if (!empty($data['test'])) {
        $test = $data['test'][0];
    } else {
        $test = [];
    }
    ?>

    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">

                <div class="card m-b-30">
                    <div class="card-header bg-transparent border-secondary">
                        <h5 class="card-title">Report Details</h5>
                    </div>
                    <div class="card-body">

                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="panel-body" id="div_print">
                                    <div id="invoicePrint" class="print-content">
                                        <div class="invoice">
                                            <header class="clearfix"
                                                style="border: 1px solid rgb(240, 233, 233); padding: 5px;">
                                                <div class="row">
                                                    <div class="col-md-6" style="">
                                                        <div class="ib">
                                                            <img src="{{ $img }}" class="img-fluid"
                                                                alt="logo">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">
                                                        <address class="m-none">
                                                            <?php
                                                            echo $global_config['institute_name'] . '<br/>';
                                                            echo $global_config['address'] . '<br/>';
                                                            echo $global_config['mobileno'] . '<br/>';
                                                            echo $global_config['institute_email'] . '<br/>';
                                                            ?>
                                                        </address>
                                                    </div>
                                                </div>
                                            </header><br><br>
                                            <div class="bill-info">
                                                <div class="row">
                                                    <div class="col-md-6 mt-sm">
                                                        <div class="bill-data">
                                                            <p class=" mb-xs text-dark text-weight-semibold">
                                                                <?php echo 'Bill No'; ?> :
                                                                <?php echo $test['bill_no']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mt-sm">
                                                        <div class="bill-data text-right">
                                                            <p class=" mb-xs text-dark text-weight-semibold">
                                                                <?php echo 'Reporting Date'; ?> :
                                                                <?php echo date('d-m-Y', strtotime($test['reporting_date'])); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive mb-md ">
                                                <table class="table table-bordered table-condensed text-dark">
                                                    <tbody>
                                                        <tr>
                                                            <th width="25%">Patient Name</th>
                                                            <td width="25%"><?php echo $test['patient_name']; ?></td>
                                                            <th width="25%">Sex & Age</th>
                                                            <td width="25%"><?php echo $test['sex'] . ' / ' . $test['age']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Category</th>
                                                            <td><?php echo $test['cname']; ?></td>
                                                            <th><?php echo 'referral'; ?></th>
                                                            <td><?php echo $test['ref_name']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php echo $test['report_description']; ?>
                                        </div>
                                    </div>
                                </div>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <button class="btn btn-warning" onClick="printdiv('div_print');"><i
                                                        class="fa fa-print"></i> Print</button>
                                            </div>

                                        </div>
                                    </div>
                                </footer>



                                {{-- </form> --}}



                            </div>


                        </div>
                    </div>

                </div>

                <!-- End col -->
            </div>
        </div>
    </div>
    <!-- End Contentbar -->


@endsection
@section('script')

    <script language="javascript">
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>

    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    <!-- Input Mask js -->
    <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
@endsection
