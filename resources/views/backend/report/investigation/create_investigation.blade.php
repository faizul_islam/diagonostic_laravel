@section('title')
    Investigation Report
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">
            <?php
            if (!empty($data['test'])) {
                $test = $data['test'][0];
                $templatelist = $data['templatelist'];
            } else {
                $test = [];
                $templatelist = [];
            }
            
            // print_r($test);
            // die();
            
            ?>
            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-wheelchair mr-2"></i>
                                    Create Report</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <form action="{{ URL::to('updateInvestigation/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" id="hidden_base_url" value="{{ url('/') }}">

                                    <div class="panel-body">
                                        <input type="hidden" name="labtest_report_id" value="<?php echo $test['id']; ?>">
                                        <div class="row">
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Bill No</label>
                                                    <input type="text" class="form-control" name="bill_no"
                                                        value="<?php echo $test['bill_no']; ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Patient Name</label>
                                                    <input type="text" class="form-control" name="patient_name"
                                                        value="<?php echo $test['patient_name']; ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Referred By</label>
                                                    <input type="text" class="form-control" name="referred_by"
                                                        value="<?php echo $test['ref_name']; ?>" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Reporting Date <span
                                                            class="required">*</span></label>
                                                    <div class="input-group">
                                                        <input type="text" name="reporting_date" id="autoclose-date"
                                                            class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                            aria-describedby="basic-addon3" required />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon3"><i
                                                                    class="feather icon-calendar"></i></span>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Template</label>
                                                    <select class="form-control" name="template_id" id="template_id"
                                                        required>
                                                        <option value="">Select
                                                        </option>

                                                        @foreach ($data['templatelist'] as $temp)
                                                            <option value="{{ $temp['id'] }}">{{ $temp['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <input type="text" class="form-control" id="returnReport" name="returnReport"
                                            readonly /> --}}
                                        <div class="row">
                                            <div class="col-md-12 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Report <span
                                                            class="required">*</span></label>
                                                    <textarea name="tinymce-example" id="tinymce-example" required>
                                                             </textarea>



                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    id="getTemplate">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>



                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("change", "#template_id", function() {
                var template_id = $('#template_id').val();
                var baseUrl = $('#hidden_base_url').val();
                //   alert(template_id);
                if (template_id == "") {
                    swal('Warning', "Please Select a Template", 'warning');
                    return false;
                }


                if (template_id != '') {
                    $("#ajax_loader").css('display', 'block');
                    $.ajax({
                        url: baseUrl + "/get_template_details",
                        type: "post",
                        data: {
                            id: template_id
                        },
                        success: function(data) {
                            tinymce.get('tinymce-example').setContent(data);


                            // $('#returnReport').val(data);


                        }
                    });
                } else {
                    swal("Required!", "You have to select Template", "warning");
                }
            });
        </script>

        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>


    @endsection
