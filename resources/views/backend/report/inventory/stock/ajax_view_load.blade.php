<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />




<?php if(!empty($data['all_data'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i> Chemical Wise Stock Report</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr style="font-size: 8px;">
                                <th>Sl</th>
                                <th>Chemical Name</th>
                                <th>Category</th>
                                <th>Chemical Code</th>

                                <th>Unit</th>
                                <th>Purchase Price</th>
                                <th>Sale Price</th>

                                <th>In Qty</th>
                                <th>Out Qty</th>
                                <th>Current Stock</th>
                                <th>Purchase Total</th>
                                <th>Sale Total</th>
                                <th>Sale Profit</th>


                            </tr>
                        </thead>
                        <tbody>

                            <?php 
				$count = 1;
				$total_purchase_price = 0;
				$total_sale_price = 0;
				$total_profit = 0;
				 foreach ($data['all_data'] as $row){
					$purchase_price_unit = $row['purchase_price'] / $row['unit_ratio'];
					$totl_out_stock = $row['in_stock'] - $row['available_stock'];
					$purchase_price = $purchase_price_unit * $totl_out_stock;
					$sales_price = $row['sales_price'] * $totl_out_stock;
					$profit = $sales_price - $purchase_price;
					$total_purchase_price += $purchase_price;
					$total_sale_price += $sales_price;
					$total_profit += $profit;

				?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['category_name']; ?></td>
                                <td><?php echo $row['code']; ?></td>
                                <td><?php echo $row['unit_name']; ?></td>
                                <td><?php echo number_format($purchase_price_unit, 2, '.', ''); ?></td>
                                <td><?php echo number_format($row['sales_price'], 2, '.', ''); ?></td>
                                <td><?php echo $row['in_stock']; ?></td>
                                <td><?php echo $totl_out_stock; ?></td>
                                <td><?php echo $row['available_stock']; ?></td>
                                <td><?php echo number_format($purchase_price, 2, '.', ''); ?></td>
                                <td><?php echo number_format($sales_price, 2, '.', ''); ?></td>
                                <td><?php echo number_format($profit, 2, '.', ''); ?></td>
                            </tr>
                            <?php  } ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><?php echo number_format($total_purchase_price, 2, '.', ''); ?></th>
                                <th><?php echo number_format($total_sale_price, 2, '.', ''); ?></th>
                                <th><?php echo number_format($total_profit, 2, '.', ''); ?></th>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
