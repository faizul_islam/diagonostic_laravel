<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />




<?php if(!empty($data['all_data'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i> Purchase Report</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr style="font-size: 8px;">
                                <th>Sl</th>
                                <th>Bill No</th>
                                <th>Supplier</th>
                                <th>Purchase Status</th>

                                <th>Payment Status</th>
                                <th>Date</th>
                                <th>Net Payable</th>

                                <th>Total Paid</th>
                                <th>Total Due</th>

                            </tr>
                        </thead>
                        <tbody>

                            <?php 
				$count = 1;
				$total_amount = 0;
				$total_paid = 0;
				$total_due = 0;
				if (!empty($data['all_data'])){
					foreach ($data['all_data'] as $row):
						$total_amount += $row['net_payable'];
						$total_paid += $row['paid'];
						$total_due += $row['due'];
				?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $row['bill_no']; ?></td>
                                <td><?php echo $row['supplier_name']; ?></td>
                                <td>
                                    <?php
                                    $status_list = [
                                        '1' => 'ordered',
                                        '2' => 'received',
                                        '3' => 'pending',
                                    ];
                                    echo $status_list[$row['purchase_status']];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $labelMode = '';
                                    $status = $row['payment_status'];
                                    if ($status == 1) {
                                        $status = 'unpaid';
                                        $labelMode = 'label-danger-custom';
                                    } elseif ($status == 2) {
                                        $status = 'partly_paid';
                                        $labelMode = 'label-info-custom';
                                    } elseif ($status == 3 || $row['due'] == 0) {
                                        $status = 'total_paid';
                                        $labelMode = 'label-success-custom';
                                    }
                                    echo "<span class='label " . $labelMode . "'>" . $status . '</span>';
                                    ?>
                                </td>
                                <td><?php echo date('d-m-Y', strtotime($row['date'])); ?></td>
                                <td><?php echo number_format($row['net_payable'], 2, '.', ''); ?></td>
                                <td><?php echo number_format($row['paid'], 2, '.', ''); ?></td>
                                <td><?php echo number_format($row['due'], 2, '.', ''); ?></td>
                            </tr>
                            <?php endforeach; }?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <td><?php echo number_format($total_amount, 2, '.', ''); ?></td>
                                <td><?php echo number_format($total_paid, 2, '.', ''); ?></td>
                                <td><?php echo number_format($total_due, 2, '.', ''); ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
