@section('title')
    Refer Manager
@endsection
@extends('backend.layouts.main')

@section('style')
    <style>
        .page-title {
            width: 200px !important;
        }

    </style>
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">

                <div class="card m-b-30">
                    <div class="card-header bg-transparent border-secondary">
                        <h5 class="card-title">Select Ground</h5>
                    </div>
                    <div class="card-body">

                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">

                                {{-- <form action="{{ URL::to('show_stock_report/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data"> --}}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                <div class="panel-body">

                                    <div class="row">


                                        <div class="col-md-3 mb-sm">
                                            <div class="form-group">
                                                <label class="control-label">Referral</label>
                                                <select class="form-control" name="staff" id="staff" required>
                                                    <option value=""> Select</option>
                                                    @foreach ($data['staff'] as $v)

                                                        <option value="{{ $v['id'] }}">
                                                            {{ $v['name'] }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>


                                        <div class="col-md-3 mb-sm">
                                            <div class="form-group">
                                                <label class="control-label">From Date</label>

                                                <div class="input-group">
                                                    <input type="text" id="autoclose-date"
                                                        class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                        aria-describedby="basic-addon3" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon3"><i
                                                                class="feather icon-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-3 mb-sm">
                                            <div class="form-group">
                                                <label class="control-label">To Date</label>

                                                <div class="input-group">
                                                    <input type="text" id="default-date"
                                                        class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                        aria-describedby="basic-addon2" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2"><i
                                                                class="feather icon-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-2 mb-sm">

                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 23px;"></label>
                                                <button type="button" id="commission_summaryReport"
                                                    class="btn btn btn-default btn-block">{{ __('Filter') }}</button>

                                            </div>


                                        </div>
                                    </div>

                                </div>



                                {{-- </form> --}}



                            </div>


                        </div>
                    </div>

                </div>





                <div class="card m-b-30" id="showReport">



                </div>
                <!-- End col -->
            </div>
        </div>
    </div>
    <!-- End Contentbar -->


@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    <!-- Input Mask js -->
    <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
@endsection
