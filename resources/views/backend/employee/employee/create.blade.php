@section('title')
    Add Employee
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-wheelchair mr-2"></i>
                                    Create Employee</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <form action="{{ URL::to('storeEmployee/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="panel-body">
                                        <!-- basic details -->
                                        <div class="headers-line mt-md">
                                            <i class="fa fa-user-check"></i>
                                            <h5> Basic Details</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Name <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <input class="form-control" name="name" type="text" required>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Gender</label>
                                                    <select class="form-control" name="gender">
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                        <option value="3">Other</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Birthday</label>
                                                    <div class="input-group">
                                                        <input type="text" name="birthday" id="autoclose-date"
                                                            class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                            aria-describedby="basic-addon3" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon3"><i
                                                                    class="feather icon-calendar"></i></span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Religion <span
                                                            class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="religion" id="religion"
                                                        required>

                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Marital Status</label>
                                                    <select class="form-control" name="marital_status">
                                                        <option value="1">Single</option>
                                                        <option value="2">Married</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Blood Group</label>

                                                    <select class="form-control" name="blood_group">
                                                        @php $get_blood_group = App\Http\Helpers::get_blood_group(); @endphp
                                                        @foreach ($get_blood_group as $blood)
                                                            <option value="{{ $blood }}"> {{ $blood }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Mobile No <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="mobile_no" name="mobile_no"
                                                            type="text" required>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group ">
                                                    <label class="control-label">Email <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">

                                                        <input type="email" class="form-control" name="email" id="email"
                                                            required />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Address</label>
                                                    <textarea class="form-control" rows="3" name="address"></textarea>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="row mb-md">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="input-file-now">Profile Picture: </label>
                                                    <input type="file" name="photo" class="dropify"
                                                        data-allowed-file-extensions="jpg png" data-height="120" />
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- emergency contact -->
                                        <div class="headers-line">
                                            <h5> <i class="fa fa-pencil-ruler"></i> Office Details</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Role <span
                                                            class="text-danger">*</span></label>

                                                    <select class="form-control" name="role" required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['role'] as $role)

                                                            <option value="{{ $role['id'] }}">
                                                                {{ $role['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group>">
                                                    <label class="control-label">Designation <span
                                                            class="text-danger">*</span></label>
                                                    <select class="form-control" name="designation" required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['designation'] as $designation)

                                                            <option value="{{ $designation['id'] }}">
                                                                {{ $designation['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Department<span
                                                            class="text-danger">*</span> </label>

                                                    <select class="form-control" name="department" required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['department'] as $department)

                                                            <option value="{{ $department['id'] }}">
                                                                {{ $department['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Joining Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="joining_date" id="default-date"
                                                            class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                            aria-describedby="basic-addon3" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon3"><i
                                                                    class="feather icon-calendar"></i></span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group>">
                                                    <label class="control-label">Qualification</label>
                                                    <textarea class="form-control" name="qualification"
                                                        type="text"></textarea>
                                                </div>
                                            </div>


                                        </div>
                                        <br>

                                        <!-- emergency contact -->
                                        <div class="headers-line">
                                            <h5> <i class="fa fa-pencil-ruler"></i> Social Links</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Facebook</label>
                                                    <input class="form-control" name="facebook_url" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group>">
                                                    <label class="control-label">Twitter</label>
                                                    <input class="form-control" name="twitter_url" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">LinkedIn</label>
                                                    <input class="form-control" name="linkedin_url" type="text">
                                                </div>
                                            </div>

                                        </div>
                                        <br>



                                        <!-- emergency contact -->
                                        <div class="headers-line">
                                            <h5> <i class="fa fa-pencil-ruler"></i> Bank Details</h5>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Bank Name Name</label>
                                                    <input class="form-control" name="bank_name" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group>">
                                                    <label class="control-label">Holder Name</label>
                                                    <input class="form-control" name="holder_name" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Bank Branch</label>
                                                    <input class="form-control" name="bank_branch" type="text">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Bank Address</label>
                                                    <input class="form-control" name="bank_address" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group>">
                                                    <label class="control-label">Ifsc Code</label>
                                                    <input class="form-control" name="ifsc_code" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Account No</label>
                                                    <input class="form-control" name="account_no" type="text">
                                                </div>
                                            </div>

                                        </div>
                                        <br>
                                        <!-- login details -->
                                        <div class="headers-line">
                                            <h5> <i class="fa fa-user-lock"></i> Login Details </h5>
                                        </div>
                                        <hr>
                                        <div class="row mb-lg">
                                            <div class="col-md-6 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Username <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">

                                                        <input type="username" class="form-control" name="username"
                                                            id="username" required />
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Password <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">

                                                        <input type="password" class="form-control" name="password"
                                                            required />
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="col-md-3 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Retype Password <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">

                                                        <input type="password" class="form-control"
                                                            name="retype_password" required />
                                                    </div>

                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>



                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $(document).on("click", "#username", function() {

                var mobile_no = $('#mobile_no').val();
                var email = $('#email').val();
                if (email == "") {
                    swal('Warning', "Please write first email", 'warning');
                    return false;
                }
                $.ajax({
                    url: "loginDupCheck",
                    // alert();
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        email: email
                    },


                    success: function(data) {

                        if (data == 1) {

                            if (email != '') {
                                $("#username").val(email);
                                $("#username").attr("readonly", true);

                            } else {
                                // swal("Required!", "You have to set mobile no first", "warning");
                                swal("Required!", "You have to set email first", "warning");
                            }
                        } else {

                            $("#username").val('');
                            swal("Already Registered this email. please try with another");
                        }

                    }
                });

            });

            // $(document).on("click", "#username", function() {

            //     var email = $('#email').val();

            //     if (email == "") {
            //         swal('Warning', "Please write first mobile number", 'warning');
            //         return false;
            //     }


            //     if (email != '') {
            //         $("#username").val(email);
            //         $("#username").attr("readonly", true);

            //     } else {
            //         swal("Required!", "You have to set mobile no first", "warning");
            //     }
            // });
        </script>
        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    @endsection
