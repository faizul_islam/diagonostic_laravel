@section('title')
    Staff Details
@endsection
@extends('backend.layouts.main')
@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Animate css -->
    <link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" type="text/css" />
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">


            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">


                        <!-- Start col -->

                        <!-- Start col -->
                        <div class="col-lg-12">

                            <div class="best-product-slider">
                                <div class="best-product-slider-item">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <?php if ($data['editVal']['photo'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/employee/' . $data['editVal']['photo']);
                                            } ?>

                                            <img src="{{ $img }}" class="img-fluid" width="120" height="120">
                                        </div>
                                        <div class="col-sm-7">
                                            <span class="font-12 text-uppercase"><i class="fa fa-user"></i> Name:
                                            </span>
                                            {{ $data['editVal']['name'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-birthday-cake"></i>
                                                Birthday: </span>
                                            {{ $data['editVal']['birthday'] }}
                                            <br>
                                            {{-- <span class="font-12 text-uppercase"><i class="fa fa-users"></i> Category:
                                            </span>
                                            {{ $data['editVal']['category_id'] }}
                                            <br> --}}
                                            <span class="font-12 text-uppercase"><i class="fa fa-phone"></i> Mobile:
                                            </span>
                                            {{ $data['editVal']['mobileno'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-envelope"></i> Email:
                                            </span>
                                            {{ $data['editVal']['email'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"> <i class="fa fa-home"></i> Address:
                                            </span>
                                            {{ $data['editVal']['address'] }}


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- End col -->
                        <hr>

                        <div class="accordion accordion-light" id="accordionwithlight">
                            <div class="card">
                                <div class="card-header" id="headingOnelight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseOnelight" aria-expanded="false"
                                            aria-controls="collapseOnelight"><i class="feather icon-user-check mr-2"></i>
                                            Profile
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOnelight" class="collapse" aria-labelledby="headingOnelight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <form action="{{ URL::to('EmployeeUpdate/' . $data['editVal']['id']) }}"
                                            method="post" class="form-horizontal" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="panel-body">
                                                <!-- basic details -->
                                                <div class="headers-line mt-md">
                                                    <i class="fa fa-user-check"></i>
                                                    <h5> Basic Details</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Name <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="name"
                                                                    value="{{ $data['editVal']['name'] }}" type="text"
                                                                    required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Gender</label>
                                                            <select class="form-control" name="gender">
                                                                <option value="1" @if ($data['editVal']['gender'] == 1) selected='selected' @endif>Male</option>
                                                                <option value="2" @if ($data['editVal']['gender'] == 2) selected='selected' @endif>Female</option>
                                                                <option value="3" @if ($data['editVal']['gender'] == 3) selected='selected' @endif>Other</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Birthday</label>
                                                            <div class="input-group">
                                                                <input type="text" name="birthday"
                                                                    value="{{ $data['editVal']['birthday'] }}"
                                                                    id="autoclose-date" class="datepicker-here form-control"
                                                                    placeholder="{{ $data['editVal']['birthday'] }}"
                                                                    aria-describedby="basic-addon3" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text" id="basic-addon3"><i
                                                                            class="feather icon-calendar"></i></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Religion <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" name="religion"
                                                                value="{{ $data['editVal']['religion'] }}" id="religion"
                                                                required>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Marital Status</label>
                                                            <select class="form-control" name="marital_status">
                                                                <option value="1" @if ($data['editVal']['marital_status'] == 1) selected='selected' @endif>Single</option>
                                                                <option value="2" @if ($data['editVal']['marital_status'] == 2) selected='selected' @endif>Married</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Blood Group</label>

                                                            <select class="form-control" name="blood_group">
                                                                @php $get_blood_group = App\Http\Helpers::get_blood_group(); @endphp
                                                                @foreach ($get_blood_group as $blood)
                                                                    <option value="{{ $blood }}"
                                                                        @if ($data['editVal']['blood_group'] == $blood) selected='selected' @endif>
                                                                        {{ $blood }}
                                                                    </option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Mobile No <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input class="form-control" name="mobile_no"
                                                                    value="{{ $data['editVal']['mobileno'] }}"
                                                                    type="text" required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group ">
                                                            <label class="control-label">Email <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input type="email" class="form-control" name="email"
                                                                    value="{{ $data['editVal']['email'] }}" id="email"
                                                                    required />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Address</label>
                                                            <textarea class="form-control" rows="3"
                                                                name="address">{{ $data['editVal']['address'] }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="row mb-md">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="input-file-now">Profile Picture: </label>
                                                            <?php if ($data['editVal']['photo'] == '') {
                                                                $img = asset('/assets/images/no_img.png');
                                                            } else {
                                                                $img = asset('/public/assets/images/uploads/employee/' . $data['editVal']['photo']);
                                                            } ?>
                                                            <img width="100px" height="100px;" id="image1" src="<?= $img ?>"
                                                                alt="...">


                                                            <input type="file" name="photo"
                                                                value="{{ asset('/public/assets/images/uploads/employee/' . $data['editVal']['photo']) }}"
                                                                accept="image/*" onchange="readURLphoto1(this);"
                                                                class="form-control" style="width: 250px;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <!-- emergency contact -->
                                                <div class="headers-line">
                                                    <h5> <i class="fa fa-pencil-ruler"></i> Office Details</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    {{-- <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Role</label>

                                                            <select class="form-control" name="role">
                                                                <option value="">Select</option>
                                                                @foreach ($data['role'] as $role)

                                                                    <option value="{{ $role['id'] }}" @if ($data['editVal']['role'] == $role['id']) selected='selected' @endif>
                                                                        {{ $role['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Designation</label>
                                                            <select class="form-control" name="designation">
                                                                <option value="">Select</option>
                                                                @foreach ($data['designation'] as $designation)

                                                                    <option value="{{ $designation['id'] }}"
                                                                        @if ($data['editVal']['designation'] == $designation['id']) selected='selected' @endif>
                                                                        {{ $designation['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Department</label>

                                                            <select class="form-control" name="department">
                                                                <option value="">Select</option>
                                                                @foreach ($data['department'] as $department)

                                                                    <option value="{{ $department['id'] }}"
                                                                        @if ($data['editVal']['department'] == $department['id']) selected='selected' @endif>
                                                                        {{ $department['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Joining Date</label>
                                                            <input class="form-control" name="joining_date"
                                                                value="{{ $data['editVal']['joining_date'] }}"
                                                                type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Qualification</label>
                                                            <textarea class="form-control" name="qualification"
                                                                type="text"><?php echo str_replace('<br />', ' ', $data['editVal']['qualification']); ?></textarea>
                                                        </div>
                                                    </div>


                                                </div>
                                                <br>

                                                <!-- emergency contact -->
                                                <div class="headers-line">
                                                    <h5> <i class="fa fa-pencil-ruler"></i> Social Links</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Facebook</label>
                                                            <input class="form-control" name="facebook_url"
                                                                value="{{ $data['editVal']['facebook_url'] }}"
                                                                type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Twitter</label>
                                                            <input class="form-control" name="twitter_url"
                                                                value="{{ $data['editVal']['twitter_url'] }}"
                                                                type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">LinkedIn</label>
                                                            <input class="form-control" name="linkedin_url"
                                                                value="{{ $data['editVal']['linkedin_url'] }}"
                                                                type="text">
                                                        </div>
                                                    </div>

                                                </div>
                                                <br>

                                                @php
                                                    
                                                    $bank = App\Models\Staff_bank_account::where('staff_id', '=', $data['editVal']['id'])->get();
                                                    
                                                    if (!empty($bank->toArray())) {
                                                        $bankDetails = $bank[0];
                                                    } else {
                                                        $bankDetails = [];
                                                    }
                                                    
                                                @endphp

                                                <!-- emergency contact -->
                                                {{-- <div class="headers-line">
                                                    <h5> <i class="fa fa-pencil-ruler"></i> Bank Details</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Bank Name Name</label>
                                                            <input class="form-control" name="bank_name"
                                                                value="{{ $bankDetails['bank_name'] }}" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Holder Name</label>
                                                            <input class="form-control" name="holder_name"
                                                                value="{{ $bankDetails['holder_name'] }}" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Bank Branch</label>
                                                            <input class="form-control" name="bank_branch"
                                                                value="{{ $bankDetails['bank_branch'] }}" type="text">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Bank Address</label>
                                                            <input class="form-control" name="bank_address"
                                                                value="{{ $bankDetails['bank_address'] }}" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Ifsc Code</label>
                                                            <input class="form-control" name="ifsc_code"
                                                                value="{{ $bankDetails['ifsc_code'] }}" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Account No</label>
                                                            <input class="form-control" name="account_no"
                                                                value="{{ $bankDetails['account_no'] }}" type="text">
                                                        </div>
                                                    </div>

                                                </div> --}}
                                                <br>

                                            </div>


                                            <footer class="panel-footer mt-md">
                                                <div class="row" style="float: right">
                                                    <div class="col-md-12 ">
                                                        <button type="submit" class="btn btn btn-default btn-block"
                                                            name="app_setting" value="1">{{ __('Update') }}</button>
                                                    </div>
                                                </div>
                                            </footer>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwolight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwolight" aria-expanded="false"
                                            aria-controls="collapseTwolight"><i class="feather icon-list mr-2"></i>Bank
                                            Account
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwolight" class="collapse" aria-labelledby="headingTwolight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info mt-1 pull-right" data-toggle="modal"
                                            data-target="#varying-bank-modal"><i class="fa fa-plus"></i>
                                            Add Bank </button>
                                        <br><br><br>

                                        <!-- Start col -->

                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sl</th>
                                                        <th>Bank Name</th>
                                                        <th>Holder Name</th>
                                                        <th>Bank Branch</th>
                                                        <th>Bank Address</th>
                                                        <th>Ifsc Code</th>
                                                        <th>Account No</th>
                                                        <th width="8%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=0; @endphp
                                                    @foreach ($data['bank'] as $item)
                                                        @php $i++; @endphp

                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item['bank_name'] }}</td>
                                                            <td>{{ $item['holder_name'] }}</td>
                                                            <td>{{ $item['bank_branch'] }}</td>
                                                            <td>{{ $item['bank_address'] }}</td>
                                                            <td>{{ $item['ifsc_code'] }}</td>
                                                            <td>{{ $item['account_no'] }}</td>
                                                            <td>{{ $item['remarks'] }}</td>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn btn-round btn-danger-rgba deleteBankData"
                                                                    id="{{ $item['id'] }}" title="Delete"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>

                                                        </tr>
                                                    @endforeach


                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- End col -->
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThreelight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseThreelight" aria-expanded="false"
                                            aria-controls="collapseThreelight"><i
                                                class="feather icon-folder-plus mr-2"></i>Document</button>
                                    </h2>
                                </div>



                                <div id="collapseThreelight" class="collapse" aria-labelledby="headingThreelight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info mt-1 pull-right" data-toggle="modal"
                                            data-target="#varying-modal"><i class="fa fa-plus"></i>
                                            Add Document </button>
                                        <br><br><br>
                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sl</th>
                                                        <th>Title</th>
                                                        <th>Document Type</th>
                                                        <th>Remarks</th>
                                                        <th width="8%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=0; @endphp
                                                    @foreach ($data['document'] as $item)
                                                        @php $i++; @endphp

                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item['title'] }}</td>
                                                            <td>{{ $item['category_id'] }}</td>
                                                            <td>{{ $item['remarks'] }}</td>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn btn-round btn-danger-rgba deleteData"
                                                                    id="{{ $item['id'] }}" title="Delete"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>

                                                        </tr>
                                                    @endforeach


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingOnelight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapsePasslight" aria-expanded="false"
                                            aria-controls="collapsePasslight"><i class="feather icon-user-check mr-2"></i>
                                            User Info
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapsePasslight" class="collapse" aria-labelledby="headingOnelight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <form action="{{ URL::to('updatePassword/' . $data['editVal']['id']) }}"
                                            method="post" class="form-horizontal" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="panel-body">
                                                <!-- basic details -->
                                                <div class="headers-line mt-md">
                                                    <i class="fa fa-user-check"></i>
                                                    <h5> Change Password</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Old Password <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="old_password"
                                                                    id="old_password" type="text" required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">New Password</label><span
                                                                class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="password" id="password"
                                                                    type="text" required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                <br>

                                            </div>


                                            <footer class="panel-footer mt-md">
                                                <div class="row" style="float: right">
                                                    <div class="col-md-12 ">
                                                        <button type="submit" class="btn btn btn-default btn-block"
                                                            name="app_setting" value="1">{{ __('Update') }}</button>
                                                    </div>
                                                </div>
                                            </footer>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->





        </div>
    </div>
    </div>
    <!-- End col -->
    </div>
    <!-- End Contentbar -->
    <!-- Modal -->
    <div class="modal fade" id="varying-modal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>ADD DOCUMENT</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ URL::to('storeStaffDocument/') }}" method="post" class="form-horizontal"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="staff_id" id="staff_id" value="{{ $data['editVal']['id'] }}">
                    <div class="modal-body">



                        <div class="form-group">
                            <label for="title" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="type" class="col-form-label">Category: </label>

                            <select class="form-control" name="category_id">
                                @php $get_document_category = App\Http\Helpers::get_document_category(); @endphp
                                @foreach ($get_document_category as $cat)
                                    <option value="{{ $cat }}">
                                        {{ $cat }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="file_name" class="col-form-label">Document File:</label>
                            <input type="file" class="form-control" id="file_name" name="file_name">
                        </div>


                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Remark:</label>
                            <textarea class="form-control" id="message-text" name="remarks"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="text" value="{{ url('/') }}" id="base_url">
@endsection

@section('script')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on("click", "#password", function() {
            var base_url = $('#base_url').val();
            var old_password = $('#old_password').val();
            var staff_id = $('#staff_id').val();


            if (old_password == "") {
                swal('Warning', "Please write first your old password", 'warning');
                return false;
            }

            $.ajax({
                url: base_url + "/verifyPassword",
                // alert();
                type: "post",
                data: {
                    old_password: old_password,
                    staff_id: staff_id
                },
                success: function(data) {
                    if (data == 1) {
                        $('#password').attr('readonly', false);
                    } else {
                        $('#password').attr('required', true);
                        $('#password').attr('readonly', true);
                        swal('Warning', "Your old password is wrong.. Please write current password ",
                            'warning');

                    }

                }
            });



        });



        $(document).ready(function() {
            $(document).on("click", ".deleteData", function() {
                var id = $(this).attr('id');
                var base_url = $('#base_url').val();
                // alert(id);
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "deleteStaffDocument",
                            // alert();
                            type: "post",
                            data: {
                                id: id
                            },
                            success: function(data) {
                                swal("Information has been deleted", {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });
            });
        });


        function readURLphoto1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image1').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <!-- Wysiwig js -->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <!-- Summernote JS -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- Code Mirror JS -->
    <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>

    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>


    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>
    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>



@endsection
