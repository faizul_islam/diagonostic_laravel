@section('title')
    Staff Details
@endsection
@extends('backend.layouts.main')
@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-wheelchair mr-2"></i>
                                    Employee List </a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sl</th>
                                                <th>Photo</th>
                                                <th>Name</th>
                                                <th>Staff ID</th>

                                                <th>Designation</th>
                                                <th>Department</th>
                                                <th>Email</th>

                                                <th>Mobile</th>
                                                <?php 
                                                    if (App\Http\Helpers::get_permission('employee', 'is_edit')) {
                                                
                                                    ?>
                                                <th width="15%">Action</th>
                                                <?php } ?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['staff'] as $item)
                                                @php
                                                    $i++;
                                                    
                                                    $department = App\Models\Staff_department::where('id', '=', $item['department'])->get();
                                                    $designation = App\Models\Staff_designation::where('id', '=', $item['designation'])->get();
                                                    
                                                    if (!empty($department->toArray())) {
                                                        $deptName = $department[0]['name'];
                                                    } else {
                                                        $deptName = '';
                                                    }
                                                    if (!empty($designation->toArray())) {
                                                        $desigName = $designation[0]['name'];
                                                    } else {
                                                        $desigName = '';
                                                    }
                                                    
                                                @endphp
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>
                                                        <?php if ($item['photo'] == '') {
                                                            $img = asset('/assets/images/no_img.png');
                                                        } else {
                                                            $img = asset('/public/assets/images/uploads/employee/' . $item['photo']);
                                                        } ?>

                                                        <img width="40" height="40" src="{{ $img }}" alt="">
                                                    </td>
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['staff_id'] }}</td>

                                                    <td>{{ $deptName }}</td>
                                                    <td>{{ $desigName }}</td>
                                                    <td>{{ $item['email'] }}</td>

                                                    <td>{{ $item['mobileno'] }}</td>
                                                    <?php 
                                                    if (App\Http\Helpers::get_permission('employee', 'is_edit')) {
                                                
                                                    ?>

                                                    <td>


                                                        <a href="{{ URL::to('/StaffProfileView/' . $item['id']) }}"
                                                            class="btn btn-round btn-primary-rgba"><i
                                                                class="fa fa-arrow-circle-o-right"
                                                                style="margin-top: 7px;"></i>
                                                        </a>


                                                        <button type="button"
                                                            class="btn btn-round btn-danger-rgba deleteData"
                                                            id="{{ $item['id'] }}" title="Delete"><i
                                                                class="fa fa-trash"></i></button>


                                                    </td>
                                                    <?php } ?>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection

    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteStaff",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });
        </script>

        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>


    @endsection
