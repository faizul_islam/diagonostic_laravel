@section('title')
    Edit Leaves Manage
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Leave Manage List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Add
                                    Leave</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered"
                                        style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                <th width="10%">Staff Name</th>
                                                {{-- <th width="15%">Role</th> --}}
                                                <th width="15%">Department</th>
                                                <th width="8%">Leave Category</th>
                                                <th width="10%">Date Of Start</th>
                                                <th width="15%">Date Of End</th>
                                                <th width="15%">Days</th>
                                                <th width="15%">Apply Date</th>
                                                <th width="15%">Status</th>
                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <?php
                                                    $staff = App\Models\Staffs::Where('id', '=', $item['staff_id'])->get();
                                                    $department = App\Models\staff_department::Where('id', '=', $staff[0]['department'])->get();
                                                    
                                                    if ($staff[0]['department'] == 0) {
                                                        $department = 'Admin';
                                                    } else {
                                                        $department = $department[0]['name'];
                                                    }
                                                    $category = App\Models\leave_categorie::Where('id', '=', $item['category_id'])->get();
                                                    
                                                    // echo '<pre>';
                                                    // print_r($staff);
                                                    // die();
                                                    
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $staff[0]['name'] }}</td>
                                                    {{-- <td>{{ $category[0]['name'] }}</td> --}}
                                                    <td>{{ $department }}</td>
                                                    <td>{{ $category[0]['name'] }}</td>


                                                    <td>{{ date('d-m-Y', strtotime($item['start_date'])) }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item['end_date'])) }}</td>
                                                    <td>{{ $item['leave_days'] }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item['apply_date'])) }}</td>
                                                    <td>{{ $item['status'] }}</td>


                                                    <td>
                                                        <form action="{{ URL::to('editLeaveManage/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade show active" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('LeaveManageUpdate/' . $data['editVal']['id']) }}" method="POST"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Staff Name') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="staff_id" required>
                                                @foreach ($data['Staffs'] as $cat)

                                                    <option value="{{ $cat['id'] }}" @if ($data['editVal']['staff_id'] == $cat['id']) selected='selected' @endif>
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Leave Type') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="category_id" required>
                                                @foreach ($data['leave_categorie'] as $cat)

                                                    <option value="{{ $cat['id'] }}" @if ($data['editVal']['category_id'] == $cat['id']) selected='selected' @endif>
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Date') }}</label>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" id="range-date" name="leave_date"
                                                    class="datepicker-here form-control"
                                                    placeholder="{{ date('d/m/Y', strtotime($data['editVal']['start_date'])) . ' - ' . date('d/m/Y', strtotime($data['editVal']['end_date'])) }}"
                                                    aria-describedby="basic-addon7" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon7"><i
                                                            class="feather icon-calendar"></i></span>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Reason') }}</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" name="reason"
                                                required>{{ $data['editVal']['reason'] }}</textarea>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Attachment') }}</label>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control" name="orig_file_name"
                                                value="{{ $data['editVal']['orig_file_name'] }}">
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Comments') }}</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" name="comments"
                                                value="{{ $data['editVal']['comments'] }}"></textarea>
                                        </div>

                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteLeaveManage",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });
        </script>

        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Input Mask js -->
        <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
    @endsection
