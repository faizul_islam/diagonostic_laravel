@section('title')
    Salary Template
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Template List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line" role="tab"
                                    aria-controls="profile-line" aria-selected="false"><i class="fa fa-edit mr-2"></i>Add
                                    Template</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                <th width="10%">Salary Grade</th>
                                                <th width="10%">Basic Salary</th>
                                                <th width="15%">Overtime</th>

                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <td>{{ $i }}</td>
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['basic_salary'] }}</td>
                                                    <td>{{ $item['overtime_salary'] }}</td>

                                                    <td>
                                                        <form action="{{ URL::to('editChemicalStock/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel" aria-labelledby="profile-tab-line">

                                <form action="{{ URL::to('storeSalaryTemplates/') }}" method="post"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Salary Grade <span
                                                class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="template_name" value=""
                                                placeholder="Grade Name Here" required />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Basic Salary <span
                                                class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="basic_salary" id="basic_salary"
                                                value="" required placeholder="Basic Salary Here" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Overtime</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="overtime_rate" value=""
                                                placeholder="Overtime Rate Here">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xl-6">
                                            <div class="card text-white border-secondary m-b-30">
                                                <h5 class="card-header text-info"><?php echo 'Allowances'; ?></h5>
                                                <div class="card-body">

                                                    <section class="panel panel-custom">

                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control"
                                                                        name="allowance[0][name]"
                                                                        placeholder="<?php echo 'Name of Allowance'; ?>" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="number" class="allowance form-control"
                                                                        name="allowance[0][amount]"
                                                                        placeholder="<?php echo 'Amount'; ?>" />
                                                                </div>
                                                            </div>
                                                            <div id="add_new_allowance"></div>
                                                            <br>
                                                            <button type="button" class="btn btn-default mt-md"
                                                                onclick="addAllowanceRows()">
                                                                <i class="fa fa-plus-circle"></i> <?php echo 'Add Rows'; ?>
                                                            </button>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-xl-6">
                                            <div class="card text-white border-secondary m-b-30">
                                                <h5 class="card-header text-info"><?php echo 'Deductions'; ?></h5>
                                                <div class="card-body">

                                                    <section class="panel panel-custom">

                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control"
                                                                        name="deduction[0][name]"
                                                                        placeholder="<?php echo 'Name of Deductions'; ?>" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="number" class="deduction form-control"
                                                                        name="deduction[0][amount]"
                                                                        placeholder="<?php echo 'Amount'; ?>" />
                                                                </div>
                                                            </div>
                                                            <div id="add_new_deduction"></div>
                                                            <br>
                                                            <button type="button" class="btn btn-default mt-md"
                                                                onclick="addDeductionRows()">
                                                                <i class="fa fa-plus-circle"></i> <?php echo 'Add Rows'; ?>
                                                            </button>
                                                        </div>
                                                    </section>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xl-6"></div>
                                        <div class="col-md-12 col-lg-12 col-xl-6">
                                            <div class="card text-white border-secondary m-b-30">
                                                <div class="card-body">
                                                    <section class="panel panel-custom">
                                                        <h5 class="card-header text-info"><?php echo 'Salary Details'; ?></h5>

                                                        <div class="panel-body">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr class="b-top-none">
                                                                        <td colspan="2"><?php echo 'Basic' . ' ' . 'Salary'; ?></td>
                                                                        <td class="text-left">
                                                                            <div class="input-group">

                                                                                <input type="text" class="form-control"
                                                                                    name="salary_amount" readonly
                                                                                    id="salary_amount" value="0" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><?php echo 'Total' . ' ' . 'Allowance'; ?></td>
                                                                        <td class="text-left">
                                                                            <div class="input-group">

                                                                                <input type="text" class="form-control"
                                                                                    name="total_allowance" readonly
                                                                                    id="total_allowance" value="0" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><?php echo 'Total' . ' ' . 'Deduction'; ?></td>
                                                                        <td class="text-left">
                                                                            <div class="input-group">

                                                                                <input type="text" class="form-control"
                                                                                    name="total_deduction" readonly
                                                                                    id="total_deduction" value="0" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr class="h4">
                                                                        <td colspan="2"><?php echo 'Net' . ' ' . 'Salary'; ?></td>
                                                                        <td class="text-left">
                                                                            <div class="input-group">

                                                                                <input type="text" class="form-control"
                                                                                    name="net_salary" readonly
                                                                                    id="net_salary" value="0" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>





                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteSalaryTemplates",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });

            $(document).ready(function() {
                //***************** add new fields for any form **************//
                var max_fields = 6; //maximum input boxes allowed
                var wrapper = $(".tbodyCell"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID
                var x = 1; //initlal text box count
                var i = 1;
                var total = $('#hidden_total').val();

                $(add_button).click(function(e) { //on add input button click
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        var chemical = $('#chemical').val();
                        var chemicalText = $("#chemical option:selected").text();
                        var quantity = $('#quantity').val();
                        var unit_price = $('#unit_price').val();
                        var discount = $('#discount').val();
                        var total_price = $('#total_price').val();


                        if (!chemical) {
                            swal("Oops! You must have to select a chart of account.", {
                                icon: "warning",
                            });
                            return;
                        } else if (quantity <= 0) {
                            swal("Oops! Amount must be greater then 0.", {
                                icon: "warning",
                            });
                            return;
                        } else {
                            var append_value = "<tr><td>" + i + "</td>" +

                                "<td><input type='hidden' name='chemical[]' value='" + chemical +
                                "'>" + chemicalText + "</td>" +

                                "<td><input type='hidden' name='unit_price[]' value='" + unit_price + "'>" +
                                unit_price + "</td>" +

                                "<td><input type='hidden' name='quantity[]' class='quantity' value='" +
                                quantity +
                                "'>" + quantity + "</td>" +
                                "<td><input type='hidden' name='discount[]' value='" + discount + "'>" +
                                discount + "</td>" +
                                "<td class='text-right'><input type='hidden' name='total_price[]' class='payC_amount' value='" +
                                total_price + "'>" + total_price + "</td>" +
                                "<td><a href='#' class='remove_field btn btn-danger btn-sm'>X</a></td></tr>";

                            $(wrapper).append(append_value); //add input box
                            total = parseFloat(total) + parseFloat(total_price);
                            var append_value_2 =
                                "<tr><td colspan='5'><b>Total</b></td><td class='text-right'><b id='totalShow'>" +
                                total + "</b></td><td></td></tr>";
                            $('.tfootCell').html(append_value_2);
                            $('#hidden_total').val(total);
                        }
                        i++;
                    }



                });

                $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
                    e.preventDefault();
                    var payRemove = $(this).closest('tr').find('.payC_amount').val();
                    //alert(parseFloat(payRemove));
                    total = parseFloat(total) - parseFloat(payRemove);
                    $(this).closest('tr').remove();
                    x--;
                    $('#totalShow').html(total);
                });


            });


            var iAllowance = 1;

            function addAllowanceRows() {
                var html_row = "<br>";
                html_row += '<div class="row" id="al_row_' + iAllowance + '"><div class="col-md-6 mt-md">';
                html_row += '<input class="form-control" name="allowance[' + iAllowance +
                    '][name]" placeholder="<?php echo 'name_of_allowance'; ?>" type="text">';
                html_row += '</div>';
                html_row +=
                    '<div class="col-md-4 mt-md"> <input type="number" class="allowance form-control" name="allowance[' +
                    iAllowance + '][amount]" placeholder="<?php echo 'amount'; ?>"></div>';
                html_row +=
                    '<div class="col-md-2 mt-md text-right"><button type="button" class="btn btn-danger" onclick="deleteAllowancRow(' +
                    iAllowance + ')"><i class="fa fa-times"></i> </button></div></div>';
                $("#add_new_allowance").append(html_row);
                iAllowance++;
            }

            function deleteAllowancRow(id) {
                $("#al_row_" + id).remove();
                totalCalculate();
            }

            var iDeduction = 1;

            function addDeductionRows() {
                var html_row = "<br>";
                html_row += '<div class="row" id="de_row_' + iDeduction + '"><div class="col-md-6 mt-md">';
                html_row += '<input class="form-control" name="deduction[' + iDeduction +
                    '][name]" placeholder="<?php echo 'name_of_deductions'; ?>" type="text">';
                html_row +=
                    '</div><div class="col-md-4 mt-md"> <input type="number" class="deduction form-control" name="deduction[' +
                    iDeduction + '][amount]" placeholder="<?php echo 'amount'; ?>"></div>';
                html_row +=
                    '<div class="col-md-2 mt-md text-right"><button type="button" class="btn btn-danger" onclick="deleteDeductionRow(' +
                    iDeduction + ')"><i class="fa fa-times"></i> </button></div></div>';
                $("#add_new_deduction").append(html_row);
                iDeduction++;
            }

            function deleteDeductionRow(id) {
                $("#de_row_" + id).remove();
                totalCalculate();
            }

            $(document).on("change", function() {
                totalCalculate();
            });

            function totalCalculate() {
                var total_allowance = 0;
                var total_deduction = 0;
                $(".allowance").each(function() {
                    total_allowance += read_number($(this).val());
                });

                $(".deduction").each(function() {
                    total_deduction += read_number($(this).val());
                });

                $("#total_allowance").val(total_allowance);
                $("#total_deduction").val(total_deduction);

                var basic = read_number($('#basic_salary').val());
                var net_amount = (basic + total_allowance) - total_deduction;

                $("#salary_amount").val(basic);
                $("#net_salary").val(net_amount);
            }

            function read_number(inputelm) {
                if (isNaN(inputelm) || inputelm.length == 0) {
                    return 0;
                } else {
                    return parseFloat(inputelm);
                }
            }
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
    @endsection
