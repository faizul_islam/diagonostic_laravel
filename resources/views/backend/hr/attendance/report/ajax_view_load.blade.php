<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />


<style>
    .page-title {
        width: 200px !important;
    }

</style>

<?php if(!empty($data['staff'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i>Attendance Report</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                <?php if (isset($data['staff'])) { ?>
                <section class="panel">
                    <header class="panel-heading">
                        <h4 class="panel-title"><i class="far fa-chart-bar"></i> <?php echo 'Attendance Report'; ?></h4>
                    </header>
                    <div class="panel-body">
                        <div class="row mt-sm">
                            <div class="col-md-offset-8 col-md-4">
                                <table class="table table-condensed table-bordered text-dark text-center">
                                    <tbody>
                                        <tr>
                                            <td>Present : <i class="fa fa-check-circle text-success"></i></td>
                                            <td>Absent : <i class="fa fa-times-circle text-danger"></i></td>
                                            <td>Holiday : H</td>
                                            <td>Late : <i class="fa fa-clock"></i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Hidden information for printing -->
                                <div class="export_title">Attendance Report </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" cellspacing="0" width="100%" id="table-export">
                                        <thead>
                                            <tr>
                                                <td><?php echo 'Staff Name'; ?></td>
                                                <?php
for($i = 1; $i <= 31; $i++){
    $date = date('Y-m-d', strtotime($i. '-' .$data['fromDate']));

?>
                                                <td class="text-center"><?php echo date('D', strtotime($date)); ?> <br> <?php echo date('d', strtotime($date)); ?>
                                                </td>
                                                <?php } ?>
                                                <td class="text-center">Total<br> Present</td>
                                                <td class="text-center">Total<br> Absent</td>
                                                <td class="text-center">Total<br> Late</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
$total_present = 0;
$total_absent = 0;
$total_late = 0;

if(count($data['staff'])){ foreach ($data['staff'] as $row){ ?>
                                            <tr>
                                                <td><?php echo $row['name']; ?></td>
                                                <?php
for ($i = 1; $i <= 31; $i++) { 
  
$date = date('Y-m-d', strtotime($i. '-' .$data['fromDate']));

  $atten = App\Models\Custom_model::get_attendance_by_date($row['id'], $date);
                              
?>
                                                <td class="center">
                                                    <?php if (!empty($atten)) {
                                                        if(!empty($atten[0]['status'])){ $status= $atten[0]['status'];}else{$status='';}
                                                         ?>
                                                    <span data-toggle="popover" data-trigger="hover"
                                                        data-placement="top" data-trigger="hover"
                                                        data-content="<?php if (!empty($atten[0]['remark'])) {
                                                            echo $atten[0]['remark'];
                                                        } else {
                                                            echo '';
                                                        } ?>">
                                                        <?php if ($status == 'A') { $total_absent++; ?>
                                                        <i class="fa fa-times-circle text-danger"></i><span
                                                            class="visible-print">A</span>
                                                        <?php } if ($status == 'P') { $total_present++; ?>
                                                        <i class="fa fa-check-circle text-success"></i><span
                                                            class="visible-print">P</span>
                                                        <?php } if ($status == 'L') { $total_late++; ?>
                                                        <i class="fa fa-clock text-info"></i><span
                                                            class="visible-print">L</span>
                                                        <?php } if ($status == 'H'){ ?>
                                                        <i class="fa fa-hospital-symbol text-tertiary"></i><span
                                                            class="visible-print">H</span>
                                                        <?php } ?>
                                                    </span>
                                                    <?php } ?>
                                                </td>
                                                <?php } ?>
                                                <td class="center"><?php echo $total_present; ?></td>
                                                <td class="center"><?php echo $total_absent; ?></td>
                                                <td class="center"><?php echo $total_late; ?></td>
                                            </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php } ?>


            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
