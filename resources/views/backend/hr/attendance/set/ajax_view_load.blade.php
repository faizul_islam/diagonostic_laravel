<!-- DataTables css -->
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive Datatable css -->
<link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
    type="text/css" />

<link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- Sweet Alert css -->
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />


<style>
    .page-title {
        width: 200px !important;
    }

</style>

<?php if(!empty($data['all_data'])){?>
<div class="card m-b-30" id="showReport">
    <div class="card-header bg-transparent border-secondary">

        <h5 class="card-title"><i class="fa fa-list-ol"></i>Staff List</h5>
    </div>

    <div class="card-body">

        <div class="tab-content" id="defaultTabContentLine">
            <div class="tab-pane fade show active" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">

                <form action="{{ URL::to('AddAttandance/') }}" method="post" class="form-horizontal"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="date" value="{{ $data['fromDate'] }}">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="font-size: 8px;">
                                    <th>Sl</th>
                                    <th>Staff Name</th>
                                    <th>Staff Id</th>
                                    <th>Status</th>
                                    <th>Remarks</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
								$count = 1;
								if(count($data['all_data'])){
									foreach ($data['all_data'] as $key => $row):
									?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row['staff_name']; ?></td>
                                    <td><?php echo $row['staffid']; ?></td>
                                    <td>
                                        <input type="hidden" name="attendance[<?php echo $key; ?>][staff_id]"
                                            value="<?php echo $row['id']; ?>">
                                        <input type="hidden" name="attendance[<?php echo $key; ?>][old_atten_id]"
                                            value="<?php echo $row['atten_id']; ?>">
                                        <div class="radio-custom radio-success radio-inline">
                                            <input type="radio" class="spresent" <?php echo $row['att_status'] == 'P' ? 'checked' : ''; ?>
                                                name="attendance[<?php echo $key; ?>][status]"
                                                id="present<?php echo $key; ?>" value="P">
                                            <label for="present<?php echo $key; ?>"><?php echo 'Present'; ?></label>
                                        </div>

                                        <div class="radio-custom radio-danger radio-inline">
                                            <input type="radio" class="sabsent" <?php echo $row['att_status'] == 'A' ? 'checked' : ''; ?>
                                                name="attendance[<?php echo $key; ?>][status]"
                                                id="absent<?php echo $key; ?>" value="A">
                                            <label for="absent<?php echo $key; ?>"><?php echo 'Absent'; ?></label>
                                        </div>

                                        <div class="radio-custom radio-info radio-inline">
                                            <input type="radio" class="sholiday" <?php echo $row['att_status'] == 'H' ? 'checked' : ''; ?>
                                                name="attendance[<?php echo $key; ?>][status]"
                                                id="holiday<?php echo $key; ?>" value="H">
                                            <label for="holiday<?php echo $key; ?>"><?php echo 'Holiday'; ?></label>
                                        </div>

                                        <div class="radio-custom radio-inline">
                                            <input type="radio" class="slate" <?php echo $row['att_status'] == 'L' ? 'checked' : ''; ?>
                                                name="attendance[<?php echo $key; ?>][status]"
                                                id="late<?php echo $key; ?>" value="L">
                                            <label for="late<?php echo $key; ?>"><?php echo 'Late'; ?></label>
                                        </div>
                                    </td>
                                    <td>
                                        <input class="form-control" name="attendance[<?php echo $key; ?>][remark]"
                                            type="text" value="<?php echo $row['remark']; ?>" />
                                    </td>
                                </tr>
                                <?php
										endforeach;
									}else{
										echo '<tr><td colspan="5"><h5 class="text-danger text-center">'.'no information available'.'</td></tr>';
									}
									?>
                        </table>
                    </div>
                    <div class="col-md-2 mb-sm">

                        <div class="form-group">
                            <label class="control-label" style="margin-top: 23px;"></label>
                            <button type="submit" class="btn btn btn-default btn-block">{{ __('Filter') }}</button>

                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<?php }else{?>
<div class="alert alert-danger" role="alert">
    Data not found..
</div>
<?php } ?>


</div>
<!-- End col -->
</div>
<!-- End Contentbar -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Datatable js -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

<!-- Sweet-Alert js -->
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

<!-- Input Mask js -->
<script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
<!-- Datepicker JS -->
<script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

<!-- Model js -->
<script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
