@section('title')
    Edit Voucher Head
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Voucher Head List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Edit
                                    Voucher Head</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" id="contact-tab-line" data-toggle="tab" href="#contact-line" role="tab"
                                    aria-controls="contact-line" aria-selected="false"><i
                                        class="feather icon-phone mr-2"></i>Logo </a>
                            </li> --}}
                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered"
                                        style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Sl</th>
                                                <th> Name</th>
                                                <th>Type</th>
                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['type'] }}</td>


                                                    <td>
                                                        <form action="{{ URL::to('editVoucherHead/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade show active" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('VoucherHeadUpdate/' . $data['editVal']['id']) }}" method="POST"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Name') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['name'] }}" name="name" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Type') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="type">

                                                <option value="Expense" @if ($data['editVal']['type'] == 'Expense') selected='selected' @endif> Expense
                                                </option>
                                                <option value="Income" @if ($data['editVal']['type'] == 'Income') selected='selected' @endif> Income
                                                </option>

                                            </select>

                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Update') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteVoucherHead",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>


    @endsection
