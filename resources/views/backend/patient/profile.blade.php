@section('title')
    Patient Details
@endsection
@extends('backend.layouts.main')
@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Animate css -->
    <link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" type="text/css" />
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">


            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">


                        <!-- Start col -->

                        <!-- Start col -->
                        <div class="col-lg-12">

                            <div class="best-product-slider">
                                <div class="best-product-slider-item">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <?php if ($data['editVal']['photo'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/patient/' . $data['editVal']['photo']);
                                            } ?>

                                            <img src="{{ $img }}" class="img-fluid" width="120" height="120">
                                        </div>
                                        <div class="col-sm-7">
                                            <span class="font-12 text-uppercase"><i class="fa fa-user"></i> Name:
                                            </span>
                                            {{ $data['editVal']['name'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-birthday-cake"></i>
                                                Birthday: </span>
                                            {{ $data['editVal']['birthday'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-users"></i> Category:
                                            </span>
                                            {{ $data['editVal']['category_id'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-phone"></i> Mobile:
                                            </span>
                                            {{ $data['editVal']['mobileno'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"><i class="fa fa-envelope"></i> Email:
                                            </span>
                                            {{ $data['editVal']['email'] }}
                                            <br>
                                            <span class="font-12 text-uppercase"> <i class="fa fa-home"></i> Address:
                                            </span>
                                            {{ $data['editVal']['address'] }}


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- End col -->
                        <hr>

                        <div class="accordion accordion-light" id="accordionwithlight">
                            <div class="card">
                                <div class="card-header" id="headingOnelight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseOnelight" aria-expanded="false"
                                            aria-controls="collapseOnelight"><i class="feather icon-user-check mr-2"></i>
                                            Profile
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOnelight" class="collapse" aria-labelledby="headingOnelight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <form action="{{ URL::to('PatientUpdate/' . $data['editVal']['id']) }}"
                                            method="post" class="form-horizontal" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                            <div class="panel-body">
                                                <!-- basic details -->
                                                <div class="headers-line mt-md">
                                                    <i class="fa fa-user-check"></i>
                                                    <h5> Basic Details</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Name <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input class="form-control"
                                                                    value="{{ $data['editVal']['name'] }}" name="name"
                                                                    type="text" required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Gender</label>
                                                            <select class="form-control" name="gender">
                                                                <option value="1" @if ($data['editVal']['sex'] == 1) selected='selected' @endif>Male</option>
                                                                <option value="2" @if ($data['editVal']['sex'] == 2) selected='selected' @endif>Female</option>
                                                                <option value="3" @if ($data['editVal']['sex'] == 3) selected='selected' @endif>Other</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Birthday</label>
                                                            <div class="input-group">
                                                                <input type="text" name="birthday"
                                                                    value="{{ date('d/m/Y', strtotime($data['editVal']['birthday'])) }}"
                                                                    id="autoclose-date" class="datepicker-here form-control"
                                                                    placeholder="{{ date('d/m/Y', strtotime($data['editVal']['birthday'])) }}"
                                                                    aria-describedby="basic-addon3" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text" id="basic-addon3"><i
                                                                            class="feather icon-calendar"></i></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Age <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" name="age"
                                                                value="{{ $data['editVal']['age'] }}" id="age" required>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Marital Status</label>
                                                            <select class="form-control" name="marital_status">
                                                                <option value="1" @if ($data['editVal']['marital_status'] == 1) selected='selected' @endif>Single</option>
                                                                <option value="2" @if ($data['editVal']['marital_status'] == 2) selected='selected' @endif>Married</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Mobile No <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input class="form-control"
                                                                    value="{{ $data['editVal']['mobileno'] }}"
                                                                    name="mobile_no" type="text" required>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group ">
                                                            <label class="control-label">Email <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input type="email" class="form-control"
                                                                    value="{{ $data['editVal']['email'] }}" name="email"
                                                                    id="email" required />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 mb-md">
                                                        <div class="form-group">
                                                            <label class="control-label">Category <span
                                                                    class="text-danger">*</span></label>
                                                            <select class="form-control" name="category_id" required>
                                                                @foreach ($data['category'] as $cat)
                                                                    <option value="{{ $cat['id'] }}"
                                                                        @if ($data['editVal']['category_id'] == $cat['id']) selected='selected' @endif>
                                                                        {{ $cat['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Blood Group</label>

                                                            <select class="form-control" name="blood_group">
                                                                @php $get_blood_group = App\Http\Helpers::get_blood_group(); @endphp
                                                                @foreach ($get_blood_group as $blood)
                                                                    <option value="{{ $blood }}"
                                                                        @if ($data['editVal']['blood_group'] == $blood) selected='selected' @endif>
                                                                        {{ $blood }}
                                                                    </option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Blood Pressure</label>
                                                            <input type="text" class="form-control"
                                                                value="{{ $data['editVal']['blood_pressure'] }}"
                                                                name="blood_pressure">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Height</label>
                                                            <input type="text" class="form-control"
                                                                value="{{ $data['editVal']['height'] }}" name="height">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Weight</label>
                                                            <input type="text" class="form-control" name="weight"
                                                                value="{{ $data['editVal']['weight'] }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Address</label>
                                                            <textarea class="form-control" rows="3"
                                                                name="address">{{ $data['editVal']['address'] }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-md">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="input-file-now">Profile Picture: </label>

                                                            <?php if ($data['editVal']['photo'] == '') {
                                                                $img = asset('/assets/images/no_img.png');
                                                            } else {
                                                                $img = asset('/public/assets/images/uploads/patient/' . $data['editVal']['photo']);
                                                            } ?>
                                                            <img width="100px" height="100px;" id="image1" src="<?= $img ?>"
                                                                alt="...">


                                                            <input type="file" name="photo"
                                                                value="{{ asset('/public/assets/images/uploads/patient/' . $data['editVal']['photo']) }}"
                                                                accept="image/*" onchange="readURLphoto1(this);"
                                                                class="form-control" style="width: 250px;">

                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <!-- emergency contact -->
                                                <div class="headers-line">
                                                    <h5> <i class="fa fa-pencil-ruler"></i> Emergency Contact</h5>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Guardian</label>
                                                            <input class="form-control"
                                                                value="{{ $data['editVal']['guardian'] }}"
                                                                name="guardian" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group>">
                                                            <label class="control-label">Relationship</label>
                                                            <input class="form-control"
                                                                value="{{ $data['editVal']['relationship'] }}"
                                                                name="relationship" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Mobile No</label>
                                                            <input class="form-control" name="gua_mobileno"
                                                                value="{{ $data['editVal']['gua_mobileno'] }}"
                                                                type="text">
                                                        </div>
                                                    </div>

                                                </div>
                                                <br>
                                                <!-- login details -->
                                                {{-- <div class="headers-line">
                                                    <h5> <i class="fa fa-user-lock"></i> login_details </h5>
                                                </div>
                                                <hr>
                                                <div class="row mb-lg">
                                                    <div class="col-md-6 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Username <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input type="username" class="form-control"
                                                                    value="{{ $data['editVal']['username'] }}"
                                                                    name="username" id="username" readonly />
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-sm">
                                                        <div class="form-group">
                                                            <label class="control-label">Password <span
                                                                    class="text-danger">*</span></label>
                                                            <div class="input-group">

                                                                <input type="password" class="form-control" name="password"
                                                                    required />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div> --}}
                                            </div>


                                            <footer class="panel-footer mt-md">
                                                <div class="row" style="float: right">
                                                    <div class="col-md-12 ">
                                                        <button type="submit" class="btn btn btn-default btn-block"
                                                            name="app_setting" value="1">{{ __('Update') }}</button>
                                                    </div>
                                                </div>
                                            </footer>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwolight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwolight" aria-expanded="false"
                                            aria-controls="collapseTwolight"><i class="feather icon-list mr-2"></i>Bill
                                            History
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwolight" class="collapse" aria-labelledby="headingTwolight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <!-- Start col -->

                                        <div class="table-responsive">
                                            <table id="datatable-buttons" class="table table-striped table-bordered"
                                                style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sl</th>
                                                        <th>Bill No</th>
                                                        <th>Delivery Date</th>
                                                        <th>Delivery Status</th>
                                                        <th>Payment Status</th>
                                                        <th>Net Payable</th>
                                                        <th>Paid</th>
                                                        <th>Due</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
								$count = 1;
								if (!empty($data['billlist'])){
									foreach($data['billlist'] as $row):
									?>
                                                    <tr>
                                                        <td><?php echo $count++; ?></td>
                                                        <td><?php echo $row['bill_no']; ?></td>
                                                        <td><?php echo date('d-m-Y', strtotime($row['delivery_date'])) . ' - ' . date('h:i A', strtotime($row['delivery_time'])); ?></td>
                                                        <td>
                                                            <?php
                                                            if ($row['delivery_status'] == 2) {
                                                                echo "<span class='btn btn-rounded btn-success-rgba'>" . 'completed' . '</span>';
                                                            } else {
                                                                echo "<span class='btn btn-rounded btn-danger-rgba'>" . 'undelivered' . '</span>';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $labelMode = '';
                                                            $status = $row['status'];
                                                            if ($status == 1) {
                                                                $status = 'unpaid';
                                                                $labelMode = 'label-danger-custom';
                                                            } elseif ($status == 2) {
                                                                $status = 'partly_paid';
                                                                $labelMode = 'label-info-custom';
                                                            } elseif ($status == 3 || $row['total_due'] == 0) {
                                                                $status = 'total_paid';
                                                                $labelMode = 'label-success-custom';
                                                            }
                                                            echo "<span class='label " . $labelMode . "'>" . $status . '</span>';
                                                            ?>
                                                        </td>
                                                        <td><?php echo number_format($row['total'] - $row['discount'] + $row['tax_amount'], 2, '.', ''); ?></td>
                                                        <td><?php echo number_format($row['paid'], 2, '.', ''); ?></td>
                                                        <td><?php echo number_format($row['due'], 2, '.', ''); ?></td>
                                                        <td><?php echo date('d-m-Y', strtotime($row['date'])); ?></td>
                                                        <td>

                                                            <a href="{{ URL::to('/test_bill_invoice/' . $row['id']) }}">
                                                                <button type="button" class="btn btn-secondary"><i
                                                                        class="fa fa-eye mr-2"></i>
                                                                    Invoice</button>

                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; }?>


                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- End col -->
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThreelight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseThreelight" aria-expanded="false"
                                            aria-controls="collapseThreelight"><i
                                                class="feather icon-folder-plus mr-2"></i>Document</button>
                                    </h2>
                                </div>



                                <div id="collapseThreelight" class="collapse" aria-labelledby="headingThreelight"
                                    data-parent="#accordionwithlight">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info mt-1 pull-right" data-toggle="modal"
                                            data-target="#varying-modal"><i class="fa fa-plus"></i>
                                            Add Document </button>
                                        <br><br><br>
                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sl</th>
                                                        <th>Title</th>
                                                        <th>Document Type</th>
                                                        <th>Remarks</th>
                                                        <th width="8%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=0; @endphp
                                                    @foreach ($data['document'] as $item)
                                                        @php $i++; @endphp

                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item['title'] }}</td>
                                                            <td>{{ $item['type'] }}</td>
                                                            <td>{{ $item['remarks'] }}</td>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn btn-round btn-danger-rgba deleteData"
                                                                    id="{{ $item['id'] }}" title="Delete"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>

                                                        </tr>
                                                    @endforeach


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->





        </div>
    </div>
    </div>
    <!-- End col -->
    </div>
    <!-- End Contentbar -->
    <!-- Modal -->
    <div class="modal fade" id="varying-modal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>ADD DOCUMENT</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ URL::to('storePatientDocument/') }}" method="post" class="form-horizontal"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="patient_id" value="{{ $data['editVal']['id'] }}">
                    <div class="modal-body">



                        <div class="form-group">
                            <label for="title" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="type" class="col-form-label">Document Type:</label>
                            <input type="text" class="form-control" id="type" name="type">
                        </div>

                        <div class="form-group">
                            <label for="file_name" class="col-form-label">Document File:</label>
                            <input type="file" class="form-control" id="file_name" name="file_name">
                        </div>


                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Remark:</label>
                            <textarea class="form-control" id="message-text" name="remarks"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $(document).on("click", ".deleteData", function() {
                var id = $(this).attr('id');
                var base_url = $('#base_url').val();
                // alert(id);
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "deletePatientDocument",
                            // alert();
                            type: "post",
                            data: {
                                id: id
                            },
                            success: function(data) {
                                swal("Information has been deleted", {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });
            });
        });


        function readURLphoto1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image1').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <!-- Wysiwig js -->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <!-- Summernote JS -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- Code Mirror JS -->
    <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
    <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>

    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>


    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>
    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>



@endsection
