@section('title')
    Access Denied
@endsection
@extends('backend.layouts.main')
@section('style')
    <!-- Apex css -->
    <link href="{{ asset('assets/plugins/apexcharts/apexcharts.css') }}" rel="stylesheet" type="text/css" />
    <!-- Slick css -->
    <link href="{{ asset('assets/plugins/slick/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">

        <!-- Start row -->
        <div class="row">
            <div class="col-lg-12 col-xl-12" style="text-align: center">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="box-header cyan-bg">
                            <i class="fa fa-user-times" aria-hidden="true">&nbsp;&nbsp;</i>NO PERMISSION
                        </div>
                        <hr>
                        <div class="box-body">
                            <div class="no-permission">
                                <img src="{{ URL::to('/') }}/assets/Access-denied.png">
                                <hr>
                                <h2>ACCESS DENIED</h2>
                                <h3>You are not permitted to access this page !</h3>
                                <p><i class="fa fa-times" aria-hidden="true">&nbsp;&nbsp;</i> Access this page is
                                    forbidden.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

        <!-- End row -->
    </div>
    <!-- End Contentbar -->
@endsection
@section('script')
    <!-- Apex js -->
    <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/apexcharts/irregular-data-series.js') }}"></script>
    <!-- Slick js -->
    <script src="{{ asset('assets/plugins/slick/slick.min.js') }}"></script>
    <!-- Custom Dashboard js -->
    <script src="{{ asset('assets/js/custom/custom-dashboard.js') }}"></script>
@endsection
