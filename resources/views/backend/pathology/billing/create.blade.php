@section('title')
    Billing
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/select2-bootstrap-theme/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-wheelchair mr-2"></i>
                                    Bill Details</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <form action="{{ URL::to('storeTestBill/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="panel-body">
                                        <!-- patient details -->

                                        <div class="headers-line mt-md">
                                            <i class="fa fa-user-tag"></i>
                                            <h5> Bill Details</h5>
                                        </div>
                                        <hr>
                                        <div class="row mb-lg">
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo 'Patient'; ?> <span
                                                            class="required">*</span></label>
                                                    <select class='form-control' id='patient_id' name="patient_id" required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['patientlist'] as $cat)

                                                            <option value="{{ $cat['id'] }}">
                                                                {{ $cat['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo 'Referred By'; ?> <span
                                                            class="required">*</span></label>
                                                    <select class="form-control" name="referral_id" id="referral_id"
                                                        required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['referrallist'] as $cat)

                                                            <option value="{{ $cat['id'] }}">
                                                                {{ $cat['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Bill Date <span
                                                            class="required">*</span></label>


                                                    <div class="input-group">
                                                        <input type="text" name="bill_date" value="<?php echo date('Y-m-d'); ?>"
                                                            id="autoclose-date" class="datepicker-here form-control"
                                                            aria-describedby="basic-addon3" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon3"><i
                                                                    class="feather icon-calendar"></i></span>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- test delivery -->

                                        <div class="headers-line mt-md">
                                            <i class="fa fa-file-medical-alt"></i>
                                            <h5> Report Details</h5>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-6 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Delivery Date <span
                                                            class="required">*</span></label>


                                                    <div class="input-group">
                                                        <input type="text" name="delivery_date" value="<?php echo date('Y-m-d'); ?>"
                                                            id="default-date" class="datepicker-here form-control"
                                                            aria-describedby="basic-addon3" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon3"><i
                                                                    class="feather icon-calendar"></i></span>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-6 mt-sm">
                                                <div class="form-group">
                                                    <label class="control-label">Remarks <span
                                                            class="required">*</span></label>
                                                    <textarea class="form-control" rows="2" name="report_remarks"
                                                        required></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-md mb-md">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover" id="tableID">
                                                        <thead>
                                                            <th><?php echo 'Category'; ?> <span class="required">*</span>
                                                            </th>
                                                            <th><?php echo 'Test' . ' ' . 'Name'; ?></th>
                                                            <th><?php echo 'Price'; ?></th>
                                                            <th><?php echo 'Discount'; ?> (%)</th>
                                                            <th><?php echo 'Total' . ' ' . 'Price'; ?></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr id="row_0">
                                                                <td class="min-w-md">
                                                                    <div class="form-group">
                                                                        <select class='form-control' data-plugin-selectTwo
                                                                            onchange='getLabTest(this.value, 0)'
                                                                            id='test_category0' name="items[0][category]"
                                                                            data-width='100%'>
                                                                            @foreach ($data['categorylist'] as $cat)

                                                                                <option value="{{ $cat['id'] }}">
                                                                                    {{ $cat['name'] }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                </td>
                                                                <td class="min-w-md">
                                                                    <div class="form-group">
                                                                        <select name="items[0][lab_test]"
                                                                            class="form-control" data-plugin-selectTwo
                                                                            data-width="100%"
                                                                            onchange="get_labtest_price(this.value, 0)"
                                                                            id="lab_test0">
                                                                            <option value=""><?php echo 'select'; ?></option>
                                                                        </select>
                                                                        <span class="error"></span>
                                                                    </div>
                                                                </td>
                                                                <td class="min-w-sm">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control unit_price"
                                                                            id="unit_price0" name="items[0][unit_price]"
                                                                            readonly value="0.00" />
                                                                    </div>
                                                                </td>
                                                                <td width="320">
                                                                    <div class="form-group">

                                                                        <div class="input-group mb-3">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">%</span>
                                                                            </div>
                                                                            <input type="number" class="form-control"
                                                                                name="items[0][dis_percent]"
                                                                                id="dis_percent0" value="0"
                                                                                onchange="discount_update(0)"
                                                                                onkeyup="discount_update(0)" />
                                                                            <div class="amount-div">
                                                                                <input type="text"
                                                                                    class="form-control items_discount"
                                                                                    name="items[0][dis_amount]"
                                                                                    id="dis_amount0" value="0" readonly />
                                                                            </div>
                                                                        </div>



                                                                    </div>
                                                                </td>
                                                                <td class="min-w-lg">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control total_price"
                                                                            name="items[0][total_price]" value="0.00"
                                                                            id="total_price0" readonly />
                                                                        <input type="hidden" class="cont_gra_total"
                                                                            name="items[0][hidden_total_price]"
                                                                            id="hidden_total_price0" value="0">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="text-align: right">
                                                                <td colspan="5"><button type="button"
                                                                        class="btn btn-default" onclick="addRows()"> <i
                                                                            class="fa fa-plus-circle"></i>
                                                                        <?php echo 'Add Rows'; ?></button></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-offset-6 col-md-6">

                                            </div>

                                            <div class="col-md-offset-6 col-md-6">
                                                <section class="panel panel-custom">
                                                    <header class="panel-heading panel-heading-custom">
                                                        <h4 class="panel-title">Bill Summary</h4>
                                                    </header>
                                                    <div class="panel-body panel-body-custom">
                                                        <table class="table b-less mb-none text-dark">
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="2">Sub Total</td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"></span>
                                                                            <input type="text" class="form-control"
                                                                                name="sub_total_amount"
                                                                                id="sub_total_amount" value="0.00" required
                                                                                readonly />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Tax ( + )</td>
                                                                    <td>

                                                                        <div class="input-group mb-3">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">%</span>
                                                                            </div>
                                                                            <input type="number" class="form-control"
                                                                                name="tax_percent" id="tax_percent"
                                                                                value="0" onkeyup="tax_update()"
                                                                                onchange="tax_update()" />
                                                                            <div class="amount-div">
                                                                                <input type="text" class="form-control"
                                                                                    name="tax_amount" id="tax_amount"
                                                                                    value="0.00" readonly />
                                                                            </div>
                                                                        </div>


                                                                        {{-- <div class="discount-group">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">%</span>
                                                                                <input type="number" class="form-control"
                                                                                    name="tax_percent" id="tax_percent"
                                                                                    value="0" onkeyup="tax_update()"
                                                                                    onchange="tax_update()" />
                                                                            </div>
                                                                            <div class="amount-div">
                                                                                <input type="text" class="form-control"
                                                                                    name="tax_amount" id="tax_amount"
                                                                                    value="0.00" readonly />
                                                                            </div>
                                                                        </div> --}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Discount ( - )</td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"></span>
                                                                            <input type="number" class="form-control"
                                                                                name="total_discount" id="total_discount"
                                                                                value="0.00" readonly />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Net Payable</td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"></span>
                                                                            <input type="text" class="form-control"
                                                                                name="net_amount" id="net_amount"
                                                                                value="0.00" readonly />
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2">Received Amount</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="number" class="form-control"
                                                                                name="payment_amount" id="payment_amount" />
                                                                            <span class="error"></span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Pay Via</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <select class='form-control' name="pay_via"
                                                                                id='pay_via' data-plugin-selectTwo
                                                                                data-width='100%'>
                                                                                <option value="">Select</option>
                                                                                @foreach ($data['payvia_list'] as $cat)

                                                                                    <option value="{{ $cat['id'] }}">
                                                                                        {{ $cat['name'] }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Remarks</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control"
                                                                                name="payment_remarks" placeholder="" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>



                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>


        <!-- End Contentbar -->
    @endsection
    @section('script')

        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
        <script src="{{ asset('assets/select2/js/select2.min.js') }}"></script>


    @endsection



    <script type="text/javascript">
        var count = 1;

        function addRows() {
            var tbody = $('#tableID').children('tbody');
            tbody.append(getDynamicInput(count));
            $(".select2_in").select2({
                theme: "bootstrap",
                width: "100%"
            });
            count++;
        }

        function deleteRow(id) {
            $("#row_" + id).remove();
            grandTotalCalculateBill();
            tax_update();
        }

        function getDynamicInput(value) {
            var html_row = "";
            html_row += '<tr id="row_' + value + '">';
            html_row += '<td><div class="form-group">';
            html_row += '<select id="test_category' + value + '" name="items[' + value +
                '][category]" class="form-control select2_in"  onchange="getLabTest(this.value, ' + value + ')" >';
            html_row += '<option value=""><?php echo 'Select'; ?></option>';
            <?php
              $categorylist = App\Models\lab_test_categorie::all();

                foreach($categorylist as $category):
            ?>
            html_row += '<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>';
            <?php endforeach; ?>
            html_row += '</select>';
            html_row += '<span class="error"></span></div></td>';
            html_row += '<td><div class="form-group">';
            html_row += '<select id="lab_test' + value + '" name="items[' + value +
                '][lab_test]" class="form-control select2_in"  onchange="get_labtest_price(this.value, ' + value + ')">';
            html_row += '<option value=""><?php echo 'Select'; ?></option>';
            html_row += '</select>';
            html_row += '<span class="error"></span></div></td>';
            html_row += '<td><div class="form-group">';
            html_row += '<input type="text" name="items[' + value + '][unit_price]" id="unit_price' + value +
                '" class="form-control items_unit_price" readonly value="0.00" />';
            html_row += '</div></td>';
            html_row += '<td><div class="form-group">';
            html_row +=
                '<div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text">%</span></div>';
            html_row += '<input type="number" name="items[' + value + '][dis_percent]" id="dis_percent' + value +
                '" class="form-control" onchange="discount_update(' + value + ')" onkeyup="discount_update(' + value +
                ')" value="0" />';



            html_row +=
                '<div class="amount-div"><input type="text" class="form-control items_discount" name="items[' +
                value + '][dis_amount]" id="dis_amount' + value + '" value="0" readonly />';
            html_row += '</div></div></td>';
            html_row += '<td>';
            html_row += '<input type="text" class="form-control total_price" name="items[' + value +
                '][total_price]" id="total_price' + value + '" value="0.00" readonly style="float: left; width: 70%;" />';
            html_row += '<input type="hidden" class="cont_gra_total" name="items[' + value +
                '][hidden_total_price]" id="hidden_total_price' + value + '" value="0" />';
            html_row += '<button type="button" class="btn btn-danger" onclick="deleteRow(' + value +
                ')" style="float: right; max-width: 30%"><i class="fa fa-times"></i> </button>';
            html_row += '</td>';
            html_row += '</tr>';
            return html_row;
        }


        function getLabTest(categoryid, rowid) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var test_id = 0;
            $("#lab_test" + rowid).html("<option value=''><?php echo 'Exploring'; ?>...</option>");
            $("#unit_price" + rowid).val(0);
            $("#total_price" + rowid).val(0);
            $("#hidden_total_price" + rowid).val(0);
            $("#dis_percent" + rowid).val(0);
            $("#dis_amount" + rowid).val(0);
            $.ajax({
                type: "POST",
                url: "get_labtest_by_category",
                data: {
                    "selected_id": test_id,
                    "category_id": categoryid
                },
                dataType: "html",
                success: function(data) {
                    $("#lab_test" + rowid).html(data);
                }
            });
        }

        function get_labtest_price(testid, rowid) {
            $("#unit_price" + rowid).val(0);
            $("#total_price" + rowid).val(0);
            $("#hidden_total_price" + rowid).val(0);
            $("#dis_percent" + rowid).val(0);
            $("#dis_amount" + rowid).val(0);
            $.ajax({
                type: "POST",
                url: "get_labtest_price",
                data: {
                    "testid": testid
                },
                dataType: "html",
                success: function(amount) {
                    $("#unit_price" + rowid).val(amount);
                    $("#total_price" + rowid).val(amount);
                    $("#hidden_total_price" + rowid).val(amount);
                    grandTotalCalculateBill();
                    tax_update();
                }
            });
        }

        // test bill grand total calculation
        function grandTotalCalculateBill() {
            var total_amount = 0;
            var discount_amount = 0;
            $(".cont_gra_total").each(function() {
                total_amount += read_number(this.value)
            });
            $(".items_discount").each(function() {
                discount_amount += read_number(this.value)
            });
            var aftergrand_total = read_number(total_amount - discount_amount);
            var tax_amount = read_number($("#tax_amount").val());
            var net_amount = aftergrand_total + tax_amount;
            $("#sub_total_amount").val(total_amount.toFixed(2));
            $("#total_discount").val(discount_amount.toFixed(2));
            $("#net_amount").val(net_amount.toFixed(2));
        }

        // test bill tax calculation
        function tax_update() {
            var sub_total_amount = read_number($('#sub_total_amount').val());
            var total_discount = read_number($('#total_discount').val());
            var tax_percent = read_number($('#tax_percent').val());
            var tax_amount = (tax_percent / 100) * sub_total_amount;
            var net_amount = (sub_total_amount - total_discount) + tax_amount;
            $('#tax_amount').val(tax_amount.toFixed(2));
            $("#net_amount").val(net_amount.toFixed(2));
        }

        // test bill discount calculation
        function discount_update(rowid) {
            var unit_price = read_number($('#unit_price' + rowid).val());
            var discount_percent = read_number($('#dis_percent' + rowid).val());
            var discount_amount = (discount_percent / 100) * unit_price;
            $('#dis_amount' + rowid).val(discount_amount.toFixed(2));
            $('#total_price' + rowid).val(unit_price - discount_amount);
            grandTotalCalculateBill();
        }

        function read_number(inputelm) {
            if (isNaN(inputelm) || inputelm.length == 0) {
                return 0;
            } else {
                return parseFloat(inputelm);
            }
        }
    </script>
