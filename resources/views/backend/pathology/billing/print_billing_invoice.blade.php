@section('title')
    Billing
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('rightbar-content')

    <?php
    use App\Http\Helpers;
    $global_config = App\Models\Global_settings::all();
    $data['global_config'] = $global_config->toArray();
    $global_config = $data['global_config'][0];
    if ($global_config['text_logo'] == '') {
        $img = asset('/assets/images/no_img.png');
    } else {
        $img = asset('/public/assets/images/uploads/settings/' . $global_config['text_logo']);
    }
    
    if (!empty($data['test'])) {
        $test_bill = $data['test'][0];
    } else {
        $test_bill = [];
    }
    
    $status = $test_bill['status'];
    
    $total = $test_bill['total'];
    
    $total_discount = $test_bill['discount'];
    $tax_amount = $test_bill['tax_amount'];
    $paid = $test_bill['paid'];
    $due_amount = $test_bill['due'];
    $net_amount = $test_bill['net_amount'];
    
    ?>
    <style>
        .required {
            color: red;
        }

        .invoice-summary ul.amounts li {
            background: #f5f5f5;
            margin-bottom: 5px;
            padding: 10px;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            font-weight: 300;
            list-style-type: none;
        }

    </style>
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">

                <div class="card m-b-30">
                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Invoice</a>
                            </li>
                            <?php 
                            $userId = Auth::user();
                            $userRole = $userId['role'];
                            if ($userRole != 7) {?>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-money mr-2"></i>Add Payment</a>
                            </li>
                            <?php } ?>

                        </ul>


                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="panel-body" id="div_print">
                                    <div id="invoicePrint" class="print-content">
                                        <div class="invoice">
                                            <header class="clearfix"
                                                style="border: 1px solid rgb(240, 233, 233); padding: 5px;">
                                                <div class="row">
                                                    <div class="col-md-6" style="">
                                                        <div class="ib">
                                                            <img src="{{ $img }}" class="img-fluid"
                                                                alt="logo">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">


                                                        <h4 class="mt-none mb-none text-dark">Bill No #<?php echo $test_bill['bill_no']; ?>
                                                        </h4>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Payment Status </span>
                                                            <span class="value">
                                                                <?php
                                                                $payment_a = [
                                                                    '1' => 'unpaid',
                                                                    '2' => 'partly_paid',
                                                                    '3' => 'total_paid',
                                                                ];
                                                                echo $payment_a[$status];
                                                                ?>
                                                            </span>
                                                        </p>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Referral : </span>
                                                            <?php echo $test_bill['referral_name']; ?>
                                                        </p>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Date : </span> <span
                                                                class="value"><?php echo date('d-m-Y', strtotime($test_bill['date'])); ?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </header><br><br>

                                            <div class="bill-info">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="bill-data">
                                                            <p class="h5 mb-xs text-dark text-weight-semibold">
                                                                Patient:</p>
                                                            <address>
                                                                <?php
                                                                
                                                                echo $test_bill['p_name'] . '<br>';
                                                                echo 'Sex' . ' & ' . 'Age' . ' : ' . $test_bill['p_sex'] . ' / ' . $test_bill['p_age'] . '<br>';
                                                                echo 'Category' . ' : ' . $test_bill['p_category'] . '<br>';
                                                                echo 'Mobile No' . ' : ' . $test_bill['p_mobileno'] . '<br>';
                                                                echo $test_bill['p_address'];
                                                                ?>
                                                            </address>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">
                                                        <div class="bill-data text-right">
                                                            <p class="h5 mb-xs text-dark text-weight-semibold">
                                                                From :</p>
                                                            <address>
                                                                <?php
                                                                echo $global_config['institute_name'] . '<br/>';
                                                                echo $global_config['address'] . '<br/>';
                                                                echo $global_config['mobileno'] . '<br/>';
                                                                echo $global_config['institute_email'] . '<br/>';
                                                                ?>
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="table-responsive">
                                                <table class="table invoice-items table-hover tbr-top mb-none">
                                                    <thead>
                                                        <tr class="text-dark">
                                                            <th id="cell-id" class="text-weight-semibold">#</th>
                                                            <th id="cell-desc" class="text-weight-semibold">
                                                                Test Category</th>
                                                            <th id="cell-item" class="text-weight-semibold">
                                                                Lab Test</th>
                                                            <th id="cell-price" class="text-weight-semibold">
                                                                Price</th>
                                                            <th id="cell-qty" class="text-weight-semibold">
                                                                Discount</th>
                                                            <th id="cell-total" class="text-center text-weight-semibold">
                                                                Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        
                                                            $count = 1;
                                                            foreach ($data['bill_details'] as $row ) {
                                                                $price = $row['price'];
                                                                $discount = $row['discount'];
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                            <td><?php echo $row['test_category']; ?></td>
                                                            <td><?php echo $row['test_name']; ?></td>
                                                            <td><?php echo $price; ?></td>
                                                            <td><?php echo $discount; ?></td>
                                                            <td class="text-center"><?php echo $price - $discount; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="invoice-summary text-right mt-lg">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                    </div>
                                                    <div class="col-lg-5" style="text-align: right;">
                                                        <ul class="amounts">
                                                            <li>Sub Total :
                                                                <?php
                                                                echo $total; ?></li>
                                                            <li>Discount :
                                                                <?php echo $total_discount; ?></li>
                                                            <li> Tax :
                                                                <?php echo $tax_amount; ?></li>
                                                            <?php  if ($status == 3){ ?>
                                                            <li>
                                                                <strong>Net Payable :
                                                                </strong>
                                                                <?php
                                                                echo $net_amount;
                                                                ?>
                                                            </li>
                                                            <?php }else{ ?>
                                                            <li>Net Payable :
                                                                <?php echo $net_amount; ?></li>
                                                            <li>Paid' :
                                                                <?php echo $paid; ?></li>
                                                            <li>
                                                                <strong>Due :
                                                                </strong>
                                                                <?php
                                                                
                                                                echo $due_amount;
                                                                ?>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><br><br>
                                            <div class="row mt-xxlg">
                                                <div class="col-md-6">
                                                    <div class="text-left">
                                                        <?php
                                                        $staff = App\Models\Staffs::where('id', '=', $test_bill['prepared_by'])->get();
                                                        
                                                        echo 'Prepared By' . ' - ' . $staff[0]['name']; ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="text-align: right">
                                                    <div class="auth-signatory">
                                                        Authorised By
                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div><br><br><br>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <button class="btn btn-warning" onClick="printdiv('div_print');"><i
                                                        class="fa fa-print"></i> Print</button>
                                            </div>

                                        </div>
                                    </div>
                                </footer>

                                {{-- </form> --}}

                            </div>


                            <div class="tab-pane fade" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">

                                <?php if($paid > 1 ){ ?>

                                <div class="panel-body" id="div_print2">
                                    <div id="invoicePrint" class="print-content">
                                        <div class="invoice">
                                            <header class="clearfix"
                                                style="border: 1px solid rgb(240, 233, 233); padding: 5px;">
                                                <div class="row">
                                                    <div class="col-md-6" style="">
                                                        <div class="ib">
                                                            <img src="{{ $img }}" class="img-fluid"
                                                                alt="logo">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">


                                                        <h4 class="mt-none mb-none text-dark">Bill No #<?php echo $test_bill['bill_no']; ?>
                                                        </h4>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Payment Status </span>
                                                            <span class="value">
                                                                <?php
                                                                $payment_a = [
                                                                    '1' => 'unpaid',
                                                                    '2' => 'partly_paid',
                                                                    '3' => 'total_paid',
                                                                ];
                                                                echo $payment_a[$status];
                                                                ?>
                                                            </span>
                                                        </p>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Referral : </span>
                                                            <?php echo $test_bill['referral_name']; ?>
                                                        </p>
                                                        <p class="mb-none">
                                                            <span class="text-dark">Date : </span> <span
                                                                class="value"><?php echo date('d-m-Y', strtotime($test_bill['date'])); ?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </header><br><br>

                                            <div class="bill-info">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="bill-data">
                                                            <p class="h5 mb-xs text-dark text-weight-semibold">
                                                                Patient:</p>
                                                            <address>
                                                                <?php
                                                                
                                                                echo $test_bill['p_name'] . '<br>';
                                                                echo 'Sex' . ' & ' . 'Age' . ' : ' . $test_bill['p_sex'] . ' / ' . $test_bill['p_age'] . '<br>';
                                                                echo 'Category' . ' : ' . $test_bill['p_category'] . '<br>';
                                                                echo 'Mobile No' . ' : ' . $test_bill['p_mobileno'] . '<br>';
                                                                echo $test_bill['p_address'];
                                                                ?>
                                                            </address>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">
                                                        <div class="bill-data text-right">
                                                            <p class="h5 mb-xs text-dark text-weight-semibold">
                                                                From :</p>
                                                            <address>
                                                                <?php
                                                                echo $global_config['institute_name'] . '<br/>';
                                                                echo $global_config['address'] . '<br/>';
                                                                echo $global_config['mobileno'] . '<br/>';
                                                                echo $global_config['institute_email'] . '<br/>';
                                                                ?>
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="table-responsive">
                                                <table class="table invoice-items table-hover tbr-top mb-none">
                                                    <thead>
                                                        <tr class="text-dark">
                                                            <th id="cell-id" class="text-weight-semibold">#</th>
                                                            <th id="cell-item" class="text-weight-semibold">
                                                                Collect By</th>
                                                            <th id="cell-price" class="text-weight-semibold">
                                                                Pay Via</th>
                                                            <th id="cell-desc" class="text-weight-semibold">
                                                                Remarks</th>
                                                            <th id="cell-qty" class="text-weight-semibold">
                                                                Paid On</th>
                                                            <th id="cell-total" class="text-center text-weight-semibold">
                                                                Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
									$count = 1;
									foreach($data['paymentHistory'] as $payment) {
									?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                            <td class="text-weight-semibold text-dark"><?php echo $payment['collect_by']; ?>
                                                            </td>
                                                            <td><?php echo $payment['pay_via']; ?></td>
                                                            <td><?php echo $payment['remarks']; ?></td>
                                                            <td><?php echo date('d-m-Y', strtotime($payment['paid_on'])); ?></td>
                                                            <td class="text-center"><?php echo $payment['amount']; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="invoice-summary text-right author">
                                                <div class="row">
                                                    <div class="col-md-7 authorised">
                                                        <div class="auth-signatory-sm">
                                                            Authorised By
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 pull-right">
                                                        <ul class="amounts">
                                                            <li>Sub Total :
                                                                <?php echo $total; ?></li>
                                                            <li>Discount :
                                                                <?php echo $total_discount; ?></li>
                                                            <li>Tax :
                                                                <?php echo $tax_amount; ?></li>
                                                            <li>Net Payable :
                                                                <?php echo $net_amount; ?></li>
                                                            <li>Paid :
                                                                <?php echo $paid; ?></li>
                                                            <li>
                                                                <strong>Due :
                                                                </strong>
                                                                <?php
                                                                
                                                                echo $due_amount;
                                                                ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><br><br>
                                            <div class="row mt-xxlg">
                                                <div class="col-md-6">
                                                    <div class="text-left">
                                                        <?php
                                                        $staff = App\Models\Staffs::where('id', '=', $test_bill['prepared_by'])->get();
                                                        
                                                        echo 'Prepared By' . ' - ' . $staff[0]['name']; ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="text-align: right">
                                                    <div class="auth-signatory">
                                                        Authorised By
                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div><br><br><br>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <button class="btn btn-warning" onClick="printdiv2('div_print2');"><i
                                                        class="fa fa-print"></i> Print</button>
                                            </div>

                                        </div>
                                    </div>
                                </footer>

                                <?php } ?>
                                <?php if($status != 3){ ?>

                                <form action="{{ URL::to('storePayment/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="bill_id" value="{{ $test_bill['id'] }}">


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Paid On <span
                                                class="required">*</span></label>
                                        <div class="col-md-6">

                                            <div class="input-group">
                                                <input type="text" name="paid_date" value="<?php echo date('Y-m-d'); ?>"
                                                    id="autoclose-date" class="datepicker-here form-control"
                                                    aria-describedby="basic-addon3" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon3"><i
                                                            class="feather icon-calendar"></i></span>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Amount <span
                                                class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="payment_amount"
                                                value="<?php echo $due_amount; ?>" required />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Pay Via <span
                                                class="required">*</span></label>
                                        <div class="col-md-6">
                                            <select class='form-control' name="pay_via" id='pay_via' data-plugin-selectTwo
                                                data-width='100%'>
                                                <option value="">
                                                    Select
                                                </option>
                                                @foreach ($data['payvia_list'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Remarks</label>
                                        <div class="col-md-6 mb-md">
                                            <textarea name="remarks" rows="2" class="form-control"
                                                placeholder=""></textarea>
                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>
                                <?php } ?>

                            </div>


                        </div>
                    </div>

                </div>

                <!-- End col -->
            </div>
        </div>
    </div>
    <!-- End Contentbar -->


@endsection
@section('script')

    <script language="javascript">
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }

        function printdiv2(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>

    <!-- Datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

    <!-- Input Mask js -->
    <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
@endsection
