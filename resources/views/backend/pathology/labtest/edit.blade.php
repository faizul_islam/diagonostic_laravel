@section('title')
    Lab Test
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Lab Test List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Add
                                    Lab Test</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade " id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sl</th>
                                                <th>Test Category</th>
                                                <th>Test Name</th>
                                                <th>Test Code</th>
                                                <th>Patient Price</th>
                                                <th>Production Cost</th>
                                                <th>Created By</th>
                                                <th>Date</th>

                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>
                                                    <?php
                                                    $cat = App\Models\Lab_test_categorie::Where('id', '=', $item['category_id'])->get();
                                                    $user = App\Models\Users::Where('id', '=', $item['created_by'])->get();
                                                    if (!empty($cat[0]['name'])) {
                                                        $catName = $cat[0]['name'];
                                                    } else {
                                                        $catName = '';
                                                    }
                                                    
                                                    if (!empty($user[0]['username'])) {
                                                        $userName = $user[0]['username'];
                                                    } else {
                                                        $userName = '';
                                                    }
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $catName }}</td>
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['test_code'] }}</td>
                                                    <td>{{ $item['patient_price'] }}</td>
                                                    <td>{{ $item['production_cost'] }}</td>
                                                    <td>{{ $userName }}</td>
                                                    <td>{{ $item['date'] }}</td>

                                                    <td>
                                                        <form action="{{ URL::to('editLabTest/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade show active" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('LabTestUpdate/' . $data['editVal']['id']) }}" method="POST"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Test Category') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="category_id">
                                                @foreach ($data['category'] as $cat)

                                                    <option value="{{ $cat['id'] }}" @if ($data['editVal']['category_id'] == $cat['id']) selected='selected' @endif>
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Name') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['name'] }}" name="name" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Code') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['test_code'] }}" name="test_code" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Production Cost') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['production_cost'] }}"
                                                name="production_cost" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Patient Price') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['patient_price'] }}" name="patient_price" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Date') }}</label>
                                        <div class="col-md-6">

                                            <input type="text" name="date" id="autoclose-date"
                                                class="datepicker-here form-control"
                                                placeholder="{{ $data['editVal']['date'] }}"
                                                aria-describedby="basic-addon3" />


                                        </div>
                                    </div>

                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Update') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteLabTest",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>


    @endsection
