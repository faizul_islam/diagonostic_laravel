<style>
    #footer {
        height: 50px;
        /* the footer's total height */
    }

    #footer-content {
        background-color: #f3e5f5;
        border: 1px solid #ab47bc;
        height: 32px;
        /* height + top/bottom paddding + top/bottom border must add up to footer height */
        padding: 8px;
    }

</style>
<div style="height:842px;">
    <div class="row" style="background: seashell;">
        <div class="col-sm-4" style="line-height:1px;">

            <h3 style="color: green;">{{ $data['doctorDetails']['name'] }}</h3>
            <p>{{ $data['staff_department']['name'] }}</p>
            <p>{!! $data['doctorDetails']['qualification'] !!}</p>
            <p>{{ $data['doctorDetails']['email'] }}</p>

        </div>
        <div class="col-sm-4" style="text-align: center"></div>
        <div class="col-sm-4" style="text-align: right;">
            <h5 style="color: green;">Chember Info :</h5>
            <p>{{ $data['chemberDetails'][0]['institute_name'] }}</p>
            <p>{{ $data['chemberDetails'][0]['address'] }}</p>
            <p>{{ $data['chemberDetails'][0]['mobileno'] }}</p>
            <p>{{ $data['chemberDetails'][0]['institute_email'] }}</p>
        </div>

    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-sm-6">
            <b> Patient Name:</b> {{ $data['patientDetails']['name'] }}
        </div>

        <div class="col-sm-3">
            <b> Age:</b> {{ $data['patientDetails']['age'] }}
        </div>

        <div class="col-sm-3" style="text-align: right">
            @php
                if ($data['patientDetails']['sex'] == 1) {
                    $gender = 'Male';
                } else {
                    $gender = 'Female';
                }
            @endphp
            <b>Date:</b> {{ date('d-m-Y') }}
        </div>


    </div>
    <hr>


    <div class="row">
        <div class="col-sm-4" style="background: seashell; height: 638px;">

            <h4>Chief Complaints</h4>
            <div style="padding-left: 15px !important;">
                {!! $data['prescriptionDetails']['chief_complain'] !!}
            </div>
            <hr>

            <h4>Diagnosis Tests</h4>
            <div style="padding-left: 15px !important;">
                {!! $data['prescriptionDetails']['diagnosis'] !!}
            </div>
            <hr>

            <h4>Investigation</h4>
            <div style="padding-left: 15px !important;">
                {!! $data['prescriptionDetails']['investigation'] !!}
            </div>



        </div>

        <div class="col-sm-8" style="height: 638px;">
            <div class="rx_header">
                <h1>℞</h1>

            </div>
            <div style="padding-left: 20px !important;">
                <?php foreach ($data['medicineList'] as $med) {?>

                <div class="left_p_header">
                    <h4 class="drug_name">{{ $med['medicine_name'] }}</h4>
                    <div class="second_value">
                        <p>
                            {{ $med['does'] }} ( {{ $med['duration'] }} ) <span>
                                --- {{ $med['instruction'] }}
                            </span>
                        </p>
                    </div>
                </div>
                <?php   }
                ?>
            </div>
            <div style="
 position:absolute;
   bottom:0;
   width:96%;
   padding:5px;
   height:auto;  
   background:#eee;
">



                <span style="font-size:16px;"> Advice: </span>
                <span style="font-size:12px;">
                    {!! $data['prescriptionDetails']['advice'] !!}

                </span>


                <?php if(!empty($data['prescriptionDetails']['next_visit_date'])){?>
                <span style="font-size:12px; float: right;">
                    Next Visit Date: <?php echo date('d-m-Y', strtotime($data['prescriptionDetails']['next_visit_date'])); ?>
                </span>
                <?php } ?>



            </div>


        </div>
    </div>
</div>


<script>
    //------------------- Adding or deleting dynamic row start(Faizul)--------------------------//
</script>
