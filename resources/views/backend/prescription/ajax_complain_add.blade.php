<table class="table table-bordered table-hover" id="tab_complain_added">
    <thead>
        <tr>
            <th width="5%">SL#</th>
            <th width="90%">Advice</th>

            <th width="5%" class="text-center">Action</th>
        </tr>
        <tr>
            <th colspan="2">
                <select name="cheif_complain" id="cheif_complain" class="form-control chosen-select">
                    <option value="">-Select Advice-</option>
                    <option value="napa">Napa</option>
                    <option value="drise">D-rise</option>

                </select>
            </th>



            <th class="text-center pr0"><span
                    class="btn btn-success btn-xs add-complain-item glyphicon glyphicon glyphicon-plus"><i
                        class="fa fa-plus"></i></span>
            </th>
        </tr>
    </thead>
    <tbody class="item-table-complain"></tbody>
</table>


<script>
    //------------------- Adding or deleting dynamic row start(Faizul)--------------------------//

    $(document).on("click", ".add-complain-item", function() {
        var cheif_complain = $('#cheif_complain').val();

        //  alert(cheif_complain);
        var cheif_complainName = $('#cheif_complain option:selected').html();

        var isAdded = false;
        $("#tab_complain_added tr").each(function() {
            check_cheif_complain = $(this).find(".check_cheif_complain").val();
            if (check_cheif_complain == cheif_complain) {
                isAdded = true;
            }
        });
        if (isAdded == false) {
            if (cheif_complain != '') {
                var html = '<tr>';
                html += '<td class="serial-cheif"></td><td>' + cheif_complainName + '</td><td align="center">';
                html += '<input type="hidden" id="cheif_complain" name="cheif_complain[]" value="' +
                    cheif_complain +
                    '" class="check_cheif_complain"/>';

                html +=
                    '<a class="item-delete-cheif text-danger" href="#"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></td></tr>';

                $('.item-table-complain').append(html);
                $('#cheif_complain').val('');

                serialMaintainComplain();
            } else {
                swal('Warning', "Please select a cheif complain.", 'warning');
            }
        } else {
            swal('Warning', "Selected cheif complain already added in your list. Try for another.", 'warning');
        }

    });

    function serialMaintainComplain() {
        var i = 1;
        var subtotal = 0;
        $('.serial-cheif').each(function(key, element) {
            $(element).html(i);
            i++;
        });
    }

    $(document).on("click", ".item-delete-cheif", function(event) {
        var element = $(this).parents('tr');
        // alert(element);
        element.remove();
        event.preventDefault();
        serialMaintainComplain();

    });

    $(document).on("click", ".item-edit", function() {
        var element = $(this).parents('tr');
        var course = element.find('input[name="course[]"]').val();
        var courseCode = element.find('input[name="courseCode[]"]').val();
        var courseCredit = element.find('input[name="courseCredit[]"]').val();
        var batches = element.find('input[name="batches[]"]').val();

        $('#course').val(course);
        $('#courseCode').val(courseCode);
        $('#courseCredit').val(courseCredit);
        $('#batches').val(batches);

        element.remove();
        e.preventDefault();
        serialMaintain();

    });
</script>
