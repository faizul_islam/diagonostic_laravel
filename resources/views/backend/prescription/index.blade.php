@section('title')
    Prescription
@endsection
@extends('backend.layouts.main')

@section('style')
    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />



    <style>
        .pres_control {
            background-color: #ffffff;
            font-size: 15px;
            color: #8A98AC;
            border: 1px solid #ced4da;
            border-radius: 3px;
            width: 50px;
        }

        .pres_drop_control {
            background-color: #ffffff;
            font-size: 15px;
            color: #8A98AC;
            border: 1px solid #ced4da;
            border-radius: 3px;
            height: 29px;
            width: 170px;
        }

        .select2.select2-container.select2-container--default {
            width: 219px !important;
        }

        .select2-container {
            width: 219px !important;
        }

        @font-face {
            font-family: "Kalpurush";
            src: url('{{ storage_path('fonts\Kalpurush.ttf') }}') format('truetype');
        }

        body {
            font-family: "Kalpurush", DejaVu Sans;
        }

        .pline {
            line-height: 0.4;
        }

        @page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

    </style>
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->

    <div class="contentbar">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">

                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <form action="{{ URL::to('PrescriptionUpdate/' . $data['editVal']['id']) }}" method="post"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    @php
                                        
                                        $patient = App\Models\Patient::where('id', '=', $data['editVal']['patient_id'])->get();
                                        $doctor_info = App\Models\Staffs::where('id', '=', $data['editVal']['doctor_id'])->get();
                                        
                                        if (!empty($patient->toArray())) {
                                            $patientName = $patient[0]['name'];
                                            $age = $patient[0]['age'];
                                        
                                            if ($patient[0]['sex'] == 1) {
                                                $gendar = 'Male';
                                            } else {
                                                $gendar = 'Female';
                                            }
                                        
                                            if ($patient[0]['marital_status'] == 1) {
                                                $marital_status = 'Unmarried';
                                            } else {
                                                $marital_status = 'Married';
                                            }
                                        } else {
                                            $patientName = '';
                                            $age = '';
                                        }
                                        if (!empty($doctor_info->toArray())) {
                                            $doctorName = $doctor_info[0]['name'];
                                        } else {
                                            $doctorName = '';
                                        }
                                    @endphp
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr class="table-primary">
                                                        <th width="35%" scope="col">Name</th>
                                                        <th width="10%" scope="col">Age</th>
                                                        <th width="10%" scope="col">Gender</th>
                                                        <th width="15%" scope="col">Marital Status</th>
                                                        <th width="30%" scope="col">Find More</th>
                                                    </tr>
                                                </thead>
                                                <tbody>


                                                    <tr class="table-success">
                                                        <th>{{ $patientName }}</th>
                                                        <td>{{ $age }}</td>
                                                        <td>{{ $gendar }}</td>
                                                        <td>{{ $marital_status }}</td>
                                                        <td>
                                                            <div class="badge-list">
                                                                <a href="#" class="badge badge-primary">Patient Info</a>
                                                                <a href="#" class="badge badge-success">History</a>
                                                                <a href="#" class="badge badge-secondary">Gym & obs</a>
                                                                <a href="#" class="badge badge-info">Child History</a>

                                                            </div>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 mb-sm">
                                                <div class="form-group">
                                                    <label class="control-label"><a href="void:javascript"
                                                            class="badge badge-info">+</a> Chief Complaints

                                                        <select id='chief_complain' name="chief_complain"
                                                            class="pres_drop_control" style=" width: 170px;">
                                                            <option value="" selected>Choose Chief Complaints</option>
                                                            <option value="Abdominal Pain">Abdominal Pain</option>
                                                            <option value="Palpitation">Palpitation</option>
                                                            <option value="Chest pain">Chest pain</option>
                                                            <option value="Weakness">Weakness</option>
                                                        </select>

                                                    </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="chief_complain_val"
                                                            name="chief_complain_val" rows="3"
                                                            placeholder="Enter Your Details."><?php echo str_replace('<br />', ' ', $data['editVal']['chief_complain']); ?></textarea>
                                                    </div>

                                                </div>

                                                {{-- <div class="form-group">
                                                    <label class="control-label"><a href="void:javascript"
                                                            class="badge badge-success">+</a> On examination

                                                        <select id='on_examination' name="on_examination"
                                                            class="pres_drop_control" style=" width: 178px;">
                                                            <option value="" selected>Choose examination</option>
                                                            <option value="X-ray">X-ray</option>
                                                            <option value="Urin">Urin</option>
                                                            <option value="Blood">Blood</option>
                                                            <option value="CT Scan">CT Scan</option>

                                                        </select>

                                                    </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="on_examination_val"
                                                            name="on_examination_val" rows="3"
                                                            placeholder="Enter Your Details."></textarea>
                                                    </div>

                                                </div> --}}

                                                <div class="form-group">
                                                    <label class="control-label"><a href="void:javascript"
                                                            class="badge badge-secondary">+</a>
                                                        Diagnosis
                                                        <select id='diagnosis' name="diagnosis"
                                                            class="form-control diagnosis" style="width:219px;">

                                                        </select>

                                                    </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="diagnosis_val"
                                                            name="diagnosis_val" rows="3"
                                                            placeholder="Enter Your Details."><?php echo str_replace('<br />', ' ', $data['editVal']['diagnosis']); ?></textarea>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label"><a href="void:javascript"
                                                            class="badge badge-danger">+</a> Investigation

                                                        <select id='investigation' name="investigation"
                                                            class="pres_drop_control" style=" width: 199px;">
                                                            <option value="" selected>Choose Investigation</option>
                                                            <option value="Abdominal Pain">Abdominal Pain</option>
                                                            <option value="Palpitation">Palpitation</option>
                                                            <option value="Chest pain">Chest pain</option>
                                                            <option value="Weakness">Weakness</option>

                                                        </select>
                                                    </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="investigation_val"
                                                            name="investigation_val" rows="3"
                                                            placeholder="Enter Your Details."><?php echo str_replace('<br />', ' ', $data['editVal']['investigation']); ?></textarea>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-8 mb-sm">
                                                Pulse: <input type="text" name="pulse" id="pulse" class="pres_control"
                                                    value="{{ $data['editVal']['pulse'] }}"> /mim
                                                &nbsp; &nbsp; &nbsp; BP: <input type="text" class="pres_control"
                                                    name="bp_high" value="{{ $data['editVal']['bp_high'] }}" id="bp_high">
                                                /
                                                <input type="text" class="pres_control" name="bp_low" id="bp_low"
                                                    value="{{ $data['editVal']['bp_low'] }}"> mmHg
                                                &nbsp; &nbsp; &nbsp; Temperature: <input type="text" name="temperature"
                                                    id="temperature" class="pres_control"
                                                    value="{{ $data['editVal']['temperature'] }}"> °F
                                                <hr>

                                                <table class="table table-bordered table-hover" id="tab_course_added">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">SL#</th>
                                                            <th width="35%">Brand Name</th>
                                                            <th width="15%">Does</th>
                                                            <th width="15%">Instruction</th>
                                                            <th width="15%">Duration</th>
                                                            <th width="5%" class="text-center">Action</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2">

                                                                <select class="itemName form-control" id="brand"
                                                                    name="brand"> </select>
                                                            </th>

                                                            <th>
                                                                <select name="does" id="does" class="form-control input-xs">
                                                                    <option value="">-Select Does-</option>
                                                                    <option value="1-1-1">1-1-1</option>
                                                                    <option value="1-0-1">1-0-1</option>
                                                                    <option value="0-1-0">0-1-0</option>
                                                                    <option value="0-0-1">0-0-1</option>
                                                                    <option value="1-0-0">1-0-0</option>
                                                                    <option value="0-1-0">0-1-0</option>
                                                                    <option value="0-1-1">0-1-1</option>
                                                                    <option value="1-1-0">1-1-0</option>
                                                                </select>
                                                            </th>

                                                            <th>
                                                                <select name="duration" id="duration"
                                                                    class="form-control input-xs">
                                                                    <option value="">-Select Instruction-</option>
                                                                    <option value="After Meal">After Meal </option>
                                                                    <option value="Before Meal">Before Meal </option>
                                                                </select>
                                                            </th>

                                                            <th>

                                                                <div class="input-group">

                                                                    <input type="text" aria-label="Select duration"
                                                                        class="form-control" name="instruction_d"
                                                                        id="instruction_d">
                                                                    <select name="instruction" id="instruction"
                                                                        class="form-control input-xs">
                                                                        <option value="">-Select Duration-</option>
                                                                        <option value=" Month Continue">Month Continue
                                                                        </option>
                                                                        <option value=" Week Continue">Week Continue
                                                                        </option>
                                                                        <option value=" Day Continue">Day Continue
                                                                        </option>
                                                                        <option value=" Week Continue">Week Continue
                                                                        </option>
                                                                        <option value=" Month Continue">Month
                                                                            Continue
                                                                        </option>
                                                                    </select>
                                                                </div>


                                                            </th>


                                                            <th class="text-center pr0"><span
                                                                    class="btn btn-success btn-xs add-item glyphicon glyphicon glyphicon-plus"><i
                                                                        class="fa fa-plus"></i></span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="item-table">
                                                        <?php if(!empty($data['prescribeList'])){
                                                            $i=1;
                                                            foreach ($data['prescribeList'] as $value) {
                                                                
                                                           
                                                            ?>

                                                        <tr>
                                                            <td class="serial">{{ $i++ }}</td>
                                                            <td>{{ $value['medicine_name'] }}</td>
                                                            <td>{{ $value['does'] }}</td>
                                                            <td>{{ $value['duration'] }}</td>
                                                            <td>{{ $value['d_instruction'] }}</td>
                                                            <td>{{ $value['instruction'] }}</td>
                                                            <td>
                                                                <input type="hidden" id="brand" name="brand[]"
                                                                    value="{{ $value['medicine_name'] }}"
                                                                    class="brand_check" />
                                                                <input type="hidden" name="does[]"
                                                                    value="{{ $value['does'] }}" />
                                                                <input type="hidden" name="duration[]"
                                                                    value="{{ $value['duration'] }}" />
                                                                <input type="hidden" name="d_instruction[]"
                                                                    value="{{ $value['instruction'] }}" />
                                                                <input type="hidden" name="instruction[]"
                                                                    value="{{ $value['instruction'] }}" />
                                                                <a class="item-delete text-danger" href="#"><i
                                                                        class="ace-icon fa fa-trash-o bigger-130"></i></a>
                                                            </td>
                                                        </tr>



                                                        <?php  } }?>
                                                    </tbody>
                                                </table>

                                                <div class="form-group">
                                                    <label class="control-label">Advice </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="advice_val" name="advice_val"
                                                            rows="3"
                                                            placeholder="Enter Your Details."><?php echo str_replace('<br />', ' ', $data['editVal']['advice']); ?></textarea>
                                                    </div><br>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-5">

                                                            <div class="badge badge-primary">+ Advice

                                                                <select id='advice' name="advice" class="pres_drop_control"
                                                                    style=" width: 199px;">
                                                                    <option value="" selected>Choose Investigation
                                                                    </option>
                                                                    <option value="দুই দিন পর আসবেন">দুই দিন পর আসবেন
                                                                    </option>
                                                                    <option value="ভারী কাজ করবেন না ">ভারী কাজ করবেন না
                                                                    </option>
                                                                    <option value="Chest pain">Chest pain</option>
                                                                    <option value="Weakness">Weakness</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">

                                                            <div class="badge badge-success">+ Handout

                                                                <select id='handout' name="handout"
                                                                    class="pres_drop_control" style=" width: 199px;">
                                                                    <option value="" selected>Choose Investigation
                                                                    </option>
                                                                    <option value="Abdominal Pain">Abdominal Pain
                                                                    </option>
                                                                    <option value="Palpitation">Palpitation</option>
                                                                    <option value="Chest pain">Chest pain</option>
                                                                    <option value="Weakness">Weakness</option>

                                                                </select>
                                                            </div>
                                                        </div>




                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-5">

                                                            <div class="badge badge-secondary">
                                                                <div class="input-group">
                                                                    <span style="line-height: 34px;">Next Visit Date:
                                                                    </span>
                                                                    <input type="text" name="next_visit_date"
                                                                        id="autoclose-date"
                                                                        class="datepicker-here form-control"
                                                                        placeholder="dd/mm/yyyy"
                                                                        aria-describedby="basic-addon3"
                                                                        style="width: 100px;" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text" id="basic-addon3"><i
                                                                                class="feather icon-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">

                                                            <div class="badge badge-secondary">+ immunization

                                                                <select id='immunization' name="immunization"
                                                                    class="pres_drop_control" style=" width: 199px;">
                                                                    <option value="" selected>Choose Investigation
                                                                    </option>
                                                                    <option value="Abdominal Pain">Abdominal Pain
                                                                    </option>
                                                                    <option value="Palpitation">Palpitation</option>
                                                                    <option value="Chest pain">Chest pain</option>
                                                                    <option value="Weakness">Weakness</option>

                                                                </select>
                                                            </div>
                                                        </div>



                                                        {{-- <a href="#" class="badge badge-primary" id="openModal">Advice</a>
                                                        <a href="#" class="badge badge-success">Handout</a>
                                                        <a href="#" class="badge badge-secondary">immunization</a> --}}
                                                        {{-- <br> --}}
                                                        {{-- <div class="col-md-6 mb-sm pull-right">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span style="margin-top: 7px;">Next Visit Date: </span>
                                                                    <input type="text" name="birthday" id="autoclose-date"
                                                                        class="datepicker-here form-control"
                                                                        placeholder="dd/mm/yyyy"
                                                                        aria-describedby="basic-addon3" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text" id="basic-addon3"><i
                                                                                class="feather icon-calendar"></i></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div> --}}


                                                    </div>

                                                    <hr>
                                                    <footer class="panel-footer mt-md" style="text-align: center">
                                                        <a href="{{ URL::to('todayQueue/') }}">
                                                            <button type="button" class="btn btn-danger"><i
                                                                    class="feather icon-list mr-2"></i> Go Queue
                                                                List</button>
                                                        </a>
                                                        <button type="submit" class="btn btn-primary"><i
                                                                class="feather icon-send mr-2"></i> Save</button>
                                                        <?php if(!empty($data['prescribeList'])){
                                                          
                                                                
                                                           
                                                            ?>
                                                        <a href="{{ URL::to('prescriptionPdf') . '/' . $data['editVal']['id'] }}"
                                                            target="_blank">

                                                            <button type="button" class="btn btn-warning">Preview &
                                                                Print</button>
                                                        </a>
                                                        {{-- <button type="button" class="btn btn-warning"
                                                                id="openPrintModal"
                                                                data-ids="{{ $data['editVal']['id'] }}"><i
                                                                    class="feather icon-upload mr-2"></i>
                                                                Print
                                                                Preview</button> --}}
                                                        <?php }?>



                                                    </footer>


                                                </div>

                                            </div>





                                        </div>



                                </form>



                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
        <!-- Modal -->
        <div class="modal fade" id="print_modal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
            aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="backgroud-color:#83a4c6;">
                        <h5>Prescription Print Preview </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {{-- <form action="{{ URL::to('UpdateStatus/') }}" method="post" class="form-horizontal"
                        enctype="multipart/form-data"> --}}
                    <div class="modal-body" id="printModalBody">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                        <a href="{{ URL::to('prescriptionPdf?id=') . $data['editVal']['id'] }}" target="_blank">
                            <button type="submit" class="btn btn-warning">Print</button>
                        </a>

                    </div>
                    {{-- </form> --}}
                </div>
            </div>
        </div>


    </div>







@endsection
@section('script')
    <script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $('.itemName').select2({
            placeholder: 'Select an item',
            minimumInputLength: 2,
            ajax: {
                url: 'get_medicine_name',
                dataType: 'json',
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.full_name,
                                id: item.full_name
                            }
                        })
                    };
                },
            }
        });
    </script>
    <script type="text/javascript">
        axios.post("{{ route('writePrescription') }}", {
            type: "getDiagnosis"
        }).then(res => {
            var xdata = []
            res.data.forEach(element => {
                xdata.push({
                    text: element.Name,
                    id: element.Name
                })
            });
            $('.diagnosis').select2({
                data: xdata
            })
        })
    </script>


    <script>
        $('#diagnosis').on('change', function() {
            document.getElementById("diagnosis_val").value += this.value + '\n';
        })

        $('#chief_complain').on('change', function() {
            document.getElementById("chief_complain_val").value += this.value + '\n';
        })

        $('#investigation').on('change', function() {
            document.getElementById("investigation_val").value += this.value + '\n';
        })

        $('#advice').on('change', function() {
            document.getElementById("advice_val").value += this.value + '\n';
        })



        // var sel = document.getElementById('chief_complain');
        // sel.onchange = function() {
        //     var val = this.options[this.selectedIndex].value + '\n';
        //     var che = document.getElementById('chief_complain_val').value;
        //     che = val;
        //     $('#chief_complain_val').append(che);

        // }

        // var sel = document.getElementById('on_examination');
        // sel.onchange = function() {
        //     var val = this.options[this.selectedIndex].value + '\n';
        //     var che = document.getElementById('on_examination_val').value;
        //     che = val;
        //     $('#on_examination_val').append(che);

        // }


        // var sel = document.getElementById('investigation');
        // sel.onchange = function() {
        //     var val = this.options[this.selectedIndex].value + '\n';
        //     var che = document.getElementById('investigation_val').value;
        //     che = val;
        //     $('#investigation_val').append(che);

        // }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on("click", "#openPrintModal", function() {

            var prescriptionId = $(this).attr('data-ids');
            var base_url = $('#base_url').val();
            $.ajax({
                url: 'ajax_print_preview_prescription',
                type: 'POST',
                data: {
                    prescriptionId: prescriptionId
                },
                success: function(data) {

                    $('#print_modal').modal('show');
                    $('#printModalBody').html(data);
                }
            });
        });

        $(document).on("click", "#username", function() {

            var mobile_no = $('#mobile_no').val();
            var email = $('#email').val();


            if (email == "") {
                swal('Warning', "Please write first email", 'warning');
                return false;
            }


            if (email != '') {
                $("#username").val(email);
                $("#username").attr("readonly", true);

            } else {
                // swal("Required!", "You have to set mobile no first", "warning");
                swal("Required!", "You have to set email first", "warning");
            }
        });



        //------------------- Adding or deleting dynamic row start(Faizul)--------------------------//

        $(document).on("click", ".add-item", function() {
            var brand = $('#brand').val();
            var does = $('#does').val();
            var duration = $('#duration').val();
            var instruction = $('#instruction').val();
            var instruction_d = $('#instruction_d').val();


            var brandName = $('#brand').val();
            // var brandName = $('#brand option:selected').html();
            var doesName = $('#does option:selected').html();
            var durationName = $('#duration option:selected').html();
            var instructionName = $('#instruction option:selected').html();
            var brand_check = "";
            var isAdded = false;
            $("#tab_course_added tr").each(function() {
                brand_check = $(this).find(".brand_check").val();
                if (brand_check == brand) {
                    isAdded = true;
                }
            });
            if (isAdded == false) {
                if (brand != '' && does != '') {
                    var html = '<tr>';
                    html += '<td class="serial"></td><td>' + brandName + '</td><td>' + doesName + '</td><td>' +
                        durationName + '</td><td>' + instruction_d + ' ' +
                        instructionName + '</td><td align="center">';
                    html += '<input type="hidden" id="brand" name="brand[]" value="' + brand +
                        '" class="brand_check"/>';
                    html += '<input type="hidden" name="does[]" value="' + does +
                        '"/>';
                    html += '<input type="hidden" name="duration[]" value="' + duration + '" />';
                    html += '<input type="hidden" name="instruction[]" value="' + instruction_d + ' ' +
                        instruction +
                        '" />';
                    html +=
                        '<a class="item-delete text-danger" href="#"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></td></tr>';

                    $('.item-table').append(html);
                    $('#brand').val('');
                    $('#does').val('');
                    $('#duration').val('');
                    $('#instruction').val('');
                    serialMaintain();
                } else {
                    swal('Warning', "Please select a brand & does. Both are required.", 'warning');
                }
            } else {
                swal('Warning', "Selected brand already added in your list. Try for another.", 'warning');
            }

        });

        function serialMaintain() {
            var i = 1;
            var subtotal = 0;
            $('.serial').each(function(key, element) {
                $(element).html(i);
                i++;
            });
        }

        $(document).on("click", ".item-delete", function(event) {
            var element = $(this).parents('tr');
            // alert(element);
            element.remove();
            event.preventDefault();
            serialMaintain();

        });

        $(document).on("click", ".item-edit", function() {
            var element = $(this).parents('tr');
            var course = element.find('input[name="course[]"]').val();
            var courseCode = element.find('input[name="courseCode[]"]').val();
            var courseCredit = element.find('input[name="courseCredit[]"]').val();
            var batches = element.find('input[name="batches[]"]').val();

            $('#course').val(course);
            $('#courseCode').val(courseCode);
            $('#courseCredit').val(courseCredit);
            $('#batches').val(batches);

            element.remove();
            e.preventDefault();
            serialMaintain();

        });
    </script>

    <!-- Datepicker JS -->
    <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

    <!-- Sweet-Alert js -->
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>
    <!-- Model js -->
    <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>

    <!-- Select2 js -->
    {{-- <script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script> --}}

    {{-- <script src="{{ asset('assets/js/custom/custom-form-select.js') }}"></script> --}}

@endsection
