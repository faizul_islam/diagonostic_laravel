      <style>
          .pres_control {
              background-color: #ffffff;
              font-size: 15px;
              color: #8A98AC;
              border: 1px solid #ced4da;
              border-radius: 3px;
              width: 100px;
          }

      </style>
      @csrf

      <label for="title" class="col-form-label pull-right">
          Patient Type : @if ($data['appointment'][0]['patient_type'] == 1)
              <span style="color: green;">(New)</span>
          @elseif($data['appointment'][0]['patient_type'] == 2)
              <span style="color: red;"> (Old)</span>
          @else
              <span style="color: #ffeb3b;"> (not found)</span>
          @endif
      </label>

      <input type="hidden" class="form-control" name="id" value="{{ $data['appointment'][0]['id'] }}" readonly>

      <div class="form-group">
          <label for="title" class="col-form-label">Pulse :</label>
          <input type="text" class="form-control" id="pulse" name="pulse">
      </div>



      <div class="form-group">
          <label for="title" class="col-form-label"> Temperature :</label>
          <input type="text" class="form-control" id="temperature" name="temperature">
      </div>

      <div class="form-group">
          <label for="title" class="col-form-label"> BP :</label>
          <input type="text" id="bp_high" name="bp_high" class="pres_control" placeholder="High"> / <input type="text"
              id="bp_high" name="bp_low" class="pres_control" placeholder="Low">
      </div>
