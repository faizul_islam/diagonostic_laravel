    <?php
    use App\Http\Helpers;
    $global_config = App\Models\Global_settings::all();
    $data['global_config'] = $global_config->toArray();
    $global_config = $data['global_config'][0];
    if ($global_config['text_logo'] == '') {
        $img = asset('/assets/images/no_img.png');
    } else {
        $img = asset('/public/assets/images/uploads/settings/' . $global_config['text_logo']);
    }
    ?>
    <div class="leftbar">
        <!-- Start Sidebar -->
        <div class="sidebar">
            <!-- Start Logobar -->
            <div class="logobar">
                <a href="{{ url('/') }}" class="logo logo-large"><img src="{{ $img }}"
                        class="img-fluid" alt="logo"></a>
                <a href="{{ url('/') }}" class="logo logo-small"><img src="{{ $img }}"
                        class="img-fluid" alt="logo"></a>
            </div>
            <!-- End Logobar -->
            <!-- Start Navigationbar -->
            <div class="navigationbar">
                <ul class="vertical-menu">
                    <li>
                        <a href="{{ url('/home') }}">
                            <img src="{{ url('/') }}/assets/images/svg-icon/dashboard.svg"
                                class="img-fluid"><span>
                                {{ __('lang.dashboard') }}</span>
                        </a>
                    </li>

                    <?php
                    if (Helpers::get_permission('frontend_setting', 'is_view') ||
                        Helpers::get_permission('frontend_menu', 'is_view') ||
                        Helpers::get_permission('frontend_section', 'is_view') ||
                        Helpers::get_permission('manage_page', 'is_view') ||
                        Helpers::get_permission('frontend_slider', 'is_view') ||
                        Helpers::get_permission('frontend_features', 'is_view') ||
                        Helpers::get_permission('frontend_testimonial', 'is_view') ||
                        Helpers::get_permission('frontend_services', 'is_view') ||
                        Helpers::get_permission('frontend_faq', 'is_view')) {
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/maps.svg" class="img-fluid"
                                alt="dashboard"><span>{{ __('lang.frontend') }}</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">

                            <?php if( Helpers::get_permission('frontend_setting', 'is_view')){ ?>
                            <li><a href="{{ url('/webSetting') }}">Setting</a></li>
                            <?php } if( Helpers::get_permission('frontend_menu', 'is_view')){ ?>
                            <li><a href="{{ url('/webMenu') }}">Menu</a></li>
                            <?php } if( Helpers::get_permission('frontend_section', 'is_view')){ ?>
                            {{-- <li><a href="{{ url('/') }}">Page Section</a></li> --}}
                            <?php } if( Helpers::get_permission('manage_page', 'is_view')){ ?>
                            <li><a href="{{ url('/ManagePage') }}">Manage Page</a></li>
                            <?php } if( Helpers::get_permission('frontend_slider', 'is_view')){ ?>
                            <li><a href="{{ url('/SliderInfo') }}">Slider</a></li>
                            <?php } if( Helpers::get_permission('frontend_features', 'is_view')){ ?>
                            <li><a href="{{ url('/Feature') }}">Features</a></li>
                            <?php } if( Helpers::get_permission('frontend_testimonial', 'is_view')){ ?>
                            <li><a href="{{ url('/Testimonial') }}">Testimonial</a></li>
                            <?php } if( Helpers::get_permission('frontend_services', 'is_view')){ ?>
                            <li><a href="{{ url('/Service') }}">Service</a></li>
                            <?php } if( Helpers::get_permission('frontend_faq', 'is_view')){ ?>
                            <li><a href="{{ url('/Faq') }}">Faq</a></li>
                            <?php } ?>

                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('patient', 'is_add') ||
                        Helpers::get_permission('patient', 'is_view') ||
                        Helpers::get_permission('patient_category', 'is_add') ||
                        Helpers::get_permission('patient_category', 'is_view') ||
                        Helpers::get_permission('patient_disable_authentication', 'is_view')) 
                        {
                    
                    ?>
                    <li>
                        <a href="javaScript:void();">
                            <i class="fa fa-wheelchair"></i>
                            <span>Patient
                                Details</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php  if( Helpers::get_permission('patient', 'is_add')){ ?>
                            <li><a href="{{ url('/PatientForm') }}">Create Patient</a></li>
                            <?php } if( Helpers::get_permission('patient', 'is_view')){ ?>
                            <li><a href="{{ url('/PatientList') }}">Patient List</a></li>
                            <?php } if( Helpers::get_permission('patient_category', 'is_add')){ ?>
                            <li><a href="{{ url('/Category') }}">Category</a></li>
                            <?php } if( Helpers::get_permission('patient_disable_authentication', 'is_view')){ ?>
                            <li><a href="{{ url('/DeactiveList') }}">Login Deactive</a></li>
                            <?php } ?>

                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('chemical', 'is_view') ||
                        Helpers::get_permission('chemical_category', 'is_view') ||
                        Helpers::get_permission('chemical_supplier', 'is_view') ||
                        Helpers::get_permission('chemical_unit', 'is_view') ||
                        Helpers::get_permission('chemical_purchase', 'is_view') ||
                        Helpers::get_permission('chemical_stock', 'is_view') ||
                        Helpers::get_permission('reagent_assigned', 'is_view') ||
                        Helpers::get_permission('inventory_report', 'is_view')) {
                    
                    ?>
                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/ecommerce.svg" class="img-fluid"
                                alt="Inventory"><span>Inventory</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('chemical', 'is_view')){ ?>
                            <li><a href="{{ url('/Chemical') }}">Chemical</a></li>
                            <?php } if( Helpers::get_permission('chemical_category', 'is_view')){ ?>
                            <li><a href="{{ url('/InvCategory') }}">Category</a></li>
                            <?php } if( Helpers::get_permission('chemical_supplier', 'is_view')){ ?>
                            <li><a href="{{ url('/InvSupplier') }}">Supplier</a></li>
                            <?php } if( Helpers::get_permission('chemical_unit', 'is_view')){ ?>
                            <li><a href="{{ url('/InvUnit') }}">Unit</a></li>
                            <?php } if( Helpers::get_permission('chemical_purchase', 'is_view')){ ?>
                            <li><a href="{{ url('/Purchase') }}">Purchase</a></li>
                            <?php } if( Helpers::get_permission('chemical_stock', 'is_view')){ ?>
                            <li><a href="{{ url('/ChemicalStock') }}">Stock</a></li>
                            <?php } if( Helpers::get_permission('reagent_assigned', 'is_view')){ ?>
                            <li><a href="{{ url('/ReagentAssigned') }}">Reagent Assigned</a></li>
                            <?php } ?>


                            <li>
                                <a href="javaScript:void();">Report<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <?php if( Helpers::get_permission('inventory_report', 'is_view')){ ?>
                                    <li><a href="{{ url('/stockReport') }}">Stock Report</a></li>
                                    <?php } if( Helpers::get_permission('inventory_report', 'is_view')){ ?>
                                    <li><a href="{{ url('/purchaseReport') }}">Purchase Report</a></li>
                                    <?php } if( Helpers::get_permission('inventory_report', 'is_view')){ ?>
                                    <li><a href="{{ url('/paymentReport') }}">Payment Report</a></li>
                                    <?php }  ?>
                                </ul>
                            </li>

                        </ul>
                    </li>

                    <?php } 
                     if (Helpers::get_permission('schedule', 'is_view')) {
                    
                     ?>
                    <li>
                        <a href="{{ url('/Schedule') }}">
                            <img src="{{ url('/') }}/assets/images/svg-icon/tables.svg" class="img-fluid"
                                alt="Schedule"><span>Schedule</span>
                        </a>
                    </li>
                    <?php  } 
                    $userInfo = Auth::user();
                                                 
                    if($userInfo['role']==7){
                     ?>
                    <li>
                        <a href="{{ url('/myAppointment') }}">
                            <img src="{{ url('/') }}/assets/images/svg-icon/pages.svg" class="img-fluid"
                                alt="Schedule"><span>My Appointment</span>
                        </a>
                    </li>
                    <?php  } 
                     if (Helpers::get_permission('appointment', 'is_add') ||
                         Helpers::get_permission('appointment', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/form_elements.svg"
                                class="img-fluid" alt="Appointment"><span>Appointment</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            {{-- <li><a href="{{ url('/Appointment') }}">Appointment List</a></li> --}}
                            <?php if( Helpers::get_permission('appointment', 'is_view')){ ?>
                            <li><a href="{{ url('/Appointment') }}"> Appointment </a></li>
                            <?php } if( Helpers::get_permission('appointment_request', 'is_view')){ ?>
                            <li><a href="{{ url('/requested_list') }}">Request List</a></li>
                            <?php } if( Helpers::get_permission('appointment', 'is_view')){ ?>
                            <li><a href="{{ url('/consultation_report') }}">Consultation Report</a></li>
                            <?php } ?>

                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('employee', 'is_view') ||
                        Helpers::get_permission('employee', 'is_add') ||
                        Helpers::get_permission('designation', 'is_view') ||
                        Helpers::get_permission('designation', 'is_add') ||
                        Helpers::get_permission('department', 'is_view') ||
                       
                        Helpers::get_permission('employee_disable_authentication', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/user.svg" class="img-fluid"
                                alt="user"><span>Employee</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('employee', 'is_view')){ ?>
                            <li><a href="{{ url('/EmployeeList') }}">Employee List</a></li>
                            <?php } if( Helpers::get_permission('department', 'is_view')){ ?>
                            <li><a href="{{ url('/Department') }}">Add Department</a></li>
                            <?php } if( Helpers::get_permission('designation', 'is_view')){ ?>
                            <li><a href="{{ url('/Designation') }}">Add Designation</a></li>
                            <?php } if( Helpers::get_permission('employee', 'is_add')){ ?>
                            <li><a href="{{ url('/EmployeeForm') }}">Add Employee</a></li>
                            <?php } if( Helpers::get_permission('employee_disable_authentication', 'is_view')){ ?>
                            <li><a href="{{ url('/') }}">Login Deactive</a></li>
                            <?php }  ?>
                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('salary_template', 'is_view') ||
                        Helpers::get_permission('salary_payment', 'is_view') ||
                        Helpers::get_permission('salary_assign', 'is_view') ||
                        Helpers::get_permission('salary_summary_report', 'is_view') ||
                        Helpers::get_permission('leave_category', 'is_view') ||
                        Helpers::get_permission('leave_category', 'is_add') ||
                        Helpers::get_permission('my_leave', 'is_view') ||
                        Helpers::get_permission('leave_manage', 'is_view') ||
                        Helpers::get_permission('staff_attendance', 'is_view') ||
                        Helpers::get_permission('staff_attendance', 'is_add')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/widgets.svg" class="img-fluid"
                                alt="icons"><span>Human
                                Resource</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">

                            <?php 
                     if (Helpers::get_permission('salary_template', 'is_view') ||
                        Helpers::get_permission('salary_payment', 'is_view') ||
                        Helpers::get_permission('salary_assign', 'is_view') ||
                        Helpers::get_permission('salary_summary_report', 'is_view')) {
                    
                    ?>

                            <li>
                                <a href="javaScript:void();">Payroll<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <?php if( Helpers::get_permission('salary_template', 'is_view')){ ?>
                                    <li><a href="{{ url('/SalaryTemplates') }}">Salary Template</a></li>
                                    <?php } if( Helpers::get_permission('salary_assign', 'is_view')){ ?>
                                    <li><a href="{{ url('/salary_assignReport') }}">Salary Assign</a></li>
                                    <?php } if( Helpers::get_permission('salary_payment', 'is_view')){ ?>
                                    <li><a href="{{ url('/salary_paymentReport') }}">Salary Payment</a></li>
                                    <?php } if( Helpers::get_permission('salary_summary_report', 'is_view')){ ?>
                                    <li><a href="{{ url('/salary_summaryReport') }}">Salary Summary</a></li>
                                    <?php }  ?>

                                </ul>
                            </li>

                            <?php } 
                     if (Helpers::get_permission('leave_category', 'is_view') ||
                        Helpers::get_permission('leave_category', 'is_add') ||
                        Helpers::get_permission('my_leave', 'is_view') ||
                        Helpers::get_permission('leave_manage', 'is_view')) {
                    
                    ?>

                            <li>
                                <a href="javaScript:void();">Leave<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <?php if( Helpers::get_permission('leave_category', 'is_view')){ ?>
                                    <li><a href="{{ url('/leaveCat') }}">Category</a></li>
                                    <?php } if( Helpers::get_permission('my_leave', 'is_view')){ ?>
                                    <li><a href="{{ url('/MyLeave') }}">My Leave</a></li>
                                    <?php } if( Helpers::get_permission('leave_manage', 'is_view')){ ?>
                                    <li><a href="{{ url('/LeaveManage') }}">Leave Manage</a></li>
                                    <?php } ?>
                                </ul>
                            </li>

                            <?php } 
                     if (Helpers::get_permission('staff_attendance', 'is_view') ||
               
                        Helpers::get_permission('staff_attendance', 'is_add')) {
                    
                    ?>

                            <li>
                                <a href="javaScript:void();">Attendance<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <?php if( Helpers::get_permission('staff_attendance', 'is_add')){ ?>
                                    <li><a href="{{ url('/AttendanceSet') }}">Set</a></li>
                                    <?php } if( Helpers::get_permission('staff_attendance', 'is_view')){ ?>
                                    <li><a href="{{ url('/attandanceReport') }}"> Report</a></li>
                                    <?php } ?>

                                </ul>
                            </li>
                            <?php } ?>

                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('lab_test', 'is_view') ||
  
                        Helpers::get_permission('test_category', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/icons.svg" class="img-fluid"
                                alt="tables"><span>Pathology</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('lab_test', 'is_view')){ ?>
                            <li><a href="{{ url('/LabTest') }}">Lab Test</a></li>
                            <?php } if( Helpers::get_permission('test_category', 'is_view')){ ?>
                            <li><a href="{{ url('/PathologyCategory') }}">Category</a></li>
                            <?php } ?>

                        </ul>
                    </li>
                    <?php } 
                     if (Helpers::get_permission('referral_assign', 'is_view') ||
                        Helpers::get_permission('referral_assign', 'is_view') ||
                        Helpers::get_permission('commission_withdrawal', 'is_view') ||
                        Helpers::get_permission('my_commission', 'is_view') ||
               
                        Helpers::get_permission('referral_reports', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <i class="fa fa-handshake-o"></i> <span>Refer
                                Manager</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php  
                    $userInfo = Auth::user();
                                                 
                    if($userInfo['role']==3){
                     ?>
                            <li> <a href="{{ url('/myCommission') }}">My Commission</a> </li>
                            <?php  }  
                    if( Helpers::get_permission('referral_assign', 'is_add')){ ?>
                            <li><a href="{{ url('/set_referReport') }}">Set Referral</a></li>
                            <?php } if( Helpers::get_permission('referral_assign', 'is_view')){ ?>
                            <li><a href="{{ url('/ReferList') }}">Referral List</a></li>
                            <?php } if( Helpers::get_permission('commission_withdrawal', 'is_view')){ ?>
                            <li><a href="{{ url('/Withdrawal') }}">Withdrawal</a></li>
                            <?php }  ?>

                            <?php if( Helpers::get_permission('referral_reports', 'is_view')){ ?>
                            <li>
                                <a href="javaScript:void();">Report<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <li><a href="{{ url('/referral_statementReport') }}">Referral Statement</a></li>
                                    <li><a href="{{ url('/commissionReport') }}"> Commision Report</a></li>
                                    <li><a href="{{ url('/commission_summaryReport') }}"> Commision Summary</a></li>
                                    <li><a href="{{ url('/payout_reportReport') }}"> Payroll Report</a></li>

                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <?php } 
                     if (Helpers::get_permission('account', 'is_view') ||
                        Helpers::get_permission('voucher_head', 'is_view') ||
                        Helpers::get_permission('voucher_head', 'is_view') ||
                        Helpers::get_permission('voucher', 'is_view') ||
                        Helpers::get_permission('accounting_reports', 'is_view')) {
                    
                    ?>


                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/pages.svg" class="img-fluid"
                                alt="pages"><span>Office
                                Accounting</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php  if( Helpers::get_permission('account', 'is_view')){ ?>
                            <li><a href="{{ url('/Account') }}">Account</a></li>
                            <?php } if( Helpers::get_permission('voucher', 'is_view')){ ?>
                            <li><a href="{{ url('/Voucher') }}">Voucher</a></li>
                            <?php } if( Helpers::get_permission('voucher_head', 'is_view')){ ?>
                            <li><a href="{{ url('/VoucherHead') }}">Voucher Head</a></li>
                            <?php } if( Helpers::get_permission('accounting_reports', 'is_view')){ ?>


                            <li>
                                <a href="javaScript:void();">Reports<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">
                                    <li><a href="{{ url('/accountReport') }}">Account Statement</a></li>
                                    <li><a href="{{ url('/incomeReport') }}">Income Report</a></li>
                                    <li><a href="{{ url('/expenseReport') }}">Expense Report</a></li>
                                    <li><a href="{{ url('/transitionReport') }}">Transition Report</a></li>
                                    <li><a href="{{ url('/show_balance_sheet_report') }}">Balance Sheet</a></li>
                                    <li><a href="{{ url('/incomevsexpenseReport') }}">Income Vs Expense</a></li>

                                </ul>
                            </li>
                            <?php }  ?>

                        </ul>
                    </li>

                    <?php } 
                     if (Helpers::get_permission('lab_test_bill', 'is_view') ||
                        Helpers::get_permission('lab_test_bill', 'is_add') ||
                        Helpers::get_permission('test_bill_report', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/pages.svg" class="img-fluid"
                                alt="pages"><span>Pathology
                                Billing</span><i class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('lab_test_bill', 'is_add')){ ?>
                            <li><a href="{{ url('/TestBill') }}">Create Test Bill</a></li>
                            <?php } if( Helpers::get_permission('lab_test_bill', 'is_view')){ ?>
                            <li><a href="{{ url('/TestBillListReport') }}">Test Bill List</a></li>
                            <?php } if( Helpers::get_permission('test_bill_report', 'is_view')){ ?>


                            <li>
                                <a href="javaScript:void();">Reports<i
                                        class="feather icon-chevron-right pull-right"></i></a>
                                <ul class="vertical-submenu">

                                    <li><a href="{{ url('/due_billReport') }}">Due Report</a></li>
                                    <li><a href="{{ url('/paid_billReport') }}">Paid Bill Report</a></li>
                                    <li><a href="{{ url('/due_collectReport') }}">Due Collect Report</a></li>


                                </ul>
                            </li>
                            <?php }  ?>

                        </ul>
                    </li>

                    <?php } 
                     if (Helpers::get_permission('test_report', 'is_view') ||
                        Helpers::get_permission('test_report_template', 'is_view')) {
                    
                    ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/pages.svg" class="img-fluid"
                                alt="ecommerce"><span>Investigation Info</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('test_report', 'is_add')){ ?>
                            <li><a href="{{ url('/createReport') }}">Create Report</a></li>
                            <?php } if( Helpers::get_permission('test_report', 'is_view')){ ?>
                            <li><a href="{{ url('/report_listReport') }}">Report List</a></li>
                            <?php } if( Helpers::get_permission('test_report_template', 'is_view')){ ?>
                            <li><a href="{{ url('/Template') }}">Template</a></li>
                            <?php }  ?>

                        </ul>
                    </li>

                    <?php } 
                     if (Helpers::get_permission('global_setting', 'is_view') ||
                        Helpers::get_permission('email_setting', 'is_view') ||
                        Helpers::get_permission('language', 'is_view') ||
                        Helpers::get_permission('database_backup', 'is_view') ||
                        Helpers::get_permission('database_restore', 'is_add')) {
                    
                    ?>


                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/settings.svg" class="img-fluid"
                                alt="ecommerce"><span>Settings</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <?php if( Helpers::get_permission('global_setting', 'is_view')){ ?>
                            <li><a href="{{ url('/globalSettings') }}">Global Setting</a></li>
                            <?php } if( Helpers::get_permission('email_setting', 'is_view')){ ?>
                            <li><a href="{{ url('/') }}">Sms Setting</a></li>
                            <?php } if( Helpers::get_permission('email_setting', 'is_view')){ ?>
                            <li><a href="{{ url('/') }}">Email Setting</a></li>
                            <?php } $role_id = Auth::user()->role;
                                    if ($role_id == 1) {?>
                            <li><a href="{{ url('/Role') }}">Role Permission </a></li>
                            <?php } if( Helpers::get_permission('language', 'is_view')){ ?>
                            <li><a href="{{ url('/') }}">Language Setting</a></li>
                            <?php } if( Helpers::get_permission('database_restore', 'is_view')){ ?>
                            <li><a href="{{ url('/') }}">Database Backup</a></li>
                            <?php }  ?>


                        </ul>
                    </li>

                    <?php }  
                               
                    if(Helpers::get_permission('prescription', 'is_view')){
                    
                     ?>

                    <li>
                        <a href="javaScript:void();">
                            <img src="{{ url('/') }}/assets/images/svg-icon/tables.svg" class="img-fluid"
                                alt="Prescription"><span>E-Prescription</span><i
                                class="feather icon-chevron-right pull-right"></i>
                        </a>
                        <ul class="vertical-submenu">
                            <li><a href="{{ url('/todayQueue') }}">Today Queue</a></li>
                            {{-- <li><a href="{{ url('/') }}">Database Backup</a></li> --}}

                        </ul>
                    </li>


                    {{-- <li>
                        <a href="{{ url('/Prescription') }}">
                            <img src="{{ url('/') }}/assets/images/svg-icon/tables.svg" class="img-fluid"
                                alt="Prescription"><span>E-Prescription </span>
                        </a>
                    </li> --}}
                    <?php  } ?>

                </ul>
            </div>
            <!-- End Navigationbar -->
        </div>
        <!-- End Sidebar -->
    </div>
