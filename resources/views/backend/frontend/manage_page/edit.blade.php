@section('title')
    EDIT PAGE
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="false"><i class="fa fa-list-ul mr-2"></i>
                                    Page List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="true"><i
                                        class="fa fa-edit mr-2"></i>Edit
                                    Page</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" id="contact-tab-line" data-toggle="tab" href="#contact-line" role="tab"
                                    aria-controls="contact-line" aria-selected="false"><i
                                        class="feather icon-phone mr-2"></i>Logo </a>
                            </li> --}}
                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade" id="home-line" role="tabpanel" aria-labelledby="home-tab-line">


                                <table id="default-datatable" class="display table table-striped table-bordered"
                                    style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Page Title</th>
                                            <th>Menu Title</th>
                                            <th>Url</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; @endphp
                                        @foreach ($data['val'] as $item)
                                            @php $i++; @endphp
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $item['page_title'] }}</td>
                                                <td>{{ $item['menu_id'] }}</td>
                                                <td>{{ $item['menu_id'] }}</td>
                                                <td>
                                                    <form action="{{ URL::to('editManagePage/') }}" method="post"
                                                        class="form-horizontal" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                        <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                class="fa fa-edit"></i></button>

                                                    </form>
                                                </td>


                                            </tr>
                                        @endforeach


                                    </tbody>

                                </table>




                            </div>
                            <div class="tab-pane fade show active" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('webSetting/ManagePageUpdate/' . $data['editVal']['id']) }}"
                                    method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Page Title') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['page_title'] }}" name="page_title" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Select Menu') }} </label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="translation"
                                                placeholder="Enter Product translation...">
                                                @foreach ($data['menu'] as $menu)

                                                    <option value="{{ $menu['id'] }}"
                                                        {{ $data['editVal']['menu_id'] == $menu['id'] ? 'selected' : '' }}>
                                                        {{ $menu['title'] }}</option>
                                                @endforeach
                                            </select>


                                            <input type="text" class="form-control"
                                                value="{{ $data['editVal']['menu_id'] }}" name="menu_id" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Content') }} </label>
                                        <div class="col-md-9">

                                            <textarea id="tinymce-example"
                                                name="content">{{ $data['editVal']['page_title'] }}</textarea>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Banner Photo') }} </label>
                                        <div class="col-md-6">
                                            <div class="col-md-3 mb-md">


                                                <?php if ($data['editVal']['banner_image'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                                } else {
                                                $img = asset('/public/assets/images/uploads/web_settings/' .
                                                $data['editVal']['banner_image']);
                                                } ?>
                                                <img width="100px" height="100px;" id="image1" src="<?= $img ?>" alt="...">
                                                                                                                                                                                                                            
                                                                                                                                                                                                        
                                                                            <input type="file" name="banner_image" value="{{ asset('/public/assets/images/uploads/web_settings/' . $data['editVal']['banner_image']) }}" accept="image/*" onchange="readURLphoto1(this);" class="form-control" style="width: 250px;">
                                                                            </div>          
                                                                        
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group row">
                                                                        <label class="col-md-3 control-label">{{ __('Meta Keyword') }}</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control"
                                                                                value="{{ $data['editVal']['page_title'] }}" name="meta_keyword" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-3 control-label">{{ __('Meta Description') }} </label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control"
                                                                                value="{{ $data['editVal']['page_title'] }}" name="meta_description" />
                                                                        </div>
                                                                    </div>



                                                                    <footer class="panel-footer mt-md">
                                                                        <div class="row" style="float: right">
                                                                            <div class="col-md-12 ">
                                                                                <button type="submit" class="btn btn btn-default btn-block"
                                                                                    name="app_setting" value="1">{{ __('Update') }}</button>
                                                                            </div>
                                                                        </div>
                                                                    </footer>
                                                                </form>

                                                            </div>
                                                            {{-- <div class="tab-pane fade" id="contact-line" role="tabpanel" aria-labelledby="contact-tab-line">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book. It has
                                    survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged.</p>
                            </div> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End col -->
                                        </div>
                                        <!-- End Contentbar -->
    @endsection
    @section('script')
                                        <script>
                                            function readURLphoto1(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                    reader.onload = function(e) {
                                                        $('#image1').attr('src', e.target.result);
                                                    }
                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }
                                        </script>
                                        <!-- Wysiwig js -->
                                        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
                                        <!-- Summernote JS -->
                                        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
                                        <!-- Code Mirror JS -->
                                        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
                                        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


                                        <!-- Datatable js -->
                                        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
                                        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
                                        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>
    @endsection
