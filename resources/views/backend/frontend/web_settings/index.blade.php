@section('title')
    WEB SETTINGS
@endsection
@extends('backend.layouts.main')

@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <?php $val = $data['web_settings'][0]; ?>
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="feather icon-globe mr-2"></i>
                                    Website Setting</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">
                                <form action="{{ URL::to('webSetting/web_edit/' . $val->id) }}" method="post"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Application Name') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="application_title"
                                                value=" {{ $val->application_title }} " />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Receive Email To') }} </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="receive_contact_email"
                                                value=" {{ $val->receive_contact_email }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('About Us') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="about_us" rows="2" class="form-control"
                                                aria-required="true">{{ $val->about_us }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('About videos url') }} </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="about_videos"
                                                value=" {{ $val->about_videos }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Working Hours') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="working_hours" rows="2" class="form-control"
                                                aria-required="true">{{ $val->working_hours }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Address') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="address" rows="2" class="form-control"
                                                aria-required="true">{{ $val->address }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Mobile No') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="mobile_no"
                                                value="{{ $val->mobile_no }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Email') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="email"
                                                value=" {{ $val->email }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Fax') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="fax"
                                                value=" {{ $val->fax }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Footer Text') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="footer_text"
                                                value=" {{ $val->footer_text }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Facebook URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="facebook_url"
                                                value="{{ $val->facebook_url }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Twitter URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="twitter_url"
                                                value="{{ $val->twitter_url }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Linkedin URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="linkedin_url"
                                                value="{{ $val->linkedin_url }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Youtube URL</label>
                                        <div class="col-md-6 mb-md">
                                            <input type="text" class="form-control" name="youtube_url"
                                                value="{{ $val->youtube_url }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Google Plus</label>
                                        <div class="col-md-6 mb-md">
                                            <input type="text" class="form-control" name="google_plus"
                                                value="{{ $val->google_plus }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Pinterest Url</label>
                                        <div class="col-md-6 mb-md">
                                            <input type="text" class="form-control" name="pinterest_url"
                                                value="{{ $val->pinterest_url }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Instagram Url</label>
                                        <div class="col-md-6 mb-md">
                                            <input type="text" class="form-control" name="instagram_url"
                                                value="{{ $val->instagram_url }}" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Logos</label>
                                        <div class="col-md-3 mb-md">
                                            <label class="control-label"> Logo</label>


                                            <?php if ($val->logo == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/web_settings/' . $val->logo);
                                            } ?>
                                            <img width="100px" height="100px;" id="image1" src="<?= $img ?>" alt="...">


                                            <input type="file" name="logo"
                                                value="{{ asset('/public/assets/images/uploads/web_settings/' . $val->logo) }}"
                                                accept="image/*" onchange="readURLphoto1(this);" class="form-control"
                                                style="width: 250px;">
                                        </div>


                                        <div class="col-md-3 mb-md">
                                            <label class="control-label">Fav Icon</label>

                                            <?php if ($val->fav_icon == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/web_settings/' . $val->fav_icon);
                                            } ?>
                                            <img width="100px" height="100px;" id="image2" src="<?= $img ?>" alt="...">


                                            <input type="file" name="fav_icon"
                                                value="{{ asset('/public/assets/images/uploads/web_settings/' . $val->fav_icon) }}"
                                                accept="image/*" onchange="readURLphoto2(this);" class="form-control"
                                                style="width: 250px;">



                                        </div>





                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Video Thumbnali</label>
                                        <div class="col-md-3 mb-md">
                                            <label class="control-label">About Video Thumbnail</label>

                                            <?php if ($val->video_image == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/web_settings/' . $val->video_image);
                                            } ?>
                                            <img width="100px" height="100px;" id="image3" src="<?= $img ?>" alt="...">


                                            <input type="file" name="video_image"
                                                value="{{ asset('/public/assets/images/uploads/web_settings/' . $val->video_image) }}"
                                                accept="image/*" onchange="readURLphoto3(this);" class="form-control"
                                                style="width: 250px;">



                                        </div>
                                    </div>

                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            function readURLphoto1(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#image1').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLphoto2(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#image2').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLphoto3(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#image3').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    @endsection
