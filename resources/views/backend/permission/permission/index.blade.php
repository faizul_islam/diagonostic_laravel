@section('title')
    Permission
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">
                    <form action="{{ URL::to('updatePermission/') }}" method="post" class="form-horizontal"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="role_id" value="<?php echo $data['role_id']; ?>">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-condensed mt-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th><?php echo 'feature'; ?></th>
                                            <th>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" id="all_view"
                                                            value="1"><i></i> <?php echo 'view'; ?></label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" id="all_add"
                                                            value="1"><i></i> <?php echo 'add'; ?></label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" id="all_edit"
                                                            value="1"><i></i> <?php echo 'edit'; ?></label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" id="all_delete"
                                                            value="1"><i></i> <?php echo 'delete'; ?></label>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
						if(count($data['val'])){ 
							foreach($data['val'] as $module):
							?>
                                        <tr>
                                            <th colspan="5"><?php echo $module['name']; ?></th>
                                        </tr>
                                        <?php
						// $permissions = $this->role_model->check_permission($module['id'], $role_id);
                         $permissions = App\Models\Custom_model::check_permission($module['id'], $data['role_id']);
                                                   
                        //    echo "<pre>"; print_r($staff); die();
						foreach($permissions as $permission):
						?>
                                        <input type="hidden" name="privileges[<?php echo $permission['id']; ?>][privileges_id]"
                                            value="<?php echo $permission['id']; ?>">
                                        <tr>
                                            <td class="pl-xl">&nbsp;&nbsp;&nbsp;
                                                <i class="fa fa-arrows"></i>
                                                <?php echo $permission['name']; ?>
                                            </td>
                                            <td>
                                                <?php if($permission['show_view']){ ?>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" class="cb_view"
                                                            name="privileges[<?php echo $permission['id']; ?>][view]"
                                                            <?php echo $permission['is_view'] == 1 ? 'checked' : ''; ?> value="1">
                                                        <i></i>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if($permission['show_add']){ ?>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" class="cb_add"
                                                            name="privileges[<?php echo $permission['id']; ?>][add]"
                                                            <?php echo $permission['is_add'] == 1 ? 'checked' : ''; ?> value="1">
                                                        <i></i>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if($permission['show_edit']){ ?>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" class="cb_edit"
                                                            name="privileges[<?php echo $permission['id']; ?>][edit]"
                                                            <?php echo $permission['is_edit'] == 1 ? 'checked' : ''; ?> value="1">
                                                        <i></i>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if($permission['show_delete']){ ?>
                                                <div class="checkbox-replace">
                                                    <label class="i-checks"><input type="checkbox" class="cb_delete"
                                                            name="privileges[<?php echo $permission['id']; ?>][delete]"
                                                            <?php echo $permission['is_delete'] == 1 ? 'checked' : ''; ?> value="1">
                                                        <i></i>
                                                    </label>
                                                </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                        <?php endforeach; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-9 col-md-3">
                                    <button type="submit" name="save" value="1" class="btn btn-default btn-block"><i
                                            class="fa fa-edit"></i> <?php echo 'update'; ?></button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
                <!-- End col -->
            </div>
            <!-- End Contentbar -->
        @endsection
        @section('script')
            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $(document).ready(function() {
                    $(document).on("click", ".deleteData", function() {
                        var id = $(this).attr('id');
                        var base_url = $('#base_url').val();
                        // alert(id);
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover file!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: "deleteRole",
                                    // alert();
                                    type: "post",
                                    data: {
                                        id: id
                                    },
                                    success: function(data) {
                                        swal("Information has been deleted", {
                                            icon: "success",
                                        });
                                        location.reload();
                                    }
                                });
                            } else {
                                swal("Your file is safe!");
                            }
                        });
                    });
                });
            </script>
            <!-- Wysiwig js -->
            <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
            <!-- Summernote JS -->
            <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
            <!-- Code Mirror JS -->
            <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
            <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
            <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
            <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
            <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
            <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


            <!-- Datatable js -->
            <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

            <!-- Sweet-Alert js -->
            <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
            <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

            <!-- Input Mask js -->
            <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
            <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>
        @endsection
