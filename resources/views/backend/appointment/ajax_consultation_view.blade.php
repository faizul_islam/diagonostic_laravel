  @csrf

  <label for="title" class="col-form-label pull-right">
      Patient Type : @if ($data['appointment'][0]['patient_type'] == 1)
          <span style="color: green;">(New)</span>
      @elseif($data['appointment'][0]['patient_type'] == 2)
          <span style="color: red;"> (Old)</span>
      @else
          <span style="color: #ffeb3b;"> (not found)</span>
      @endif
  </label>


  <div class="form-group">
      <label for="title" class="col-form-label">Consultation Fees :</label>
      <input type="text" class="form-control" id="consultation_fees" name="consultation_fees"
          value="{{ $data['appointment'][0]['consultation_fees'] }}" readonly>
  </div>
  <input type="hidden" class="form-control" name="id" value="{{ $data['appointment'][0]['id'] }}" readonly>

  <div class="form-group">
      <label for="title" class="col-form-label">Patient Paid :</label>
      <input type="text" class="form-control" id="paid_amount" name="paid_amount"
          value="{{ $data['appointment'][0]['consultation_fees'] }}">
  </div>
