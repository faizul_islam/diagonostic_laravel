@section('title')
    Appointment
@endsection
@extends('backend.layouts.main')

@section('style')

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Appointment List</a>
                            </li>
                            <?php 
                             if (App\Http\Helpers::get_permission('appointment', 'is_add')) {
                          
                            ?>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Add
                                    Appointment</a>
                            </li>
                            <?php }?>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                {{-- <th width="10%">Appointment Id</th> --}}
                                                <th width="10%">Doctor Name</th>
                                                <th width="10%">Patient Name</th>
                                                <th width="8%">Date</th>
                                                <th width="10%">Consultation Schedule</th>
                                                <th width="10%">Serial</th>
                                                <th width="10%">Remarks</th>
                                                <th width="10%">Status</th>
                                                <?php 
                                                if (App\Http\Helpers::get_permission('appointment', 'is_add')) {
                                            
                                                ?>
                                                <th width="15%">Action</th>
                                                <?php }?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <?php
                                                    $staff = App\Models\Staffs::Where('id', '=', $item['doctor_id'])->get();
                                                    $patient = App\Models\Patient::Where('id', '=', $item['patient_id'])->get();
                                                    $appointmentDate = date('d-m-Y', strtotime($item['appointment_date']));
                                                    if (!empty($staff[0]['name'])) {
                                                        $staffName = $staff[0]['name'];
                                                    } else {
                                                        $staffName = '';
                                                    }
                                                    
                                                    if (!empty($patient[0]['name'])) {
                                                        $patientName = $patient[0]['name'];
                                                    } else {
                                                        $patientName = '';
                                                    }
                                                    
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    {{-- <td>{{ $item['appointment_id '] }}</td> --}}
                                                    <td>{{ $staffName }}</td>
                                                    <td>{{ $patientName }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item['appointment_date'])) }}</td>
                                                    <td>{{ App\Http\Helpers::get_schedule_details($item['doctor_id'], $item['appointment_date'], $item['schedule']) }}
                                                    </td>
                                                    {{-- <td>-</td> --}}
                                                    <td>{{ $item['schedule'] }}</td>

                                                    <td>{{ $item['consultation_fees'] }}</td>
                                                    <td>
                                                        <?php
                                                        if ($item['status'] == 1) {?>
                                                        <button type="button"
                                                            class="btn btn-rounded btn-success-rgba">Confirm</button>
                                                        <?php  } elseif ($item['status'] == 2) {?>

                                                        <button type="button"
                                                            class="btn btn-rounded btn-danger-rgba">Close</button>
                                                        <?php    }elseif($item['status'] == 3){?>
                                                        <button type="button"
                                                            class="btn btn-rounded btn-danger-rgba">Processing..</button>
                                                        <?php    }
                                                        ?>

                                                    </td>


                                                    <td>
                                                        <?php 
                                                    if (App\Http\Helpers::get_permission('appointment', 'is_edit')) {
                                                
                                                    ?>
                                                        {{-- <form action="{{ URL::to('editAppointment/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}"> --}}
                                                        <input type="hidden" name="id" value="{{ $item['id'] }}">
                                                        <button type="button"
                                                            class="btn btn-round btn-danger-rgba deleteData"
                                                            id="{{ $item['id'] }}" title="Delete"><i
                                                                class="fa fa-trash"></i></button>
                                                        @if ($item['status'] != 2)

                                                            {{-- <button type="button" class="btn btn-round btn-success-rgba"
                                                                    id="{{ $item['id'] }}" data-toggle="modal"
                                                                    data-target="#varying-modal" title=" Status"><i
                                                                        class="fa fa-check"></i></button> --}}
                                                            {{-- <button type="submit"
                                                                    class="btn btn-round btn-primary-rgba"><i
                                                                        class="fa fa-edit"></i></button> --}}
                                                            <button type="submit" id="openModal"
                                                                data-ids="{{ $item['id'] }}"
                                                                class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>
                                                        @endif
                                                        <?php 
                                                        }
                                                    if (App\Http\Helpers::get_permission('appointment', 'is_view')) {
                                                
                                                    ?>
                                                        @if ($item['status'] != 3)
                                                            <button type="submit" id="goPrescription"
                                                                data-ids="{{ $item['id'] }}"
                                                                class="btn btn-round btn-warning-rgba"><i
                                                                    class="fa fa-wheelchair"></i></button>
                                                        @endif

                                                        <?php
                                                         }
                                                        ?>

                                                        {{-- </form> --}}


                                                    </td>



                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('storeAppointment/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Appointment Date') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" name="appointment_date" id="autoclose-date"
                                                class="datepicker-here form-control" placeholder="dd/mm/yyyy"
                                                aria-describedby="basic-addon3" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Shift') }}</label>
                                        <div class="col-md-6">
                                            <div class="form-check">


                                                <input class="form-check-input" type="radio" name="shift" id="shift"
                                                    value="1" checked="yes">
                                                <label class="form-check-label">
                                                    Evining
                                                </label>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input class="form-check-input" type="radio" name="shift" id="shift"
                                                    value="2">
                                                <label class="form-check-label">
                                                    Morning
                                                </label>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Patient Name') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="patient_id" id="patient_id" required>
                                                <option value="">Select</option>
                                                @foreach ($data['patient'] as $patient)

                                                    <option value="{{ $patient['id'] }}">
                                                        {{ $patient['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Doctor Name') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="doctor_id" id="adoctor_id" required>
                                                <option value="">Select</option>
                                                @foreach ($data['staff'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Time Slot') }}</label>
                                        <div class="col-md-6">
                                            {{-- <input type="text" class="form-control" value="test" name="schedule" /> --}}
                                            <select name="available_schedule" class="form-control" data-plugin-selectTwo
                                                data-width="100%" data-minimum-results-for-search="Infinity"
                                                id="available_schedule" required>
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Patient Type') }}</label>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="patient_type"
                                                    id="patient_type" value="1" checked="yes">
                                                <label class="form-check-label">
                                                    New Patient
                                                </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                <input class="form-check-input" type="radio" name="patient_type"
                                                    id="patient_type" value="2">
                                                <label class="form-check-label">
                                                    Old Patient
                                                </label>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __(' Consultation Fees') }}</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="consultation_fees"
                                                id="consultation_fees" />
                                        </div>
                                    </div>





                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Discount') }}</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="discount" value="0.00"
                                                id="discount" onchange="netBillCalculation()"
                                                onkeyup="netBillCalculation(this.value)" />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Net Payable') }}</label>
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" name="net_payable" readonly
                                                value="0.00" id="net_payable" />
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Remarks') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="remarks" rows="2" class="form-control"
                                                aria-required="true"></textarea>
                                        </div>
                                    </div>

                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->

        <!-- Modal -->
        <div class="modal fade" id="varying-modal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="backgroud-color:#83a4c6;">
                        <h5>Consultation Fees </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ URL::to('UpdateStatus/') }}" method="post" class="form-horizontal"
                        enctype="multipart/form-data">
                        <div class="modal-body" id="consaltDetailsBody">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="openBpSetModal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="backgroud-color:#83a4c6;">
                        <h5>Prescription (Set basic examination) </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ URL::to('StorePrescription/') }}" method="post" class="form-horizontal"
                        enctype="multipart/form-data">
                        <div class="modal-body" id="openBpSetModalBody">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteAppointment",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });


                $(document).on("click", ".StatusChance", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once status change, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "StatusChance",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Status has been changed", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });


            });

            $(document).on("click", "#openModal", function() {

                var appointmentId = $(this).attr('data-ids');
                var base_url = $('#base_url').val();
                $.ajax({
                    url: 'ajax_consultation_view',
                    type: 'POST',
                    data: {
                        appointmentId: appointmentId
                    },
                    success: function(data) {

                        $('#varying-modal').modal('show');
                        $('#consaltDetailsBody').html(data);
                    }
                });
            });

            $(document).on("click", "#patient_type", function() {

                var doctor_id = $('#adoctor_id').val();
                // var old_fee = $('#old_fee').val();
                var patient_type = $("#patient_type:checked").val();
                var appointment_date = $('#autoclose-date').val();
                $.ajax({
                    url: 'ajax_get_consultation_fee',
                    type: 'POST',
                    data: {
                        appointment_date: appointment_date,
                        doctor_id: doctor_id,
                        patient_type: patient_type
                    },
                    success: function(data) {

                        $('#consultation_fees').val(data);
                        netBillCalculation();

                    }
                });
            });


            $(document).on("change", "#autoclose-date, #adoctor_id,#shift", function() {

                var shift = $('#shift:checked').val();
                var doctor_id = $('#adoctor_id').val();
                var appointment_date = $('#autoclose-date').val();
                // alert(appointment_date);
                if (doctor_id !== "" && appointment_date !== "") {
                    $('#discount').val('0.00');
                    $("#available_schedule").html("<option value=''><?php echo 'exploring'; ?>...</option>");
                    $.ajax({
                        url: "get_appointment_schedule",
                        type: "POST",
                        data: {
                            'appointment_date': appointment_date,
                            'doctor_id': doctor_id,
                            'shift': shift
                        },
                        dataType: 'html',
                        success: function(data) {
                            var response = jQuery.parseJSON(data);
                            $('#available_schedule').html(response.schedule);
                            $('#consultation_fees').val(response.fees);
                            netBillCalculation();
                        }
                    });
                }
            });

            $(document).on("click", "#goPrescription", function() {

                var appointmentId = $(this).attr('data-ids');
                var base_url = $('#base_url').val();
                $.ajax({
                    url: 'ajax_bpSet_view',
                    type: 'POST',
                    data: {
                        appointmentId: appointmentId
                    },
                    success: function(data) {

                        $('#openBpSetModal').modal('show');
                        $('#openBpSetModalBody').html(data);
                    }
                });
            });

            function netBillCalculation() {
                var discount = $('#discount').val();
                var consultation_fees = $('#consultation_fees').val();
                var net_payable = (consultation_fees - discount).toFixed(2);
                $('#net_payable').val(net_payable);
            }
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Input Mask js -->
        <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>


        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>

        <!-- Model js -->
        <script src="{{ asset('assets/js/custom/custom-model.js') }}"></script>
    @endsection
