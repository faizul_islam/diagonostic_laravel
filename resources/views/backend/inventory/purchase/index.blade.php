@section('title')
    Purchase
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css">
    <!-- Code Mirror css -->
    <link href="{{ asset('assets/plugins/code-mirror/codemirror.css') }}" rel="stylesheet" type="text/css">

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Purchase List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line" role="tab"
                                    aria-controls="profile-line" aria-selected="false"><i class="fa fa-edit mr-2"></i>Add
                                    Purchase Stock</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                <th width="10%">Bill No</th>
                                                <th width="10%">Supplier Name</th>
                                                <th width="15%">Purchase Status</th>
                                                <th width="15%">Payment Status</th>
                                                <th width="8%">Purchase Date</th>
                                                <th width="10%">Net Payable</th>
                                                <th width="15%">Paid</th>
                                                <th width="15%">Due</th>
                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <?php
                                                    $Supplier = App\Models\Supplier::Where('id', '=', $item['supplier_id'])->get();
                                                    if (!empty($Supplier->toArray())) {
                                                        $Supplier = $Supplier[0]['name'];
                                                    } else {
                                                        $Supplier = '';
                                                    }
                                                    if ($item['payment_status'] == 1) {
                                                        $payment_status = 'Unpaid';
                                                    } else {
                                                        $payment_status = 'Paid';
                                                    }
                                                    
                                                    if ($item['purchase_status'] == 1) {
                                                        $purchase_status = 'Ordered';
                                                    } elseif ($item['purchase_status'] == 2) {
                                                        $purchase_status = 'Received';
                                                    } else {
                                                        $purchase_status = 'Pending';
                                                    }
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $item['bill_no'] }}</td>


                                                    <td>{{ $Supplier }}</td>

                                                    <td>{{ $purchase_status }}</td>
                                                    <td>{{ $payment_status }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item['date'])) }}</td>

                                                    <td>{{ $item['total'] }}</td>
                                                    <td>{{ $item['paid'] }}</td>
                                                    <td>{{ $item['due'] }}</td>



                                                    <td>
                                                        <form action="{{ URL::to('editChemicalStock/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel" aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('storeChemicalStock/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Bill No') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="inovice_no" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Supplier') }}</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="supplier_id">
                                                @foreach ($data['Supplier'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    {{-- <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Chemical Name') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="chemical_id">
                                                @foreach ($data['Chemical'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div> --}}


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Purchase Status') }}</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="purchase_status">

                                                <option value="1"> Ordered </option>
                                                <option value="2"> Received </option>
                                                <option value="3"> Pending </option>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label
                                            class="col-md-3 control-label">{{ __('Stock Quantity ( Sale Unit ) ') }}</label>
                                        <div class="col-md-6">

                                            <input type="text" class="form-control" name="stock_quantity" />

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Remarks') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="remarks" rows="2" class="form-control"
                                                aria-required="true"></textarea>
                                        </div>
                                    </div>

                                    <div id="payment" style="background:#eee; padding: 9px;">
                                        <div class="form-row">
                                            <div class="form-group col-md-2">
                                                <label for="inputPassword4" style="font-size: 13px;">Chemical</label>
                                                <select class="form-control" id="chemical" name="chemical">
                                                    <option value="">Select </option>
                                                    @foreach ($data['Chemical'] as $cat)

                                                        <option value="{{ $cat['id'] }}">
                                                            {{ $cat['name'] }}
                                                        </option>
                                                    @endforeach

                                                </select>

                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="inputDate" style="font-size: 13px;">Unit Price</label>
                                                <input type="text" class="form-control" id="unit_price" name="unit_price" />
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="inputEmail4" style="font-size: 13px;">Quantity</label>
                                                <input class="form-control" type="text" id="quantity">
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="inputPassword4" style="font-size: 13px;">Discount</label>
                                                <input class="form-control" type="text" id="discount">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="inputAddress" style="font-size: 13px;">Total Price</label>
                                                <input class="form-control" type="text" id="total_price">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <button type="button" class="add_field_button btn btn-primary"
                                                    style="margin-top: 31px"><i class="fa fa-plus"></i>&nbsp; Add</button>
                                            </div>
                                            <input class="form-control" type="hidden" placeholder="Remarks" name="remarks">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" style="min-height: 230px">
                                                    <table id="default-datatable"
                                                        class="display table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%">SL</th>
                                                                <th width="15%">CHEMICAL</th>
                                                                <th width="15%">UNIT PRICE</th>
                                                                <th width="25%">QUANTITY</th>
                                                                <th width="30%">DISCOUNT</th>
                                                                <th width="15%">TOTAL PRICE</th>
                                                                <th width="10%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="tbodyCell">
                                                        </tbody>
                                                        <tfoot class="tfootCell">
                                                        </tfoot>
                                                        <input type="hidden" id="hidden_total" value="0">
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteChemicalStock",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });

            $(document).ready(function() {
                //***************** add new fields for any form **************//
                var max_fields = 6; //maximum input boxes allowed
                var wrapper = $(".tbodyCell"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID
                var x = 1; //initlal text box count
                var i = 1;
                var total = $('#hidden_total').val();

                $(add_button).click(function(e) { //on add input button click
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        var chemical = $('#chemical').val();
                        var chemicalText = $("#chemical option:selected").text();
                        var quantity = $('#quantity').val();
                        var unit_price = $('#unit_price').val();
                        var discount = $('#discount').val();
                        var total_price = $('#total_price').val();


                        if (!chemical) {
                            swal("Oops! You must have to select a chart of account.", {
                                icon: "warning",
                            });
                            return;
                        } else if (quantity <= 0) {
                            swal("Oops! Amount must be greater then 0.", {
                                icon: "warning",
                            });
                            return;
                        } else {
                            var append_value = "<tr><td>" + i + "</td>" +

                                "<td><input type='hidden' name='chemical[]' value='" + chemical +
                                "'>" + chemicalText + "</td>" +

                                "<td><input type='hidden' name='unit_price[]' value='" + unit_price + "'>" +
                                unit_price + "</td>" +

                                "<td><input type='hidden' name='quantity[]' class='quantity' value='" +
                                quantity +
                                "'>" + quantity + "</td>" +
                                "<td><input type='hidden' name='discount[]' value='" + discount + "'>" +
                                discount + "</td>" +
                                "<td class='text-right'><input type='hidden' name='total_price[]' class='payC_amount' value='" +
                                total_price + "'>" + total_price + "</td>" +
                                "<td><a href='#' class='remove_field btn btn-danger btn-sm'>X</a></td></tr>";

                            $(wrapper).append(append_value); //add input box
                            total = parseFloat(total) + parseFloat(total_price);
                            var append_value_2 =
                                "<tr><td colspan='5'><b>Total</b></td><td class='text-right'><b id='totalShow'>" +
                                total + "</b></td><td></td></tr>";
                            $('.tfootCell').html(append_value_2);
                            $('#hidden_total').val(total);
                        }
                        i++;
                    }



                });

                $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
                    e.preventDefault();
                    var payRemove = $(this).closest('tr').find('.payC_amount').val();
                    //alert(parseFloat(payRemove));
                    total = parseFloat(total) - parseFloat(payRemove);
                    $(this).closest('tr').remove();
                    x--;
                    $('#totalShow').html(total);
                });


            });
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Datepicker JS -->
        <script src="{{ asset('assets/plugins/datepicker/datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datepicker/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-datepicker.js') }}"></script>
    @endsection
