@section('title')
    SETTINGS
@endsection
@extends('backend.layouts.main')

@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <?php $global_config = $data['global_config'][0]; ?>
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="feather icon-home mr-2"></i>
                                    General Setting</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line" role="tab"
                                    aria-controls="profile-line" aria-selected="false"><i
                                        class="feather icon-user mr-2"></i>Theme Setting</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" id="contact-tab-line" data-toggle="tab" href="#contact-line" role="tab"
                                    aria-controls="contact-line" aria-selected="false"><i
                                        class="feather icon-phone mr-2"></i>Logo </a>
                            </li> --}}
                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">
                                <form action="{{ URL::to('globalSettings/edit/' . $global_config['id']) }}" method="post"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('System Name') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="institute_name"
                                                value=" {{ $global_config['institute_name'] }} " />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Mobile No') }} </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="mobileno"
                                                value=" {{ $global_config['mobileno'] }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Address') }}</label>
                                        <div class="col-md-6">
                                            <textarea name="address" rows="2" class="form-control"
                                                aria-required="true">{{ $global_config['address'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Currency') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="currency"
                                                value="{{ $global_config['currency'] }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Currency Symbol') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="currency_symbol"
                                                value=" {{ $global_config['currency_symbol'] }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Language') }} </label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="translation">
                                                @foreach ($data['language_list'] as $lang)

                                                    <option value="{{ $lang->name }}"
                                                        {{ $global_config['translation'] == $lang->name ? 'selected' : '' }}>
                                                        {{ $lang->name }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Timezone') }} </label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="timezone"
                                                placeholder="Enter Product timezone...">
                                                @php $timezones = App\Http\Helpers::timezone_list(); @endphp
                                                @foreach ($timezones as $zone)
                                                    <option value="{{ $zone }}"> {{ $zone }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Date Format') }}</label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="date_format"
                                                placeholder="Enter Product date_format...">
                                                @php $getDateformat = App\Http\Helpers::get_date_format(); @endphp
                                                @foreach ($getDateformat as $format)
                                                    <option value="{{ $format }}"> {{ $format }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Footer Text') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="footer_text"
                                                value=" {{ $global_config['footer_text'] }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Facebook URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="facebook_url"
                                                value="{{ $global_config['facebook_url'] }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Twitter URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="twitter_url"
                                                value="{{ $global_config['twitter_url'] }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Linkedin URL</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="linkedin_url"
                                                value="{{ $global_config['linkedin_url'] }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Youtube URL</label>
                                        <div class="col-md-6 mb-md">
                                            <input type="text" class="form-control" name="youtube_url"
                                                value="{{ $global_config['youtube_url'] }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Logos</label>
                                        <div class="col-md-2 mb-md">
                                            <label class="control-label">System Logo</label>


                                            <?php if ($global_config['system_logo'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/settings/' . $global_config['system_logo']);
                                            } ?>
                                            <img width="100px" height="100px;" id="image1" src="<?= $img ?>" alt="...">


                                            <input type="file" name="system_logo"
                                                value="{{ asset('/public/assets/images/uploads/settings/' . $global_config['system_logo']) }}"
                                                accept="image/*" onchange="readURLphoto1(this);" class="form-control"
                                                style="width: 250px;">

                                        </div>

                                        <div class="col-md-2 mb-md">
                                            <label class="control-label">Text Logo</label>

                                            <?php if ($global_config['text_logo'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/settings/' . $global_config['text_logo']);
                                            } ?>
                                            <img width="100px" height="100px;" id="image2" src="<?= $img ?>" alt="...">


                                            <input type="file" name="text_logo"
                                                value="{{ asset('/public/assets/images/uploads/settings/' . $global_config['text_logo']) }}"
                                                accept="image/*" onchange="readURLphoto2(this);" class="form-control"
                                                style="width: 250px;">



                                        </div>

                                        <div class="col-md-2 mb-md">
                                            <label class="control-label">Printing Logo</label>
                                            <?php if ($global_config['printing_logo'] == '') {
                                                $img = asset('/assets/images/no_img.png');
                                            } else {
                                                $img = asset('/public/assets/images/uploads/settings/' . $global_config['printing_logo']);
                                            } ?>
                                            <img width="100px" height="100px;" id="image3" src="<?= $img ?>" alt="...">


                                            <input type="file" name="printing_logo"
                                                value="{{ asset('/public/assets/images/uploads/settings/' . $global_config['printing_logo']) }}"
                                                accept="image/*" onchange="readURLphoto3(this);" class="form-control"
                                                style="width: 250px;">

                                        </div>
                                    </div>


                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>


                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel" aria-labelledby="profile-tab-line">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book. It has
                                    survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged.</p>
                            </div>
                            {{-- <div class="tab-pane fade" id="contact-line" role="tabpanel" aria-labelledby="contact-tab-line">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book. It has
                                    survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged.</p>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            </ </div>
            <!-- End Contentbar -->
        @endsection
        @section('script')
            <script>
                function readURLphoto1(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#image1').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function readURLphoto2(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#image2').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function readURLphoto3(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#image3').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
            </script>
        @endsection
