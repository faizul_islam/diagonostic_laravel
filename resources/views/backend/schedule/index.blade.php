@section('title')
    Schedule
@endsection
@extends('backend.layouts.main')

@section('style')
    <!-- DataTables css -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive Datatable css -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/plugins/datepicker/datepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Switchery css -->
    <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
    <!-- Select2 css -->
    <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Tagsinput css -->
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css') }}" rel="stylesheet"
        type="text/css">
    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <style>
        .select2-selection__rendered {
            border: 1px solid #ced4da !important;
        }

    </style>
    <!-- Start Contentbar -->
    <div class="contentbar">
        <!-- Start row -->
        <div class="row">

            <!-- Start col -->
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">

                    <div class="card-body">
                        <ul class="nav nav-tabs custom-tab-line mb-3" id="defaultTabLine" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab-line" data-toggle="tab" href="#home-line" role="tab"
                                    aria-controls="home-line" aria-selected="true"><i class="fa fa-list-ul mr-2"></i>
                                    Schedule List</a>
                            </li>
                            <?php
                              if (App\Http\Helpers::get_permission('schedule', 'is_add')) {
                          
                            ?>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-line" data-toggle="tab" href="#profile-line"
                                    role="tab" aria-controls="profile-line" aria-selected="false"><i
                                        class="fa fa-edit mr-2"></i>Add
                                    Schedule</a>
                            </li>
                            <?php }?>

                        </ul>
                        <div class="tab-content" id="defaultTabContentLine">
                            <div class="tab-pane fade show active" id="home-line" role="tabpanel"
                                aria-labelledby="home-tab-line">


                                <div class="table-responsive">
                                    <table id="default-datatable" class="display table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">Sl</th>
                                                <th width="10%">Doctor</th>
                                                <th width="15%">Consultation Fees</th>
                                                <th width="15%">Week Day</th>
                                                <th width="8%">Time Start</th>
                                                <th width="10%">Time End</th>
                                                <th width="15%">Per Patient Duration</th>
                                                <th width="15%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach ($data['val'] as $item)
                                                @php $i++; @endphp
                                                <tr>


                                                    <?php
                                                    $staff = App\Models\Staffs::Where('id', '=', $item['doctor_id'])->get();
                                                    if (!empty($staff[0]['name'])) {
                                                        $staffName = $staff[0]['name'];
                                                    } else {
                                                        $staffName = '';
                                                    }
                                                    ?>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $staffName }}</td>
                                                    <td>{{ $item['consultation_fees'] }}</td>
                                                    <td>{{ $item['day'] }}</td>
                                                    <td>{{ $item['time_start'] }}</td>
                                                    <td>{{ $item['time_end'] }}</td>
                                                    <td>{{ $item['per_patient_time'] }}</td>

                                                    <td>
                                                        <?php
                                                           if (App\Http\Helpers::get_permission('schedule', 'is_edit')) {
                          
                                                        ?>
                                                        <form action="{{ URL::to('editSchedule/') }}" method="post"
                                                            class="form-horizontal" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token"
                                                                value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $item['id'] }}">

                                                            <button type="submit" class="btn btn-round btn-primary-rgba"><i
                                                                    class="fa fa-edit"></i></button>



                                                            <button type="button"
                                                                class="btn btn-round btn-danger-rgba deleteData"
                                                                id="{{ $item['id'] }}" title="Delete"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </form>
                                                        <?php
                                                         }
                                                        ?>


                                                    </td>


                                                </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                </div>



                            </div>
                            <div class="tab-pane fade" id="profile-line" role="tabpanel"
                                aria-labelledby="profile-tab-line">
                                <form action="{{ URL::to('storeSchedule/') }}" method="post" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Doctor Name') }} <span
                                                class="text-danger">*</span></label>
                                        <div class="col-md-6">

                                            <select class="form-control" name="doctor_id" required>
                                                <option value="">Select</option>
                                                @foreach ($data['staff'] as $cat)

                                                    <option value="{{ $cat['id'] }}">
                                                        {{ $cat['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label
                                            class="col-md-3 control-label">{{ __('New Patient Consultation Fees') }}<span
                                                class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="consultation_fees" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Old Patient Consultation Fees') }}
                                            <span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="old_consultation_fees"
                                                required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Shift') }}</label>
                                        <div class="col-md-6">
                                            <div class="form-check">


                                                <input class="form-check-input" type="radio" name="shift" id="shift"
                                                    value="1" checked="yes">
                                                <label class="form-check-label">
                                                    Evining
                                                </label>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input class="form-check-input" type="radio" name="shift" id="shift"
                                                    value="2">
                                                <label class="form-check-label">
                                                    Morning
                                                </label>

                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        {{-- <label class="col-md-3 control-label">{{ __('Week Day') }}</label>
                                        <div class="col-md-6">

                                            <select class="select2-multi-select form-control" name="states[]"
                                                id="choice_attributes" style="width: 100%" name="choice_attributes[]"
                                                multiple="multiple">
                                                @php $days = App\Http\Helpers::get_days(); @endphp
                                                @foreach ($days as $day)
                                                    <option value="{{ $day }}"> {{ $day }}</option>
                                                @endforeach
                                            </select>

                                        </div> --}}



                                        <div class="card-body">

                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <label for="meta_title">Week Day <span
                                                            class="text-danger">*</span></label>
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="select2-multi-select form-control" name="states[]"
                                                        id="choice_attributes" style="width: 100%"
                                                        name="choice_attributes[]" multiple="multiple" required>
                                                        @php $days = App\Http\Helpers::get_days(); @endphp
                                                        @foreach ($days as $day)
                                                            <option value="{{ $day }}"> {{ $day }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="customer_choice_options" id="customer_choice_options">

                                            </div>

                                        </div>
                                    </div>




                                    {{-- <div class="form-group row">
                                        <label class="col-md-3 control-label">{{ __('Time Slot') }} <span
                                                class="text-danger">*</span></label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" id="inputmask-time" name="time_start"
                                                placeholder="hh:mm" required>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" id="inputmask-time-end"
                                                name="time_end" placeholder="hh:mm" required>

                                        </div>
                                    </div> --}}



                                    <footer class="panel-footer mt-md">
                                        <div class="row" style="float: right">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn btn-default btn-block"
                                                    name="app_setting" value="1">{{ __('Save') }}</button>
                                            </div>
                                        </div>
                                    </footer>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End Contentbar -->
    @endsection
    @section('script')
        <script>
            $(document).on("change", "#choice_attributes", function() {

                // alert('Hi');
                $('#customer_choice_options').html(null);
                $.each($("#choice_attributes option:selected"), function() {
                    add_more_customer_choice_option($(this).val(), $(this).text());
                });
                // update_sku();
            });

            function add_more_customer_choice_option(i, name) {
                $('#customer_choice_options').append(
                    '<div class="form-group row"><div class="col-md-3"><input type="hidden" name="choice_no[]" value="' +
                    i + '"><input type="text" class="form-control" name="choice[]" value="' + name +
                    '" placeholder="Choice Title" readonly></div><div class="col-md-2"><input type="text" id="inputmask-time-sun" class="form-control" name="choice_options_' +
                    i +
                    '[]" placeholder="hh:mm"></div><div class="col-md-2"><input type="text" id="inputmask-time-sat" class="form-control" name="end_time_' +
                    i +
                    '[]" placeholder="hh:mm"></div><div class="col-md-2"><input type="number"  class="form-control" name="duration_' +
                    i + '[]" placeholder="duration"></div></div>');
                // $("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
            }



            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                $(document).on("click", ".deleteData", function() {
                    var id = $(this).attr('id');
                    var base_url = $('#base_url').val();
                    // alert(id);
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "deleteSchedule",
                                // alert();
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    swal("Information has been deleted", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
                });
            });
        </script>
        <!-- Wysiwig js -->
        <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
        <!-- Summernote JS -->
        <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- Code Mirror JS -->
        <script src="{{ asset('assets/plugins/code-mirror/codemirror.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/htmlmixed.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/css.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/javascript.js') }}"></script>
        <script src="{{ asset('assets/plugins/code-mirror/xml.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-editor.js') }}"></script>


        <!-- Datatable js -->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>

        <!-- Sweet-Alert js -->
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-sweet-alert.js') }}"></script>

        <!-- Input Mask js -->
        <script src="{{ asset('assets/plugins/bootstrap-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-inputmask.js') }}"></script>

        <!-- Switchery js -->
        <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
        <!-- Select2 js -->
        <script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
        <!-- Tagsinput js -->
        <script src="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-tagsinput/typeahead.bundle.js') }}"></script>
        <script src="{{ asset('assets/js/custom/custom-form-select.js') }}"></script>
    @endsection
