<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style>
        .pline {
            line-height: 0.4;
        }

        @page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

        #footer {
            position: absolute;
            bottom: 2px;
        }

    </style>
</head>

<div style="height:842px; padding:0px; margin:0px 0px 0px; 0px;">
    <div style="background: seashell; width:100%; padding:5px; height: 150px;">
        <div style="float: left; width:33.33%">
            {{-- <p style="font-family: Kalpurush">দুই দিন পর আসবেন</p> --}}
            <h3 style="color: green; font-family: examplefont">{{ $doctorDetails['name'] }}</h3>
            <p class="pline">{{ $staff_department['name'] }}</p>
            <p class="pline">{!! $doctorDetails['qualification'] !!}</p>
            <p class="pline">{{ $doctorDetails['email'] }}</p>

        </div>
        <div style="text-align: right; float:right;  width:33.33%">
            <h4 style="color: green;">Chember Info :</h4>
            <p class="pline">{{ $chemberDetails[0]['institute_name'] }}</p>
            <p class="pline">{{ $chemberDetails[0]['address'] }}</p>
            <p class="pline">{{ $chemberDetails[0]['mobileno'] }}</p>
            <p class="pline">{{ $chemberDetails[0]['institute_email'] }}</p>
        </div>
        {{-- <div style="text-align: center; float:left;  width:33.33%">
             <h3 style="color: green;">{{ $doctorDetails['name'] }}</h3>
             <p class="pline">{{ $staff_department['name'] }}</p>
             <p class="pline">{{ $doctorDetails['qualification'] }}</p>
             <p class="pline">{{ $doctorDetails['email'] }}</p>
         </div> --}}
    </div>

    <div style="width:100%; margin-top:5px;">
        <div style=" width:60%; padding:5px; float:left;">
            <b> Patient Name:</b> {{ $patientDetails['name'] }}
        </div>
        <div style=" width:16%; padding:5px; float:left;">
            <b> Age:</b> {{ $patientDetails['age'] }}
        </div>

        <div style=" width:20%; padding:5px; float:left; text-align: right;">
            @php
                if ($patientDetails['sex'] == 1) {
                    $gender = 'Male';
                } else {
                    $gender = 'Female';
                }
            @endphp
            <b>Date:</b> {{ date('d-m-Y') }}
        </div>


    </div>
    <hr>

    <div style=" width:100%; ">
        <div style="background: seashell; height: 823px; float: left; width:33%;padding:5px;">


            <span style="font-weight:600;">Chief Complaints</span><br>
            <div style="padding-left: 15px !important; padding-top:8px;">
                <span style="font-size: 14px; background:#eee">
                    @if (!empty($prescriptionDetails['bp_high']))
                        BP: {{ $prescriptionDetails['bp_high'] . '/' . $prescriptionDetails['bp_low'] }}
                    @endif
                    @if (!empty($prescriptionDetails['pulse']))
                        Pulse: {{ $prescriptionDetails['pulse'] }}
                    @endif
                    @if (!empty($prescriptionDetails['temperature']))
                        Temperature: {{ $prescriptionDetails['temperature'] }}
                    @endif
                </span>
                {!! $prescriptionDetails['chief_complain'] !!}
            </div>
            <hr>


            <span style="font-weight:600;">Diagnosis Tests</span><br>
            <div style="padding-left: 15px !important; padding-top:8px;">
                {!! $prescriptionDetails['diagnosis'] !!}
            </div>
            <hr>
            <span style="font-weight:600;">Investigation</span><br>
            <div style="padding-left: 15px !important; padding-top:8px;">
                {!! $prescriptionDetails['investigation'] !!}
            </div>



        </div>

        <div style="height: 738px; float: left; width:65%; ">
            <div class="pline" style="margin-top: -5px;">
                <p class="pline" style="padding:5px;"><b>Rx.</b></p>

            </div>
            <div style="padding-left: 20px !important; line-height:0.3px; margin-left: 17px;">
                <?php foreach ($medicineList as $med) {?>

                <div class="pline" style=" margin-top: -20px;">
                    <h5 class="pline">{{ $med['medicine_name'] }}</h5>

                    <p class="pline">
                        {{ $med['does'] }} ( {{ $med['duration'] }} ) <span>
                            --- {{ $med['instruction'] }}
                        </span>
                    </p>

                </div>
                <?php   }
                ?>
            </div><br><br>
            <div style="padding-left: 20px !important; margin-left: 17px;">


                <span style=" font-size:16px;"> Advice:</span>
                <div style="margin-left: 27px;">

                    {!! $prescriptionDetails['advice'] !!}

                </div>


                <br>
                <?php if(!empty($prescriptionDetails['next_visit_date'])){?>
                <span style="font-size:12px;">
                    Next Visit Date: <?php echo date('d-m-Y', strtotime($prescriptionDetails['next_visit_date'])); ?>
                </span>
                <?php } ?>



            </div>


        </div>
    </div>
    <div id="footer" style="background: #eee; padding:5px; text-align:center;">
        For Appointment: 0111111122<br>
        Daily except Friday and Public Holiday
    </div>
</div>


<!-- Start JS -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset('assets/js/detect.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/vertical-menu.js') }}"></script>
<script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/js/report.js') }}"></script>
@yield('script')
<!-- Core JS -->
<script src="{{ asset('assets/js/core.js') }}"></script>
<!-- End JS -->
