@section('title')
    DASHBOARD
@endsection
@extends('layouts.main')
@section('style')
    <!-- Apex css -->
    <link href="{{ asset('assets/plugins/apexcharts/apexcharts.css') }}" rel="stylesheet" type="text/css" />
    <!-- Slick css -->
    <link href="{{ asset('assets/plugins/slick/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('rightbar-content')
    <!-- Start Contentbar -->
    <div class="contentbar">

        <!-- Start row -->
        <div class="row">
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/bitcoin.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Invoice</h5>
                                <p class="mb-0" style="text-align: right">0</p>
                                <hr>
                                Today Total: <span style="text-align: right">0</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/1.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/ethereum.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Commission</h5>
                                <p class="mb-0" style="text-align: right">0</p>
                                <hr>
                                Today Total: <span style="text-align: right">0</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/2.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/ripple.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Income</h5>
                                <p class="mb-0" style="text-align: right">0</p>
                                <hr>
                                Today Total: <span style="text-align: right">0</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/3.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="media">
                            <img class="mr-3 rounded-circle" src="assets/images/crypto/bcc.png"
                                alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mb-2">Expense</h5>
                                <p class="mb-0" style="text-align: right">0</p>
                                <hr>
                                Today Total: <span style="text-align: right">0</span>
                            </div>
                            <img class="action-bg rounded-circle" src="assets/images/crypto/4.png"
                                alt="Generic placeholder image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->
        <!-- Start row -->
        <div class="row">
            <!-- Start col -->
            <div class="col-lg-12 col-xl-6">

                <!-- Start row -->
                <div class="row">
                    <!-- Start col -->
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <h5 class="card-title mb-0">Revenue Statistics</h5>
                                    </div>
                                    <div class="col-3">
                                        <div class="dropdown">
                                            <button class="btn btn-link p-0 font-18 float-right" type="button"
                                                id="widgetRevenue" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false"><i class="feather icon-more-horizontal-"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="widgetRevenue">
                                                <a class="dropdown-item font-13" href="#">Refresh</a>
                                                <a class="dropdown-item font-13" href="#">Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body py-0">
                                <div class="row align-items-center">
                                    <div class="col-lg-3">
                                        <div class="revenue-box border-bottom mb-2">
                                            <h4>+ 4598</h4>
                                            <p>Inward Amount</p>
                                        </div>
                                        <div class="revenue-box">
                                            <h4>- 296</h4>
                                            <p>Outward Amount</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div id="apex-line-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End col -->
                    <!-- Start col -->
                    <div class="col-lg-6 col-xl-6">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="media">
                                    <span class="align-self-center mr-3 action-icon badge badge-secondary-inverse"><i
                                            class="feather icon-folder"></i></span>
                                    <div class="media-body">
                                        <p class="mb-0">Projects</p>
                                        <h5 class="mb-0">85</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End col -->
                    <!-- Start col -->
                    <div class="col-lg-6 col-xl-6">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="media">
                                    <span class="align-self-center mr-3 action-icon badge badge-secondary-inverse"><i
                                            class="feather icon-clipboard"></i></span>
                                    <div class="media-body">
                                        <p class="mb-0">Tasks</p>
                                        <h5 class="mb-0">259</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End col -->
                </div>
                <!-- End row -->
            </div>
            <!-- End col -->
            <!-- Start col -->
            <div class="col-lg-12 col-xl-6">
                <div class="card m-b-30 dash-widget">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-5">
                                <h5 class="card-title mb-0">Index</h5>
                            </div>
                            <div class="col-7">
                                <ul class="nav nav-pills float-right" id="pills-index-tab-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-sales-tab-justified" data-toggle="pill"
                                            href="#pills-sales-justified" role="tab" aria-selected="true">Sales</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-clients-tab-justified" data-toggle="pill"
                                            href="#pills-clients-justified" role="tab" aria-selected="false">Clients</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-0 pl-0 pr-2">
                        <div id="apex-bar-chart"></div>
                    </div>
                </div>
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->

        <!-- End row -->
    </div>
    <!-- End Contentbar -->
@endsection
@section('script')
    <!-- Apex js -->
    <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/apexcharts/irregular-data-series.js') }}"></script>
    <!-- Slick js -->
    <script src="{{ asset('assets/plugins/slick/slick.min.js') }}"></script>
    <!-- Custom Dashboard js -->
    <script src="{{ asset('assets/js/custom/custom-dashboard.js') }}"></script>
@endsection
