
$(document).on("click", "#stockReport", function () {
        var category_id = $('#category_id').val();
        var unit_id = $('#unit_id').val();
       
        //var baseUrl = $('#hidden_base_url').val();
        // alert(programName)
        if(category_id != '' && unit_id !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_stock_report",
                type: "post",
                data: { category_id:category_id, unit_id:unit_id},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#purchaseReport", function () {
        var payment_status = $('#payment_status').val();
        var supplier = $('#supplier').val();
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
        if (payment_status == "") {
            swal('Warning', "Please Select Payment Status", 'warning');
            return false;
        }
        if (supplier == "") {
            swal('Warning', "Please Select Supplier", 'warning');
            return false;
        }
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(payment_status != '' && supplier !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_purchase_report",
                type: "post",
                data: { supplier:supplier, payment_status:payment_status,fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#paymentReport", function () {
       
        var supplier = $('#supplier').val();
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
      
        if (supplier == "") {
            swal('Warning', "Please Select Supplier", 'warning');
            return false;
        }
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(supplier !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_payment_report",
                type: "post",
                data: { supplier:supplier,fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


     $(document).on("click", "#accountReport", function () {
        var account  = $('#account').val();
        var type     = $('#type').val();
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
        if (account == "") {
            swal('Warning', "Please Select Account", 'warning');
            return false;
        }
        if (type == "") {
            swal('Warning', "Please Select Type", 'warning');
            return false;
        }
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(account != '' && type !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_account_report",
                type: "post",
                data: { account:account, type:type,fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#incomeReport", function () {
       
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       

        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_income_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#expenseReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_expense_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

     $(document).on("click", "#transitionsReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_transitions_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

     $(document).on("click", "#incomevsexpenseReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_incomevsexpense_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


         $(document).on("click", "#due_billReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_due_bill_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#paid_billReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_paid_bill_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#due_collectReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_due_collect_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


             $(document).on("click", "#paid_billReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_paid_bill_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


         $(document).on("click", "#due_collectReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_due_collect_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


     $(document).on("click", "#createReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_create_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#report_listReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_report_list_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


     $(document).on("click", "#referral_statementReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_referral_statement_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

         $(document).on("click", "#commissionReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
        var staff    = $('#staff').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }

          if (staff == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_commission_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate,staff:staff},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

     $(document).on("click", "#commission_summary", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
        var staff    = $('#staff').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }

          if (staff == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_commission_summary_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate,staff:staff},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#payoutReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
        var staff    = $('#staff').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }

          if (staff == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_payout_report_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate,staff:staff},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#attendanceSet", function () {
        var fromDate = $('#autoclose-date').val();

        var staff_role    = $('#staff_role').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        

          if (staff_role == "") {
            swal('Warning', "Please Select Role", 'warning');
            return false;
        }
       
        if(fromDate != '' && staff_role !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_attendance_set_val",
                type: "post",
                data: { fromDate:fromDate,staff_role:staff_role},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#attandanceReport", function () {
        var fromDate = $('#month-view-date').val();

        var staff_role    = $('#staff_role').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        

          if (staff_role == "") {
            swal('Warning', "Please Select Role", 'warning');
            return false;
        }
       
        if(fromDate != '' && staff_role !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_attandance_report",
                type: "post",
                data: { fromDate:fromDate,staff_role:staff_role},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#salary_assignReport", function () {
        var designation = $('#designation').val();

        var staff_role    = $('#staff_role').val();
       
  
        if (designation == "") {
            swal('Warning', "Please Select Designation", 'warning');
            return false;
        }
        

          if (staff_role == "") {
            swal('Warning', "Please Select Role", 'warning');
            return false;
        }
       
        if(designation != '' && staff_role !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_salary_assign_report",
                type: "post",
                data: { designation:designation,staff_role:staff_role},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#salary_paymentReport", function () {
        var fromDate = $('#month-view-date').val();

        var staff_role    = $('#staff_role').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        

          if (staff_role == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && staff_role !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_salary_payment_report",
                type: "post",
                data: { fromDate:fromDate,staff_role:staff_role},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


    $(document).on("click", "#salary_summaryReport", function () {
        var fromDate = $('#month-view-date').val();

       
        if (fromDate == "") {
            swal('Warning', "Please Select Month", 'warning');
            return false;
        }
        

       
        if(fromDate != ''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_salary_summary_report",
                type: "post",
                data: { fromDate:fromDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });


        $(document).on("click", "#set_referReport", function () {
        var commission = $('#commission').val();

        var staff_role    = $('#staff_role').val();
       
  
        if (commission == "") {
            swal('Warning', "Please Select Commission", 'warning');
            return false;
        }
        

          if (staff_role == "") {
            swal('Warning', "Please Select Role", 'warning');
            return false;
        }
       
        if(commission != '' && staff_role !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_set_refer_report",
                type: "post",
                data: { commission:commission,staff_role:staff_role},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

      $(document).on("click", "#TestBillReport", function () {
       
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       

        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_test_bill_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

    $(document).on("click", "#consaltationReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
        var staff    = $('#staff').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }

          if (staff == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }
       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_consultation_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate,staff:staff},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });



      $(document).on("click", "#myCommisionReport", function () {
        var fromDate = $('#autoclose-date').val();
        var toDate   = $('#default-date').val();
       
  
        if (fromDate == "") {
            swal('Warning', "Please Select From Date", 'warning');
            return false;
        }
        if (toDate == "") {
            swal('Warning', "Please Select To Date", 'warning');
            return false;
        }

       
        if(fromDate != '' && toDate !=''){
            $("#ajax_loader").css('display','block');
            $.ajax({
                url: "show_my_commission_report",
                type: "post",
                data: { fromDate:fromDate,toDate:toDate},
                success: function(data){
                    //$("#ajax_loader").css('display','none');
                    // $("#ajax_loader").css('display','none');
                    $("#showReport").html(data);
                }
            });
        }else{
            swal("Required!", "You have to select both parameter", "warning");
        }
    });

       