<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/user-login', 'UsersController@login')->name('login');
// Route::post('/loginInfo', 'UsersController@loginInfo');
Route::get('/logout', 'UsersController@logout')->name('logout');

Auth::routes();

Route::middleware(['auth'])->group(function () {


    Route::get('/home', 'HomeController@index')->name('home');


    Route::post('/updatePassword/{id}', 'EmployeeController@updatePassword');
    Route::post('/ajax_consultation_view/', 'AppointmentController@ajax_consultation_view');

    //######### Global Settings #############
    Route::get('/globalSettings', 'SettingsController@index')->name('globalSettings');
    Route::post('globalSettings/edit/{id}', 'SettingsController@update');

    //######### Frontend Settings #############
    Route::get('/webSetting', 'SettingsController@webSetting')->name('webSetting');
    Route::post('webSetting/web_edit/{id}', 'SettingsController@wb_update');

    // ---------------- Menu -------------

    Route::get('/webMenu', 'SettingsController@webMenu')->name('webMenu');
    Route::post('storeMenu', 'SettingsController@storeMenu');
    Route::post('editMenu', 'SettingsController@editMenu')->name('editMenu');
    Route::post('webSetting/menuUpdate/{id}', 'SettingsController@menuUpdate');

    // ---------------- Manage page  -------------
    Route::get('/ManagePage', 'SettingsController@ManagePage')->name('ManagePage');
    Route::post('storeManagePage', 'SettingsController@storeManagePage');
    Route::post('editManagePage', 'SettingsController@editManagePage')->name('editManagePage');
    Route::post('webSetting/ManagePageUpdate/{id}', 'SettingsController@ManagePageUpdate');


    // ---------------- Slider  -------------
    Route::get('/SliderInfo', 'SettingsController@SliderInfo')->name('SliderInfo');
    Route::post('storeSliderInfo', 'SettingsController@storeSliderInfo');
    Route::post('editSliderInfo', 'SettingsController@editSliderInfo')->name('editSliderInfo');
    Route::post('SliderInfoUpdate/{id}', 'SettingsController@SliderInfoUpdate');
    Route::post('deleteSlider', 'SettingsController@destroySlider');


    // ---------------- Feature  -------------
    Route::get('/Feature', 'SettingsController@Feature')->name('Feature');
    Route::post('storeFeature', 'SettingsController@storeFeature');
    Route::post('editFeature', 'SettingsController@editFeature')->name('editFeature');
    Route::post('webSetting/FeatureUpdate/{id}', 'SettingsController@FeatureUpdate');
    Route::post('deleteFeature', 'SettingsController@destroyFeature');


    // ---------------- Testimonial  -------------
    Route::get('/Testimonial', 'SettingsController@Testimonial')->name('Testimonial');
    Route::post('storeTestimonial', 'SettingsController@storeTestimonial');
    Route::post('editTestimonial', 'SettingsController@editTestimonial')->name('editTestimonial');
    Route::post('webSetting/TestimonialUpdate/{id}', 'SettingsController@TestimonialUpdate');
    Route::post('deleteTestimonial', 'SettingsController@destroyTestimonial');


    // ---------------- Service  -------------
    Route::get('/Service', 'SettingsController@Service')->name('Service');
    Route::post('storeService', 'SettingsController@storeService');
    Route::post('editService', 'SettingsController@editService')->name('editService');
    Route::post('webSetting/ServiceUpdate/{id}', 'SettingsController@ServiceUpdate');
    Route::post('deleteService', 'SettingsController@destroyService');


    // ---------------- Faq  -------------
    Route::get('/Faq', 'SettingsController@Faq')->name('Faq');
    Route::post('storeFaq', 'SettingsController@storeFaq');
    Route::post('editFaq', 'SettingsController@editFaq')->name('editFaq');
    Route::post('webSetting/FaqUpdate/{id}', 'SettingsController@FaqUpdate');
    Route::post('deleteFaq', 'SettingsController@destroyFaq');

    // ################## Patient #################

    // ---------------- Patient create  -------------
    Route::get('/PatientList', 'PatientController@PatientList')->name('PatientList');
    Route::get('/PatientForm', 'PatientController@PatientForm')->name('PatientForm');
    Route::post('storePatient', 'PatientController@storePatient');
    Route::post('ProfileView', 'PatientController@ProfileView')->name('ProfileView');
    Route::post('PatientUpdate/{id}', 'PatientController@PatientUpdate');
    Route::post('deletePatient', 'PatientController@destroyPatient');
    Route::get('patientProfile', 'PatientController@patientProfile')->name('patientProfile');


    // ---------------- ctegory  -------------
    Route::get('/Category', 'PatientController@Category')->name('Category');
    Route::post('storeCategory', 'PatientController@storeCategory');
    Route::post('editCategory', 'PatientController@editCategory')->name('editCategory');
    Route::post('Patient/CategoryUpdate/{id}', 'PatientController@CategoryUpdate');
    Route::post('deleteCategory', 'PatientController@destroyCategory');

    Route::get('/DeactiveList', 'PatientController@DeactiveList')->name('DeactiveList');
    Route::post('activeUser', 'PatientController@activeUser');


    // ---------------- storePatientDocument  -------------
    Route::post('storePatientDocument', 'PatientController@storePatientDocument');
    Route::post('editFaq', 'SettingsController@editFaq')->name('editFaq');
    Route::post('deletePatientDocument', 'PatientController@deletePatientDocument');

    ######################## INVENTORY #################################

    // ---------------- Chemical  -------------
    Route::get('/Chemical', 'InventoryController@Chemical')->name('Chemical');
    Route::post('storeChemical', 'InventoryController@storeChemical');
    Route::post('editChemical', 'InventoryController@editChemical')->name('editChemical');
    Route::post('ChemicalUpdate/{id}', 'InventoryController@ChemicalUpdate');
    Route::post('deleteChemical', 'InventoryController@destroyChemical');


    // ---------------- Inventory Ctegory  -------------
    Route::get('/InvCategory', 'InventoryController@InvCategory')->name('InvCategory');
    Route::post('storeInvCategory', 'InventoryController@storeInvCategory');
    Route::post('editInvCategory', 'InventoryController@editInvCategory')->name('editInvCategory');
    Route::post('InvCategoryUpdate/{id}', 'InventoryController@InvCategoryUpdate');
    Route::post('deleteInvCategory', 'InventoryController@destroyInvCategory');


    // ---------------- Inventory Unit  -------------
    Route::get('/InvUnit', 'InventoryController@InvUnit')->name('InvUnit');
    Route::post('storeInvUnit', 'InventoryController@storeInvUnit');
    Route::post('editInvUnit', 'InventoryController@editInvUnit')->name('editInvUnit');
    Route::post('InvUnitUpdate/{id}', 'InventoryController@InvUnitUpdate');
    Route::post('deleteInvUnit', 'InventoryController@destroyInvUnit');

    // ---------------- Supplier  -------------
    Route::get('/InvSupplier', 'InventoryController@InvSupplier')->name('InvSupplier');
    Route::post('storeInvSupplier', 'InventoryController@storeInvSupplier');
    Route::post('editInvSupplier', 'InventoryController@editInvSupplier')->name('editInvSupplier');
    Route::post('InvSupplierUpdate/{id}', 'InventoryController@InvSupplierUpdate');
    Route::post('deleteInvSupplier', 'InventoryController@destroyInvSupplier');

    // ---------------- Stock  -------------
    Route::get('/ChemicalStock', 'InventoryController@ChemicalStock')->name('ChemicalStock');
    Route::post('storeChemicalStock', 'InventoryController@storeChemicalStock');
    Route::post('editChemicalStock', 'InventoryController@editChemicalStock')->name('editChemicalStock');
    Route::post('ChemicalStockUpdate/{id}', 'InventoryController@ChemicalStockUpdate');
    Route::post('deleteChemicalStock', 'InventoryController@destroyChemicalStock');


    // ---------------- ReagentAssigned  -------------
    Route::get('/ReagentAssigned', 'InventoryController@ReagentAssigned')->name('ReagentAssigned');
    Route::post('storeReagentAssigned', 'InventoryController@storeReagentAssigned');
    Route::post('editReagentAssigned', 'InventoryController@editReagentAssigned')->name('editReagentAssigned');
    Route::post('ReagentAssignedUpdate/{id}', 'InventoryController@ReagentAssignedUpdate');
    Route::post('deleteReagentAssigned', 'InventoryController@destroyReagentAssigned');

    // ---------------- Purchase  -------------
    Route::get('/Purchase', 'PurchaseController@Purchase')->name('Purchase');
    Route::post('storePurchase', 'PurchaseController@storePurchase');
    Route::post('editPurchase', 'PurchaseController@editPurchase')->name('editPurchase');
    Route::post('PurchaseUpdate/{id}', 'PurchaseController@PurchaseUpdate');
    Route::post('deletePurchase', 'PurchaseController@destroyPurchase');


    // ---------------- Schedule  -------------
    Route::get('/Schedule', 'ScheduleController@Schedule')->name('Schedule');
    Route::post('storeSchedule', 'ScheduleController@storeSchedule');
    Route::post('editSchedule', 'ScheduleController@editSchedule')->name('editSchedule');
    Route::post('ScheduleUpdate/{id}', 'ScheduleController@ScheduleUpdate');
    Route::post('deleteSchedule', 'ScheduleController@destroySchedule');


    // ---------------- Appointment  -------------
    Route::get('/Appointment', 'AppointmentController@index')->name('Appointment');
    Route::post('storeAppointment', 'AppointmentController@storeAppointment');
    Route::post('editAppointment', 'AppointmentController@editAppointment')->name('editAppointment');
    Route::post('AppointmentUpdate/{id}', 'AppointmentController@AppointmentUpdate');
    Route::post('deleteAppointment', 'AppointmentController@destroyAppointment');
    Route::post('UpdateStatus', 'AppointmentController@UpdateStatus');
    Route::get('/requested_list', 'AppointmentController@requested_list')->name('requested_list');
    Route::get('/consultation_report', 'AppointmentController@consultation_report');
    Route::post('/show_consultation_report', 'AppointmentController@show_consultation_report');

    Route::post('get_appointment_schedule', 'AppointmentController@get_appointment_schedule');
    Route::post('ajax_get_consultation_fee', 'AppointmentController@ajax_get_consultation_fee');


    Route::get('/myAppointment', 'AppointmentController@myAppointment')->name('myAppointment');

    // ---------------- Employee  -------------

    Route::get('/EmployeeForm', 'EmployeeController@EmployeeForm')->name('EmployeeForm');
    Route::post('storeEmployee', 'EmployeeController@storeEmployee');
    Route::get('/EmployeeList', 'EmployeeController@EmployeeList')->name('EmployeeList');
    Route::get('StaffProfileView/{uid}', 'EmployeeController@StaffProfileView')->name('StaffProfileView');
    Route::post('deleteStaff', 'EmployeeController@deleteStaff');
    Route::post('editEmployee', 'EmployeeController@editEmployee')->name('editEmployee');
    Route::post('EmployeeUpdate/{id}', 'EmployeeController@EmployeeUpdate');
    Route::post('deleteEmployee', 'EmployeeController@destroyEmployee');
    Route::post('verifyPassword', 'EmployeeController@verifyPassword');


    // ---------------- storeStaffDocument  -------------
    Route::post('storeStaffDocument', 'EmployeeController@storeStaffDocument');
    // Route::post('editFaq', 'SettingsController@editFaq')->name('editFaq');
    Route::post('deleteStaffDocument', 'EmployeeController@deleteStaffDocument');


    // ---------------- Designation  -------------
    Route::get('/Designation', 'EmployeeController@Designation')->name('Designation');
    Route::post('storeDesignation', 'EmployeeController@storeDesignation');
    Route::post('editDesignation', 'EmployeeController@editDesignation')->name('editDesignation');
    Route::post('DesignationUpdate/{id}', 'EmployeeController@DesignationUpdate');
    Route::post('deleteDesignation', 'EmployeeController@destroyDesignation');

    // ---------------- Designation  -------------
    Route::get('/Department', 'EmployeeController@Department')->name('Department');
    Route::post('storeDepartment', 'EmployeeController@storeDepartment');
    Route::post('editDepartment', 'EmployeeController@editDepartment')->name('editDepartment');
    Route::post('DepartmentUpdate/{id}', 'EmployeeController@DepartmentUpdate');
    Route::post('deleteDepartment', 'EmployeeController@destroyDepartment');

    ######################## PATHOLOGY #################################

    // ---------------- Lap Test  -------------
    Route::get('/LabTest', 'PathologyController@LabTest')->name('LabTest');
    Route::post('storeLabTest', 'PathologyController@storeLabTest');
    Route::post('editLabTest', 'PathologyController@editLabTest')->name('editLabTest');
    Route::post('LabTestUpdate/{id}', 'PathologyController@LabTestUpdate');
    Route::post('deleteLabTest', 'PathologyController@destroyLabTest');


    // ----------------  Ctegory  -------------
    Route::get('/PathologyCategory', 'PathologyController@PathologyCategory')->name('PathologyCategory');
    Route::post('storePathologyCategory', 'PathologyController@storePathologyCategory');
    Route::post('editPathologyCategory', 'PathologyController@editPathologyCategory')->name('editPathologyCategory');
    Route::post('PathologyCategoryUpdate/{id}', 'PathologyController@PathologyCategoryUpdate');
    Route::post('deletePathologyCategory', 'PathologyController@destroyPathologyCategory');

    // ----------------  Account  -------------
    Route::get('/Account', 'AccountController@Account')->name('Account');
    Route::post('storeAccount', 'AccountController@storeAccount');
    Route::post('editAccount', 'AccountController@editAccount')->name('editAccount');
    Route::post('AccountUpdate/{id}', 'AccountController@AccountUpdate');
    Route::post('deleteAccount', 'AccountController@destroyAccount');

    // ----------------  Voucher  -------------
    Route::get('/Voucher', 'AccountController@Voucher')->name('Voucher');
    Route::post('storeVoucher', 'AccountController@storeVoucher');
    Route::post('editVoucher', 'AccountController@editVoucher')->name('editVoucher');
    Route::post('VoucherUpdate/{id}', 'AccountController@VoucherUpdate');
    Route::post('deleteVoucher', 'AccountController@destroyVoucher');

    // ----------------  Voucher Head  -------------
    Route::get('/VoucherHead', 'AccountController@VoucherHead')->name('VoucherHead');
    Route::post('storeVoucherHead', 'AccountController@storeVoucherHead');
    Route::post('editVoucherHead', 'AccountController@editVoucherHead')->name('editVoucherHead');
    Route::post('VoucherHeadUpdate/{id}', 'AccountController@VoucherHeadUpdate');
    Route::post('deleteVoucherHead', 'AccountController@destroyVoucherHead');

    ######################## HR #################################

    // ----------------  Leave Category  -------------
    Route::get('/leaveCat', 'HrController@leaveCat')->name('leaveCat');
    Route::post('storeleaveCat', 'HrController@storeleaveCat');
    Route::post('editleaveCat', 'HrController@editleaveCat')->name('editleaveCat');
    Route::post('leaveCatUpdate/{id}', 'HrController@leaveCatUpdate');
    Route::post('deleteleaveCat', 'HrController@destroyleaveCat');

    // ----------------  Leave Manage  -------------
    Route::get('/LeaveManage', 'HrController@LeaveManage')->name('LeaveManage');
    Route::post('storeLeaveManage', 'HrController@storeLeaveManage');
    Route::post('updateStafLeave', 'HrController@updateStafLeave');
    Route::post('editLeaveManage', 'HrController@editLeaveManage')->name('editLeaveManage');
    Route::post('LeaveManageUpdate/{id}', 'HrController@LeaveManageUpdate');
    Route::post('deleteLeaveManage', 'HrController@destroyLeaveManage');


    // ----------------  My Leave  -------------
    Route::get('/MyLeave', 'HrController@MyLeave')->name('MyLeave');
    Route::post('storeMyLeave', 'HrController@storeMyLeave');
    Route::post('editMyLeave', 'HrController@editMyLeave')->name('editMyLeave');
    Route::post('MyLeaveUpdate/{id}', 'HrController@MyLeaveUpdate');
    Route::post('deleteMyLeave', 'HrController@destroyMyLeave');


    //################# REPORT ##################
    //----------------- Inventory Report ---------
    Route::get('/stockReport', 'ReportController@stockReport')->name('stockReport');
    Route::post('show_stock_report', 'ReportController@show_stock_report');

    //----------------- Purchase Report ---------
    Route::get('/purchaseReport', 'ReportController@purchaseReport')->name('purchaseReport');
    Route::post('show_purchase_report', 'ReportController@show_purchase_report');

    //----------------- Payment Report ---------
    Route::get('/paymentReport', 'ReportController@paymentReport')->name('paymentReport');
    Route::post('show_payment_report', 'ReportController@show_payment_report');

    //----------------- Account Statement Report ---------
    Route::get('/accountReport', 'ReportController@accountReport')->name('accountReport');
    Route::post('show_account_report', 'ReportController@show_account_report');


    //----------------- Income Statement Report ---------
    Route::get('/incomeReport', 'ReportController@incomeReport')->name('incomeReport');
    Route::post('show_income_report', 'ReportController@show_income_report');

    //----------------- Expense Statement Report ---------
    Route::get('/expenseReport', 'ReportController@expenseReport')->name('expenseReport');
    Route::post('show_expense_report', 'ReportController@show_expense_report');

    //----------------- Transitions Report ---------
    Route::get('/transitionReport', 'ReportController@transitionReport')->name('transitionReport');
    Route::post('show_transitions_report', 'ReportController@show_transition_report');

    //----------------- incomevsexpense Report ---------
    Route::get('/incomevsexpenseReport', 'ReportController@incomevsexpenseReport')->name('incomevsexpenseReport');
    Route::post('show_incomevsexpense_report', 'ReportController@show_incomevsexpense_report');

    //----------------- balance_sheet Report ---------
    Route::get('show_balance_sheet_report', 'ReportController@show_balance_sheet_report');

    //----------------- Paid Bill Report ---------
    Route::get('/paid_billReport', 'ReportController@paid_billReport')->name('paid_billReport');
    Route::post('show_paid_bill_report', 'ReportController@show_paid_bill_report');

    //----------------- Due Bill Report ---------
    Route::get('/due_billReport', 'ReportController@due_billReport')->name('due_billReport');
    Route::post('show_due_bill_report', 'ReportController@show_due_bill_report');

    //----------------- Due Collect Report ---------
    Route::get('/due_collectReport', 'ReportController@due_collectReport')->name('due_collectReport');
    Route::post('show_due_collect_report', 'ReportController@show_due_collect_report');


    //----------------- Create Report ---------
    Route::get('/createReport', 'ReportController@createReport')->name('createReport');
    Route::post('show_create_report', 'ReportController@show_create_report');
    Route::get('/create_investigation/{uid}', 'PathologyController@create_investigation');
    Route::post('get_template_details', 'PathologyController@get_template_details');
    Route::post('updateInvestigation', 'PathologyController@updateInvestigation');
    Route::get('/print_investigation/{uid}', 'PathologyController@print_investigation');

    Route::get('/test_bill_invoice/{uid}', 'PathologyController@test_bill_invoice');
    Route::post('storePayment', 'PathologyController@storePayment');

    //-----------------  Report List ---------
    Route::get('/report_listReport', 'ReportController@report_listReport')->name('report_listReport');
    Route::post('show_report_list_report', 'ReportController@show_report_list_report');

    // ---------------- Template   -------------
    Route::get('/Template', 'SettingsController@Template')->name('Template');
    Route::post('storeTemplate', 'SettingsController@storeTemplate');
    Route::post('editTemplate', 'SettingsController@editTemplate')->name('editTemplate');
    Route::post('TemplateUpdate/{id}', 'SettingsController@TemplateUpdate');
    Route::post('deleteTemplate', 'SettingsController@destroyTemplate');


    //-----------------  referral Statement ---------
    Route::get('/referral_statementReport', 'ReportController@referral_statementReport')->name('referral_statementReport');
    Route::post('show_referral_statement_report', 'ReportController@show_referral_statement_report');

    //-----------------  commission_report ---------
    Route::get('/commissionReport', 'ReportController@commissionReport')->name('commissionReport');
    Route::post('show_commission_report', 'ReportController@show_commission_report');

    //-----------------  commission_report ---------
    Route::get('/myCommission', 'ReportController@myCommission')->name('myCommission');
    Route::post('show_my_commission_report', 'ReportController@show_my_commission_report');

    //-----------------  commision_summary ---------
    Route::get('/commission_summaryReport', 'ReportController@commission_summaryReport')->name('commission_summaryReport');
    Route::post('show_commission_summary_report', 'ReportController@show_commission_summary_report');

    //-----------------  referral Statement ---------
    Route::get('/payout_reportReport', 'ReportController@payout_reportReport')->name('payout_reportReport');
    Route::post('show_payout_report_report', 'ReportController@show_payout_report_report');

    //----------------- Attendance Set ---------
    Route::get('/AttendanceSet', 'AttendanceController@AttendanceSet')->name('AttendanceSet');
    Route::post('show_attendance_set_val', 'AttendanceController@show_attendance_set_val');
    Route::post('AddAttandance', 'AttendanceController@AddAttandance');

    //-----------------  Attendance Report ---------
    Route::get('/attandanceReport', 'ReportController@attandanceReport')->name('attandanceReport');
    Route::post('show_attandance_report', 'ReportController@show_attandance_report');

    //-----------------  salary_assign Report ---------
    Route::get('/salary_assignReport', 'ReportController@salary_assignReport')->name('salary_assignReport');
    Route::post('show_salary_assign_report', 'ReportController@show_salary_assign_report');

    //-----------------  salary_payment Report ---------
    Route::get('/salary_paymentReport', 'ReportController@salary_paymentReport')->name('salary_paymentReport');
    Route::post('show_salary_payment_report', 'ReportController@show_salary_payment_report');

    //-----------------  salary_summary Report ---------
    Route::get('/salary_summaryReport', 'ReportController@salary_summaryReport')->name('salary_summaryReport');
    Route::post('show_salary_summary_report', 'ReportController@show_salary_summary_report');
    Route::post('storeSalaryTemplate', 'ReportController@storeSalaryTemplate');


    // ---------------- SalaryTemplates  -------------
    Route::get('/SalaryTemplates', 'HrController@SalaryTemplates')->name('SalaryTemplates');
    Route::post('storeSalaryTemplates', 'HrController@storeSalaryTemplates');
    Route::post('editSalaryTemplates', 'HrController@editSalaryTemplates')->name('editSalaryTemplates');
    Route::post('SalaryTemplatesUpdate/{id}', 'HrController@SalaryTemplatesUpdate');
    Route::post('deleteSalaryTemplates', 'HrController@destroySalaryTemplates');


    //-----------------  setRefer  ---------
    Route::get('/set_referReport', 'ReportController@set_referReport')->name('set_referReport');
    Route::post('show_set_refer_report', 'ReportController@show_set_refer_report');
    Route::post('storeSetRefer', 'ReportController@storeSetRefer');

    //-----------------  ReferList  ---------
    Route::get('/ReferList', 'ReportController@ReferList')->name('ReferList');

    // ---------------- Withdrawal (In Refer)  -------------
    Route::get('/Withdrawal', 'RefererController@withdrawal')->name('Withdrawal');
    Route::post('storeWithdrawal', 'RefererController@storeWithdrawal');
    Route::post('editWithdrawal', 'RefererController@editWithdrawal')->name('editWithdrawal');
    Route::post('get_staff_balance', 'RefererController@get_staff_balance')->name('get_staff_balance');
    Route::post('WithdrawalUpdate/{id}', 'RefererController@WithdrawalUpdate');
    Route::post('deleteWithdrawal', 'RefererController@destroyWithdrawal');


    // ---------------- Role/Permission -------------
    Route::get('/Role', 'RoleController@Role')->name('Role');
    Route::post('storeRole', 'RoleController@storeRole');
    Route::get('/editRole/{uid}', 'RoleController@editRole');
    // Route::post('editRole', 'RoleController@editRole')->name('editRole');
    Route::post('RoleUpdate/{id}', 'RoleController@RoleUpdate');
    Route::post('deleteRole', 'RoleController@destroyRole');

    Route::get('/permission/{uid}', 'RoleController@permission');
    Route::post('updatePermission', 'RoleController@updatePermission');

    // ---------------- Role/Permission -------------
    Route::get('/TestBill', 'PathologyController@TestBill')->name('Role');
    Route::post('storeTestBill', 'PathologyController@storeTestBill');

    Route::post('get_labtest_by_category', 'PathologyController@get_labtest_by_category');
    Route::post('get_labtest_price', 'PathologyController@get_labtest_price');

    //-----------------  Test Bill List ---------
    Route::get('/TestBillListReport', 'ReportController@TestBillListReport')->name('TestBillListReport');
    Route::post('show_test_bill_report', 'ReportController@show_test_bill_report');


    // ---------------- Prescription  -------------
    Route::get('/Prescription', 'PrescriptionController@Prescription')->name('Prescription');
    Route::post('StorePrescription', 'PrescriptionController@StorePrescription');
    Route::post('writePrescription', 'PrescriptionController@writePrescription')->name('writePrescription');
    Route::post('PrescriptionUpdate/{id}', 'PrescriptionController@PrescriptionUpdate');
    Route::post('deletePrescription', 'PrescriptionController@destroyPrescription');

    Route::post('/ajax_print_preview_prescription/', 'PrescriptionController@ajax_print_preview_prescription');
    Route::get('/get_medicine_name/', 'PrescriptionController@get_medicine_name');
    Route::post('/ajax_bpSet_view/', 'PrescriptionController@ajax_bpSet_view');

    Route::get('/todayQueue', 'PrescriptionController@todayQueue')->name('todayQueue');
    Route::get('writePrescriptionSave', 'PrescriptionController@writePrescriptionSave')->name('writePrescriptionSave');
    Route::get('prescriptionPdf/{uid}', 'PrescriptionController@prescriptionPdf')->name('prescriptionPdf');
});

// ---------------- Website (Frontend)  -------------
Route::get('/', 'WebController@index');
Route::post('StorePrescription', 'PrescriptionController@StorePrescription');
Route::post('writePrescription', 'PrescriptionController@writePrescription')->name('writePrescription');
Route::post('PrescriptionUpdate/{id}', 'PrescriptionController@PrescriptionUpdate');
Route::post('deletePrescription', 'PrescriptionController@destroyPrescription');
Route::post('loginDupCheck', 'PatientController@loginDupCheck');
Route::post('storePatientForAppointment', 'PatientController@storePatientForAppointment');


Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});


// Route::get('/home', function () {
//     return view('index');
// });


// Route::get('/home', 'HomeController@index')->name('home');
