<?php

namespace App\Models;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Custom_model extends Model
{

    public static function get_stock_data($category_id, $unit_id)
    {
        $where1 = '';
        $where2 = '';
        if ($category_id != 'all') {
            $where1 = 'chemicals . category_id =' . $category_id;
        }

        if ($unit_id != 'all') {
            $where2 = 'AND chemicals.sales_unit_id = ' . $unit_id;
        }

        $sql = DB::select(
            "SELECT chemicals.*,chemical_units.name as unit_name,chemical_categories.name as category_name,chemical_stocks.stock_quantity AS in_stock "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM chemicals LEFT JOIN chemical_categories ON chemical_categories.id = chemicals.category_id "
                . "LEFT JOIN chemical_units ON chemical_units.id = chemicals.sales_unit_id "
                . "LEFT JOIN chemical_stocks ON chemical_stocks.chemical_id = chemicals.id "
                . "$where1 "
                . "$where2"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_purchase_data($supplier, $payment_status, $fromDate, $toDate)
    {
        $where1 = '';
        $where2 = '';
        if ($supplier != 'all') {
            $where1 = 'purchase_bills.supplier_id =' . $supplier;
        }

        if ($payment_status != 'all') {
            $where2 = ' AND purchase_bills.payment_status = ' . $payment_status;
        }

        $sql = DB::select(
            "SELECT purchase_bills.*,purchase_bills.total - purchase_bills.discount as net_payable,suppliers.name as supplier_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM purchase_bills LEFT JOIN suppliers ON purchase_bills.supplier_id = suppliers.id "
                . "WHERE (purchase_bills.date BETWEEN '$fromDate' AND '$toDate') "
                . "$where1 "
                . "$where2"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_payment_data($supplier, $fromDate, $toDate)
    {
        $where1 = '';

        if ($supplier != 'all') {
            $where1 = 'purchase_bills.supplier_id =' . $supplier;
        }

        $sql = DB::select(
            "SELECT purchase_payment_historys.*,purchase_bills.bill_no,purchase_bills.id as bill_id,purchase_bills.hash,suppliers.name as supplier_name,payment_types.name as paidvia "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM purchase_payment_historys INNER JOIN purchase_bills ON purchase_bills.id = purchase_payment_historys.purchase_bill_id "
                . "LEFT JOIN payment_types ON payment_types.id = purchase_payment_historys.pay_via "
                . "LEFT JOIN suppliers ON suppliers.id = purchase_bills.supplier_id "
                . "WHERE (purchase_payment_historys.paid_on BETWEEN '$fromDate' AND '$toDate') "
                . "$where1 "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_account_data($account_id, $type, $fromDate, $toDate)
    {
        $where1 = 'WHERE ';

        if ($type != 'all') {
            $where1 = " WHERE transactions.type = '$type' AND ";
        }

        $sql = DB::select(
            "SELECT transactions.*,voucher_heads.name as v_head "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM transactions LEFT JOIN voucher_heads ON voucher_heads.id =  transactions.voucher_head_id "
                . "$where1 "
                . "transactions.account_id = $account_id AND (transactions.date BETWEEN '$fromDate' AND '$toDate') "

            // . "ORDER BY transactions.id"


        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_income_expense_data($fromDate, $toDate, $type)
    {

        $sql = DB::select(
            "SELECT transactions.*,accounts.name as ac_name,voucher_heads.name as v_head,payment_types.name as via_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM transactions LEFT JOIN accounts ON accounts.id = transactions.account_id "
                . "LEFT JOIN voucher_heads ON voucher_heads.id = transactions.voucher_head_id "
                . "LEFT JOIN payment_types ON payment_types.id = transactions.pay_via "
                . "WHERE (transactions.date BETWEEN '$fromDate' AND '$toDate') AND transactions.type = '$type' "
                . "ORDER BY transactions.id"


        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_transitions_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT transactions.*, accounts.name as ac_name, voucher_heads.name as v_head, payment_types.name as via_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM transactions LEFT JOIN accounts ON accounts.id = transactions.account_id "
                . "LEFT JOIN voucher_heads ON voucher_heads.id = transactions.voucher_head_id "
                . "LEFT JOIN payment_types ON payment_types.id = transactions.pay_via "
                . "WHERE (transactions.date BETWEEN '$fromDate' AND '$toDate') "
                . "ORDER BY transactions.id"


        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_incomevsexpense_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT transactions.*, voucher_heads.name as v_head, transactions.dr as total_dr, transactions.cr as total_cr "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM voucher_heads LEFT JOIN transactions ON transactions.voucher_head_id = voucher_heads.id "

                . "WHERE (transactions.date BETWEEN '$fromDate' AND '$toDate') "
                . "ORDER BY transactions.id"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_balance_sheet_data()
    {

        $sql = DB::select(
            "SELECT transactions.*,transactions.dr as total_dr,transactions.cr as total_cr,accounts.name as ac_name,accounts.balance as fbalance "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM accounts LEFT JOIN transactions ON transactions.account_id = accounts.id "
                . "ORDER BY transactions.id"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_due_bill_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT labtest_bills.*,(labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount) as net_amount,patients.name as patient_name,staffs.name as referral_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_bills LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_bills.status != 3 "
                . "ORDER BY labtest_bills.id"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_paid_bill_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT labtest_bills.*,(labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount) as net_amount,patients.name as patient_name,staffs.name as referral_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_bills LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_bills.status = 3 "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_due_collect_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT labtest_payment_histories.*,labtest_bills.bill_no,labtest_bills.id as bill_id,labtest_bills.date as bill_date,labtest_bills.hash,labtest_bills.status,
		labtest_bills.paid,labtest_bills.due,patients.name as patient_name,patients.patient_id, staffs.name as collect_by,payment_types.name as pay_via "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_payment_histories LEFT JOIN labtest_bills ON labtest_bills.id = labtest_payment_histories.labtest_bill_id "
                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_payment_histories.collect_by "
                . "LEFT JOIN payment_types ON payment_types.id = labtest_payment_histories.method_id "

                . "WHERE (labtest_payment_histories.paid_on BETWEEN '$fromDate' AND '$toDate') AND labtest_payment_histories.coll_type = 1 "
                . "ORDER BY labtest_payment_histories.id"


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_pending_report_data($fromDate, $toDate)
    {
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole == 7) {
            $user_id = $userId['user_id'];
            $where = "WHERE labtest_bills.patient_id = '$user_id' AND";
        } else {
            $where = 'WHERE ';
        }


        $sql = DB::select(
            "SELECT labtest_reports.*,labtest_bills.id as billid,labtest_bills.bill_no,labtest_bills.date as bill_date,patients.name as patient_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_reports LEFT JOIN labtest_bills ON labtest_bills.id = labtest_reports.labtest_bill_id "
                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "

                . "$where (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_reports.status = 2 "
                . "ORDER BY labtest_reports.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_pending_report2_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT labtest_reports.*,labtest_bills.id as billid,labtest_bills.bill_no,labtest_bills.date as bill_date,patients.name as patient_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_reports LEFT JOIN labtest_bills ON labtest_bills.id = labtest_reports.labtest_bill_id "
                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_reports.status = 1 "
                . "ORDER BY labtest_reports.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_referral_statement_data($fromDate, $toDate)
    {

        $sql = DB::select(
            "SELECT labtest_bills.id as invoice_q,labtest_bills.commission as total_commission,labtest_bills.total as total,labtest_bills.total+labtest_bills.tax_amount-labtest_bills.discount as net_amount,staffs.name as staff_name,staffs.staff_id as staffid,staff_departments.name as department_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_bills LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') "
                . "ORDER BY labtest_bills.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_commission_data($fromDate, $toDate, $staff)
    {

        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total+labtest_bills.tax_amount-labtest_bills.discount as net_amount,staffs.name as staff_name,staffs.staff_id as staffid,patients.name as patient_name,patients.patient_id as patientid "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_bills LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "
                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_bills.referral_id = $staff "
                . "ORDER BY labtest_bills.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_commission_summary_data($fromDate, $toDate, $staff)
    {

        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total+labtest_bills.tax_amount-labtest_bills.discount as net_amount,staffs.name as staff_name,staffs.staff_id as staffid,patients.name as patient_name,patients.patient_id as patientid "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_bills LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "
                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') AND labtest_bills.referral_id = $staff "
                . "ORDER BY labtest_bills.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_payout_report_data($fromDate, $toDate, $staff)
    {

        $sql = DB::select(
            "SELECT payout_commissions.*,payment_types.name as payvia,staffs.name as staff_name,staffs.staff_id as staffid,staff_departments.name as department_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM payout_commissions LEFT JOIN payment_types ON payment_types.id = payout_commissions.pay_via "
                . "LEFT JOIN staffs ON staffs.id = payout_commissions.staff_id "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "

                . "WHERE (payout_commissions.created_at BETWEEN '$fromDate' AND '$toDate') AND payout_commissions.staff_id = $staff "
                . "ORDER BY payout_commissions.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_attendance_set_data($fromDate, $staff)
    {

        $sql = DB::select(
            "SELECT staffs.id, staffs.name as staff_name, staffs.staff_id as staffid, staff_attendances.id as atten_id, staff_attendances.date,staff_attendances.status as att_status, staff_attendances.status, staff_attendances.remark "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staffs LEFT JOIN staff_attendances ON staff_attendances.staff_id = staffs.id AND staff_attendances.date = '$fromDate' "
                . "LEFT JOIN users ON users.user_id = staffs.id AND users.role != 7 "

                . "WHERE users.role = $staff AND users.active = 1 "
                . "ORDER BY staffs.id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_attendance_by_date($staff, $Date)
    {

        $sql = DB::select(
            "SELECT staff_attendances.* "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staff_attendances "

                . "WHERE staff_id = $staff AND date = '$Date' "
                . "ORDER BY id"

            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_employee_data($staff, $designation)
    {

        $sql = DB::select(
            "SELECT staffs.*,staff_designations.name as designation_name,staff_departments.name as department_name,users.role as role_id, roles.name as role "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staffs INNER JOIN users ON users.user_id = staffs.id "
                . "LEFT JOIN roles ON users.role = roles.id "
                . "LEFT JOIN staff_designations ON staff_designations.id = staffs.designation "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "

                . "WHERE users.active = 1 AND staffs.designation = '$designation'"


            // . "GROUP BY chemical_stocks.stock_quantity"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_employee_payment_data($staffRoll, $month, $year)
    {

        $sql = DB::select(
            "SELECT staffs.*,staff_designations.name as designation_name,staff_departments.name as department_name,users.role as role_id, roles.name as role, payslips.id as salary_id, payslips.hash as salary_hash,salary_templates.name as template_name, salary_templates.basic_salary "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staffs INNER JOIN users ON users.user_id = staffs.id "
                . "LEFT JOIN roles ON users.role = roles.id "
                . "LEFT JOIN staff_designations ON staff_designations.id = staffs.designation "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "
                . "LEFT JOIN payslips ON payslips.staff_id = staffs.id AND payslips.month ='$month' AND payslips.year = '$year' "
                . "LEFT JOIN salary_templates ON salary_templates.id = staffs.salary_template_id "

                . "WHERE users.active = 1 AND users.role = '$staffRoll' AND staffs.salary_template_id != 0 "



        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_summary_data($month, $year)
    {

        $sql = DB::select(
            "SELECT payslips.*,staffs.name as staff_name,staffs.mobileno,staff_designations.name as designation_name,staff_departments.name as department_name,payment_types.name as payvia "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM payslips LEFT JOIN staffs ON payslips.staff_id = staffs.id "

                . "LEFT JOIN staff_designations ON staff_designations.id = staffs.designation "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "
                . "LEFT JOIN payment_types ON payment_types.id = payslips.pay_via "

                . "WHERE payslips.month = '$month' AND payslips.year ='$year' "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_staff_by_role($staffRoll, $commision)
    {

        $sql = DB::select(
            "SELECT staffs.*,staff_designations.name as designation_name,staff_departments.name as department_name,users.role as role_id,IFNULL(referral_commissions.id, 0) as commission_id,referral_commissions.percentage as percentage "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staffs LEFT JOIN users ON users.user_id = staffs.id AND users.role != '7' "

                . "LEFT JOIN staff_designations ON staff_designations.id = staffs.designation "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "
                . "LEFT JOIN referral_commissions ON referral_commissions.staff_id = staffs.id AND referral_commissions.test_id ='$commision' "

                . "WHERE users.active = 1 AND users.role = '$staffRoll' "

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_staff_commission_list()
    {

        $sql = DB::select(
            "SELECT referral_commissions.*, staffs.staff_id as staffid, staffs.name as staff_name, staff_designations.name as designation_name, staff_departments.name as department_name, users.role as role_id, roles.name as role, lab_tests.name as test_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM staffs LEFT JOIN referral_commissions ON referral_commissions.staff_id = staffs.id "

                . "LEFT JOIN users ON users.user_id = staffs.id AND users.role != '7' "
                . "LEFT JOIN roles ON users.role = roles.id "
                . "LEFT JOIN staff_designations ON staff_designations.id = staffs.designation "
                . "LEFT JOIN staff_departments ON staff_departments.id = staffs.department "
                . "LEFT JOIN lab_tests ON referral_commissions.test_id = lab_tests.id "

                . "WHERE users.active = 1 "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function check_permission($module_id, $role_id)
    {

        $sql = DB::select(
            "SELECT permissions.*, staff_privileges.id as staff_privileges_id,staff_privileges.is_add,staff_privileges.is_edit,staff_privileges.is_view,staff_privileges.is_delete "
                . "FROM permissions LEFT JOIN staff_privileges ON staff_privileges.permission_id = permissions.id AND staff_privileges.role_id = '$role_id' "

                . "WHERE permissions.module_id = '$module_id' "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_staff_permissions($role_id)
    {

        $sql = DB::select(
            "SELECT staff_privileges.*, permissions.id as permission_id,permissions.prefix as permission_prefix "
                . "FROM staff_privileges LEFT JOIN permissions ON staff_privileges.permission_id = permissions.id "

                . "WHERE staff_privileges.role_id = '$role_id' "

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_bill_list_date_wise($fromDate, $toDate)
    {
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole == 7) {
            $user_id = $userId['id'];
            $where = "AND labtest_bills.patient_id = '$user_id'";
        } else {
            $where = '';
        }

        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount as net_amount,patients.name as patient_name,patients.patient_id,staffs.name as referral_name,labtest_reports.delivery_date,labtest_reports.delivery_time,labtest_reports.status as delivery_status "

                . "FROM labtest_bills INNER JOIN labtest_reports ON labtest_reports.labtest_bill_id = labtest_bills.id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') $where "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_bill_list()
    {
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole == 7) {
            $user_id = $userId['id'];
            $where = "WHERE labtest_bills.patient_id = '$user_id'";
        } else {
            $where = '';
        }

        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount as net_amount,patients.name as patient_name,patients.patient_id,staffs.name as referral_name,labtest_reports.delivery_date,labtest_reports.delivery_time,labtest_reports.status as delivery_status "

                . "FROM labtest_bills INNER JOIN labtest_reports ON labtest_reports.labtest_bill_id = labtest_bills.id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . " $where "

        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_test_details($id)
    {
        $sql = DB::select(
            "SELECT labtest_reports.*,labtest_bills.id as billid,labtest_bills.bill_no,labtest_bills.date as bill_date,patients.name as patient_name,patients.sex,patients.age,patients.mobileno,patient_categories.name as cname,staffs.name as ref_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_reports INNER JOIN labtest_bills ON labtest_bills.id = labtest_reports.labtest_bill_id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN patient_categories ON patient_categories.id = patients.category_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE labtest_reports.id = $id AND labtest_reports.status = 1 "

        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_bill_list_individual($id)
    {
        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount as net_amount,patients.name as patient_name,patients.patient_id,staffs.name as referral_name,labtest_reports.delivery_date,labtest_reports.delivery_time,labtest_reports.status as delivery_status "

                . "FROM labtest_bills INNER JOIN labtest_reports ON labtest_reports.labtest_bill_id = labtest_bills.id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE labtest_bills.patient_id = $id "


                . "ORDER BY labtest_bills.id "

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_test_print_details($id)
    {
        $sql = DB::select(
            "SELECT labtest_reports.*,labtest_bills.id as billid,labtest_bills.bill_no,labtest_bills.date as bill_date,patients.name as patient_name,patients.sex,patients.age,patients.mobileno,patient_categories.name as cname,staffs.name as ref_name "
                // . "SUM(chemical_stocks.stock_quantity) AS in_stock "
                . "FROM labtest_reports INNER JOIN labtest_bills ON labtest_bills.id = labtest_reports.labtest_bill_id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                . "LEFT JOIN patient_categories ON patient_categories.id = patients.category_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "


                . "WHERE labtest_reports.id = $id AND labtest_reports.status = 2 "

        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_labtest_bill($id)
    {
        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total-labtest_bills.discount+labtest_bills.tax_amount as net_amount,patients.name as p_name,patients.patient_id,patients.mobileno as p_mobileno,patients.address as p_address,patients.age as p_age,patients.sex as p_sex,patient_categories.name as p_category,staffs.staff_id,staffs.name as referral_name "

                . "FROM labtest_bills LEFT JOIN patients ON patients.id = labtest_bills.patient_id "

                . "LEFT JOIN patient_categories ON patient_categories.id = patients.category_id "
                . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "


                . "WHERE labtest_bills.id = $id "


        );
        return json_decode(json_encode($sql), true);
    }


    public static function get_labtest_bill_details($id)
    {
        $sql = DB::select(
            "SELECT labtest_bill_details.*,lab_tests.name as test_name,lab_test_categories.name as test_category "

                . "FROM labtest_bill_details LEFT JOIN lab_tests ON lab_tests.id = labtest_bill_details.test_id "

                . "LEFT JOIN lab_test_categories ON lab_test_categories.id = labtest_bill_details.category_id "

                . "WHERE labtest_bill_details.labtest_bill_id = $id "
                . "ORDER BY labtest_bill_details.id "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_test_paymenthistory($id)
    {
        $sql = DB::select(
            "SELECT labtest_payment_histories.*,payment_types.name as pay_via,staffs.name as collect_by "

                . "FROM labtest_payment_histories LEFT JOIN payment_types ON payment_types.id = labtest_payment_histories.method_id "

                . "LEFT JOIN staffs ON staffs.id = labtest_payment_histories.collect_by "


                . "WHERE labtest_payment_histories.labtest_bill_id = $id "
                . "ORDER BY labtest_payment_histories.id "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_consaltation_data($fromDate, $toDate, $staff)
    {

        $sql = DB::select(
            "SELECT appointments.*,staffs.name as staff_name,staffs.staff_id as staffid,patients.name as patient_name,patients.patient_id as patientid "
                . "FROM appointments LEFT JOIN staffs ON staffs.id = appointments.doctor_id "
                . "LEFT JOIN patients ON patients.id = appointments.patient_id "

                . "WHERE (appointments.appointment_date BETWEEN '$fromDate' AND '$toDate') AND appointments.status = 2 AND appointments.doctor_id= $staff "
                . "ORDER BY appointments.id"

        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_commission_data_doctor_wise($fromDate, $toDate)
    {
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole == 3) {
            $user_id = $userId['user_id'];
            $where = "AND labtest_bills.referral_id = '$user_id'";
        } else {
            $where = '';
        }

        $sql = DB::select(
            "SELECT labtest_bills.*,labtest_bills.total+labtest_bills.tax_amount-labtest_bills.discount as net_amount, staffs.name as staff_name, staffs.staff_id as staffid, patients.name as patient_name, patients.patient_id as patientid "

                . "FROM labtest_bills LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "LEFT JOIN patients ON patients.id = labtest_bills.patient_id "
                // . "LEFT JOIN staffs ON staffs.id = labtest_bills.referral_id "

                . "WHERE (labtest_bills.date BETWEEN '$fromDate' AND '$toDate') $where "


        );
        return json_decode(json_encode($sql), true);
    }

    public static function get_income_vs_expense()
    {
        // $userId = Auth::user();
        // $userRole = $userId['role'];
        // if ($userRole == 3) {
        //     $user_id = $userId['user_id'];
        //     $where = "AND labtest_bills.referral_id = '$user_id'";
        // } else {
        //     $where = '';
        // }


        $year = date('Y');
        $cr = array();
        $dr = array();
        for ($month = 1; $month <= 12; $month++) {
            $date = $year . '-' . $month . '-1';
            $month_start = date('Y-m-d', strtotime($date));
            $month_end = date("Y-m-t", strtotime($date));


            $sql = DB::select(
                "SELECT  dr, cr "
                    . "FROM transactions "
                    . "WHERE (transactions.date BETWEEN '$month_start' AND '$month_end') "

            );

            if (!empty($sql)) {
                $cr[] = (float) $sql[0]->cr;
                $dr[] = (float) $sql[0]->dr;
            }
        }

        return array('income' => $cr, 'expense' => $dr);

        // return json_decode(json_encode($sql), true);
    }

    public static function get_monthly_patient_fees()
    {
        $days = array();
        $total_bill = array();
        $total_paid = array();
        $total_due = array();
        $startdate = date('Y-m-01');
        $enddate = date('Y-m-t');
        $start = strtotime($startdate);
        $end = strtotime($enddate);
        for ($day = 1; $day <= 30; $day++) {
            // while ($startdate <= $enddate) {
            $today = date('Y-m-' . $day, $start);

            $result = DB::select(
                "SELECT  total-discount+tax_amount as net_bill, paid, due "
                    . "FROM labtest_bills "
                    . "WHERE (labtest_bills.date = '$today') "

            );
            $days[] = $day;
            if (!empty($result)) {

                $total_bill[] = (float) $result[0]->net_bill;
                $total_paid[] = (float) $result[0]->paid;
                $total_due[] = (float) $result[0]->due;
                $start = strtotime('+1 day', $start);
            }
            // print_r($result);
            // die();
        }

        return array('total_bill' => $total_bill, 'total_paid' => $total_paid, 'total_due' => $total_due, 'days' => $days);
    }
}
