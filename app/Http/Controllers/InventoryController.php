<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Chemical_categorie;
use App\Models\Chemical_unit;
use App\Models\Supplier;
use App\Models\Chemical;
use App\Models\Chemical_stock;
use App\Models\Chemical_assigned;
use App\Models\Lab_test;
use App\Http\Helpers;

class InventoryController extends Controller
{


    public function Chemical(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical::all();
        $data['val'] = $datas->toArray();


        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.chamical.index', compact('data'));
    }


    public function storeChemical(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $chemical = new Chemical();

        $chemical->name             = $request['name'];
        $chemical->code             = $request['code'];
        $chemical->category_id      = $request['category_id'];
        $chemical->purchase_unit_id = $request['purchase_unit_id'];
        $chemical->sales_unit_id    = $request['sales_unit_id'];
        $chemical->unit_ratio       = $request['unit_ratio'];
        $chemical->purchase_price   = $request['purchase_price'];
        $chemical->sales_price      = $request['sales_price'];
        $chemical->available_stock  = $request['available_stock'];
        $chemical->remarks          = $request['remarks'];
        $chemical->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Chemical')->with('success', 'Information has been updated!!');
    }


    public function editChemical(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Chemical::all();
        $data['val'] = $datas->toArray();

        $editVal = Chemical::find($id);
        $data['editVal'] = $editVal->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.chamical.edit', compact('data'));
    }

    public function ChemicalUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $chemical = Chemical::where('id', '=', $id)->first();

        $chemical->name             = $request['name'];
        $chemical->code             = $request['code'];
        $chemical->category_id      = $request['category_id'];
        $chemical->purchase_unit_id = $request['purchase_unit_id'];
        $chemical->sales_unit_id    = $request['sales_unit_id'];
        $chemical->unit_ratio       = $request['unit_ratio'];
        $chemical->purchase_price   = $request['purchase_price'];
        $chemical->sales_price      = $request['sales_price'];
        $chemical->available_stock  = $request['available_stock'];
        $chemical->remarks          = $request['remarks'];
        $chemical->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Chemical')->with('success', 'Information has been updated!!');
    }


    public function destroyChemical(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $cat = Chemical::find($id);
        $cat->delete();
        return redirect('Chemical')->with('success', 'Information has been Deleted!!');
    }



    public function InvCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_category', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical_categorie::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.category.index', compact('data'));
    }


    public function storeInvCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_category', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $cat = new Chemical_categorie();

        $cat->name           = $request['name'];


        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvCategory')->with('success', 'Information has been updated!!');
    }


    public function editInvCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_category', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Chemical_categorie::all();
        $data['val'] = $datas->toArray();

        $editVal = Chemical_categorie::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.category.edit', compact('data'));
    }

    public function InvCategoryUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $cat = Chemical_categorie::where('id', '=', $id)->first();

        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvCategory')->with('success', 'Information has been updated!!');
    }


    public function destroyInvCategory(Request $request)
    {

        $permissionCheck = Helpers::get_permission('chemical_category', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $cat = Chemical_categorie::find($id);
        $cat->delete();
        return redirect('InvCategory')->with('success', 'Information has been Deleted!!');
    }


    public function InvUnit(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_unit', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical_unit::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.unit.index', compact('data'));
    }


    public function storeInvUnit(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_unit', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $cat = new Chemical_unit();

        $cat->name           = $request['name'];


        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvUnit')->with('success', 'Information has been updated!!');
    }


    public function editInvUnit(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_unit', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Chemical_unit::all();
        $data['val'] = $datas->toArray();

        $editVal = Chemical_unit::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.unit.edit', compact('data'));
    }

    public function InvUnitUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $cat = Chemical_unit::where('id', '=', $id)->first();

        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvUnit')->with('success', 'Information has been updated!!');
    }


    public function destroyInvUnit(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_unit', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Chemical_unit::find($id);
        $cat->delete();
        return redirect('InvUnit')->with('success', 'Information has been Deleted!!');
    }


    public function InvSupplier(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_supplier', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Supplier::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.supplier.index', compact('data'));
    }


    public function storeInvSupplier(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_supplier', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $supplier = new Supplier();

        $supplier->name            = $request['name'];
        $supplier->address         = $request['address'];
        $supplier->mobileno        = $request['mobileno'];
        $supplier->email           = $request['email'];
        $supplier->company_name    = $request['company_name'];
        $supplier->product_list    = $request['product_list'];
        $supplier->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvSupplier')->with('success', 'Information has been updated!!');
    }


    public function editInvSupplier(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_supplier', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Supplier::all();
        $data['val'] = $datas->toArray();

        $editVal = Supplier::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.supplier.edit', compact('data'));
    }

    public function InvSupplierUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $supplier = Supplier::where('id', '=', $id)->first();

        $supplier->name            = $request['name'];
        $supplier->address         = $request['address'];
        $supplier->mobileno        = $request['mobileno'];
        $supplier->email           = $request['email'];
        $supplier->company_name    = $request['company_name'];
        $supplier->product_list    = $request['product_list'];
        $supplier->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('InvSupplier')->with('success', 'Information has been updated!!');
    }


    public function destroyInvSupplier(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_supplier', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Supplier::find($id);
        $cat->delete();
        return redirect('InvSupplier')->with('success', 'Information has been Deleted!!');
    }


    public function ChemicalStock(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_stock', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical_stock::all();
        $data['val'] = $datas->toArray();


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.stock.index', compact('data'));
    }


    public function storeChemicalStock(Request $request)
    {
        $userid = Session::get('id');
        $permissionCheck = Helpers::get_permission('chemical_stock', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $chemical = new Chemical_stock();

        $chemical->inovice_no       = $request['inovice_no'];
        $chemical->chemical_id      = $request['chemical_id'];
        $chemical->date             = date('Y-m-d', strtotime($request['date']));
        $chemical->stock_quantity   = $request['stock_quantity'];
        $chemical->remarks          = $request['remarks'];
        $chemical->stock_by         = $userid[0];
        $chemical->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('ChemicalStock')->with('success', 'Information has been updated!!');
    }


    public function editChemicalStock(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_stock', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Chemical_stock::all();
        $data['val'] = $datas->toArray();


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        $editVal = Chemical_stock::find($id);
        $data['editVal'] = $editVal->toArray();



        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.stock.edit', compact('data'));
    }

    public function ChemicalStockUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $chemical = Chemical_stock::where('id', '=', $id)->first();

        $chemical->inovice_no       = $request['inovice_no'];
        $chemical->chemical_id      = $request['chemical_id'];
        $chemical->date             = date('Y-m-d', strtotime($request['date']));
        $chemical->stock_quantity   = $request['stock_quantity'];
        $chemical->remarks          = $request['remarks'];
        $chemical->stock_by         = $userid[0];
        $chemical->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('ChemicalStock')->with('success', 'Information has been updated!!');
    }


    public function destroyChemicalStock(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_stock', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];
        $cat = Chemical_stock::find($id);
        $cat->delete();
        return redirect('ChemicalStock')->with('success', 'Information has been Deleted!!');
    }



    public function ReagentAssigned(Request $request)
    {
        $permissionCheck = Helpers::get_permission('reagent_assigned', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical_assigned::all();
        $data['val'] = $datas->toArray();


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Lab_test::all();
        $data['Lab_test'] = $chemicalCate->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.reagent_assigned.index', compact('data'));
    }


    public function storeReagentAssigned(Request $request)
    {
        $permissionCheck = Helpers::get_permission('reagent_assigned', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        foreach ($request['chemical_id'] as $camId) {
            $chemical = new Chemical_assigned();

            $chemical->chemical_id  = $camId;
            $chemical->test_id      = $request['test_id'];

            $chemical->save();
        }
        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('ReagentAssigned')->with('success', 'Information has been Added!!');
    }


    public function editReagentAssigned(Request $request)
    {
        $permissionCheck = Helpers::get_permission('reagent_assigned', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        $editVal = Chemical_assigned::find($id);
        $data['editVal'] = $editVal->toArray();



        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.reagent_assigned.edit', compact('data'));
    }

    public function ReagentAssignedUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $chemical = Chemical_assigned::where('id', '=', $id)->first();

        $chemical->inovice_no       = $request['inovice_no'];
        $chemical->chemical_id      = $request['chemical_id'];
        $chemical->date             = date('Y-m-d', strtotime($request['date']));
        $chemical->stock_quantity   = $request['stock_quantity'];
        $chemical->remarks          = $request['remarks'];
        $chemical->stock_by         = $userid[0];
        $chemical->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('ReagentAssigned')->with('success', 'Information has been updated!!');
    }


    public function destroyReagentAssigned(Request $request)
    {

        $permissionCheck = Helpers::get_permission('reagent_assigned', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Chemical_assigned::find($id);
        $cat->delete();
        return redirect('ReagentAssigned')->with('success', 'Information has been Deleted!!');
    }

    public function Purchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Chemical_assigned::all();
        $data['val'] = $datas->toArray();


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Lab_test::all();
        $data['Lab_test'] = $chemicalCate->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.purchase.index', compact('data'));
    }


    public function storePurchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        foreach ($request['chemical_id'] as $camId) {
            $chemical = new Chemical_assigned();

            $chemical->chemical_id  = $camId;
            $chemical->test_id      = $request['test_id'];

            $chemical->save();
        }
        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Purchase')->with('success', 'Information has been Added!!');
    }


    public function editPurchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];

        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        $editVal = Chemical_assigned::find($id);
        $data['editVal'] = $editVal->toArray();



        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.reagent_assigned.edit', compact('data'));
    }

    public function PurchaseUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $chemical = Chemical_assigned::where('id', '=', $id)->first();

        $chemical->inovice_no       = $request['inovice_no'];
        $chemical->chemical_id      = $request['chemical_id'];
        $chemical->date             = date('Y-m-d', strtotime($request['date']));
        $chemical->stock_quantity   = $request['stock_quantity'];
        $chemical->remarks          = $request['remarks'];
        $chemical->stock_by         = $userid[0];
        $chemical->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Purchase')->with('success', 'Information has been updated!!');
    }


    public function destroyPurchase(Request $request)
    {

        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Chemical_assigned::find($id);
        $cat->delete();
        return redirect('Purchase')->with('success', 'Information has been Deleted!!');
    }
}
