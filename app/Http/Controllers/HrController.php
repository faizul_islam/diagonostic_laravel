<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;

use App\Models\Leave_application;
use App\Models\leave_categorie;
use App\Models\Staffs;
use App\Models\Salary_template;
use App\Models\Salary_template_detail;
use App\Http\Helpers;


class HrController extends Controller
{


    public function LeaveManage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_manage', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['val'] = Leave_application::all();
        $data['leave_categorie'] = leave_categorie::all();
        $data['Staffs'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.leave_manage.index', compact('data'));
    }


    public function storeLeaveManage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_manage', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $Leave_application = new Leave_application();
        $leave_date = explode('-', $request['leave_date']);
        $start_date = date('Y-m-d', strtotime($leave_date[0]));
        $end_date   = date('Y-m-d', strtotime($leave_date[1]));;

        $Leave_application->staff_id            = $request['staff_id'];
        $Leave_application->category_id         = $request['category_id'];
        $Leave_application->reason              = $request['reason'];
        $Leave_application->start_date          = $start_date;
        $Leave_application->end_date            = $end_date;
        $Leave_application->leave_days          = $request['leave_days'];
        $Leave_application->apply_date          = date('Y-m-d H:i:s');
        $Leave_application->leave_days          = 1;
        $Leave_application->comments            = $request['comments'];
        $Leave_application->orig_file_name      = $request['orig_file_name'];

        if (!empty($request->orig_file_name)) {
            $request->validate([
                'orig_file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->orig_file_name->extension();

            $request->orig_file_name->move(public_path('assets/images/uploads/leave'), $imageName);
            $Leave_application->orig_file_name          = $imageName;
        }

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('LeaveManage')->with('success', 'Information has been updated!!');
    }


    public function editLeaveManage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_manage', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val']  = Leave_application::all();
        $data['editVal'] = Leave_application::find($id);

        $data['leave_categorie'] = leave_categorie::all();
        $data['Staffs'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.leave_manage.edit', compact('data'));
    }

    public function LeaveManageUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $Leave_application = Leave_application::where('id', '=', $id)->first();

        $leave_date = explode('-', $request['leave_date']);

        if (!empty($leave_date[0])) {
            $start_date = date('Y-m-d', strtotime($leave_date[0]));
            $end_date   = date('Y-m-d', strtotime($leave_date[1]));
            $Leave_application->start_date          = $start_date;
            $Leave_application->end_date            = $end_date;
        } else {
        }

        $Leave_application->staff_id            = $request['staff_id'];
        $Leave_application->category_id         = $request['category_id'];
        $Leave_application->reason              = $request['reason'];

        $Leave_application->apply_date          = date('Y-m-d H:i:s');
        $Leave_application->leave_days          = 1;
        $Leave_application->comments            = $request['comments'];
        $Leave_application->orig_file_name      = $request['orig_file_name'];

        if (!empty($request->orig_file_name)) {
            $request->validate([
                'orig_file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->orig_file_name->extension();

            $request->orig_file_name->move(public_path('assets/images/uploads/leave'), $imageName);
            $Leave_application->orig_file_name          = $imageName;
        }

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('LeaveManage')->with('success', 'Information has been updated!!');
    }


    public function destroyLeaveManage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_manage', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];

        $cat = Leave_application::find($id);
        $cat->delete();
        return redirect('LeaveManage')->with('success', 'Information has been Deleted!!');
    }



    public function MyLeave(Request $request)
    {
        $userid = Auth::user()->getStaff->id;
        $permissionCheck = Helpers::get_permission('my_leave', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $data['val'] = Leave_application::where('staff_id', '=', $userid)->get();
        $data['leave_categorie'] = leave_categorie::all();
        $data['Staffs'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.my_leave.index', compact('data'));
    }


    public function storeMyLeave(Request $request)
    {
        $permissionCheck = Helpers::get_permission('my_leave', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $userid = Auth::user()->getStaff->id;

        $Leave_application = new Leave_application();
        $leave_date = explode('-', $request['leave_date']);
        $start_date = date('Y-m-d', strtotime($leave_date[0]));
        $end_date   = date('Y-m-d', strtotime($leave_date[1]));;

        $Leave_application->staff_id            = $userid;
        $Leave_application->category_id         = $request['category_id'];
        $Leave_application->reason              = $request['reason'];
        $Leave_application->start_date          = $start_date;
        $Leave_application->end_date            = $end_date;
        $Leave_application->leave_days          = $request['leave_days'];
        $Leave_application->apply_date          = date('Y-m-d H:i:s');
        $Leave_application->leave_days          = 1;
        $Leave_application->comments            = $request['comments'];
        $Leave_application->orig_file_name      = $request['orig_file_name'];

        if (!empty($request->orig_file_name)) {
            $request->validate([
                'orig_file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->orig_file_name->extension();

            $request->orig_file_name->move(public_path('assets/images/uploads/leave'), $imageName);
            $Leave_application->orig_file_name          = $imageName;
        }

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('MyLeave')->with('success', 'Information has been updated!!');
    }

    public function updateStafLeave(Request $request)
    {
        $userid = Session::get('id');

        $Leave_application = Leave_application::where('id', '=', $request['val_Id'])->first();


        $Leave_application->comments            = $request['comments'];
        $Leave_application->status              = $request['status'];

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('MyLeave')->with('success', 'Information has been updated!!');
    }

    public function editMyLeave(Request $request)
    {
        $permissionCheck = Helpers::get_permission('my_leave', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val']  = Leave_application::all();
        $data['editVal'] = Leave_application::find($id);

        $data['leave_categorie'] = leave_categorie::all();
        $data['Staffs'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.my_leave.edit', compact('data'));
    }

    public function MyLeaveUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $Leave_application = Leave_application::where('id', '=', $id)->first();

        $leave_date = explode('-', $request['leave_date']);

        if (!empty($leave_date[0])) {
            $start_date = date('Y-m-d', strtotime($leave_date[0]));
            $end_date   = date('Y-m-d', strtotime($leave_date[1]));
            $Leave_application->start_date          = $start_date;
            $Leave_application->end_date            = $end_date;
        } else {
        }

        $Leave_application->staff_id            = $request['staff_id'];
        $Leave_application->category_id         = $request['category_id'];
        $Leave_application->reason              = $request['reason'];

        $Leave_application->apply_date          = date('Y-m-d H:i:s');
        $Leave_application->leave_days          = 1;
        $Leave_application->comments            = $request['comments'];
        $Leave_application->orig_file_name      = $request['orig_file_name'];

        if (!empty($request->orig_file_name)) {
            $request->validate([
                'orig_file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->orig_file_name->extension();

            $request->orig_file_name->move(public_path('assets/images/uploads/leave'), $imageName);
            $Leave_application->orig_file_name          = $imageName;
        }

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('MyLeave')->with('success', 'Information has been updated!!');
    }


    public function destroyMyLeave(Request $request)
    {

        $permissionCheck = Helpers::get_permission('my_leave', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $cat = Leave_application::find($id);
        $cat->delete();
        return redirect('MyLeave')->with('success', 'Information has been Deleted!!');
    }


    public function leaveCat(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_category', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['val'] = leave_categorie::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.leave_category.index', compact('data'));
    }


    public function storeleaveCat(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_category', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $leave_categorie = new leave_categorie();

        $leave_categorie->name           = $request['name'];
        $leave_categorie->days           = $request['days'];


        $leave_categorie->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('leaveCat')->with('success', 'Information has been updated!!');
    }


    public function editleaveCat(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_category', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $data['val'] = leave_categorie::all();

        $data['editVal'] = leave_categorie::find($id);

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.leaves.leave_category.edit', compact('data'));
    }

    public function leaveCatUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $leave_categorie = leave_categorie::where('id', '=', $id)->first();

        $leave_categorie->name           = $request['name'];
        $leave_categorie->days           = $request['days'];

        $leave_categorie->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('leaveCat')->with('success', 'Information has been updated!!');
    }


    public function destroyleaveCat(Request $request)
    {
        $permissionCheck = Helpers::get_permission('leave_category', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $cat = leave_categorie::find($id);
        $cat->delete();
        return redirect('leaveCat')->with('success', 'Information has been Deleted!!');
    }


    public function SalaryTemplates(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_payment', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Salary_template::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.payroll.salary_template.index', compact('data'));
    }

    public function storeSalaryTemplates(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_payment', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $allowance = $request['allowance'];
        $deduction = $request['deduction'];
        $Salary_template = new Salary_template();


        $Salary_template->name            = $request['template_name'];
        $Salary_template->basic_salary    = $request['basic_salary'];
        $Salary_template->overtime_salary = $request['overtime_rate'];

        $Salary_template->save();
        $Salary_template_Id = $Salary_template->id;

        foreach ($allowance as $key => $allowan) {
            $allowanceSave = new Salary_template_detail();
            $allowanceSave->salary_template_id    = $Salary_template_Id;
            $allowanceSave->name    = $allowan['name'];
            $allowanceSave->amount  = $allowan['amount'];
            $allowanceSave->type        = 1;

            $allowanceSave->save();
        }

        foreach ($deduction as $key => $deduct) {
            $deductionSave = new Salary_template_detail();
            $deductionSave->salary_template_id    = $Salary_template_Id;
            $deductionSave->name    = $deduct['name'];
            $deductionSave->amount  = $deduct['amount'];
            $deductionSave->type        = 2;

            $deductionSave->save();
        }

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('SalaryTemplates')->with('success', 'Information has been updated!!');
    }

    public function editSalaryTemplates(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_payment', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];

        $data['val']  = Leave_application::all();
        $data['editVal'] = Leave_application::find($id);

        $data['leave_categorie'] = leave_categorie::all();
        $data['Staffs'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.payroll.salary_template.edit', compact('data'));
    }

    public function SalaryTemplatesUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $Leave_application = Leave_application::where('id', '=', $id)->first();

        $leave_date = explode('-', $request['leave_date']);

        if (!empty($leave_date[0])) {
            $start_date = date('Y-m-d', strtotime($leave_date[0]));
            $end_date   = date('Y-m-d', strtotime($leave_date[1]));
            $Leave_application->start_date          = $start_date;
            $Leave_application->end_date            = $end_date;
        } else {
        }

        $Leave_application->staff_id            = $request['staff_id'];
        $Leave_application->category_id         = $request['category_id'];
        $Leave_application->reason              = $request['reason'];

        $Leave_application->apply_date          = date('Y-m-d H:i:s');
        $Leave_application->leave_days          = 1;
        $Leave_application->comments            = $request['comments'];
        $Leave_application->orig_file_name      = $request['orig_file_name'];

        if (!empty($request->orig_file_name)) {
            $request->validate([
                'orig_file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->orig_file_name->extension();

            $request->orig_file_name->move(public_path('assets/images/uploads/leave'), $imageName);
            $Leave_application->orig_file_name          = $imageName;
        }

        $Leave_application->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('SalaryTemplates')->with('success', 'Information has been updated!!');
    }


    public function destroySalaryTemplates(Request $request)
    {

        $permissionCheck = Helpers::get_permission('salary_payment', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];
        $Salary_template = Salary_template::find($id);

        Salary_template_detail::where('salary_template_id', $id)->delete();
        $Salary_template->delete();

        return redirect('SalaryTemplates')->with('success', 'Information has been Deleted!!');
    }
}
