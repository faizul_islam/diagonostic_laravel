<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use App\Models\Lab_test;
use App\Models\Lab_test_categorie;
use App\Http\Helpers;
use App\Models\Payment_type;
use App\Models\Patient;
use App\Models\Staffs;
use App\Models\Labtest_bill;
use App\Models\Labtest_bill_detail;
use App\Models\labtest_payment_historie;
use App\Models\Chemical_assigned;
use App\Models\Chemical;
use App\Models\Referral_commission;
use App\Models\Labtest_report;
use App\Models\Lab_report_template;
use App\Models\Custom_model;
use App\Models\Users;
use App\Models\Appointment;
use App\Models\Front_cms_settings;
use App\Models\front_cms_services_list;
use App\Models\Front_cms_testimonial;

class WebController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['front_settings']     = Front_cms_settings::first();
        $data['front_service']      = front_cms_services_list::get();
        $data['doctor_list']        = Staffs::where('designation', 3)->get();
        $data['testimonial_list']   = Front_cms_testimonial::get();

        return view('frontend.index', $data);
    }
}
