<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Staffs;
use App\Models\Schedule;
use App\Http\Helpers;



class ScheduleController extends Controller
{
    public function Schedule(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val']   = Schedule::all();
        $data['staff'] = Staffs::where('designation', '=', 3)->get();

        return view('backend.schedule.index', compact('data'));
    }


    public function storeSchedule(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $choice_options = array();
        $end_time_val = array();
        $duration_val = array();
        if ($request->has('choice_no')) {
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_' . $no;
                $end_time = 'end_time_' . $no;
                $duration = 'duration_' . $no;
                $item['attribute_id'] = $no;
                $data = array();
                foreach (explode(',', $request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue);
                }
                $item['values'] = $data;
                $values = implode(',', $item['values']);
                $items = $values;
                array_push($choice_options, $items);

                $dataa = array();
                foreach (explode(',', $request[$end_time][0]) as $key => $eachValue) {
                    array_push($dataa, $eachValue);
                }
                $item['values'] = $dataa;
                $values = implode(',', $item['values']);
                $items = $values;
                array_push($end_time_val, $items);


                $dataaa = array();
                foreach (explode(',', $request[$duration][0]) as $key => $eachValue) {
                    array_push($dataaa, $eachValue);
                }
                $item['values'] = $dataaa;
                $values = implode(',', $item['values']);
                $items = $values;
                array_push($duration_val, $items);
            }
        }

        $i = 0;
        foreach ($request['choice_no'] as $choice) {

            $scheduleDupCheck = Schedule::where('day', '=', $choice)->where('doctor_id', '=', $request['doctor_id'])->where('shift', '=', $request['shift'])->get();

            if (empty($scheduleDupCheck->toArray())) {
                $Schedule = new Schedule();
                $Schedule->day              = $choice;
                $Schedule->doctor_id        = $request['doctor_id'];
                $Schedule->shift        = $request['shift'];
                $Schedule->time_start       = $choice_options[$i];
                $Schedule->time_end         = $end_time_val[$i];
                $Schedule->per_patient_time = $duration_val[$i];
                $Schedule->consultation_fees = $request['consultation_fees'];
                $Schedule->old_consultation_fees = $request['old_consultation_fees'];
                $Schedule->save();
            }

            $i++;
        }

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Schedule')->with('success', 'Information has been Added!!');
    }


    public function editSchedule(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];


        $data['val'] = Schedule::all();
        $data['editVal'] = Schedule::find($id);
        $data['staff'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.schedule.edit', compact('data'));
    }

    public function ScheduleUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $Schedule = Schedule::where('id', '=', $id)->first();

        $Schedule->day              = $request['day'];
        $Schedule->doctor_id        = $request['doctor_id'];
        $Schedule->shift        = $request['shift'];
        $Schedule->time_start       = $request['time_start'];
        $Schedule->time_end         = $request['time_end'];
        $Schedule->per_patient_time = $request['per_patient_time'];
        $Schedule->consultation_fees = $request['consultation_fees'];
        $Schedule->old_consultation_fees = $request['old_consultation_fees'];

        $Schedule->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Schedule')->with('success', 'Information has been updated!!');
    }


    public function destroySchedule(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Schedule::find($id);
        $cat->delete();
        return redirect('Schedule')->with('success', 'Information has been Deleted!!');
    }
}
