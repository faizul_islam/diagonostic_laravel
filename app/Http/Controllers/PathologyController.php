<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use App\Models\Lab_test;
use App\Models\Lab_test_categorie;
use App\Http\Helpers;
use App\Models\Payment_type;
use App\Models\Patient;
use App\Models\Staffs;
use App\Models\Labtest_bill;
use App\Models\Labtest_bill_detail;
use App\Models\labtest_payment_historie;
use App\Models\Chemical_assigned;
use App\Models\Chemical;
use App\Models\Referral_commission;
use App\Models\Labtest_report;
use App\Models\Lab_report_template;
use App\Models\Custom_model;

class PathologyController extends Controller
{


    public function LabTest(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Lab_test::all();

        $data['category'] = Lab_test_categorie::all();

        // echo "<pre>";    print_r($data);die();
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole != 7) {
            return view('backend.pathology.labtest.index', compact('data'));
        } else {
            return view('backend.pathology.labtest.patientIndex', compact('data'));
        }
    }


    public function storeLabTest(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $userId = Auth::user();
        $Lab_test = new Lab_test();

        $Lab_test->name             = $request['name'];
        $Lab_test->category_id      = $request['category_id'];
        $Lab_test->patient_price    = $request['patient_price'];
        $Lab_test->production_cost  = $request['production_cost'];
        $Lab_test->test_code        = $request['test_code'];
        $Lab_test->date             = date('Y-m-d', strtotime($request['date']));
        $Lab_test->created_by       = $userId['user_id'];

        $Lab_test->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('LabTest')->with('success', 'Information has been updated!!');
    }


    public function editLabTest(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Lab_test::all();
        $data['val'] = $datas->toArray();

        $editVal = Lab_test::find($id);
        $data['editVal'] = $editVal->toArray();

        $data['category'] = Lab_test_categorie::all();


        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.labtest.edit', compact('data'));
    }

    public function LabTestUpdate(Request $request, $id)
    {
        $userId = Auth::user();

        $Lab_test = Lab_test::where('id', '=', $id)->first();

        $Lab_test->name             = $request['name'];
        $Lab_test->category_id      = $request['category_id'];
        $Lab_test->patient_price    = $request['patient_price'];
        $Lab_test->production_cost  = $request['production_cost'];
        $Lab_test->test_code        = $request['test_code'];
        $Lab_test->date             = date('Y-m-d', strtotime($request['date']));
        $Lab_test->created_by       = $userId['user_id'];
        $Lab_test->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('LabTest')->with('success', 'Information has been updated!!');
    }


    public function destroyLabTest(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Lab_test::find($id);
        $cat->delete();
        return redirect('LabTest')->with('success', 'Information has been Deleted!!');
    }



    public function PathologyCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_category', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Lab_test_categorie::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.category.index', compact('data'));
    }


    public function storePathologyCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_category', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userId = Auth::user();
        $cat = new Lab_test_categorie();

        $cat->name           = $request['name'];


        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PathologyCategory')->with('success', 'Information has been updated!!');
    }


    public function editPathologyCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_category', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val'] = Lab_test_categorie::all();

        $data['editVal'] = Lab_test_categorie::find($id);

        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.category.edit', compact('data'));
    }

    public function PathologyCategoryUpdate(Request $request, $id)
    {
        $userId = Auth::user();

        $cat = Lab_test_categorie::where('id', '=', $id)->first();

        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PathologyCategory')->with('success', 'Information has been updated!!');
    }


    public function destroyPathologyCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_category', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Lab_test_categorie::find($id);
        $cat->delete();
        return redirect('PathologyCategory')->with('success', 'Information has been Deleted!!');
    }

    public function TestBill(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test_bill', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['patientlist']    = Patient::all();
        $data['referrallist']   = Staffs::all();
        $data['payvia_list']    = Payment_type::all();
        $data['categorylist']   = Lab_test_categorie::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.billing.create', compact('data'));
    }


    public function storeTestBill(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $status = 1;
        // $bill_id = $this->app_lib->get_bill_no('labtest_bill');
        $payment_amount = (empty($request['payment_amount']) ? 0 : $request['payment_amount']);
        $total_discount = (empty($request['total_discount']) ? 0 : $request['total_discount']);
        $tax_amount = (empty($request['tax_amount']) ? 0 : $request['tax_amount']);
        $sub_total_amount = (empty($request['sub_total_amount']) ? 0 : $request['sub_total_amount']);
        $net_amount = (empty($request['net_amount']) ? 0 : $request['net_amount']);
        if ($payment_amount > 0) {
            $status = 2;
        }
        if ($payment_amount == $net_amount) {
            $status = 3;
        }


        $userId = Auth::user();


        $billNoGet  = Labtest_bill::latest()->first();
        $id = $billNoGet['bill_no'];
        if (!empty($id)) {
            $bill = $id + 1;
        } else {
            $bill = 1;
        }
        $billNo = str_pad($bill, 4, '0', STR_PAD_LEFT);
        $hass   = md5(rand() . microtime() . time() . uniqid());


        $bill_date    = Carbon::createFromFormat('d/m/Y', $request['bill_date'])->format('Y-m-d');
        $delivery_date    = Carbon::createFromFormat('d/m/Y', $request['delivery_date'])->format('Y-m-d');

        $Lab_test   = new Labtest_bill();

        $Lab_test->bill_no      = $billNo;
        $Lab_test->patient_id   = $request['patient_id'];
        $Lab_test->referral_id  = $request['referral_id'];
        $Lab_test->total        = $sub_total_amount;
        $Lab_test->discount     = $total_discount;
        $Lab_test->tax_amount   = $tax_amount;
        $Lab_test->paid         = $payment_amount;
        $Lab_test->due          = ($net_amount - $payment_amount);
        $Lab_test->status       = $status;
        $Lab_test->date         = $bill_date;
        $Lab_test->hash         = $hass;
        $Lab_test->prepared_by  = $userId['id'];
        $Lab_test->save();
        $labTestId = $Lab_test->id;


        // record payment history save in db
        if ($payment_amount > 1) {
            $Labtest_payment_history   = new labtest_payment_historie();
            $Labtest_payment_history->labtest_bill_id = $labTestId;
            $Labtest_payment_history->collect_by    = $userId['id'];
            $Labtest_payment_history->amount        = $payment_amount;
            $Labtest_payment_history->method_id     = $request['pay_via'];
            $Labtest_payment_history->remarks       = $request['payment_remarks'];
            $Labtest_payment_history->paid_on       = $bill_date;
            $Labtest_payment_history->save();
        }

        $items = $request['items'];

        foreach ($items as $key => $value) {

            // inventory reagent here
            $assignedResult = Chemical_assigned::where('test_id', $value['lab_test'])->get();

            foreach ($assignedResult as $assign) {

                $Chemical = Chemical::where('id', '=', $assign['chemical_id'])->first();
                $Chemical->available_stock                = $Chemical['available_stock'] - 1;
                $Chemical->save();
            }

            $query = Referral_commission::where('staff_id', $request['referral_id'])->where('test_id', $value['lab_test'])->get();


            // referral commission calculation here
            if (!empty($query->toArray())) {

                $afterDiscount = $value['total_price'];
                // if you want removed production cost
                $production_cost_info = Lab_test::where('id', '=', $value['lab_test'])->first();

                $production_cost = $production_cost_info['production_cost'];
                $sub_total_amount = $afterDiscount - $production_cost;
                $comm_percentage = $query[0]['percentage'];
                $commission = ($comm_percentage / 100) * $sub_total_amount;
            } else {
                $commission = 0;
            }

            $Labtest_bill_detail   = new Labtest_bill_detail();
            $Labtest_bill_detail->labtest_bill_id   = $labTestId;
            $Labtest_bill_detail->category_id       = $value['category'];
            $Labtest_bill_detail->test_id           = $value['lab_test'];
            $Labtest_bill_detail->price             = $value['unit_price'];
            $Labtest_bill_detail->discount          = $value['dis_amount'];
            $Labtest_bill_detail->commission_amount = $commission;
            $Labtest_bill_detail->save();
        }

        $Labtest_report   = new Labtest_report();
        $Labtest_report->labtest_bill_id   = $labTestId;
        $Labtest_report->report_remarks       = $request['remark'];
        $Labtest_report->delivery_date           = $delivery_date;
        $Labtest_report->status             = 1;
        $Labtest_report->save();


        $request->session()->flash('alert-success', 'Information successfully added!');
        return redirect('/test_bill_invoice/' . $labTestId);
    }

    public function get_labtest_by_category(Request $request)
    {

        $category_id = $request['category_id'];
        $selected_id = $request['selected_id'];

        $productlist = Lab_test::where('category_id', $category_id)->get();
        // print_r($productlist);
        // die();
        $html = "<option value=''>" . 'Select' . "</option>";
        foreach ($productlist as $product) {
            $selected = ($product['id'] == $selected_id ? 'selected' : '');
            $html .= "<option value='" . $product['id'] . "' " . $selected . ">" . $product['name'] . " (" . $product['test_code'] . ")</option>";
        }
        echo $html;
    }

    public function get_labtest_price(Request $request)
    {

        $testid = $request['testid'];

        // $r = Lab_test::where('id', $testid)->get();
        $r = Lab_test::where('id', '=', $testid)->first();

        if (empty($r['patient_price'])) {
            echo "0.00";
        } else {
            echo $r['patient_price'];
        }
    }


    public function create_investigation($id)
    {
        $data['templatelist']   = Lab_report_template::all();
        $data['test'] = Custom_model::get_test_details($id);
        // echo "<pre>";
        // print_r($data);
        // die();
        return view('backend.report.investigation.create_investigation', compact('data'));
    }

    public function get_template_details(Request $request)
    {
        $id = $request['id'];
        $templateData   = Lab_report_template::find($id);
        $template = $templateData['template'];

        // echo "<pre>";
        // print_r($data);
        // die();
        return $template;
    }



    public function updateInvestigation(Request $request)
    {
        $userId = Auth::user();
        $id = $request['labtest_report_id'];

        $reporting_date      = Carbon::createFromFormat('d/m/Y', $request['reporting_date'])->format('Y-m-d');
        $inv = labtest_report::where('id', '=', $id)->first();
        // print_r($reporting_date);
        // die();

        $inv->report_description = $request['tinymce-example'];
        $inv->reporting_date     = $reporting_date;
        $inv->status             = 2;
        $inv->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('/print_investigation/' . $id);
    }


    public function print_investigation($id)
    {
        $data['test'] = Custom_model::get_test_print_details($id);
        // echo "<pre>";
        // print_r($data);
        // die();
        return view('backend.report.investigation.print_investigation', compact('data'));
    }

    public function test_bill_invoice($id)
    {
        $data['test'] = Custom_model::get_labtest_bill($id);
        $data['bill_details'] = Custom_model::get_labtest_bill_details($id);
        $data['paymentHistory'] = Custom_model::get_test_paymenthistory($id);
        $data['payvia_list']    = Payment_type::all();

        // echo "<pre>";
        // print_r($data['paymentHistory']);
        // die();
        return view('backend.pathology.billing.print_billing_invoice', compact('data'));
    }

    public function storePayment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_bill_payment', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $labtest_bill = Labtest_bill::where('id', $request['bill_id'])->get();

        $userId = Auth::user();
        $payment = new Labtest_payment_historie();
        $bill_date    = Carbon::createFromFormat('d/m/Y', $request['paid_date'])->format('Y-m-d');
        $payment->labtest_bill_id   = $request['bill_id'];
        $payment->collect_by        = $userid[0];
        $payment->amount            = $request['payment_amount'];
        $payment->method_id         = $request['pay_via'];
        $payment->remarks           = $request['remarks'];
        $payment->paid_on           = $bill_date;
        $payment->coll_type         = 1;
        $payment->save();
        if ($labtest_bill[0]['due'] <= $request['payment_amount']) {
            $status = 3;
        } else {
            $status = 2;
        }

        $bill_data = Labtest_bill::where('id', '=', $request['bill_id'])->first();
        $bill_data->status    = $status;
        $bill_data->paid      = $request['payment_amount'] + $bill_data['paid'];
        $bill_data->due       = $bill_data['due'] - $request['payment_amount'];
        $bill_data->save();


        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('/test_bill_invoice/' . $request['bill_id']);
    }
}
