<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

use App\Models\Account;
use App\Models\Voucher_head;
use App\Models\Lab_test_categorie;
use App\Http\Helpers;

class AccountController extends Controller
{


    public function Account(Request $request)
    {
        $permissionCheck = Helpers::get_permission('account', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Account::all();
        return view('backend.account.account.index', compact('data'));
    }


    public function storeAccount(Request $request)
    {
        $permissionCheck = Helpers::get_permission('account', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $Account = new Account();

        $Account->name             = $request['name'];
        $Account->description      = $request['description'];
        $Account->balance          = $request['balance'];

        $Account->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Account')->with('success', 'Information has been updated!!');
    }


    public function editAccount(Request $request)
    {

        $permissionCheck = Helpers::get_permission('account', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val']  = Account::all();

        $data['editVal'] = Account::find($id);

        // echo "<pre>";    print_r($data);die();
        return view('backend.account.account.edit', compact('data'));
    }

    public function AccountUpdate(Request $request, $id)
    {

        $Account = Account::where('id', '=', $id)->first();

        $Account->name             = $request['name'];
        $Account->description      = $request['description'];
        $Account->balance          = $request['balance'];

        $Account->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Account')->with('success', 'Information has been updated!!');
    }


    public function destroyAccount(Request $request)
    {
        $permissionCheck = Helpers::get_permission('account', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Account::find($id);
        $cat->delete();
        return redirect('Account')->with('success', 'Information has been Deleted!!');
    }



    public function Voucher(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Lab_test_categorie::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.category.index', compact('data'));
    }


    public function storeVoucher(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $cat = new Lab_test_categorie();
        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PathologyCategory')->with('success', 'Information has been updated!!');
    }


    public function editVoucher(Request $request)
    {

        $permissionCheck = Helpers::get_permission('voucher', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $data['val'] = Lab_test_categorie::all();
        $data['editVal'] = Lab_test_categorie::find($id);
        // echo "<pre>";    print_r($data);die();
        return view('backend.pathology.category.edit', compact('data'));
    }

    public function VoucherUpdate(Request $request, $id)
    {

        $cat = Lab_test_categorie::where('id', '=', $id)->first();

        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Voucher')->with('success', 'Information has been updated!!');
    }


    public function destroyVoucher(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Lab_test_categorie::find($id);
        $cat->delete();
        return redirect('Voucher')->with('success', 'Information has been Deleted!!');
    }


    public function VoucherHead(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher_head', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val'] = Voucher_head::all();
        return view('backend.account.voucherHead.index', compact('data'));
    }


    public function storeVoucherHead(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher_head', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $Voucher_head = new Voucher_head();
        $Voucher_head->name           = $request['name'];
        $Voucher_head->type           = $request['type'];
        $Voucher_head->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('VoucherHead')->with('success', 'Information has been updated!!');
    }


    public function editVoucherHead(Request $request)
    {

        $permissionCheck = Helpers::get_permission('voucher_head', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val'] = Voucher_head::all();
        $data['editVal'] = Voucher_head::find($id);

        return view('backend.account.voucherHead.edit', compact('data'));
    }

    public function VoucherHeadUpdate(Request $request, $id)
    {

        $Voucher_head = Voucher_head::where('id', '=', $id)->first();

        $Voucher_head->name           = $request['name'];
        $Voucher_head->type           = $request['type'];

        $Voucher_head->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('VoucherHead')->with('success', 'Information has been updated!!');
    }


    public function destroyVoucherHead(Request $request)
    {
        $permissionCheck = Helpers::get_permission('voucher_head', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Voucher_head::find($id);
        $cat->delete();
        return redirect('VoucherHead')->with('success', 'Information has been Deleted!!');
    }
}
