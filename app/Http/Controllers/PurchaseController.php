<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Chemical_categorie;
use App\Models\Chemical_unit;
use App\Models\Supplier;
use App\Models\Chemical;
use App\Models\Chemical_assigned;
use App\Models\Purchase_bill;
use App\Http\Helpers;

class PurchaseController extends Controller
{
    public function Purchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $data['val']      = Purchase_bill::all();
        $data['Chemical'] = Chemical::all();
        $data['Supplier'] = Supplier::all();

        $data['Chemical'] = Chemical::all();
        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.purchase.index', compact('data'));
    }


    public function storePurchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        foreach ($request['chemical_id'] as $camId) {
            $chemical = new Chemical_assigned();

            $chemical->chemical_id  = $camId;
            $chemical->test_id      = $request['test_id'];

            $chemical->save();
        }
        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Purchase')->with('success', 'Information has been Added!!');
    }


    public function editPurchase(Request $request)
    {

        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];


        $Chemical = Chemical::all();
        $data['Chemical'] = $Chemical->toArray();

        $chemicalCate = Chemical_categorie::all();
        $data['category'] = $chemicalCate->toArray();

        $Chemical_unit = Chemical_unit::all();
        $data['Chemical_unit'] = $Chemical_unit->toArray();

        $editVal = Chemical_assigned::find($id);
        $data['editVal'] = $editVal->toArray();



        // echo "<pre>";    print_r($data);die();
        return view('backend.inventory.reagent_assigned.edit', compact('data'));
    }

    public function PurchaseUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $chemical = Chemical_assigned::where('id', '=', $id)->first();

        $chemical->inovice_no       = $request['inovice_no'];
        $chemical->chemical_id      = $request['chemical_id'];
        $chemical->date             = date('Y-m-d', strtotime($request['date']));
        $chemical->stock_quantity   = $request['stock_quantity'];
        $chemical->remarks          = $request['remarks'];
        $chemical->stock_by         = $userid[0];
        $chemical->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Purchase')->with('success', 'Information has been updated!!');
    }


    public function destroyPurchase(Request $request)
    {
        $permissionCheck = Helpers::get_permission('chemical_purchase', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Chemical_assigned::find($id);
        $cat->delete();
        return redirect('Purchase')->with('success', 'Information has been Deleted!!');
    }
}
