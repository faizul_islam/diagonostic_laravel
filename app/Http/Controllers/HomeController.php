<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use App\Models\Lab_test;
use App\Models\Lab_test_categorie;
use App\Http\Helpers;
use App\Models\Payment_type;
use App\Models\Patient;
use App\Models\Staffs;
use App\Models\Labtest_bill;
use App\Models\Labtest_bill_detail;
use App\Models\labtest_payment_historie;
use App\Models\Chemical_assigned;
use App\Models\Chemical;
use App\Models\Referral_commission;
use App\Models\Labtest_report;
use App\Models\Lab_report_template;
use App\Models\Custom_model;
use App\Models\Users;
use App\Models\Appointment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['labTestBill'] = Labtest_bill::whereDate('created_at', Carbon::today())->count();

        $data['commision']          = DB::table('labtest_bills')->where('created_at', Carbon::today())->sum('commission');
        $data['get_today_expense']  = DB::table('transactions')->where('created_at', Carbon::today())->sum('dr');
        $data['get_today_income']   = DB::table('transactions')->where('created_at', Carbon::today())->sum('cr');

        $data['total_patient'] = Patient::count();
        $data['total_doctor'] = Users::where('role', 3)->count();
        $data['total_employee'] = Users::whereNotIn('role', [1, 3, 7])->count();
        $data['total_appointment'] = Appointment::where('status', 1)->count();
        $data['total_pending_appointment'] = Appointment::where('status', 4)->count();

        $data['yearly_income_expense'] = Custom_model::get_income_vs_expense();
        $data['patient_fees_summary'] = Custom_model::get_monthly_patient_fees();


        // echo "<pre>";
        // print_r($data['patient_fees_summary']);
        // die();
        return view('backend.index', $data);
    }
}
