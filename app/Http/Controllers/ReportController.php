<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use App\Models\Chemical_categorie;
use App\Models\Chemical_unit;
use App\Models\Supplier;
use App\Models\Chemical;
use App\Models\Chemical_assigned;
use App\Models\Purchase_bill;
use App\Models\Custom_model;
use App\Models\Account;
use App\Models\Staffs;
use App\Models\Role;
use App\Models\Staff_designation;
use App\Models\Salary_template;
use App\Models\Lab_test;
use App\Models\Referral_commission;
use App\Http\Helpers;

class ReportController extends Controller
{
    public function stockReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('inventory_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['cat'] = Chemical_categorie::all();
        $data['unit'] = Chemical_unit::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.inventory.stock.report_form', compact('data'));
    }


    public function show_stock_report(Request $request)
    {
        $userid = Session::get('id');
        $category_id = $request['category_id'];
        $unit_id     = $request['unit_id'];

        $data['all_data'] = Custom_model::get_stock_data($category_id, $unit_id);

        return view('backend.report.inventory.stock.ajax_view_load', compact('data'));
    }


    public function purchaseReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('inventory_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['suppliers'] = Supplier::all();
        $data['unit'] = Chemical_unit::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.inventory.purchase.report_form', compact('data'));
    }


    public function show_purchase_report(Request $request)
    {
        $userid = Session::get('id');
        $payment_status = $request['payment_status'];
        $supplier     = $request['supplier'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_purchase_data($supplier, $payment_status, $fromDate, $toDate);
        // echo "<pre>";
        // print_r($toDate);
        // die();
        return view('backend.report.inventory.purchase.ajax_view_load', compact('data'));
    }


    public function paymentReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('inventory_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $data['suppliers'] = Supplier::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.inventory.payment.report_form', compact('data'));
    }


    public function show_payment_report(Request $request)
    {
        $userid = Session::get('id');

        $supplier     = $request['supplier'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_payment_data($supplier, $fromDate, $toDate);
        // echo "<pre>";
        // print_r($toDate);
        // die();
        return view('backend.report.inventory.payment.ajax_view_load', compact('data'));
    }

    public function accountReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['account'] = Account::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.account_statement.report_form', compact('data'));
    }


    public function show_account_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];

        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_account_data($account, $type, $fromDate, $toDate);

        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.accounts.account_statement.ajax_view_load', compact('data'));
    }

    public function incomeReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['account'] = Account::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.income_statement.report_form', compact('data'));
    }


    public function show_income_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_income_expense_data($fromDate, $toDate, 'income');
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.accounts.income_statement.ajax_view_load', compact('data'));
    }

    public function expenseReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['account'] = Account::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.expense_statement.report_form', compact('data'));
    }


    public function show_expense_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_income_expense_data($fromDate, $toDate, 'expense');
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.accounts.expense_statement.ajax_view_load', compact('data'));
    }


    public function transitionReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['account'] = Account::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.transitions_reports.report_form', compact('data'));
    }


    public function show_transition_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_transitions_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.accounts.transitions_reports.ajax_view_load', compact('data'));
    }

    public function incomevsexpenseReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['account'] = Account::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.incomevsexpense.report_form', compact('data'));
    }


    public function show_incomevsexpense_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_incomevsexpense_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.accounts.incomevsexpense.ajax_view_load', compact('data'));
    }


    public function show_balance_sheet_report(Request $request)
    {
        $permissionCheck = Helpers::get_permission('accounting_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['all_data'] = Custom_model::get_balance_sheet_data();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.accounts.balance_sheet.report_show', compact('data'));
    }


    public function paid_billReport(Request $request)
    {


        return view('backend.report.billing.paid_bill_report.report_form');
    }


    public function show_paid_bill_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_paid_bill_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.billing.paid_bill_report.ajax_view_load', compact('data'));
    }


    public function due_billReport(Request $request)
    {


        return view('backend.report.billing.due_bill_report.report_form');
    }


    public function show_due_bill_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_due_bill_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.billing.due_bill_report.ajax_view_load', compact('data'));
    }


    public function due_collectReport(Request $request)
    {


        return view('backend.report.billing.due_collect_report.report_form');
    }


    public function show_due_collect_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_due_collect_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.billing.due_collect_report.ajax_view_load', compact('data'));
    }


    public function createReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        return view('backend.report.investigation.create_report.report_form');
    }


    public function show_create_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_pending_report2_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($toDate);
        // die();
        return view('backend.report.investigation.create_report.ajax_view_load', compact('data'));
    }

    public function report_listReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        return view('backend.report.investigation.report_list.report_form');
    }


    public function show_report_list_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_pending_report_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.investigation.report_list.ajax_view_load', compact('data'));
    }


    public function referral_statementReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        return view('backend.report.refer.referral_statement.report_form');
        $permissionCheck = Helpers::get_permission('referral_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
    }


    public function show_referral_statement_report(Request $request)
    {
        $userid = Session::get('id');

        $account     = $request['account'];
        $type        = $request['type'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_referral_statement_data($fromDate, $toDate);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.refer.referral_statement.ajax_view_load', compact('data'));
    }

    public function commissionReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['staff'] = Staffs::all();
        return view('backend.report.refer.commission_report.report_form', compact('data'));
    }


    public function show_commission_report(Request $request)
    {
        $userid = Session::get('id');

        $staff       = $request['staff'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_commission_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.refer.commission_report.ajax_view_load', compact('data'));
    }

    public function commission_summaryReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['staff'] = Staffs::all();

        return view('backend.report.refer.commission_summary.report_form', compact('data'));
    }


    public function show_commission_summary_report(Request $request)
    {
        $userid = Session::get('id');

        $staff        = $request['staff'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_commission_summary_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.refer.commission_summary.ajax_view_load', compact('data'));
    }

    public function payout_reportReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_reports', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['staff'] = Staffs::all();
        return view('backend.report.refer.payout_report.report_form', compact('data'));
    }


    public function show_payout_report_report(Request $request)
    {
        $userid = Session::get('id');

        $staff        = $request['staff'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_payout_report_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.report.refer.payout_report.ajax_view_load', compact('data'));
    }


    public function attandanceReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('staff_attendance', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['role'] = Role::all();
        return view('backend.hr.attendance.report.report_form', compact('data'));
    }


    public function show_attandance_report(Request $request)
    {
        $userid = Session::get('id');

        $staff        = $request['staff'];

        $data['fromDate']    = date('m-Y', strtotime($request['fromDate']));

        $data['staff'] = Staffs::all();

        // $data['all_data'] = Custom_model::get_payout_report_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.hr.attendance.report.ajax_view_load', compact('data'));
    }

    public function salary_assignReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_assign', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['role'] = Role::all();
        $data['designation'] = Staff_designation::all();
        return view('backend.hr.payroll.salary_assign.report_form', compact('data'));
    }


    public function show_salary_assign_report(Request $request)
    {
        $userid = Session::get('id');

        $staff         = $request['staff'];
        $designation   = $request['designation'];

        $data['staff'] = Staffs::all();
        $data['salary_template'] = Salary_template::all();

        $data['all_data'] = Custom_model::get_employee_data($staff, $designation);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.hr.payroll.salary_assign.ajax_view_load', compact('data'));
    }

    public function storeSalaryTemplate(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_assign', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $stafflist    = $request['stafflist'];
        $salary_template    = $request['salary_template'];

        $i = 0;
        foreach ($stafflist as $val) {

            $staff = Staffs::where('id', '=', $val)->first();
            $staff->salary_template_id           = $salary_template[$i];
            $staff->save();
            $i++;
        }

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('salary_assignReport')->with('success', 'Information has been updated!!');
    }

    public function salary_paymentReport(Request $request)
    {

        $data['role'] = Role::all();
        return view('backend.hr.payroll.salary_payment.report_form', compact('data'));
    }


    public function show_salary_payment_report(Request $request)
    {
        $userid = Session::get('id');

        $staffRoll        = $request['staff'];

        $fromDate    = date('m-Y', strtotime($request['fromDate']));
        $month       = $fromDate[0];
        $year        = $fromDate[1];

        $data['staff'] = Staffs::all();

        $data['all_data'] = Custom_model::get_employee_payment_data($staffRoll, $month, $year);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.hr.payroll.salary_payment.ajax_view_load', compact('data'));
    }

    public function salary_summaryReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('salary_summary_report', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['role'] = Role::all();
        return view('backend.hr.payroll.salary_summary.report_form', compact('data'));
    }


    public function show_salary_summary_report(Request $request)
    {
        $userid = Session::get('id');

        $fromDate    = date('m-Y', strtotime($request['fromDate']));
        $month       = $fromDate[0];
        $year        = $fromDate[1];

        $data['staff'] = Staffs::all();

        $data['all_data'] = Custom_model::get_summary_data($month, $year);

        // $data['all_data'] = Custom_model::get_payout_report_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.hr.payroll.salary_summary.ajax_view_load', compact('data'));
    }



    public function set_referReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_assign', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['role'] = Role::all();
        $data['Lab_test'] = Lab_test::all();
        return view('backend.refer.set_refer.report_form', compact('data'));
    }


    public function show_set_refer_report(Request $request)
    {
        $userid = Session::get('id');

        $staff         = $request['staff_role'];
        $commission    = $request['commission'];
        $data['test_id'] = $request['commission'];


        $data['all_data'] = Custom_model::get_staff_by_role($staff, $commission);
        // echo "<pre>";
        // print_r($staff);
        // die();
        return view('backend.refer.set_refer.ajax_view_load', compact('data'));
    }

    public function storeSetRefer(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_assign', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userId = Auth::user();
        $userid = $userId['id'];

        $assign    = $request['assign'];
        $test_id    = $request['test_id'];
        // echo "<pre>";
        // print_r($userid);
        // die();
        foreach ($assign as $key => $value) {
            if (isset($value['cb_select_staff'])) {

                if ($value['cb_select_staff'] == 0) {

                    $referral_commission = new Referral_commission();
                    $referral_commission->staff_id     = $value['staff_id'];
                    $referral_commission->test_id      = $test_id;
                    $referral_commission->percentage   = $value['percentage'];
                    $referral_commission->date         = date("Y-m-d");
                    $referral_commission->assign_by    = $userid;
                    $referral_commission->save();
                } else {

                    $referral_commission = Referral_commission::where('id', '=', $value['cb_select_staff'])->first();
                    $referral_commission->staff_id     = $value['staff_id'];
                    $referral_commission->test_id      = $test_id;
                    $referral_commission->percentage   = $value['percentage'];
                    $referral_commission->assign_by    = $userid;
                    $referral_commission->save();
                }
            } else {

                $whereArray = array('staff_id' => $value['staff_id'], 'test_id' => $test_id);

                $query = DB::table('referral_commissions');
                foreach ($whereArray as $field => $value) {
                    $query->where($field, $value);
                }
                $query->delete();


                // $dlt_ref_commission = Referral_commission::where('staff_id', '=', $value['staff_id'])->where('test_id', '=', $test_id)->first();
                // $dlt_ref_commission->delete();
            }
        }


        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('set_referReport')->with('success', 'Information has been updated!!');
    }

    public function ReferList(Request $request)
    {
        $permissionCheck = Helpers::get_permission('referral_assign', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['all_data'] = Custom_model::get_staff_commission_list();
        return view('backend.refer.referList.ajax_view_load', compact('data'));
    }

    public function TestBillListReport(Request $request)
    {
        $permissionCheck = Helpers::get_permission('lab_test_bill', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['all_data'] = Custom_model::get_bill_list();
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.pathology.billing.report_form', compact('data'));
    }


    public function show_test_bill_report(Request $request)
    {
        $userid = Session::get('id');

        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_bill_list_date_wise($fromDate, $toDate);

        // $data['all_data'] = Custom_model::get_payout_report_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.pathology.billing.ajax_view_load', compact('data'));
    }


    public function myCommission(Request $request)
    {
        $permissionCheck = Helpers::get_permission('my_commission', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        return view('backend.refer.my_commission.report_form');
    }


    public function show_my_commission_report(Request $request)
    {
        $permissionCheck = Helpers::get_permission('my_commission', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_commission_data_doctor_wise($fromDate, $toDate);

        // $data['all_data'] = Custom_model::get_payout_report_data($fromDate, $toDate, $staff);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.refer.my_commission.ajax_view_load', compact('data'));
    }
}
