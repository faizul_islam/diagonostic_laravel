<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Models\Patient_categorie;
use App\Models\Users;
use App\Models\Patient;
use App\Models\Patient_document;
use App\Http\Helpers;
use App\Models\Custom_model;


class PatientController extends Controller
{


    public function PatientList()
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Patient::all();
        $data['patient'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.patient.index', compact('data'));
    }

    public function patientForm()
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $datas = Patient_categorie::all();
        $data['category'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.patient.create', compact('data'));
    }


    public function storePatient(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $value = new Patient();
        $uniqId = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 7);
        $value->name            = $request['name'];
        $value->sex             = $request['gender'];
        $value->patient_id      = $uniqId;
        $value->birthday        = date('Y-m-d', strtotime($request['birthday']));
        $value->age             = $request['age'];
        $value->marital_status  = $request['marital_status'];
        $value->mobileno        = $request['mobile_no'];
        $value->email           = $request['email'];
        $value->category_id     = $request['category_id'];
        $value->blood_group     = $request['blood_group'];
        $value->blood_pressure  = $request['blood_pressure'];
        $value->height          = $request['height'];
        $value->weight          = $request['weight'];
        $value->address         = $request['address'];
        $value->guardian        = $request['guardian'];
        $value->relationship    = $request['relationship'];
        $value->gua_mobileno    = $request['gua_mobileno'];

        if (!empty($request->photo)) {
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->photo->extension();

            $request->photo->move(public_path('assets/images/uploads/patient'), $imageName);
            $value->photo          = $imageName;
        }
        $value->save();
        $patientId = $value->id;

        if (!empty($patientId)) {
            $hashedPassword = Hash::make($request['password']);
            $user = new Users();
            $user->user_id             = $patientId;
            $user->role                = 7;
            $user->active              = 1;
            $user->username            = $request['username'];
            $user->email               = $request['username'];
            $user->password            = $hashedPassword;
            $user->save();
        }
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PatientList')->with('success', 'Information has been updated!!');
    }


    public function ProfileView(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['billlist'] = Custom_model::get_bill_list_individual($id);

        $editVal = Patient::find($id);
        $data['editVal'] = $editVal->toArray();

        $datas = Patient_categorie::all();
        $data['category'] = $datas->toArray();

        $document = Patient_document::where('patient_id', '=', $data['editVal']['id'])->get();
        $data['document'] = $document->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.patient.profile', compact('data'));
    }

    public function PatientUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $value = Patient::where('id', '=', $id)->first();
        $value->name            = $request['name'];
        $value->sex             = $request['gender'];
        $value->birthday        = date('Y-m-d', strtotime($request['birthday']));
        $value->age             = $request['age'];
        $value->marital_status  = $request['marital_status'];
        $value->mobileno        = $request['mobile_no'];
        $value->email           = $request['email'];
        $value->category_id     = $request['category_id'];
        $value->blood_group     = $request['blood_group'];
        $value->blood_pressure  = $request['blood_pressure'];
        $value->height          = $request['height'];
        $value->weight          = $request['weight'];
        $value->address         = $request['address'];
        $value->guardian        = $request['guardian'];
        $value->relationship    = $request['relationship'];
        $value->gua_mobileno    = $request['gua_mobileno'];

        if (!empty($request->photo)) {
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->photo->extension();

            $request->photo->move(public_path('assets/images/uploads/patient'), $imageName);
            $value->photo          = $imageName;
        }
        $value->save();
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole != 7) {
            $request->session()->flash('alert-success', 'Information successfully updated!');
            return redirect('PatientList')->with('success', 'Information has been updated!!');
        } else {
            $request->session()->flash('alert-success', 'Information successfully updated!');
            return redirect('patientProfile')->with('success', 'Information has been updated!!');
        }
    }


    public function destroyPatient(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $patient = Patient::find($id);
        $patient->delete();


        $user = Users::where('user_id', '=', $id)->first();
        $user->delete();


        return redirect('PatientList')->with('success', 'Information has been Deleted!!');
    }

    public function loginDupCheck(Request $request)
    {

        $email = $request['email'];
        $user = Users::where('email', '=', $email)->first();
        if (empty($user)) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function Category(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient_category', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Patient_categorie::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.patient.category.index', compact('data'));
    }


    public function storeCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient_category', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $cat = new Patient_categorie();

        $cat->name           = $request['name'];


        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Category')->with('success', 'Information has been updated!!');
    }


    public function editCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient_category', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $datas = Patient_categorie::all();
        $data['val'] = $datas->toArray();

        $editVal = Patient_categorie::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.patient.category.edit', compact('data'));
    }

    public function CategoryUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $cat = Patient_categorie::where('id', '=', $id)->first();

        $cat->name           = $request['name'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Category')->with('success', 'Information has been updated!!');
    }


    public function destroyCategory(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient_category', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Patient_categorie::find($id);
        $cat->delete();
        return redirect('Category')->with('success', 'Information has been Deleted!!');
    }


    public function DeactiveList(Request $request)
    {

        $permissionCheck = Helpers::get_permission('patient', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $datas = Users::where('active', '=', 0)->get();
        $data['deactiveList'] = $datas->toArray();

        // echo "<pre>";
        // print_r($data);
        // die();
        return view('backend.patient.deactiveList', compact('data'));
    }

    public function activeUser(Request $request)
    {
        $userid = Session::get('id');
        $cat = new Users();
        $activeReqIds    = $request['activeData'];
        foreach ($activeReqIds as $val) {
            $page = Users::where('id', '=', $val)->first();
            $page->active           = 1;
            $page->save();
        }

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('DeactiveList')->with('success', 'Information has been updated!!');
    }

    public function storePatientDocument(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $value = new Patient_document();

        $value->patient_id      = $request['patient_id'];
        $value->title           = $request['title'];
        $value->type            = $request['type'];
        $value->remarks         = $request['remarks'];
        $value->file_name       = $request['file_name'];
        $value->enc_name        = $request['file_name'];


        if (!empty($request->file_name)) {
            $request->validate([
                'file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->file_name->extension();

            $request->file_name->move(public_path('assets/images/uploads/patientDocument'), $imageName);
            $value->file_name          = $imageName;
        }
        $value->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PatientList')->with('success', 'Information has been updated!!');
    }

    public function deletePatientDocument(Request $request)
    {
        $permissionCheck = Helpers::get_permission('patient', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $Patient_document = Patient_document::find($id);
        $Patient_document->delete();

        return redirect('PatientList')->with('success', 'Information has been Deleted!!');
    }

    public function patientProfile(Request $request)
    {

        $userId = Auth::user();
        $id = $userId['user_id'];


        $editVal = Patient::find($id);
        $data['editVal'] = $editVal->toArray();

        $datas = Patient_categorie::all();
        $data['category'] = $datas->toArray();

        return view('backend.patient.patient_wise_profile', compact('data'));
    }


    public function storePatientForAppointment(Request $request)
    {


        $value = new Patient();
        $uniqId = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 7);
        $value->name            = $request['name'];
        $value->sex             = $request['gender'];
        $value->patient_id      = $uniqId;
        $value->birthday        = date('Y-m-d', strtotime($request['birthday']));
        $value->age             = $request['age'];
        $value->marital_status  = $request['marital_status'];
        $value->mobileno        = $request['mobile_no'];
        $value->email           = $request['email'];
        $value->category_id     = $request['category_id'];
        $value->blood_group     = $request['blood_group'];
        $value->blood_pressure  = $request['blood_pressure'];
        $value->height          = $request['height'];
        $value->weight          = $request['weight'];
        $value->address         = $request['address'];
        $value->guardian        = $request['guardian'];
        $value->relationship    = $request['relationship'];
        $value->gua_mobileno    = $request['gua_mobileno'];

        if (!empty($request->photo)) {
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->photo->extension();

            $request->photo->move(public_path('assets/images/uploads/patient'), $imageName);
            $value->photo          = $imageName;
        }
        $value->save();
        $patientId = $value->id;

        if (!empty($patientId)) {
            $hashedPassword = Hash::make($request['password']);
            $user = new Users();
            $user->user_id             = $patientId;
            $user->role                = 7;
            $user->active              = 1;
            $user->username            = $request['username'];
            $user->email               = $request['username'];
            $user->password            = $hashedPassword;
            $user->save();
        }
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('PatientList')->with('success', 'Information has been updated!!');
    }
}
