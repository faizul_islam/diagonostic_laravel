<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Hash;
use App\Models\Staff_designation;
use App\Models\Staff_department;
use App\Models\Staffs;
use App\Models\Role;
use App\Models\Staff_bank_account;
use App\Models\Users;
use App\Models\Staff_document;
use App\Http\Helpers;


class EmployeeController extends Controller
{

    public function EmployeeList(Request $request)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Staffs::all();
        $data['staff'] = $datas->toArray();

        return view('backend.employee.employee.index', compact('data'));
    }

    public function EmployeeForm(Request $request)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['val']      = Staffs::all();
        $data['designation']      = Staff_designation::all();
        $data['department']       = Staff_department::all();
        $data['role']       = Role::all();

        return view('backend.employee.employee.create', compact('data'));
    }

    public function storeEmployee(Request $request)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');

        $Staff = new Staffs();
        $uniqId = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 7);
        $Staff->staff_id        = $uniqId;
        $Staff->name            = $request['name'];
        $Staff->gender          = $request['gender'];
        $Staff->department      = $request['department'];
        $Staff->designation     = $request['designation'];
        $Staff->qualification   = nl2br($request['qualification']);
        $Staff->joining_date    = date('Y-m-d', strtotime($request['joining_date']));

        $Staff->birthday        = date('Y-m-d', strtotime($request['birthday']));
        $Staff->religion        = $request['religion'];
        $Staff->marital_status  = $request['marital_status'];
        $Staff->mobileno        = $request['mobile_no'];
        $Staff->email           = $request['email'];
        $Staff->blood_group     = $request['blood_group'];
        $Staff->address         = $request['address'];


        $Staff->facebook_url    = $request['facebook_url'];
        $Staff->linkedin_url    = $request['linkedin_url'];
        $Staff->twitter_url     = $request['twitter_url'];

        if (!empty($request->photo)) {
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->photo->extension();

            $request->photo->move(public_path('assets/images/uploads/employee'), $imageName);
            $Staff->photo          = $imageName;
        }
        $Staff->save();
        $employeeId = $Staff->id;

        if (!empty($employeeId)) {

            $bank = new Staff_bank_account();
            $bank->staff_id           = $employeeId;
            $bank->bank_name          = $request['bank_name'];
            $bank->holder_name        = $request['holder_name'];
            $bank->bank_branch        = $request['bank_branch'];
            $bank->bank_address       = $request['bank_address'];
            $bank->ifsc_code          = $request['ifsc_code'];
            $bank->account_no         = $request['account_no'];
            $bank->save();
        }


        if (!empty($employeeId)) {
            $hashedPassword = Hash::make($request['password']);
            $user = new Users();
            $user->user_id             = $employeeId;
            $user->role                = $request['role'];
            $user->active              = 1;
            $user->email               = $request['username'];
            $user->username            = $request['username'];
            $user->password            = $hashedPassword;
            $user->save();
        }

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('EmployeeList')->with('success', 'Information has been Added!!');
    }

    public function StaffProfileView($id)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $editVal = Staffs::find($id);
        $data['editVal'] = $editVal->toArray();

        $data['designation']      = Staff_designation::all();
        $data['department']       = Staff_department::all();
        $data['role']             = Role::all();

        $document = Staff_document::where('staff_id', '=', $data['editVal']['id'])->get();
        $data['document'] = $document->toArray();

        $data['bank'] = Staff_bank_account::where('staff_id', '=', $data['editVal']['id'])->get();

        // echo "<pre>";    print_r($data);die();
        return view('backend.employee.employee.profile', compact('data'));
    }

    public function EmployeeUpdate(Request $request, $id)
    {
        $userid = Session::get('id');


        $Staff = Staffs::where('id', '=', $id)->first();

        $Staff->name            = $request['name'];
        $Staff->gender             = $request['gender'];
        $Staff->department             = $request['department'];
        $Staff->designation             = $request['designation'];
        $Staff->qualification             = nl2br($request['qualification']);
        $Staff->joining_date             = date('Y-m-d', strtotime($request['joining_date']));

        $Staff->birthday        = date('Y-m-d', strtotime($request['birthday']));
        $Staff->religion             = $request['religion'];
        $Staff->marital_status  = $request['marital_status'];
        $Staff->mobileno        = $request['mobile_no'];
        $Staff->email           = $request['email'];
        $Staff->blood_group     = $request['blood_group'];
        $Staff->address         = $request['address'];


        $Staff->facebook_url         = $request['facebook_url'];
        $Staff->linkedin_url    = $request['linkedin_url'];
        $Staff->twitter_url    = $request['twitter_url'];

        if (!empty($request->photo)) {
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->photo->extension();

            $request->photo->move(public_path('assets/images/uploads/employee'), $imageName);
            $Staff->photo          = $imageName;
        }
        $Staff->save();



        // $bank = Staffs::where('staff_id', '=', $id)->first();
        // $bank->bank_name          = $request['bank_name'];
        // $bank->holder_name        = $request['holder_name'];
        // $bank->bank_branch        = $request['bank_branch'];
        // $bank->bank_address       = $request['bank_address'];
        // $bank->ifsc_code          = $request['ifsc_code'];
        // $bank->account_no         = $request['account_no'];
        // $bank->save();



        // if (!empty($employeeId)) {
        //     $hashedPassword = Hash::make($request['password']);
        //     $user = new Users();
        //     $user->user_id             = $employeeId;
        //     $user->role                = $request['role'];
        //     $user->active              = 1;
        //     $user->username            = $request['username'];
        //     $user->password            = $hashedPassword;
        //     $user->save();
        // }

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('EmployeeList')->with('success', 'Information has been Added!!');
    }

    public function deleteStaff(Request $request)
    {

        $permissionCheck = Helpers::get_permission('employee', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $patient = Staffs::find($id);
        $patient->delete();


        $user = Users::where('user_id', '=', $id)->first();
        $user->delete();


        return redirect('EmployeeList')->with('success', 'Information has been Deleted!!');
    }


    public function storeStaffDocument(Request $request)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $value = new Staff_document();

        $value->staff_id      = $request['staff_id'];
        $value->title           = $request['title'];
        $value->category_id            = $request['category_id'];
        $value->remarks         = $request['remarks'];
        $value->file_name       = $request['file_name'];
        $value->enc_name        = $request['file_name'];


        if (!empty($request->file_name)) {
            $request->validate([
                'file_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->file_name->extension();

            $request->file_name->move(public_path('assets/images/uploads/employee'), $imageName);
            $value->file_name          = $imageName;
        }
        $value->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('EmployeeList')->with('success', 'Information has been updated!!');
    }

    public function deleteStaffDocument(Request $request)
    {
        $permissionCheck = Helpers::get_permission('employee', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $Patient_document = Staff_document::find($id);
        $Patient_document->delete();

        return redirect('EmployeeList')->with('success', 'Information has been Deleted!!');
    }


    public function Designation(Request $request)
    {
        $permissionCheck = Helpers::get_permission('designation', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val']      = Staff_designation::all();

        return view('backend.employee.designation.index', compact('data'));
    }


    public function storeDesignation(Request $request)
    {
        $permissionCheck = Helpers::get_permission('designation', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $designation = new Staff_designation();
        $designation->name      = $request['name'];
        $designation->save();

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Designation')->with('success', 'Information has been Added!!');
    }


    public function editDesignation(Request $request)
    {
        $permissionCheck = Helpers::get_permission('designation', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];


        $data['val'] = Staff_designation::all();

        $data['editVal'] = Staff_designation::find($id);
        // $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.employee.designation.edit', compact('data'));
    }

    public function DesignationUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $designation = Staff_designation::where('id', '=', $id)->first();

        $designation->name       = $request['name'];
        $designation->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Designation')->with('success', 'Information has been updated!!');
    }


    public function destroyDesignation(Request $request)
    {
        $permissionCheck = Helpers::get_permission('designation', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $designation = Staff_designation::find($id);
        $designation->delete();
        return redirect('Designation')->with('success', 'Information has been Deleted!!');
    }


    public function Department(Request $request)
    {
        $permissionCheck = Helpers::get_permission('department', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val']      = Staff_department::all();

        return view('backend.employee.department.index', compact('data'));
    }


    public function storeDepartment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('department', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $department = new Staff_department();
        $department->name      = $request['name'];
        $department->save();

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Department')->with('success', 'Information has been Added!!');
    }


    public function editDepartment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('department', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $data['val'] = Staff_department::all();

        $data['editVal'] = Staff_department::find($id);
        // $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.employee.department.edit', compact('data'));
    }

    public function DepartmentUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $department = Staff_department::where('id', '=', $id)->first();

        $department->name       = $request['name'];
        $department->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Department')->with('success', 'Information has been updated!!');
    }


    public function destroyDepartment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('department', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];
        $department = Staff_department::find($id);
        $department->delete();
        return redirect('Department')->with('success', 'Information has been Deleted!!');
    }

    public function verifyPassword(Request $request)
    {

        $old_password = $request['old_password'];
        $staff_id     = $request['staff_id'];
        $userInfo = Users::where('user_id', '=', $staff_id)->first();

        if (Hash::check($old_password, $userInfo['password'])) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function updatePassword(Request $request, $id)
    {

        $staff_id     = $id;
        $user = Users::where('user_id', '=', $staff_id)->first();

        $hashedPassword = Hash::make($request['password']);
        $user->password            = $hashedPassword;
        $user->save();
        $request->session()->flash('alert-success', 'Your password successfully changed!');
        return redirect('/StaffProfileView/' . $staff_id);
    }
}
