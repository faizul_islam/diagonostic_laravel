<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Staffs;
use App\Models\Role;
use App\Models\Permission_module;
use App\Models\Staff_privilege;
use App\Http\Helpers;




class RoleController extends Controller
{
    public function Role(Request $request)
    {

        $data['val']   = Role::all()->where('id', '!=', 1);
        return view('backend.permission.role.index', compact('data'));
    }


    public function storeRole(Request $request)
    {
        $userid = Session::get('id');

        $Role = new Role();

        $Role->name      = $request['name'];
        $Role->prefix    = strtolower(str_replace(' ', '', $request['name']));


        $Role->save();

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Role')->with('success', 'Information has been Added!!');
    }


    public function editRole($id)
    {
        $data['editVal'] = Role::find($id);
        $data['val']   = Role::all();
        // echo "<pre>";    print_r($data);die();
        return view('backend.permission.role.edit', compact('data'));
    }


    public function RoleUpdate(Request $request, $id)
    {


        $Role = Role::where('id', '=', $id)->first();


        $Role->name      = $request['name'];
        $Role->prefix    = strtolower(str_replace(' ', '', $request['name']));

        $Role->save();

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Role')->with('success', 'Information has been updated!!');
    }


    public function destroyRole(Request $request)
    {
        $id = $request['id'];
        $cat = Role::find($id);
        $cat->delete();
        return redirect('Role')->with('success', 'Information has been Deleted!!');
    }

    public function permission($id)
    {
        $data['val']   = Permission_module::all();
        $data['role_id']   = $id;
        // echo "<pre>";
        // print_r($id);
        // die();
        return view('backend.permission.permission.index', compact('data'));
    }

    public function updatePermission(Request $request)
    {

        $role_id    =  $request['role_id'];
        $privileges =  $request['privileges'];
        foreach ($privileges as $key => $value) {
            $is_add = (isset($value['add']) ? 1 : 0);
            $is_edit = (isset($value['edit']) ? 1 : 0);
            $is_view = (isset($value['view']) ? 1 : 0);
            $is_delete = (isset($value['delete']) ? 1 : 0);
            $arrayData = array(
                'role_id' => $role_id,
                'permission_id' => $key,
                'is_add' => $is_add,
                'is_edit' => $is_edit,
                'is_view' => $is_view,
                'is_delete' => $is_delete,
            );
            $exist_privileges = DB::table('staff_privileges')->where('role_id', $role_id)->where('permission_id', $key)->exists();
            // print_r($exist_privileges);
            // die();
            if ($exist_privileges > 0) {
                $Staff_privilege = Staff_privilege::where('role_id', $role_id)->where('permission_id', $key)->first();
                // print_r($Staff_privilege);
                // die();
                $Staff_privilege->role_id       = $role_id;
                $Staff_privilege->permission_id = $key;
                $Staff_privilege->is_add        = $is_add;
                $Staff_privilege->is_edit       = $is_edit;
                $Staff_privilege->is_view       = $is_view;
                $Staff_privilege->is_delete     = $is_delete;
                $Staff_privilege->save();
            } else {
                $Staff_privilege_add = new Staff_privilege();
                $Staff_privilege_add->role_id       = $role_id;
                $Staff_privilege_add->permission_id = $key;
                $Staff_privilege_add->is_add        = $is_add;
                $Staff_privilege_add->is_edit       = $is_edit;
                $Staff_privilege_add->is_view       = $is_view;
                $Staff_privilege_add->is_delete     = $is_delete;
                $Staff_privilege_add->save();
            }
        }

        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Role')->with('success', 'Information has been updated!!');
    }
}
