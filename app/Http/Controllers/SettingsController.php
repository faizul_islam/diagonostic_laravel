<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use App\Models\Global_settings;
use App\Models\Front_cms_settings;
use App\Models\Front_cms_menu;
use App\Models\Front_cms_page;
use App\Models\Front_cms_slider;
use App\Models\Front_cms_feature;
use App\Models\Front_cms_testimonial;
use App\Models\Front_cms_services_list;
use App\Models\Front_cms_faq_list;
use App\Models\Lab_report_template;
use App\Http\Helpers;


class SettingsController extends Controller
{
    public function index(Request $request)
    {
        $permissionCheck = Helpers::get_permission('global_setting', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['language'] = DB::table('languages')->select('*')->get();
        $data['language_list'] = DB::table('language_list')->select('*')->where('status', '=', 1)->get();
        $global_config = Global_settings::all();
        $data['global_config'] = $global_config->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.global_settings.index', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $permissionCheck = Helpers::get_permission('global_setting', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $settings = Global_settings::where('id', '=', $id)->first();
        $settings->institute_name = $request['institute_name'];
        $settings->mobileno         = $request['mobileno'];
        $settings->address          = $request['address'];
        $settings->currency         = $request['currency'];
        $settings->currency_symbol  = $request['currency_symbol'];
        $settings->translation      = $request['translation'];
        $settings->timezone         = $request['timezone'];
        $settings->date_format      = $request['date_format'];
        $settings->footer_text      = $request['footer_text'];
        $settings->facebook_url     = $request['facebook_url'];
        $settings->twitter_url      = $request['twitter_url'];
        $settings->linkedin_url     = $request['linkedin_url'];
        $settings->youtube_url      = $request['youtube_url'];


        if (!empty($request->system_logo)) {
            $request->validate([
                'system_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->system_logo->extension();

            $request->system_logo->move(public_path('assets/images/uploads/settings'), $imageName);
            $settings->system_logo          = $imageName;
        }

        if (!empty($request->text_logo)) {
            $request->validate([
                'text_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->text_logo->extension();

            $request->text_logo->move(public_path('assets/images/uploads/settings'), $imageName);
            $settings->text_logo          = $imageName;
        }

        if (!empty($request->printing_logo)) {
            $request->validate([
                'printing_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->printing_logo->extension();

            $request->printing_logo->move(public_path('assets/images/uploads/settings'), $imageName);
            $settings->printing_logo          = $imageName;
        }



        // $conf->updated_by = $userid[0];
        // $conf->updated_at = date('Y-m-d H:i:s');
        $settings->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('globalSettings')->with('success', 'Information has been updated!!');
    }


    public function webSetting(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_setting', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $web_settings   = DB::table('front_cms_settings')->select('*')->get();
        $data['web_settings'] = $web_settings->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.web_settings.index', compact('data'));
    }


    public function wb_update(Request $request, $id)
    {
        $permissionCheck = Helpers::get_permission('frontend_setting', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $settings = Front_cms_settings::where('id', '=', $id)->first();
        // echo "<pre>";
        // print_r($settings);
        // die();
        $settings->application_title = $request['application_title'];
        $settings->address          = $request['address'];
        $settings->mobile_no        = $request['mobile_no'];
        $settings->fax              = $request['fax'];
        $settings->receive_contact_email  = $request['receive_contact_email'];
        $settings->working_hours    = $request['working_hours'];
        $settings->email            = $request['email'];
        $settings->footer_text      = $request['footer_text'];
        $settings->google_plus      = $request['google_plus'];
        $settings->pinterest_url    = $request['pinterest_url'];
        $settings->facebook_url     = $request['facebook_url'];
        $settings->twitter_url      = $request['twitter_url'];
        $settings->linkedin_url     = $request['linkedin_url'];
        $settings->youtube_url      = $request['youtube_url'];
        $settings->instagram_url    = $request['instagram_url'];
        $settings->about_us         = $request['about_us'];
        $settings->about_videos     = $request['about_videos'];


        if (!empty($request->logo)) {
            $request->validate([
                'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->logo->extension();

            $request->logo->move(public_path('assets/images/uploads/web_settings'), $imageName);
            $settings->logo          = $imageName;
        }

        if (!empty($request->fav_icon)) {
            $request->validate([
                'fav_icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->fav_icon->extension();

            $request->fav_icon->move(public_path('assets/images/uploads/web_settings'), $imageName);
            $settings->fav_icon          = $imageName;
        }


        if (!empty($request->video_image)) {
            $request->validate([
                'video_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->video_image->extension();

            $request->video_image->move(public_path('assets/images/uploads/web_settings'), $imageName);
            $settings->video_image          = $imageName;
        }

        // $conf->updated_by = $userid[0];
        // $conf->updated_at = date('Y-m-d H:i:s');
        $settings->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('webSetting')->with('success', 'Information has been updated!!');
    }


    public function webMenu(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_menu', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Front_cms_menu::all();
        $data['val'] = $datas->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.menu.index', compact('data'));
    }


    public function storeMenu(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_menu', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $menu = new Front_cms_menu();
        $menu->title = $request['title'];
        $menu->ordering         = $request['ordering'];
        $menu->alias         = Str::slug($request['title']);

        if (!empty($request['publish'])) {
            $menu->publish     = 1;
        } else {
            $menu->publish    = 0;
        }

        $menu->open_new_tab     = $request['open_new_tab'];
        if (!empty($request['open_new_tab'])) {
            $menu->open_new_tab     = 1;
        } else {
            $menu->open_new_tab     = 0;
        }
        $menu->ext_url_address  = $request['ext_url_address'];
        if (empty($request['ext_url_address'])) {
            $menu->ext_url      = 0;
        } else {
            $menu->ext_url      = 1;
        }

        $menu->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('webMenu')->with('success', 'Information has been updated!!');
    }

    public function editMenu(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_menu', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $role = Session::get('role');
        $id = $request['id'];

        $datas = Front_cms_menu::all();
        $data['val'] = $datas->toArray();

        $menu = Front_cms_menu::find($id);
        $data['menu'] = $menu->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.menu.edit', compact('data'));
    }


    public function menuUpdate(Request $request, $id)
    {
        $userid = Session::get('id');
        $menu = Front_cms_menu::where('id', '=', $id)->first();
        $menu->title = $request['title'];
        $menu->ordering         = $request['ordering'];
        $menu->alias         = Str::slug($request['title']);

        if (!empty($request['publish'])) {
            $menu->publish     = 1;
        } else {
            $menu->publish    = 0;
        }

        $menu->open_new_tab     = $request['open_new_tab'];
        if (!empty($request['open_new_tab'])) {
            $menu->open_new_tab     = 1;
        } else {
            $menu->open_new_tab     = 0;
        }
        $menu->ext_url_address  = $request['ext_url_address'];
        if (empty($request['ext_url_address'])) {
            $menu->ext_url      = 0;
        } else {
            $menu->ext_url      = 1;
        }

        $menu->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('webMenu')->with('success', 'Information has been updated!!');
    }


    public function ManagePage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('manage_page', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $datas = Front_cms_page::all();
        $data['val'] = $datas->toArray();

        $menu = Front_cms_menu::all();
        $data['menu'] = $menu->toArray();
        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.manage_page.index', compact('data'));
    }


    public function storeManagePage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('manage_page', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $page = new Front_cms_page();

        $page->page_title           = $request['page_title'];
        $page->menu_id              = $request['menu_id'];
        $page->meta_keyword         = $request['meta_keyword'];
        $page->meta_description     = $request['meta_description'];
        $page->content              = $request['content'];

        if (!empty($request->banner_image)) {
            $request->validate([
                'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->banner_image->extension();

            $request->banner_image->move(public_path('assets/images/uploads/web_settings'), $imageName);
            $page->banner_image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('ManagePage')->with('success', 'Information has been updated!!');
    }


    public function editManagePage(Request $request)
    {
        $permissionCheck = Helpers::get_permission('manage_page', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $role = Session::get('role');
        $id = $request['id'];
        if (!empty($userid)) {
            $datas = Front_cms_page::all();
            $data['val'] = $datas->toArray();

            $editVal = Front_cms_page::find($id);
            $data['editVal'] = $editVal->toArray();

            $menu = Front_cms_menu::all();
            $data['menu'] = $menu->toArray();


            // echo "<pre>";    print_r($data);die();
            return view('backend.frontend.manage_page.edit', compact('data'));
        } else {
            return redirect('user-login');
        }
    }

    public function ManagePageUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $page = Front_cms_page::where('id', '=', $id)->first();

        $page->page_title           = $request['page_title'];
        $page->menu_id              = $request['menu_id'];
        $page->meta_keyword         = $request['meta_keyword'];
        $page->meta_description     = $request['meta_description'];
        $page->content              = $request['content'];

        if (!empty($request->banner_image)) {
            $request->validate([
                'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->banner_image->extension();

            $request->banner_image->move(public_path('assets/images/uploads/web_settings'), $imageName);
            $page->banner_image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('ManagePage')->with('success', 'Information has been updated!!');
    }


    public function SliderInfo(Request $request)
    {

        $permissionCheck = Helpers::get_permission('frontend_slider', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $userid = Session::get('id');
        $role = Session::get('role');

        $datas = Front_cms_slider::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.slider.index', compact('data'));
    }


    public function storeSliderInfo(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_slider', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $page = new Front_cms_slider();

        $page->title           = $request['title'];
        $page->button_name              = $request['button_name'];
        $page->button_url         = $request['button_url'];
        $page->description     = $request['description'];


        if (!empty($request->image)) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->image->extension();

            $request->image->move(public_path('assets/images/uploads/slider'), $imageName);
            $page->image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('SliderInfo')->with('success', 'Information has been updated!!');
    }


    public function editSliderInfo(Request $request)
    {

        $permissionCheck = Helpers::get_permission('frontend_slider', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $role = Session::get('role');
        $id = $request['id'];

        $datas = Front_cms_slider::all();
        $data['val'] = $datas->toArray();

        $editVal = Front_cms_slider::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.slider.edit', compact('data'));
    }

    public function SliderInfoUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $page = Front_cms_slider::where('id', '=', $id)->first();

        $page->title           = $request['title'];
        $page->button_name     = $request['button_name'];
        $page->button_url      = $request['button_url'];
        $page->description     = $request['description'];


        if (!empty($request->image)) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->image->extension();

            $request->image->move(public_path('assets/images/uploads/slider'), $imageName);
            $page->image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('SliderInfo')->with('success', 'Information has been updated!!');
    }


    public function destroySlider(Request $request)
    {

        $permissionCheck = Helpers::get_permission('frontend_slider', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];

        $slider = Front_cms_slider::find($id);
        $slider->delete();
        return redirect('SliderInfo')->with('success', 'Information has been Deleted!!');
    }


    public function Feature(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_features', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Front_cms_feature::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.feature.index', compact('data'));
    }


    public function storeFeature(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_features', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $userid = Session::get('id');
        $page = new Front_cms_feature();

        $page->title           = $request['title'];
        $page->button_text     = $request['button_text'];
        $page->button_url      = $request['button_url'];
        $page->icon            = $request['icon'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Feature')->with('success', 'Information has been updated!!');
    }


    public function editFeature(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_features', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $datas = Front_cms_feature::all();
        $data['val'] = $datas->toArray();

        $editVal = Front_cms_feature::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.feature.edit', compact('data'));
    }

    public function FeatureUpdate(Request $request, $id)
    {


        $page = Front_cms_feature::where('id', '=', $id)->first();

        $page->title           = $request['title'];
        $page->button_text     = $request['button_text'];
        $page->icon     = $request['icon'];
        $page->button_url      = $request['button_url'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Feature')->with('success', 'Information has been updated!!');
    }


    public function destroyFeature(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_features', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $slider = Front_cms_feature::find($id);
        $slider->delete();
        return redirect('Feature')->with('success', 'Information has been Deleted!!');
    }


    public function Testimonial(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_testimonial', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $datas = Front_cms_testimonial::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.testimonial.index', compact('data'));
    }


    public function storeTestimonial(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_testimonial', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $page = new Front_cms_testimonial();

        $page->patient_name           = $request['patient_name'];
        $page->surname     = $request['surname'];
        $page->rank      = $request['rank'];
        $page->description     = $request['description'];
        $page->title     = $request['title'];


        if (!empty($request->image)) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->image->extension();

            $request->image->move(public_path('assets/images/uploads/testimonial'), $imageName);
            $page->image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Testimonial')->with('success', 'Information has been updated!!');
    }


    public function editTestimonial(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_testimonial', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $userid = Session::get('id');
        $role = Session::get('role');
        $id = $request['id'];
        if (!empty($userid)) {
            $datas = Front_cms_testimonial::all();
            $data['val'] = $datas->toArray();

            $editVal = Front_cms_testimonial::find($id);
            $data['editVal'] = $editVal->toArray();

            // echo "<pre>";    print_r($data);die();
            return view('backend.frontend.testimonial.edit', compact('data'));
        } else {
            return redirect('user-login');
        }
    }

    public function TestimonialUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $page = Front_cms_testimonial::where('id', '=', $id)->first();

        $page->patient_name           = $request['patient_name'];
        $page->surname     = $request['surname'];
        $page->rank      = $request['rank'];
        $page->description     = $request['description'];
        $page->title     = $request['title'];


        if (!empty($request->image)) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . '.' . $request->image->extension();

            $request->image->move(public_path('assets/images/uploads/testimonial'), $imageName);
            $page->image          = $imageName;
        }

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Testimonial')->with('success', 'Information has been updated!!');
    }


    public function destroyTestimonial(Request $request)
    {

        $permissionCheck = Helpers::get_permission('frontend_testimonial', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $slider = Front_cms_testimonial::find($id);
        $slider->delete();
        return redirect('Testimonial')->with('success', 'Information has been Deleted!!');
    }



    public function Service(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_services', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $datas = Front_cms_services_list::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.service.index', compact('data'));
    }


    public function storeService(Request $request)
    {

        $permissionCheck = Helpers::get_permission('frontend_services', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $page = new Front_cms_services_list();

        $page->title           = $request['title'];
        $page->icon            = $request['icon'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Service')->with('success', 'Information has been updated!!');
    }


    public function editService(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_services', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Front_cms_services_list::all();
        $data['val'] = $datas->toArray();

        $editVal = Front_cms_services_list::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.service.edit', compact('data'));
    }

    public function ServiceUpdate(Request $request, $id)
    {


        $page = Front_cms_services_list::where('id', '=', $id)->first();

        $page->title           = $request['title'];
        $page->icon            = $request['icon'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Service')->with('success', 'Information has been updated!!');
    }


    public function destroyService(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_services', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $id = $request['id'];

        $slider = Front_cms_services_list::find($id);
        $slider->delete();
        return redirect('Service')->with('success', 'Information has been Deleted!!');
    }

    public function Faq(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_faq', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $datas = Front_cms_faq_list::all();
        $data['val'] = $datas->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.faq.index', compact('data'));
    }


    public function storeFaq(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_faq', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $userid = Session::get('id');
        $page = new Front_cms_faq_list();

        $page->title           = $request['title'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Faq')->with('success', 'Information has been updated!!');
    }


    public function editFaq(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_faq', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $datas = Front_cms_faq_list::all();
        $data['val'] = $datas->toArray();

        $editVal = Front_cms_faq_list::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.frontend.faq.edit', compact('data'));
    }

    public function FaqUpdate(Request $request, $id)
    {

        $page = Front_cms_faq_list::where('id', '=', $id)->first();

        $page->title           = $request['title'];
        $page->description     = $request['description'];

        $page->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Faq')->with('success', 'Information has been updated!!');
    }


    public function destroyFaq(Request $request)
    {
        $permissionCheck = Helpers::get_permission('frontend_faq', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $slider = Front_cms_faq_list::find($id);
        $slider->delete();
        return redirect('Faq')->with('success', 'Information has been Deleted!!');
    }


    public function Template(Request $request)
    {

        $permissionCheck = Helpers::get_permission('test_report_template', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $data['val'] = Lab_report_template::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.investigation.template.index', compact('data'));
    }


    public function storeTemplate(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_report_template', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $userid = Session::get('id');
        $cat = new Lab_report_template();

        $cat->name           = $request['name'];
        $cat->template       = $request['template'];


        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Template')->with('success', 'Information has been updated!!');
    }


    public function editTemplate(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_report_template', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];

        $datas = Lab_report_template::all();
        $data['val'] = $datas->toArray();

        $editVal = Lab_report_template::find($id);
        $data['editVal'] = $editVal->toArray();

        // echo "<pre>";    print_r($data);die();
        return view('backend.report.investigation.template.edit', compact('data'));
    }

    public function TemplateUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $cat = Lab_report_template::where('id', '=', $id)->first();

        $cat->name           = $request['name'];
        $cat->template       = $request['template'];

        $cat->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Template')->with('success', 'Information has been updated!!');
    }


    public function destroyTemplate(Request $request)
    {
        $permissionCheck = Helpers::get_permission('test_report_template', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];

        $cat = Lab_report_template::find($id);
        $cat->delete();
        return redirect('Template')->with('success', 'Information has been Deleted!!');
    }
}
