<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Staffs;
use App\Models\Schedule;
use App\Models\Appointment;
use App\Models\Patient;
use App\Http\Helpers;
use App\Models\Custom_model;
use Auth;




class AppointmentController extends Controller
{
    public function index(Request $request)
    {

        $permissionCheck = Helpers::get_permission('appointment', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['val'] = Appointment::where('status', '!=', 2)->get();
        $data['staff'] = Staffs::where('designation', '=', 3)->get();
        $data['patient'] = Patient::all();

        return view('backend.appointment.index', compact('data'));
    }


    public function storeAppointment(Request $request)
    {
        $userId = Auth::user();
        $userRole = $userId['role'];
        if ($userRole != 7) {
            $permissionCheck = Helpers::get_permission('appointment', 'is_add');
            if ($permissionCheck == false) {
                return view('backend.access_denied');
            }
        }

        $appNoGet  = Appointment::latest()->first();
        $id = $appNoGet['appointment_id'];
        if (!empty($id)) {
            $bill = $id + 1;
        } else {
            $bill = 1;
        }
        $AppointmentNo = str_pad($bill, 4, '0', STR_PAD_LEFT);

        $Appointment = new Appointment();
        $date     = Carbon::createFromFormat('d/m/Y', $request['appointment_date'])->format('Y-m-d');

        $Appointment->appointment_id   = $AppointmentNo;
        $Appointment->doctor_id        = $request['doctor_id'];
        $Appointment->patient_id       = $request['patient_id'];
        $Appointment->consultation_fees = $request['consultation_fees'];
        $Appointment->discount         = $request['discount'];
        $Appointment->schedule         = $request['available_schedule'];
        $Appointment->remarks          = $request['remarks'];
        $Appointment->patient_type     = $request['patient_type'];
        $Appointment->appointment_date = $date;
        $Appointment->status           = 1;

        $Appointment->save();


        if ($userRole != 7) {
            $request->session()->flash('alert-success', 'Information successfully Added!');
            return redirect('Appointment')->with('success', 'Information has been Added!!');
        } else {
            $request->session()->flash('alert-success', 'Information successfully Added!');
            return redirect('myAppointment')->with('success', 'Information has been Added!!');
        }
    }


    public function editAppointment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('appointment', 'is_edit');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $id = $request['id'];


        $data['val'] = Schedule::all();
        $data['editVal'] = Schedule::find($id);
        $data['staff'] = Staffs::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.appointment.edit', compact('data'));
    }

    public function AppointmentUpdate(Request $request, $id)
    {
        $userid = Session::get('id');

        $Schedule = Schedule::where('id', '=', $id)->first();

        $Schedule->day              = $request['day'];
        $Schedule->doctor_id        = $request['doctor_id'];
        $Schedule->time_start       = $request['time_start'];
        $Schedule->time_end         = $request['time_end'];
        $Schedule->per_patient_time = $request['per_patient_time'];
        $Schedule->consultation_fees = $request['consultation_fees'];
        $Schedule->patient_type      = $request['patient_type'];

        $Schedule->save();
        $request->session()->flash('alert-success', 'Information successfully updated!');
        return redirect('Appointment')->with('success', 'Information has been updated!!');
    }


    public function destroyAppointment(Request $request)
    {
        $permissionCheck = Helpers::get_permission('appointment', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Appointment::find($id);
        $cat->delete();
        return redirect('Appointment')->with('success', 'Information has been Deleted!!');
    }

    public function UpdateStatus(Request $request)
    {
        $permissionCheck = Helpers::get_permission('appointment', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $p_amount = $request['paid_amount'];
        $Appointment = Appointment::where('id', '=', $id)->first();

        $Appointment->status              = 2;
        $Appointment->paid_amount         = $p_amount;
        $Appointment->save();

        return redirect('Appointment')->with('success', 'Statu has been Changed!!');
    }

    public function requested_list(Request $request)
    {
        $permissionCheck = Helpers::get_permission('appointment', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['val']   = Appointment::all();
        $data['staff'] = Staffs::where('designation', '=', 3)->get();
        $data['patient'] = Patient::all();
        return view('backend.appointment.requested_list', compact('data'));
    }

    public function get_appointment_schedule(Request $request)
    {


        $doctor_id  = $request['doctor_id'];
        $date     = Carbon::createFromFormat('d/m/Y', $request['appointment_date'])->format('Y-m-d');


        $shift = $request['shift'];
        $appointment_id = $request['appointment_id'];
        $schedule_id = (isset($_POST['schedule_id']) ? $_POST['schedule_id'] : 0);

        $nameOfDay = date('l', strtotime($date));

        $query = Schedule::where('doctor_id', '=', $doctor_id)->where('day', '=', $nameOfDay)->where('shift', '=', $shift)->get();



        if (count($query) > 0) {
            $count = 1;
            $srow = $query[0];

            $per_time = $srow['per_patient_time'];

            $consultation_fees = $srow['consultation_fees'];
            $min = $per_time * 60;
            $start = strtotime($srow['time_start']);
            $end = strtotime($srow['time_end']) - $per_time;
            $html = "<option value=''>" . 'select' . "</option>";
            for ($i = $start; $i <= $end; $i = $i + $min) {
                $cID = $count++;
                if ($appointment_id != "") {
                    // $this->db->where_not_in('id', $appointment_id);
                }

                // $this->db->where_in('status', array(1, 2));

                $exist = Appointment::where('doctor_id', '=', $doctor_id)->where('appointment_date', '=', $date)->where('shift', '=', $shift)->where('schedule', '=', $cID)->get();
                // print_r($exist);
                // die();
                // $this->db->where(array('doctor_id' => $doctor_id, 'appointment_date' => $date, 'schedule' => $cID));
                // $exist = $this->db->get('appointment')->num_rows();
                if (count($exist) > 0) {
                    continue;
                }

                $sel = ($cID == $schedule_id ? 'selected' : '');
                $html .= "<option value='" . $cID . "' " . $sel . ">" . date('h:i A', $i) . ' - ' . date('h:i A', $i + $min) . "</option>";
            }
        } else {
            $consultation_fees = '0.00';
            $html = "<option value=''>" . 'No Schedule Found' . "</option>";
        }
        echo json_encode(['schedule' => $html, 'fees' => $consultation_fees]);
    }

    public function ajax_consultation_view(Request $request)
    {

        $appointmentId = $request['appointmentId'];
        $data['appointment'] = Appointment::where('id', '=', $appointmentId)->get();
        // echo "<pre>";
        // print_r($data['appointment']);
        // die();
        return view('backend.appointment.ajax_consultation_view', compact('data'));
    }


    public function consultation_report(Request $request)
    {
        $permissionCheck = Helpers::get_permission('appointment', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }
        $data['staff'] = Staffs::where('designation', '=', 3)->get();
        return view('backend.appointment.report_form', compact('data'));
    }


    public function show_consultation_report(Request $request)
    {
        $userid = Session::get('id');

        $staff       = $request['staff'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $toDate      = Carbon::createFromFormat('d/m/Y', $request['toDate'])->format('Y-m-d');


        $data['all_data'] = Custom_model::get_consaltation_data($fromDate, $toDate, $staff);

        // echo "<pre>";
        // print_r($data['all_data']);
        // die();

        return view('backend.report.appointment.ajax_view_load', compact('data'));
    }

    public function ajax_get_consultation_fee(Request $request)
    {

        $doctor_id  = $request['doctor_id'];
        $patient_type  = $request['patient_type'];
        $date     = Carbon::createFromFormat('d/m/Y', $request['appointment_date'])->format('Y-m-d');

        $nameOfDay = date('l', strtotime($date));

        $query = Schedule::where('doctor_id', '=', $doctor_id)->where('day', '=', $nameOfDay)->get();
        $consultation_fees = $query[0]['consultation_fees'];
        $old_consultation_fees = $query[0]['old_consultation_fees'];

        if ($patient_type == 1) {
            echo $consultation_fees;
        } else {
            echo $old_consultation_fees;
        }
    }

    public function myAppointment(Request $request)
    {

        // $permissionCheck = Helpers::get_permission('appointment', 'is_view');
        // if ($permissionCheck == false) {
        //     return view('backend.access_denied');
        // }

        $userInfo = Auth::user();
        $userId = $userInfo['user_id'];
        $data['val'] = Appointment::where('status', '=', 1)->where('patient_id', '=', $userId)->get();
        $data['staff'] = Staffs::where('designation', '=', 3)->get();
        $data['patient'] = Patient::all();

        return view('backend.appointment.my_appointment', compact('data'));
    }
}
