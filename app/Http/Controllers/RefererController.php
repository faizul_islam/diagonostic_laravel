<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Models\Patient_categorie;
use App\Models\Users;
use App\Models\Patient;
use App\Models\Patient_document;
use App\Models\Payout_commission;
use App\Models\Role;
use App\Models\Payment_type;
use App\Models\Staffs;
use App\Models\Staff_balance;
use App\Http\Helpers;

class RefererController extends Controller
{


    public function Withdrawal()
    {
        $permissionCheck = Helpers::get_permission('commission_withdrawal', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['role'] = Role::all();
        $data['payout_commission'] = Payout_commission::all();
        $data['Payment_type'] = Payment_type::all();
        $data['staff'] = Staffs::all();
        // echo "<pre>";
        // print_r($data['payout_commission']);
        // die();
        return view('backend.refer.withdrawal.index', compact('data'));
    }




    public function storeWithdrawal(Request $request)
    {
        $permissionCheck = Helpers::get_permission('commission_withdrawal', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $billNo = Payout_commission::max('id');
        $userId = Auth::user()->id;
        $value = new Payout_commission();
        $amountHave = Staff_balance::where('staff_id', '=', $request['user_id'])->first();

        $value->staff_id        = $request['user_id'];
        $value->before_payout   = $amountHave['amount'];
        $value->pay_via         = $request['Payment_type'];
        $value->bill_no         = $billNo + 1;
        $value->created_at      = date('Y-m-d');
        $value->remarks         = $request['remarks'];
        $value->paid_by         = $userId;
        $value->amount          = $request['amount'];

        $value->save();
        $WidId = $value->id;

        if (!empty($WidId)) {
            $valuebl = Staff_balance::where('staff_id', '=', $request['user_id'])->first();
            $valuebl->amount                = $valuebl['amount'] - $request['amount'];

            $valuebl->save();
        }
        // $request->session()->flash('alert-success', 'Information successfully Save!');
        return redirect('Withdrawal')->with('success', 'Information has been Save!!');
    }



    public function destroyWithdrawal(Request $request)
    {

        $permissionCheck = Helpers::get_permission('commission_withdrawal', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }



        $WidId = $request['id'];
        $Payout_commission = Payout_commission::find($WidId);

        if (!empty($WidId)) {
            $valuebl = Staff_balance::where('staff_id', '=', $Payout_commission['staff_id'])->first();
            $valuebl->amount                = $valuebl['amount'] + $Payout_commission['amount'];
            $valuebl->save();
        }


        $Payout_commission->delete();



        return redirect('Withdrawal')->with('success', 'Information has been Deleted!!');
    }


    public function get_staff_balance(Request $request)
    {
        $staff = $request['staff'];

        $balanceInfo = Staff_balance::where('staff_id', '=', $staff)->first();
        if (!empty($balanceInfo['amount'])) {
            $amountIs = $balanceInfo['amount'];
        } else {
            $amountIs = 0;
        }
        echo $amountIs;
    }
}
