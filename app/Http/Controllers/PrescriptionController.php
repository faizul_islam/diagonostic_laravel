<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use PDF;
use Auth;
use Carbon\Carbon;
use App\Models\Staffs;
use App\Models\Schedule;
use App\Models\Patient_categorie;
use App\Models\Appointment;
use App\Models\Prescription;
use App\Models\Patient;
use App\Models\Prescribe_med_list;
use App\Models\Staff_department;
use App\Models\Global_settings;
use App\Http\Helpers;



class PrescriptionController extends Controller
{
    public function Prescription(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $url = 'https://mdb.microxen.com/api/mTests';

        $fields = array();
        $response = Helpers::get_post_data($url, $fields);
        if ($response['success']) {
            $data['m_tests'] = $response['data'];
        } else {
            $data['m_tests'] = array();
        }

        print_r($data['m_tests']);
        die();

        // $MsearchUrl = 'https://mdb.microxen.com/api/mSearch';
        // $m_fields = array(
        //     'q' => urlencode('napa'),
        // );
        // $m_response = Helpers::get_post_data($MsearchUrl, $m_fields);

        // dd($response);


        // echo "<pre>";
        // print_r($response);
        // print_r($m_response);
        // die();


        $datas = Patient_categorie::all();
        $data['category'] = $datas->toArray();

        $data['val']   = Schedule::all();
        $data['staff'] = Staffs::where('designation', '=', 3)->get();

        return view('backend.prescription.index', compact('data'));
    }


    public function StorePrescription(Request $request)
    {
        // $permissionCheck = Helpers::get_permission('prescription', 'is_add');
        // if ($permissionCheck == false) {
        //     return view('backend.access_denied');
        // }
        $usersInfo = Auth::user();
        $userId = $usersInfo['user_id'];
        $appointment = Appointment::where('id', '=', $request['id'])->get();
        $prscription = new Prescription();
        // $date     = Carbon::createFromFormat('d/m/Y', $request['appointment_date'])->format('Y-m-d');

        $prscription->appointment_id    = $request['id'];
        $prscription->pulse             = $request['pulse'];
        $prscription->patient_id        = $appointment[0]['patient_id'];
        $prscription->bp_high           = $request['bp_high'];
        $prscription->bp_low            = $request['bp_low'];
        $prscription->temperature       = $request['temperature'];
        $prscription->doctor_id         = $appointment[0]['doctor_id'];
        $prscription->created_by        = $userId;

        $prscription->save();

        $Appointment = Appointment::where('id', '=', $request['id'])->first();
        $Appointment->status              = 3;
        $Appointment->save();

        $request->session()->flash('alert-success', 'Information successfully Added!');
        return redirect('Appointment')->with('success', 'Information has been Added!!');
    }


    public function writePrescription(Request $request)
    {

        if ($request->type == 'getDiagnosis') {
            $url = 'https://mdb.microxen.com/api/mTests';

            $fields = array();
            $response = Helpers::get_post_data($url, $fields);
            if ($response['success']) {
                return  $data['m_tests'] = $response['data'];
            } else {
                return  $data['m_tests'] = array();
            }
        }
        $id = $request['id'];
        $data['editVal'] = Prescription::find($id);

        return view('backend.prescription.index', compact('data'));
    }

    public function PrescriptionUpdate(Request $request, $id)
    {
        $usersInfo = Auth::user();
        $userId = $usersInfo['user_id'];

        // $appointment = Appointment::where('id', '=', $request['id'])->get();
        $prscription = Prescription::where('id', '=', $request['id'])->first();

        $medicineName   = $request['brand'];
        $does           = $request['does'];
        $duration       = $request['duration'];
        $instruction    = $request['instruction'];


        if (!empty($request['next_visit_date'])) {
            $date     = Carbon::createFromFormat('d/m/Y', $request['next_visit_date'])->format('Y-m-d');
            $prscription->next_visit_date   = $date;
        } else {
            /// Do nothing
        }

        $prscription->appointment_id    = $prscription->appointment_id;
        $prscription->pulse             = $request['pulse'];
        $prscription->bp_high           = $request['bp_high'];
        $prscription->bp_low            = $request['bp_low'];
        $prscription->temperature       = $request['temperature'];
        $prscription->chief_complain    = nl2br($request['chief_complain_val']);
        $prscription->diagnosis         = nl2br($request['diagnosis_val']);
        $prscription->investigation     = nl2br($request['investigation_val']);
        $prscription->advice            = nl2br($request['advice_val']);

        $prscription->updated_by        = $userId;
        $prscription->save();
        $updatedId = $prscription->id;
        if (!empty($updatedId)) {
            $prescribeDelete = Prescribe_med_list::where('prescription_id', '=', $id)->delete();
            // $prescribeDelete->delete();
            $i = 0;
            foreach ($medicineName as $val) {


                $medicine = new Prescribe_med_list();
                $medicine->prescription_id   = $updatedId;
                $medicine->medicine_name     = $val;
                $medicine->does              = $does[$i];
                $medicine->duration          = $duration[$i];
                $medicine->instruction       = $instruction[$i];
                $medicine->created_by        = $userId;
                $medicine->save();
                $i++;
            }
        }

        return redirect()->route('writePrescriptionSave', ['id' => $updatedId]);
        // return redirect('writePrescription')->with('id', $updatedId);
    }
    public function writePrescriptionSave(Request $request)
    {

        if ($request->type == 'getDiagnosis') {
            $url = 'https://mdb.microxen.com/api/mTests';

            $fields = array();
            $response = Helpers::get_post_data($url, $fields);
            if ($response['success']) {
                return  $data['m_tests'] = $response['data'];
            } else {
                return  $data['m_tests'] = array();
            }
        }
        $id = $request['id'];
        $data['editVal'] = Prescription::find($id);
        $data['prescribeList'] = Prescribe_med_list::where('prescription_id', '=', $id)->get();

        // echo "<pre>";
        // print_r($data['prescribeList']);
        // die();

        return view('backend.prescription.index', compact('data'));
    }

    public function destroyPrescription(Request $request)
    {
        $permissionCheck = Helpers::get_permission('schedule', 'is_delete');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $id = $request['id'];
        $cat = Schedule::find($id);
        $cat->delete();
        return redirect('Schedule')->with('success', 'Information has been Deleted!!');
    }

    public function ajax_print_preview_prescription(Request $request)
    {
        $id  = $request['prescriptionId'];
        $data['prescriptionDetails'] = Prescription::find($id);
        $doctor_id = $data['prescriptionDetails']['doctor_id'];
        $data['doctorDetails'] = staffs::find($doctor_id);
        $data['staff_department'] = Staff_department::find($data['doctorDetails']['department']);
        $data['medicineList'] = Prescribe_med_list::where('prescription_id', '=', $id)->get();
        $data['chemberDetails'] = Global_settings::get();
        $data['patientDetails'] = Patient::find($data['prescriptionDetails']['patient_id']);

        // echo "<pre>";
        // print_r($data['chemberDetails'][0]);
        // die();

        return view('backend.prescription.ajax_print_preview_prescription', compact('data'));
    }

    public function get_medicine_name(Request $request)
    {

        // $search = $request->queryr;
        $search = $request->q;

        $MsearchUrl = 'https://mdb.microxen.com/api/mSearch';
        $m_fields = array(
            'q' => urlencode($search),
        );
        $m_response = Helpers::get_post_data($MsearchUrl, $m_fields);
        if ($m_response['success'] == true) {
            $data = $m_response['data'];
        } else {
            $data = '';
        }
        // echo "<pre>";
        // print_r($data);

        // die();

        return response()->json($data);

        // dd($response);


        // echo "<pre>";
        // print_r($response);
        // print_r($m_response);
        // die();



        // return view('backend.prescription.ajax_complain_add');
    }


    public function ajax_bpSet_view(Request $request)
    {

        $appointmentId = $request['appointmentId'];
        $data['appointment'] = Appointment::where('id', '=', $appointmentId)->get();
        // echo "<pre>";
        // print_r($data['appointment']);
        // die();

        return view('backend.prescription.ajax_bpSet_view', compact('data'));
    }

    public function todayQueue(Request $request)
    {

        $permissionCheck = Helpers::get_permission('appointment', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $mytime = Carbon::now();
        $t = $mytime->toDateString();

        $usersInfo = Auth::user();
        $userId = $usersInfo['user_id'];

        $data['val'] = Prescription::whereDate('created_at',  Carbon::today()->toDateString())->where('doctor_id', $userId)->get();
        // echo "<pre>";
        // print_r($data);
        // die();


        return view('backend.prescription.today_queue', compact('data'));
    }

    public function prescriptionPdf($id)
    {
        $data = ['title' => 'Welcome to ItSolutionStuff.com'];

        // $id  = $request['prescriptionId'];

        $data['prescriptionDetails'] = Prescription::find($id);
        $doctor_id = $data['prescriptionDetails']['doctor_id'];
        $data['doctorDetails'] = staffs::find($doctor_id);
        $data['staff_department'] = Staff_department::find($data['doctorDetails']['department']);
        $data['medicineList'] = Prescribe_med_list::where('prescription_id', '=', $id)->get();
        $data['chemberDetails'] = Global_settings::get();
        $data['patientDetails'] = Patient::find($data['prescriptionDetails']['patient_id']);
        // echo "<pre>";
        // print_r($data['doctorDetails']);
        // die();
        //PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Kalpurush']);
        $tt =  view('backend.pdfView.prescription', $data);
        $mpdf = new \Mpdf\Mpdf([
            'default_font_size' => 12,
            'default_font' => 'nikosh',
        ]);
        $mpdf->WriteHTML($tt);
        $mpdf->Output();
        // $pdf = PDF::loadView('backend.pdfView.prescription', $data);

        // return $pdf->stream('Patient Prescription.pdf');
    }

    public function pdfHtml()
    {
        $output = "দুই দিন পর আসবেন";
        return $output;
    }
}
