<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Carbon\Carbon;
use App\Models\Leave_application;
use App\Models\leave_categorie;
use App\Models\Staffs;
use App\Models\Role;
use App\Models\Custom_model;
use App\Models\Staff_attendance;
use App\Http\Helpers;

class AttendanceController extends Controller
{


    public function AttendanceSet(Request $request)
    {
        $permissionCheck = Helpers::get_permission('staff_attendance', 'is_view');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }

        $data['role'] = Role::all();

        // echo "<pre>";    print_r($data);die();
        return view('backend.hr.attendance.set.index', compact('data'));
    }


    public function show_attendance_set_val(Request $request)
    {
        $userid = Session::get('id');

        $staff_role        = $request['staff_role'];
        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['fromDate'])->format('Y-m-d');
        $data['fromDate'] = $request['fromDate'];

        $data['all_data'] = Custom_model::get_attendance_set_data($fromDate, $staff_role);
        // echo "<pre>";
        // print_r($data['all_data']);
        // die();
        return view('backend.hr.attendance.set.ajax_view_load', compact('data'));
    }


    public function AddAttandance(Request $request)
    {

        $permissionCheck = Helpers::get_permission('staff_attendance', 'is_add');
        if ($permissionCheck == false) {
            return view('backend.access_denied');
        }


        $userid = Session::get('id');

        $fromDate    = Carbon::createFromFormat('d/m/Y', $request['date'])->format('Y-m-d');
        $attendance            = $request['attendance'];
        // echo "<pre>";
        // print_r($attendance);
        // die();

        foreach ($attendance as $key => $value) {
            if (empty($value['old_atten_id'])) {
                $staff_attendance = new Staff_attendance();
                $staff_attendance->staff_id          = $value['staff_id'];
                $staff_attendance->status            = $value['status'];
                $staff_attendance->remark            = $value['remark'];
                $staff_attendance->date              = $fromDate;
                $staff_attendance->save();
            } else {
                $staff_attendance = Staff_attendance::where('id', '=', $value['old_atten_id'])->first();
                $staff_attendance->staff_id          = $value['staff_id'];
                $staff_attendance->status            = $value['status'];
                $staff_attendance->remark            = $value['remark'];
                $staff_attendance->date              = $fromDate;
                $staff_attendance->save();
            }
        }


        $request->session()->flash('alert-success', 'Attendance Added Successfully !');
        return redirect('AttendanceSet')->with('success', 'Attendance Added Successfully !');
    }
}
