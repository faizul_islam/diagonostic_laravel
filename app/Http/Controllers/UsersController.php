<?php

/****** 
 * @Author: Ahsan Ullah 
 * Purpose: User related functionalities all type
 * *****/

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;
//use App\Login_history;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Session;

use DB;
use Carbon\Carbon;
use Mail;


use Illuminate\Support\Str;

//use App\Routes_name;
//use App\Permission;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $value = $request->session()->get('employee_id');
        //print_r($value);die();
        if (isset($value[0])) {
            return redirect('home');
        } else {
            return redirect('user-login');
        }
    }

    // public function userList(Request $request)
    // {
    //     /*************** check user permission and access ******************/
    //     $userid = Session::get('id');
    //     $role = Session::get('user_role');
    //     $routeUrl = explode('@', Route::getCurrentRoute()->getActionName());
    //     $routeName = Routes_name::where('name', '=', $routeUrl[1])->get();
    //     $permissionArray = array();
    //     if (!empty($routeName->toArray())) {
    //         foreach ($routeName[0]->permissions as $routePermission) {
    //             array_push($permissionArray, $routePermission['id']);
    //         }
    //         $user = Users::find($userid[0]);
    //         $userPermissions = array();
    //         foreach ($user->permissions as $permission) {
    //             array_push($userPermissions, $permission['id']);
    //         }
    //         $hasAccess = array();
    //         foreach ($permissionArray as $p) {
    //             if (in_array($p, $userPermissions)) {
    //                 array_push($hasAccess, $p);
    //             }
    //         }
    //         /******* end access checks ***********/
    //         if (!empty($hasAccess) || $role[0] == 'admin') {
    //             $data['users'] = Users::where('is_active', '=', 1)->get();
    //             $data['breadcrumb'][0] = array('breadUrl' => "userProfile", 'name' => "Profile");
    //             //echo "<pre>";print_r($data['breadcrumb']);die();
    //             return view('users/index', $data);
    //         } else {
    //             return view('access_denied');
    //         }
    //     } else {
    //         return view('access_denied');
    //     }
    // }
    /************** User profile section ****************************/
    // public function userProfile(Request $request)
    // {
    //     $value = $request->session()->get('employee_id');
    //     $curl = curl_init();

    //     curl_setopt($curl, CURLOPT_URL, config('constants.api_url') . "employees/employee_details/" . $value[0] . "?X-API-KEY=" . config('constants.api_key'));
    //     curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
    //     curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    //     curl_setopt($curl, CURLOPT_TIMEOUT, 45);
    //     curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    //     curl_setopt($curl, CURLOPT_USERPWD, config('constants.api_user') . ":" . config('constants.api_password'));
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //     $response = curl_exec($curl);

    //     $err = curl_error($curl);

    //     curl_close($curl);
    //     $responses = json_decode($response, true);
    //     $data['response'] = $responses['message'];
    //     $data['breadcrumb'][0] = array('breadUrl' => "userProfile", 'name' => "Profile");
    //     //echo "<pre>";print_r($data['breadcrumb']);die();
    //     return view('user_profile', $data);
    // }
    // public function doesUserNameExist(Request $r)
    // {
    //     $username = $r['username'];
    //     //      dd($username);
    //     $userinfo = Users::where('user_name', '=', $username)->get();
    //     //      dd($userinfo);
    //     if (count($userinfo) < 1) {
    //         echo 'true';
    //     } else {
    //         echo 'false';
    //     }
    // }
    /************** Application login section ****************************/
    public function login()
    {
        return view('user-login');
    }

    public function loginInfo(Request $r)
    {
        /******* Render physical properties of PC **********/
        ob_start();                                 // Turn on output buffering
        system('ipconfig /all');           // Execute external program to display output
        $mycom = ob_get_contents();                   // Capture the output into a variable
        ob_clean();                                 // Clean (erase) the output buffer
        $findme = "Physical";
        $pmac = strpos($mycom, $findme); // Find the position of Physical text
        $physicaAddress = substr($mycom, ($pmac + 36), 17); // Get Physical Address
        /******* End Rendering physical properties of PC **********/
        $username = $r['username'];
        $password = $r['password'];

        $userinfo = Users::where('username', '=', $username)->first();
        if (!empty($userinfo)) {

            if (Hash::check($password, $userinfo->password)) {

                if ($userinfo->role == 1) {
                    Session::push('id', $userinfo->id);

                    Session::push('user_id', $userinfo->user_id);
                    Session::push('user_name', $userinfo->username);
                    Session::push('role', $userinfo->role);

                    //------- update user login ---------//
                    $user = Users::where('id', '=', $userinfo->id)->first();
                    $user->last_login = date('Y-m-d H:i:s');
                    $user->save();

                    return redirect('home');
                } else {


                    // Session::push('id', $userinfo->id);
                    // Session::push('pin', $userinfo->pin);
                    // Session::push('employee_id', $userinfo->employee_id);
                    // Session::push('user_name', $userinfo->user_name);
                    // Session::push('user_role', $userinfo->role);
                    // Session::push('full_name', $employeesDetails[0]['full_name']);
                    // Session::push('email', $employeesDetails[0]['email']);
                    // Session::push('designation_name', $employeesDetails[0]['designation_name']);
                    // Session::push('photo', $employeesDetails[0]['photo']);
                    // //------ insert login history ---------//


                    // $history = new Login_history;
                    // $history->users_id = $userinfo->id;
                    // $history->login_time = date('Y-m-d H:i:s');
                    // $history->ip_or_mac_address = $physicaAddress;
                    // $history->status = 0;
                    // $history->save();
                    // //------- update user login ---------//
                    // $user = Users::where('id', '=', $userinfo->id)->first();
                    // $user->login_status = 1;
                    // $user->last_login_time = date('Y-m-d H:i:s');
                    // $user->save();
                    // return redirect('home');


                }
            } else {
                $r->session()->flash('flash_notification.danger', 'Sorry! Your Password do not match!');
                return redirect('user-login');
            }
        } else {
            $r->session()->flash('flash_notification.danger', 'Sorry! We could not found you!');
            return redirect('user-login');
        }
    }

    public function logout(Request $r)
    {

        Auth::logout();
        return redirect('login');

        // $userid = $r->session()->get('id');
        // if (!empty($userid)) {
        //     // if ($userid != "") {
        //     //     $user = Users::where('id', '=', $userid)->first();
        //     //     $user->login_status = 0;
        //     //     $user->save();
        //     // }
        //     Session::flush();
        //     return redirect('user-login');
        // } else {
        //     Session::flush();
        //     return redirect('user-login');
        // }
    }

    /************** Application user registration section ****************************/
    // public function userRegistration()
    // {
    //     /*************** check user permission and access ******************/
    //     $userid = Session::get('id');
    //     $role = Session::get('user_role');
    //     $routeUrl = explode('@', Route::getCurrentRoute()->getActionName());
    //     $routeName = Routes_name::where('name', '=', $routeUrl[1])->get();
    //     $permissionArray = array();
    //     //print_r($routeName->toArray());die();
    //     if (!empty($routeName->toArray())) {
    //         foreach ($routeName[0]->permissions as $routePermission) {
    //             array_push($permissionArray, $routePermission['id']);
    //         }
    //         //echo "<pre>";print_r($userid);die();
    //         $user = Users::find($userid[0]);
    //         $userPermissions = array();
    //         foreach ($user->permissions as $permission) {
    //             array_push($userPermissions, $permission['id']);
    //         }
    //         $hasAccess = array();
    //         foreach ($permissionArray as $p) {
    //             if (in_array($p, $userPermissions)) {
    //                 array_push($hasAccess, $p);
    //             }
    //         }
    //         /******* end access checks ***********/
    //         if (!empty($hasAccess) || $role[0] == 'admin') {
    //             /******** Permitted users func section **********/
    //             //$employeedata = "http://localhost/api_ums/employees/index?X-API-KEY=123456";
    //             $curl = curl_init();

    //             curl_setopt($curl, CURLOPT_URL, config('constants.api_url') . "employees/all_admin_employees?X-API-KEY=" . config('constants.api_key'));
    //             curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
    //             curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    //             curl_setopt($curl, CURLOPT_TIMEOUT, 45);
    //             curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    //             curl_setopt($curl, CURLOPT_USERPWD, config('constants.api_user') . ":" . config('constants.api_password'));
    //             curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    //             $response = curl_exec($curl);
    //             $err = curl_error($curl);

    //             curl_close($curl);
    //             $responses = json_decode($response, true);
    //             $data['response'] = $responses['message'];

    //             $roles = Role::where('is_active', '=', 1)->get();
    //             $data['roles'] = $roles;
    //             $data['script_files'] = array('app/access-control.js');
    //             return view('user_registration', $data);
    //         } else {
    //             return view('access_denied');
    //         }
    //     } else {
    //         return view('access_denied');
    //     }
    // }

    // public function saveUserInfo(Request $r)
    // {
    //     //return view('userinfo');
    //     $hashedPassword = Hash::make(Input::get('password'));
    //     $hashedPin = Crypt::encrypt(Input::get('user_pin'));
    //     $user = new Users;
    //     $user->user_name = Input::get('user_name');
    //     $user->password = $hashedPassword;
    //     $user->pin = $hashedPin;
    //     $user->mac_address = Input::get('mac_address');
    //     $user->role = Input::get('user_role');
    //     $user->new_password_requird = 1;
    //     $user->login_status = 0;
    //     $user->employee_id = Input::get('employee_id');
    //     $user->last_login_time = date('Y-m-d H:i:s');
    //     $user->created_at = date('Y-m-d H:i:s');
    //     $user->save();
    //     $userId = $user->id;
    //     $r->session()->flash('alert-success', 'Information successfully added!');
    //     return redirect()->back();
    // }
    // public function editInfo($id)
    // {
    //     $userid = Session::get('id');
    //     $role = Session::get('user_role');
    //     $routeUrl = explode('@', Route::getCurrentRoute()->getActionName());
    //     $routeName = Routes_name::where('name', '=', $routeUrl[1])->get();
    //     $permissionArray = array();
    //     if (!empty($routeName->toArray())) {
    //         foreach ($routeName[0]->permissions as $routePermission) {
    //             array_push($permissionArray, $routePermission['id']);
    //         }
    //         $user = Users::find($userid[0]);
    //         $userPermissions = array();
    //         foreach ($user->permissions as $permission) {
    //             array_push($userPermissions, $permission['id']);
    //         }
    //         $hasAccess = array();
    //         foreach ($permissionArray as $p) {
    //             if (in_array($p, $userPermissions)) {
    //                 array_push($hasAccess, $p);
    //             }
    //         }
    //         if (!empty($hasAccess) || $role[0] == 'admin') {
    //             $usr = Users::where('id', $id)->first();
    //             //  echo "<pre>"; print_r($usr); die();
    //             return view('users.edit', compact('usr', 'id'));
    //         } else {
    //             return view('access_denied');
    //         }
    //     } else {
    //         return view('access_denied');
    //     }
    // }
    // public function updateInfo(Request $request, $id)
    // {
    //     $encryptedPIN =  Crypt::encrypt($request->input('new_pin'));
    //     $user = Users::where('id', '=', $id)->first();
    //     $user->pin = $encryptedPIN;
    //     $user->updated_at = date('Y-m-d H:i:s');
    //     $user->save();
    //     $request->session()->flash('alert-success', 'Information successfully updated!');
    //     return redirect()->back();
    // }
    // public function changePin($id)
    // {
    //     $userid = Session::get('id');
    //     $role = Session::get('user_role');
    //     $routeUrl = explode('@', Route::getCurrentRoute()->getActionName());
    //     //DB::enableQueryLog();
    //     $routeName = Routes_name::where('name', '=', $routeUrl[1])->get();
    //     $permissionArray = array();
    //     if (!empty($routeName->toArray())) {
    //         foreach ($routeName[0]->permissions as $routePermission) {
    //             array_push($permissionArray, $routePermission['id']);
    //         }
    //         $user = Users::find($userid[0]);
    //         $userPermissions = array();
    //         foreach ($user->permissions as $permission) {
    //             array_push($userPermissions, $permission['id']);
    //         }
    //         $hasAccess = array();
    //         foreach ($permissionArray as $p) {
    //             if (in_array($p, $userPermissions)) {
    //                 array_push($hasAccess, $p);
    //             }
    //         }
    //         if (!empty($hasAccess) || $role[0] == 'admin') {
    //             $usr = Users::where('id', $id)->first();

    //             return view('users.change_pin', compact('usr', 'id'));
    //         } else {
    //             return view('access_denied');
    //         }
    //     } else {
    //         return view('access_denied');
    //     }
    // }
    // public function updatePin(Request $request, $id)
    // {

    //     $encryptedPIN =  Crypt::encrypt($request->input('new_pin'));
    //     $user = Users::where('id', '=', $id)->first();
    //     $user->pin = $encryptedPIN;
    //     $user->updated_at = date('Y-m-d H:i:s');
    //     $user->save();
    //     $request->session()->flash('alert-success', 'Information successfully updated!');
    //     return redirect()->back();
    // }
    // public function changePassword($id)
    // {
    //     $userid = Session::get('id');
    //     $role = Session::get('user_role');
    //     $routeUrl = explode('@', Route::getCurrentRoute()->getActionName());
    //     //DB::enableQueryLog();
    //     $routeName = Routes_name::where('name', '=', $routeUrl[1])->get();
    //     $permissionArray = array();
    //     if (!empty($routeName->toArray())) {
    //         foreach ($routeName[0]->permissions as $routePermission) {
    //             array_push($permissionArray, $routePermission['id']);
    //         }
    //         $user = Users::find($userid[0]);
    //         $userPermissions = array();
    //         foreach ($user->permissions as $permission) {
    //             array_push($userPermissions, $permission['id']);
    //         }
    //         $hasAccess = array();
    //         foreach ($permissionArray as $p) {
    //             if (in_array($p, $userPermissions)) {
    //                 array_push($hasAccess, $p);
    //             }
    //         }
    //         if (!empty($hasAccess) || $role[0] == 'admin') {
    //             $usr = Users::where('id', $id)->first();

    //             return view('users.change_password', compact('usr', 'id'));
    //         } else {
    //             return view('access_denied');
    //         }
    //     } else {
    //         return view('access_denied');
    //     }
    // }
    // public function updatePassword(Request $request, $id)
    // {
    //     $hashedPassword = Hash::make($request->input('new_password'));
    //     $user = Users::where('id', '=', $id)->first();
    //     $user->password = $hashedPassword;
    //     $user->updated_at = date('Y-m-d H:i:s');
    //     $user->save();
    //     $request->session()->flash('alert-success', 'Information successfully updated!');
    //     return redirect()->back();
    // }

    public function showForgetPasswordForm()

    {

        return view('auth.passwords.email');
    }

    public function sendPassword(Request $request)

    {

        $token = Str::random(64);

        DB::table('password_resets')->insert([

            'email' => $request->email,

            'token' => $token,

            'created_at' => Carbon::now()

        ]);



        Mail::send('email.forgetPassword', ['token' => $token], function ($message) use ($request) {

            $message->to($request->email);

            $message->subject('Reset Password');
        });



        return back()->with('message', 'We have e-mailed your password reset link!');
    }
}
